//
//  BigClause.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 06.10.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#include <cassert>
#include <limits>

#include "BigClause.h"
#include "bfexception.h"
#include "LiteralInfo.h"

bf::BigClause::~BigClause()
{
   if (literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
   if (watch_list_ != NULL)
   {
      delete [] watch_list_;
      watch_list_ = NULL;
   }
}

void bf::BigClause::InitWithNFClause(int a_cid, Clause * a_nfclause)
{
#ifndef NDEBUG
   if(a_nfclause == NULL)
   {
      fputs("[bf::BigClause::InitWithNFClause] a_nfclause is NULL.\n", stderr);
      throw bf::UnexpectedNULLException();
   }
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
   if(watch_list_ != NULL)
   {
      delete [] watch_list_;
      watch_list_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = capacity_ = watch_list_size_ = a_nfclause->NumberOfLiterals();
   literals_ = new int[a_nfclause->NumberOfLiterals()];
   watch_list_ = new int[a_nfclause->NumberOfLiterals()];
   int index = 0;
   for(bf::Clause::LiteralSet::const_iterator lit
         = a_nfclause->literals().begin();
         lit != a_nfclause->literals().end();
         ++ lit)
   {
      literals_[index] = watch_list_[index] = *lit;
      ++index;
   }

}

void bf::BigClause::InitWithLiterals(int a_cid, int * a_literals,
      size_t a_size)
{
#ifndef NDEBUG
   if(a_literals == NULL)
   {
      fputs("[bf::BigClause::InitWithLiterals] a_literals is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
   if(watch_list_ != NULL)
   {
      delete [] watch_list_;
      watch_list_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = capacity_ = watch_list_size_ = a_size;
   literals_ = new int[a_size];
   watch_list_ = new int[a_size];
   while(a_size > 0)
   {
      -- a_size;
      watch_list_[a_size] = literals_[a_size] = a_literals[a_size];
   }
}

void bf::BigClause::InitWithClause(int a_cid, ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if(a_clause == NULL)
   {
      fputs("[bf::BigClause::InitWithClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
   if(watch_list_ != NULL)
   {
      delete [] watch_list_;
      watch_list_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = capacity_ = watch_list_size_ = a_clause->size();
   literals_ = new int[a_clause->size()];
   watch_list_ = new int[a_clause->size()];
   for(size_t index = 0; index < a_clause->size(); ++index)
   {
      literals_[index] = watch_list_[index] = a_clause->literal(index);
   }
}

void bf::BigClause::InitWithCapacity(int a_cid, size_t a_capacity)
{
#ifndef NDEBUG
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
   if(watch_list_ != NULL)
   {
      delete [] watch_list_;
      watch_list_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = 0;
   watch_list_size_ = 0;
   capacity_ = a_capacity;
   literals_ = new int[capacity_];
   watch_list_ = new int[capacity_];
}

void bf::BigClause::Clear()
{
   size_ = 0;
   watch_list_size_ = 0;
   while(!assigned_heap_.empty())
   {
      assigned_heap_.pop();
   }
}

size_t bf::BigClause::size()
{
   return size_;
}

size_t bf::BigClause::capacity()
{
   return capacity_;
}

int bf::BigClause::literal(size_t lit_index)
{
   return(lit_index < size_ ? literals_[lit_index] : kNoLiteral);
}

size_t bf::BigClause::IndexOfLiteral(int lit, size_t from_index)
{
   if(size_ == 0)
   {
      return std::numeric_limits<size_t>::max();
   }
   size_t left = from_index;
   if(from_index >= size_)
   {
      return std::numeric_limits<size_t>::max();
   }
   size_t right = size_ - 1;
   if(lit == literals_[left])
   {
      return left;
   }
   if(lit == literals_[right])
   {
      return right;
   }
   while(right >= left)
   {
      size_t middle =(right+left)/2;
      if(lit == literals_[middle])
      {
         return middle;
      }
      else if(lit < literals_[middle])
      {
         right = middle - 1;
      }
      else
      {
         left = middle + 1;
      }
   }
   return std::numeric_limits<size_t>::max();
}

void bf::BigClause::AddLiteral(int lit)
{
   if(size_ < capacity_)
   {
      literals_[size_] = lit;
      ++size_;
      watch_list_[watch_list_size_] = lit;
      ++watch_list_size_;
   }
   else
   {
      fputs("[BigClause::AddLiteral] Clause is already full.\n", stderr);
      throw IndexOutOfBoundsException();
   }
}

void bf::BigClause::SetLiteralsFromClause(ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if(a_clause == NULL)
   {
      fputs("[bf::BigClause::SetLiteralsFromClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if(a_clause->size() > capacity_)
   {
      fputs("[bf::BigClause::SetLiteralsFromClause] Literals from a_clause do not fit in.\n", stderr);
      throw IndexOutOfBoundsException();
   }
#endif
   size_ = a_clause->size();
   watch_list_size_ = a_clause->size();
   while(!assigned_heap_.empty())
   {
      assigned_heap_.pop();
   }
   for(size_t index = 0; index < a_clause->size(); ++index)
   {
      literals_[index] = watch_list_[index] = a_clause->literal(index);
   }
}

int bf::BigClause::WatchedLiteralA()
{
   return (size_ > 0 ? watch_list_[0] : kNoLiteral);
}

int bf::BigClause::WatchedLiteralB()
{
   return (size_ > 1 ? watch_list_[1] : kNoLiteral);
}

bool bf::BigClause::CheckState(LiteralInfo * literal_info,
      ClauseState * state)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::BigClause::CheckState] literal_info == NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if (state == NULL)
   {
      fputs ("[bf::BigClause::CheckState] state == NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
#endif
   if (literal_info[watch_list_[0]].value_ == kTrueValue
         || literal_info[watch_list_[1]].value_ == kTrueValue)
   {
      *state = kSatisfied;
      return false;
   }
   bool watch_changed = false;
   if (literal_info[watch_list_[0]].value_ == kFalseValue)
   {
      // Try to find a replacement.
      while (watch_list_size_ > 2
            && literal_info[watch_list_[watch_list_size_-1]].value_
            == kFalseValue)
      {
         assigned_heap_.push(std::make_pair(
                  literal_info[watch_list_[watch_list_size_-1]].level_,
                  watch_list_[watch_list_size_-1]));
         -- watch_list_size_;
      }
      if (watch_list_size_ > 2)
      {
         int swap = watch_list_[0];
         watch_list_[0] = watch_list_[watch_list_size_-1];
         watch_list_[watch_list_size_-1] = swap;
         watch_changed = true;
         if (literal_info[watch_list_[0]].value_ == kTrueValue)
         {
            *state = kSatisfied;
            return true;
         }
      }
   }
   if (literal_info[watch_list_[1]].value_ == kFalseValue)
   {
      // Try to find a replacement.
      while (watch_list_size_ > 2
            && literal_info[watch_list_[watch_list_size_-1]].value_
            == kFalseValue)
      {
         assigned_heap_.push(std::make_pair(
                  literal_info[watch_list_[watch_list_size_-1]].level_,
                  watch_list_[watch_list_size_-1]));
         -- watch_list_size_;
      }
      if (watch_list_size_ > 2)
      {
         int swap = watch_list_[1];
         watch_list_[1] = watch_list_[watch_list_size_-1];
         watch_list_[watch_list_size_-1] = swap;
         watch_changed = true;
         if (literal_info[watch_list_[1]].value_ == kTrueValue)
         {
            *state = kSatisfied;
            return true;
         }
      }
   }
   // Remove tail to find a third unassigned literal.
   while (watch_list_size_ > 2
         && literal_info[watch_list_[watch_list_size_-1]].value_
         == kFalseValue)
   {
      assigned_heap_.push(std::make_pair(
               literal_info[watch_list_[watch_list_size_-1]].level_,
               watch_list_[watch_list_size_-1]));
      -- watch_list_size_;
   }
   if (watch_list_size_ > 2)
   {
      *state = kUnresolved;
   }
   else
   {
      int unassigned = 0;
      if (literal_info[watch_list_[0]].value_ == kUnknownValue)
      {
         ++ unassigned;
      }
      if (literal_info[watch_list_[1]].value_ == kUnknownValue)
      {
         if (!unassigned)
         {
            int swap = watch_list_[0];
            watch_list_[0] = watch_list_[1];
            watch_list_[1] = swap;
         }
         ++ unassigned;
      }
      if (unassigned == 2)
      {
         *state = kBinary;
      }
      else if (unassigned == 1)
      {
         *state = kUnit;
      }
      else
      {
         /*fputs ("Checked big clause:\n", stderr);
         Print(stderr);
         fputc ('\n', stderr);*/

         *state = kUnsatisfied;
      }
   }

   return watch_changed;
}

bf::ClauseInterface::ClauseState bf::BigClause::DryCheckState(LiteralInfo * literal_info)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::BigClause::DryCheckState] literal_info == NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
#endif
   int unassigned = 0;
   for (size_t lit_index = 0;
         lit_index < size_ && unassigned < 3;
         ++ lit_index)
   {
      switch (literal_info[literals_[lit_index]].value_)
      {
         case kTrueValue:
            return kSatisfied;
         case kUnknownValue:
            ++ unassigned;
            break;
         default:
            break;
      }
   }
   switch (unassigned)
   {
      case 0:
         return kUnsatisfied;
      case 1:
         return kUnit;
      case 2:
         return kBinary;
      default:
         return kUnresolved;
   }
   return kUnresolved;
}

void bf::BigClause::SetWatchIndices(size_t watch_a, size_t watch_b)
{
#ifndef NDEBUG
   if (watch_a > size_)
   {
      fputs("[bf::BigClause::SetWatchIndices] watch_a is out of bounds.\n", stderr);
      throw IndexOutOfBoundsException();
   }
   if (watch_b > size_)
   {
      fputs("[bf::BigClause::SetWatchIndices] watch_b is out of bounds.\n", stderr);
      throw IndexOutOfBoundsException();
   }
   if (watch_a == watch_b)
     {
       fputs ("[bf::BigClause::SetWatchIndices] watch_a == watch_b.\n", stderr);
       throw BadParameterException();
     }
#endif
/*   for (size_t lit_index=0; lit_index<size_; ++lit_index)
   {
      watch_list_[lit_index] = literals_[lit_index];
   }*/
   assert(assigned_heap_.empty());
   if (watch_a == 0)
   {
      if (watch_b > 1)
      {
         watch_list_[1] = literals_[watch_b];
         watch_list_[watch_b] = literals_[1];
      }
   }
   else if (watch_a == 1)
   {
      if (watch_b == 0)
      {
         watch_list_[0] = literals_[1];
         watch_list_[1] = literals_[0];
      }
      else // watch_b > 1
      {
         watch_list_[0] = literals_[1];
         watch_list_[1] = literals_[watch_b];
         watch_list_[watch_b] = literals_[0];
      }
   }
   else // watch_a >= 2
   {
      if (watch_b == 0)
      {
         watch_list_[1] = literals_[0];
         watch_list_[0] = literals_[watch_a];
         watch_list_[watch_a] = literals_[1];
      }
      else if (watch_b == 1)
      {
         //watch_list_[1] = literals_[1];
         watch_list_[0] = literals_[watch_a];
         watch_list_[watch_a] = literals_[0];
      }
      else // watch_b > 1
      {
         watch_list_[0]=literals_[watch_a];
         watch_list_[watch_a]=literals_[0];
         watch_list_[1]=literals_[watch_b];
         watch_list_[watch_b]=literals_[1];
      }
   }
/*   if (!CheckWatchInvariant())
   {
      fprintf (stderr,
            "[bf::BigClause::SetWatchIndices(%lu, %lu)] Watch invariant failed at the end.\n",
            watch_a, watch_b);
      Print(stderr);
      fputc('\n', stderr);
      Print(stderr);
      fputs("[", stderr);
      for (size_t literal_ind = 0; literal_ind < size(); ++literal_ind)
      {
         int lit = watch_list_[literal_ind];
         if (lit % 2)
         {
            fprintf (stderr, "-%d ", lit / 2);
         }
         else
         {
            fprintf (stderr, "%d ", lit / 2);
         }
      }
      fputs("]\n", stderr);
      throw InvariantFailedException();
   }*/
}

bool bf::BigClause::CheckWatchInvariant()
{
   for (size_t lit_index = 2; lit_index < size_; ++lit_index)
   {
      if (watch_list_[0] == watch_list_[lit_index]
            || watch_list_[1] == watch_list_[lit_index])
      {
         return false;
      }
   }
   return true;
}

bool bf::BigClause::NeedsBacktrackNotification()
{
   return true;
}

void bf::BigClause::Backtrack(int level)
{
   // All assigned literals at levels up to given one are returned at
   // the end of the watch_list_.
   while (!assigned_heap_.empty()
         && assigned_heap_.top().first>level)
   {
      watch_list_[watch_list_size_] = assigned_heap_.top().second;
      ++ watch_list_size_;
      assigned_heap_.pop();
   }
}
