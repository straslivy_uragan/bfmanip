//
//  BigClause.h
//  bfmanip++
//
//  Created by Petr Kučera on 06.10.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef BIGCLAUSE_H_
#define BIGCLAUSE_H_

#include "ClauseInterface.h"
//#include "IntHeap.h"

#include <queue>

namespace bf {
   /**@brief Class representing a big clause. */
   class BigClause: public bf::ClauseInterface {
      friend class ClauseInterface;
   private:
      size_t capacity_;
      size_t size_;
      int * literals_;
      int * watch_list_;
      size_t watch_list_size_;
      std::priority_queue< std::pair<int, int> > assigned_heap_;

   protected:
      inline BigClause ()
      : capacity_(0), size_(0), literals_(NULL), watch_list_(NULL)
      {
      }
      virtual void InitWithNFClause (int a_cid, Clause * a_nfclause);
      virtual void InitWithLiterals (int a_cid, int * a_literals,
                                     size_t a_size);

      virtual void InitWithClause (int a_cid, ClauseInterface * a_clause);
      virtual void InitWithCapacity (int a_cid, size_t a_capacity);

   public:
      virtual ~BigClause ();
      virtual void Clear ();
      virtual size_t size ();
      virtual size_t capacity ();
      virtual int literal (size_t lit_index);
      virtual size_t IndexOfLiteral (int lit, size_t from_index);
      virtual void AddLiteral (int lit);
      virtual void SetLiteralsFromClause (ClauseInterface * a_clause);
      virtual int WatchedLiteralA ();
      virtual int WatchedLiteralB ();
      virtual bool CheckState(LiteralInfo * literal_info,
               ClauseState * state);
      virtual ClauseState DryCheckState(LiteralInfo * literal_info);
      virtual void SetWatchIndices(size_t watch_a, size_t watch_b);

      virtual bool CheckWatchInvariant();

      virtual bool NeedsBacktrackNotification();
      virtual void Backtrack(int bt_level);
   private:
      BigClause (BigClause&);
      void operator= (const BigClause&);
   };

}

#endif
