/*
 * bf::BinaryClause.cpp
 *
 *  Created on: 27.9.2011
 *      Author: su
 */

#include <cstddef>
#include <cstdio>
#include <limits>

#include "BinaryClause.h"
#include "bfexception.h"
#include "LiteralInfo.h"

bf::BinaryClause::~BinaryClause () {} 

void bf::BinaryClause::InitWithNFClause (int a_cid, bf::Clause * a_nfclause)
{
#ifndef NDEBUG
   if (a_nfclause == NULL)
   {
      fputs ("[bf::BinaryClause::InitWithNFClause] a_nfclause is NULL.", stderr);
      throw bf::UnexpectedNULLException ();
   }
   if (a_nfclause->NumberOfLiterals () > 2)
   {
      fputs ("[bf::BinaryClause::InitWithNFClause] Given clause contains more than two literals.", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif
   setCid(a_cid);
   literals_[0] = literals_[1] = kNoLiteral;
   Clause::LiteralSet::const_iterator lit = a_nfclause->literals ().begin ();
   if (lit != a_nfclause->literals ().end ())
   {
      literals_[0] = *lit;
      ++ lit;
   }
   if (lit != a_nfclause->literals ().end ())
   {
      literals_[1] = *lit;
      ++ lit;
   }
   watch_list_[0] = literals_[0];
   watch_list_[1] = literals_[1];
}

void bf::BinaryClause::InitWithLiterals (int a_cid, int * a_literals,
      size_t a_size)
{
#ifndef NDEBUG
   if (a_literals == NULL)
   {
      fputs ("[bf::BinaryClause::InitWithLiterals] a_literals is NULL.", stderr);
      throw UnexpectedNULLException ();
   }
   if (a_size > 2)
   {
      fputs ("[bf::BinaryClause::InitWithLiterals] Set of literals contains more than two literals.", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif
   setCid(a_cid);;
   literals_[0] = literals_[1] = kNoLiteral;
   while (a_size > 0)
   {
      -- a_size;
      literals_[a_size] = a_literals[a_size];
   }
   watch_list_[0] = literals_[0];
   watch_list_[1] = literals_[1];
}

void bf::BinaryClause::InitWithTwoLiterals (int a_cid,
      int a_literal, int b_literal)
{
   setCid(a_cid);;
   literals_[0] = a_literal;
   literals_[1] = b_literal;
   watch_list_[0] = literals_[0];
   watch_list_[1] = literals_[1];
}

void bf::BinaryClause::InitWithClause (int a_cid, ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs ("[bf::BinaryClause::InitWithClause] a_clause is NULL.", stderr);
      throw UnexpectedNULLException ();
   }
   if (a_clause->size () > 2)
   {
      fputs ("[bf::BinaryClause::InitWithClause] Given clause contains more than two literals.", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif
   setCid(a_cid);;
   literals_[0] = literals_[1] = kNoLiteral;
   if (a_clause->size () > 0)
   {
      literals_[0] = a_clause->literal (0);
      if (a_clause->size () == 2)
      {
         literals_[1] = a_clause->literal (1);
      }
   }
   watch_list_[0] = literals_[0];
   watch_list_[1] = literals_[1];
}

void bf::BinaryClause::InitWithCapacity(int a_cid, size_t a_capacity)
{
#ifndef NDEBUG
   if (a_capacity != 2)
   {
      fputs("[bf::BinaryClause::InitWithCapacity] a_capacity != 2.\n", stderr);
      throw BadParameterException();
   }
#endif
   setCid(a_cid);;
   literals_[0] = literals_[1] = kNoLiteral;
   watch_list_[0] = watch_list_[1] = kNoLiteral;
}

void bf::BinaryClause::Clear ()
{
   setCid(0);
   watch_list_[0] = watch_list_[1] = kNoLiteral;
   literals_[0] = literals_[1] = kNoLiteral;
}

size_t bf::BinaryClause::size ()
{
   return 2;
}

size_t bf::BinaryClause::capacity ()
{
   return 2;
}

int bf::BinaryClause::literal (size_t lit_index)
{
#ifndef NDEBUG
   if (lit_index > 1)
   {
      fputs ("[bf::BinaryClause::literal] lit_index > 1", stderr);
      throw UnexpectedNULLException ();
   }
#endif
   return literals_[lit_index];
}

size_t bf::BinaryClause::IndexOfLiteral (int lit, size_t from_index)
{
   size_t index = std::numeric_limits<size_t>::max();
   while (from_index < 2)
   {
      if (literals_[from_index] == lit)
      {
         index = from_index;
         break;
      }
      ++ from_index;
   }
   return index;
}


void bf::BinaryClause::AddLiteral (int lit)
{
   size_t index = 0;
   while (literals_[index] != kNoLiteral)
   {
      ++ index;
   }
   if (index < 2)
   {
      literals_[index] = lit;
   }
   else
   {
      fputs ("[bf::BinaryClause::AddLiteral] Clause is already full.", stderr);
      throw IndexOutOfBoundsException ();
   }
   watch_list_[0] = literals_[0];
   watch_list_[1] = literals_[1];
}

void bf::BinaryClause::SetLiteralsFromClause (ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs ("[bf::BinaryClause::SetLiteralsFromClause] a_clause is NULL", stderr);
      throw UnexpectedNULLException ();
   }
   if (a_clause->size () > 2)
   {
      fputs ("bf::BinaryClause::SetLiteralsFromClause] Given clause contains too many literals.", stderr);
      throw IndexOutOfBoundsException ();
   }
#endif
   literals_[0] = a_clause->literal (0);
   literals_[1] = a_clause->literal (1);
   watch_list_[0] = literals_[0];
   watch_list_[1] = literals_[1];
}

int bf::BinaryClause::WatchedLiteralA ()
{
   return watch_list_[0];
}

int bf::BinaryClause::WatchedLiteralB ()
{
   return watch_list_[1];
}

bool bf::BinaryClause::CheckState (LiteralInfo * literal_info,
      bf::ClauseInterface::ClauseState * state)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::BinaryClause::CheckState] literal_info == NULL", stderr);
      throw UnexpectedNULLException ();
   }
   if (state == NULL)
   {
      fputs ("[bf::BinaryClause::CheckState] state == NULL", stderr);
      throw UnexpectedNULLException ();
   }
#endif
   *state = DryCheckState (literal_info);
   if (literal_info[literals_[0]].value_ == kFalseValue
         && literal_info[literals_[1]].value_ == kUnknownValue)
   {
      watch_list_[0] = literals_[1];
      watch_list_[1] = literals_[0];
   }
   else
   {
      watch_list_[0] = literals_[0];
      watch_list_[1] = literals_[1];
   }
#if 0
   fputs ("[bf::BinaryClause::CheckState] ", stderr);
   Print(stderr);
   fputc('\n', stderr);
   fprintf (stderr, "%d %d %d\n",
         literal_info[literals_[0]].value_,
         literal_info[literals_[1]].value_,
         static_cast<int>(*state));
#endif
   return false;
}

   bf::ClauseInterface::ClauseState
bf::BinaryClause::DryCheckState (LiteralInfo * literal_info)
{
#ifndef NDEBUG
    if (literals_[1] == kNoLiteral)
   {
      fputs ("[bf::BinaryClause::DryCheckState] Not a binary clause\n", stderr);
      throw IndexOutOfBoundsException ();
   }
#endif
   PIValues first_literal_value = literal_info[literals_[0]].value_;
   PIValues second_literal_value = literal_info[literals_[1]].value_;
   if (first_literal_value == kTrueValue
         || second_literal_value == kTrueValue)
   {
      return kSatisfied;
   }
   if (first_literal_value == kFalseValue)
   {
      return (second_literal_value == kFalseValue ? kUnsatisfied : kUnit);
   }
   return (second_literal_value == kFalseValue ? kUnit : kBinary);
}

bool bf::BinaryClause::UpdateWatches (LiteralInfo * literal_info)
{
	if (literal_info[literals_[0]].value_ == kFalseValue
	         && literal_info[literals_[1]].value_ == kUnknownValue)
	   {
	      watch_list_[0] = literals_[1];
	      watch_list_[1] = literals_[0];
	   }
	   else
	   {
	      watch_list_[0] = literals_[0];
	      watch_list_[1] = literals_[1];
	   	}
   return false;
}
