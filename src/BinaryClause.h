/*
 * BinaryClause.h
 *
 *  Created on: 27.9.2011
 *      Author: su
 */

#ifndef BINARYCLAUSE_H_
#define BINARYCLAUSE_H_

#include "ClauseInterface.h"

namespace bf {

   class BinaryClause: public bf::ClauseInterface {
      friend class ClauseInterface;
      private:
         int literals_[3];
         int watch_list_[2];
      protected:
         inline BinaryClause()
         {
            literals_[0] = literals_[1] = literals_[2] = kNoLiteral;
            watch_list_[0] = watch_list_[1] = kNoLiteral;
         }
         virtual void InitWithNFClause (int a_cid, Clause * a_nfclause);
         virtual void InitWithLiterals (int a_cid, int * a_literals,
               size_t a_size);
         virtual void InitWithTwoLiterals (int a_cid,
               int a_literal, int b_literal);
         virtual void InitWithClause (int a_cid, ClauseInterface * a_clause);
         virtual void InitWithCapacity (int a_cid, size_t a_capacity);

      public:
         virtual ~BinaryClause();
         virtual void Clear ();
         virtual size_t size ();
         virtual size_t capacity ();
         virtual int literal (size_t lit_index);
         virtual size_t IndexOfLiteral(int lit, size_t from_index);
         virtual void AddLiteral (int lit);
         virtual void SetLiteralsFromClause (ClauseInterface * a_clause);
         virtual int WatchedLiteralA ();
         virtual int WatchedLiteralB ();
         virtual bool CheckState(LiteralInfo * literal_info,
               ClauseState * state);
         virtual ClauseState DryCheckState(LiteralInfo * literal_info);
         virtual bool UpdateWatches(LiteralInfo * literal_info);

      private:
         BinaryClause (BinaryClause&);
         void operator=(const BinaryClause&);
   };

} /* namespace bf */
#endif /* BINARYCLAUSE_H_ */
