//
//  BufferClause.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 06.10.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#include <limits>

#include "BufferClause.h"
#include "bfexception.h"
#include "LiteralInfo.h"

bf::BufferClause::~BufferClause()
{
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
}

void bf::BufferClause::InitWithNFClause(int a_cid, Clause * a_nfclause)
{
#ifndef NDEBUG
   if(a_nfclause == NULL)
   {
      fputs("[bf::BufferClause::InitWithNFClause] a_nfclause is NULL.\n", stderr);
      throw bf::UnexpectedNULLException();
   }
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = capacity_ = a_nfclause->NumberOfLiterals();
   literals_ = new int[a_nfclause->NumberOfLiterals()];
   int index = 0;
   for(Clause::LiteralSet::const_iterator lit
         = a_nfclause->literals().begin();
         lit != a_nfclause->literals().end();
         ++ lit)
   {
      literals_[index] = *lit;
      ++index;
   }

}

void bf::BufferClause::InitWithLiterals(int a_cid, int * a_literals,
      size_t a_size)
{
#ifndef NDEBUG
   if(a_literals == NULL)
   {
      fputs("[bf::BufferClause::InitWithLiterals] a_literals is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = capacity_ = a_size;
   literals_ = new int[a_size];
   while(a_size > 0)
   {
      -- a_size;
      literals_[a_size] = a_literals[a_size];
   }
}

void bf::BufferClause::InitWithClause(int a_cid, ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if(a_clause == NULL)
   {
      fputs("[bf::BufferClause::InitWithClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = capacity_ = a_clause->size();
   literals_ = new int[a_clause->size()];
   for(size_t index = 0; index < a_clause->size(); ++index)
   {
      literals_[index] = a_clause->literal(index);
   }
}
      
void bf::BufferClause::InitWithCapacity(int a_cid, size_t a_capacity)
{
#ifndef NDEBUG
   if(literals_ != NULL)
   {
      delete [] literals_;
      literals_ = NULL;
   }
#endif
   setCid(a_cid);;
   size_ = 0;
   capacity_ = a_capacity;
   literals_ = new int[capacity_];
}

void bf::BufferClause::Clear()
{
   size_ = 0;
}

size_t bf::BufferClause::size()
{
   return size_;
}

size_t bf::BufferClause::capacity()
{
   return capacity_;
}

int bf::BufferClause::literal(size_t lit_index)
{
#ifndef NDEBUG
   return (lit_index < size_ ? literals_[lit_index] : kNoLiteral);
#else
    return literals_[lit_index];
#endif
}

size_t bf::BufferClause::IndexOfLiteral(int lit, size_t from_index)
{
   if(size_ == 0)
   {
      return std::numeric_limits<size_t>::max();
   }
   size_t left = from_index;
   if(from_index >= size_)
   {
      return std::numeric_limits<size_t>::max();
   }
   size_t right = size_ - 1;
   if(lit == literals_[left])
   {
      return left;
   }
   if(lit == literals_[right])
   {
      return right;
   }
   while(right >= left)
   {
      size_t middle =(right+left)/2;
      if(lit == literals_[middle])
      {
         return middle;
      }
      else if(lit < literals_[middle])
      {
         right = middle - 1;
      }
      else
      {
         left = middle + 1;
      }
   }
   return std::numeric_limits<size_t>::max();
}

void bf::BufferClause::AddLiteral(int lit)
{
#ifndef NDEBUG
   if (lit == kNoLiteral)
   {
      fputs("[bf::BufferClause::AddLiteral] No literal at input.\n", stderr);
      throw BadParameterException();
   }
#endif
   if(size_ < capacity_)
   {
      literals_[size_] = lit;
      ++size_;
   }
   else
   {
      fputs("[BufferClause::AddLiteral] Clause is already full.\n", stderr);
      throw IndexOutOfBoundsException();
   }
}

void bf::BufferClause::SetLiteralsFromClause(ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if(a_clause == NULL)
   {
      fputs("[bf::BufferClause::SetLiteralsFromClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if(a_clause->size() > capacity_)
   {
      fputs("[bf::BufferClause::SetLiteralsFromClause] Literals from a_clause do not fit in.\n", stderr);
      throw IndexOutOfBoundsException();
   }
#endif
   size_ = a_clause->size();
   for(size_t index = 0; index < a_clause->size(); ++index)
   {
      literals_[index] = a_clause->literal(index);
   }
}

int bf::BufferClause::WatchedLiteralA()
{
   return kNoLiteral;
}

int bf::BufferClause::WatchedLiteralB()
{
   return kNoLiteral;
}

bool bf::BufferClause::CheckState(LiteralInfo *, ClauseState *)
{
   fputs ("[bf::BufferClause::CheckState] Cannot check state of a buffer clause.\n", stderr);
   throw BadMethodCallException();
}

bf::ClauseInterface::ClauseState bf::BufferClause::DryCheckState(LiteralInfo * literal_info)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::BufferClause::DryCheckState] literal_info == NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
#endif
   int unassigned = 0;
   for (size_t lit_index = 0; lit_index < size_; ++ lit_index)
   {
      switch (literal_info[literals_[lit_index]].value_)
      {
         case kTrueValue:
            return kSatisfied;
         case kUnknownValue:
            ++ unassigned;
            break;
         default:
            break;
      }
   }
   switch (unassigned)
   {
      case 0:
         return kUnsatisfied;
      case 1:
         return kUnit;
      case 2:
         return kBinary;
      default:
         return kUnresolved;
   }
   return kUnresolved;
}

bool bf::BufferClause::UpdateWatches(LiteralInfo *)
{
   fputs ("[bf::BufferClause::UpdateWatches] Cannot update watches of a buffer clause.\n", stderr);
   throw BadMethodCallException();
}

