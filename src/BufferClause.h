//
//  BufferClause.h
//  bfmanip++
//
//  Created by Petr Kučera on 06.10.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef BUFFERCLAUSE_H_
#define BUFFERCLAUSE_H_

#include "ClauseInterface.h"

namespace bf {
   /**@brief Class representing a small clause with a simple watched
    * literals management. */
   class BufferClause: public bf::ClauseInterface {
      friend class ClauseInterface;
   private:
      size_t capacity_;
      size_t size_;
      int * literals_;
   protected:
      inline BufferClause ()
      : capacity_(0), size_(0), literals_(NULL)
      {
      }
      virtual void InitWithNFClause (int a_cid, Clause * a_nfclause);
      virtual void InitWithLiterals (int a_cid, int * a_literals,
                                     size_t a_size);
      virtual void InitWithClause (int a_cid, ClauseInterface * a_clause);
      virtual void InitWithCapacity (int a_cid, size_t a_capacity);
      
   public:
      virtual ~BufferClause ();
      virtual void Clear ();
      virtual size_t size ();
      virtual size_t capacity ();
      virtual int literal (size_t lit_index);
      virtual size_t IndexOfLiteral (int lit, size_t from_index);
      virtual void AddLiteral (int lit);
      virtual void SetLiteralsFromClause (ClauseInterface * a_clause);
      virtual int WatchedLiteralA ();
      virtual int WatchedLiteralB ();
      virtual bool CheckState(LiteralInfo * literal_info,
               ClauseState * state);
      virtual ClauseState DryCheckState(LiteralInfo * literal_info);
      virtual bool UpdateWatches (LiteralInfo * literal_info);
   private:
      BufferClause (BufferClause&);
      void operator= (const BufferClause&);
   };

}

#endif
