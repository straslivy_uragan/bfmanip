/*
 * ClauseInterface.cpp
 *
 *  Created on: 22.9.2011
 *      Author: su
 */

#include "ClauseInterface.h"
#include "UnitClause.h"
#include "BinaryClause.h"
#include "CubicClause.h"
#include "SmallClause.h"
#include "BigClause.h"
#include "BufferClause.h"
#include "bfexception.h"

bf::ClauseInterface::~ClauseInterface() {}

bf::ClauseInterface * bf::ClauseInterface::NewClauseForSize(size_t a_size)
{
   ClauseInterface * a_clause = NULL;
   if (a_size == 1)
   {
      a_clause = new UnitClause();
   }
   else if (a_size == 2)
   {
      a_clause = new BinaryClause();
     }
   else if (a_size == 3)
   {
      a_clause = new CubicClause();
    }
   else if (a_size < kSmallClauseThreshold)
   {
      a_clause = new SmallClause();
   }
   else
   {
      //a_clause = new SmallClause();
      a_clause = new BigClause();
   }
   return a_clause;
}

bf::ClauseInterface * bf::ClauseInterface::CreateClauseWithCapacity (int a_cid,
      size_t a_capacity)
{
#ifndef NDEBUG
   if (a_capacity == 0)
   {
      fputs("[bf::CreateClauseWithCapacity] Cannot create clause with capacity equal to 0.\n", stderr);
      throw IndexOutOfBoundsException();
   }
#endif
   ClauseInterface * a_clause = NewClauseForSize(a_capacity);
   a_clause->InitWithCapacity(a_cid, a_capacity);
   return a_clause;
}

bf::ClauseInterface * bf::ClauseInterface::CreateClauseWithNFClause (int a_cid,
         bf::Clause * a_nfclause)
{
#ifndef NDEBUG
   if (a_nfclause == NULL)
   {
      fputs("[bf::CreateClauseWithCapacity] a_nfclause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
#endif
   ClauseInterface * a_clause = NewClauseForSize(a_nfclause->NumberOfLiterals());
   a_clause->InitWithNFClause(a_cid, a_nfclause);
   return a_clause;
}

bf::ClauseInterface * bf::ClauseInterface::CreateClauseWithLiterals (int a_cid,
      int * a_literals, size_t a_size)
{
#ifndef NDEBUG
   if (a_literals == NULL)
   {
      fputs("[bf::CreateClauseWithLiterals] a_literals is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if (a_size == 0)
   {
      fputs("[bf::CreateClauseWithLiterals] a_size is 0.\n", stderr);
      throw IndexOutOfBoundsException();
   }
#endif
   ClauseInterface * a_clause = NewClauseForSize(a_size);
   a_clause->InitWithLiterals(a_cid, a_literals, a_size);
   return a_clause;
}

bf::ClauseInterface * bf::ClauseInterface::CreateClauseWithClause (int a_cid,
         bf::ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs("[bf::CreateClauseWithLiterals] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
#endif
   ClauseInterface * clause = NewClauseForSize(a_clause->size());
   clause->InitWithClause(a_cid, a_clause);
   return clause;
}

bf::ClauseInterface * bf::ClauseInterface::CreateBufferClause (size_t a_capacity)
{
#ifndef NDEBUG
   if (a_capacity == 0)
   {
      fputs("[bf::ClauseInterface::CreateBufferClause] Cannot create a buffer clause with 0 capacity.\n", stderr);
      throw IndexOutOfBoundsException();
   }
#endif
   BufferClause * a_clause = new BufferClause();
   a_clause->InitWithCapacity(0, a_capacity);
   return a_clause;
}

void bf::ClauseInterface::SetWatchIndices(size_t , size_t)
{
}

bool bf::ClauseInterface::Resolve(ClauseInterface * other_parent,
      ClauseInterface * resolvent)
{
#ifndef NDEBUG
   if (other_parent == NULL)
   {
      fputs ("[bf::ClauseInterface::Resolve] other_parent == NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (resolvent == NULL)
   {
      fputs ("[bf::ClauseInterface::Resolve] resolvent == NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
#endif
   resolvent->Clear ();
   size_t my_literal_ind = 0;
   size_t other_literal_ind = 0;
   bool found_conflict = false;
   while (my_literal_ind < size()
         && other_literal_ind < other_parent->size())
   {
      int my_literal = literal(my_literal_ind);
      int other_literal = other_parent->literal(other_literal_ind);
      if (my_literal/2 == other_literal/2)
      {
         if (my_literal == other_literal)
         {
            resolvent->AddLiteral (my_literal);
         }
         else if (found_conflict)
         {
            return false;
         }
         else
         {
            found_conflict = true;
         }
         ++ my_literal_ind;
         ++ other_literal_ind;
      }
      else if (my_literal < other_literal)
      {
         resolvent->AddLiteral (my_literal);
         ++my_literal_ind;
      }
      else
      {
         resolvent->AddLiteral (other_literal);
         ++other_literal_ind;
      }
   }
   if (!found_conflict)
   {
      return false;
   }
   while (other_literal_ind < other_parent->size())
   {
      resolvent->AddLiteral (other_parent->literal(other_literal_ind++));
   }
   while (my_literal_ind < size())
   {
      resolvent->AddLiteral (literal(my_literal_ind++));
   }
#if 0
   fputs("R(", stderr);
   Print(stderr);
   fputs(", ", stderr);
   other_parent->Print(stderr);
   fputs(") = ", stderr);
   resolvent->Print(stderr);
   fputc('\n', stderr);
#endif
   return true;
}

bool bf::ClauseInterface::SubsumingResolve(ClauseInterface * other_parent,
      ClauseInterface * resolvent)
{
#ifndef NDEBUG
   if (other_parent == NULL)
   {
      fputs ("[bf::ClauseInterface::SubsumingResolve] other_parent == NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (resolvent == NULL)
   {
      fputs ("[bf::ClauseInterface::SubsumingResolve] resolvent == NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
#endif
   if (other_parent->size() > size())
   {
      return false;
   }
   resolvent->Clear ();
   size_t my_literal_ind = 0;
   size_t other_literal_ind = 0;
   bool found_conflict = false;
      while (my_literal_ind < size()
         && other_literal_ind < other_parent->size())
   {
      int my_literal = literal(my_literal_ind);
      int other_literal = other_parent->literal(other_literal_ind);
      if (my_literal/2 == other_literal/2)
      {
         if (my_literal == other_literal)
         {
            resolvent->AddLiteral (my_literal);
         }
         else if (found_conflict)
         {
            return false;
         }
         else
         {
            found_conflict = true;
         }
         ++ my_literal_ind;
         ++ other_literal_ind;
      }
      else if (my_literal < other_literal)
      {
         resolvent->AddLiteral (my_literal);
         ++my_literal_ind;
      }
      else
      {
         return false;
      }
   }
   while (!found_conflict || other_literal_ind < other_parent->size())
   {
      return false;
   }
   while (my_literal_ind < size())
   {
      resolvent->AddLiteral (literal(my_literal_ind++));
   }
   return true;
}

void bf::ClauseInterface::Print(FILE * file)
{
   if (file == NULL)
   {
      file = stdout;
   }
   for (size_t literal_ind = 0; literal_ind < size(); ++literal_ind)
   {
      int lit = literal(literal_ind);
      if (lit % 2)
      {
         fprintf (file, "-%d ", lit / 2);
      }
      else
      {
         fprintf (file, "%d ", lit / 2);
      }
   }
   fputs ("0 (", file);
   if (WatchedLiteralA () != kNoLiteral)
   {
      fprintf (file, "%s%d, ",
            (WatchedLiteralA () % 2 ? "-" : ""), WatchedLiteralA ()/2);
   }
   else
   {
      fputs ("-, ", file);
   }
   if (WatchedLiteralA () != kNoLiteral)
   {
      fprintf (file, "%s%d)",
            (WatchedLiteralB () % 2 ? "-" : ""), WatchedLiteralB ()/2);
   }
   else
   {
      fputs ("-)", file);
   }
}

bool bf::ClauseInterface::NeedsBacktrackNotification()
{
   return false;
}

void bf::ClauseInterface::Backtrack(int)
{
}
