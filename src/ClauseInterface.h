/*
 * ClauseInterface.h
 *
 *  Created on: 22.9.2011
 *      Author: Straslivy Uragan
 */

#ifndef CLAUSEINTERFACE_H_
#define CLAUSEINTERFACE_H_

#include <climits>

#include "input.h"
#include "normalform.h"

/**@file ClauseInterface.h
 * @brief Contains declaration of an abstract interface class defining
 * common properties of clauses used in a sat solver based on cdcl.
 */

namespace bf {

   struct LiteralInfo;

   /**@brief An abstract interface class definining common properties
    * of clause representations in a CDCL like solver.
    *
    * Among common properties belong a watch management. Also no data
    * fields are defined here, instead they should be defined in
    * descendants.
    */
   class ClauseInterface {
      public:
         /**@brief Constants for determining state of a clause.
          *
          * These constants are returned by
          * #CheckState(PIValues*,ClauseState*) method.
          */
         enum ClauseState {
            /**@brief Clause is satisfied.
             *
             * It has at least one literal which evaluate to 1.
             */
            kSatisfied,
            /**@brief Clause is unsatisfied.
             *
             * All its literals evaluate to 0.
             */
            kUnsatisfied,
            /**@brief Clause is unit.
             *
             * It has only one unassigned literal and all other
             * literals are falsified.
             */
            kUnit,
            /**@brief Clause is binary.
             *
             * It has two unassigned literals and all other
             * literals are falsified.
             */
            kBinary,
            /**@brief Clause is unresolved.
             *
             * It has three or more unassigned literals and all
             * other literals are falsified.
             */
            kUnresolved
         };

         static const int kNoLiteral = INT_MAX;
         static const size_t kSmallClauseThreshold = 48;
      private:
         int cid_;

      protected:
         /**@brief Initalizes this clause as a copy of given
          * NFElement type of a clause.
          *
          * @return 0 if everything went o.k., nonzero value in case
          * of an error, e.g. if size of given NFElement does not fit
          * within the capacity of this clause.
          */
         virtual void InitWithNFClause (int a_cid,
               Clause * a_nfclause) = 0;
         virtual void InitWithLiterals (int a_cid, int * a_literals,
               size_t a_size) = 0;
         virtual void InitWithClause (int a_cid,
               ClauseInterface * a_clause) = 0;
         virtual void InitWithCapacity (int a_cid, size_t a_capacity) = 0;
      public:
      ClauseInterface() : cid_(0) {};
         
         inline int cid() { return cid_; }
         inline void setCid(int a_cid) { cid_ = a_cid; }

         virtual ~ClauseInterface();
         virtual void Clear() = 0;
         virtual size_t size() = 0;
         virtual size_t capacity() = 0;
         /**@brief Returns literal at given index.
          *
          * If lit_index is bigger than the size (or even capacity) of
          * this clause, then this method returns kNoLiteral.
          */
         virtual int literal(size_t lit_index) = 0;
         virtual size_t IndexOfLiteral(int lit, size_t from_index) = 0;
         virtual void AddLiteral(int lit) = 0;
         virtual void SetLiteralsFromClause(ClauseInterface * a_clause) = 0;
         virtual int WatchedLiteralA() = 0;
         virtual int WatchedLiteralB() = 0;
         virtual bool CheckState(LiteralInfo * literal_info,
               ClauseState * state) = 0;
         virtual ClauseState DryCheckState(LiteralInfo * literal_info) = 0;
         virtual void SetWatchIndices(size_t watch_a, size_t watch_b);
         virtual bool Resolve(ClauseInterface * other_parent, 
               ClauseInterface * resolvent);
         virtual bool SubsumingResolve(ClauseInterface * other_parent,
               ClauseInterface * resolvent);
         virtual void Print(FILE * file);

         virtual bool NeedsBacktrackNotification();
         virtual void Backtrack(int bt_level);
      private:
         ClauseInterface (ClauseInterface&);
         void operator=(const ClauseInterface&);
         static ClauseInterface * NewClauseForSize(size_t a_size);
      public:
         static ClauseInterface * CreateClauseWithCapacity (int a_cid,
               size_t a_capacity);
         static ClauseInterface * CreateClauseWithNFClause (int a_cid,
               Clause * a_nfclause);
         static ClauseInterface * CreateClauseWithLiterals (int a_cid,
               int * a_literals, size_t a_size);
         static ClauseInterface * CreateClauseWithClause (int a_cid,
               ClauseInterface * a_clause);
         static ClauseInterface * CreateBufferClause (size_t a_capacity);
   };
} /* namespace bf */
#endif /* CLAUSEINTERFACE_H_ */
