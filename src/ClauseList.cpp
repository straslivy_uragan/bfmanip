/*
 * ClauseList.cpp
 *
 *  Created on: 10.10.2011
 *      Author: su
 */

#include "ClauseList.h"
#include <assert.h>
#include "ClauseList.h"
#include "bfexception.h"

bf::ClauseList::Element * bf::ClauseList::Element::free_elements_stack_ = NULL;

bf::ClauseList::Element::~Element ()
{
   clause_ = NULL;
   prev_ = next_ = NULL;
}

bf::ClauseList::Element * bf::ClauseList::Element::NewElement ()
{
   Element * element = NULL;
   if (free_elements_stack_ != NULL)
   {
      element = free_elements_stack_;
      free_elements_stack_ = free_elements_stack_ -> next_;
      if (free_elements_stack_ != NULL)
      {
         free_elements_stack_->prev_ = NULL;
      }
      element->next_ = element->prev_ = NULL;
      element->clause_ = NULL;
   }
   else
   {
      element = new Element ();
   }
   return element;
}

void bf::ClauseList::Element::ReleaseElement (bf::ClauseList::Element * element)
{
   element->next_ = free_elements_stack_;
   element->prev_ = NULL;
   element->clause_ = NULL;
   free_elements_stack_ = element;
}

void bf::ClauseList::Element::DeleteFreeElements ()
{
   while (free_elements_stack_ != NULL)
   {
      Element * element = free_elements_stack_;
      free_elements_stack_ = free_elements_stack_->next_;
      delete element;
   }
}

void bf::ClauseList::Element::DeleteClause ()
{
   if (clause_ != NULL)
   {
      delete clause_;
      clause_ = NULL;
   }
}

bf::ClauseList::~ClauseList ()
{
   Clear ();
}

void bf::ClauseList::PushBack (bf::ClauseInterface * a_clause)
{
   Element * element = Element::NewElement ();
   assert (element != NULL);
   element->setClause(a_clause);
   if (head_== NULL)
   {
      head_ = tail_ = element;
   }
   else
   {
      tail_->setNext(element);
      element->setPrev(tail_);
      tail_ = element;
   }
}

void bf::ClauseList::PushFront (bf::ClauseInterface * a_clause)
{
   Element * element = Element::NewElement ();
   assert (element != NULL);
   element->setClause(a_clause);
   if (head_== NULL)
   {
      head_ = tail_ = element;
   }
   else
   {
      head_->setPrev(element);
      element->setNext(head_);
      head_ = element;
   }
}

bf::ClauseInterface * bf::ClauseList::PopBack ()
{
   ClauseInterface * clause = NULL;
   if (tail_ != NULL)
   {
      clause = tail_->clause();
      tail_->setClause(NULL);
      Element * element = tail_;
      tail_ = tail_->prev();
      if (tail_ != NULL)
      {
         tail_->setNext(NULL);
      }
      else
      {
         head_ = NULL;
      }
      Element::ReleaseElement (element);
   }
   return clause;
}

bf::ClauseInterface * bf::ClauseList::PopFront ()
{
   ClauseInterface * clause = NULL;
   if (head_ != NULL)
   {
      clause = head_->clause();
      head_->setClause(NULL);
      Element * element = head_;
      head_ = head_->next();
      if (head_ != NULL)
      {
         head_->setPrev(NULL);
      }
      else
      {
         tail_ = NULL;
      }
      Element::ReleaseElement (element);
   }
   return clause;
}

void bf::ClauseList::Clear ()
{
   while (head_ != NULL)
   {
      Element * element = head_;
      head_ = head_->next();
      Element::ReleaseElement (element);
   }
   tail_ = NULL;
}

void bf::ClauseList::DeleteAllElements ()
{
   while (head_ != NULL)
   {
      Element * element = head_;
      head_ = head_->next();
      element->DeleteClause();
      Element::ReleaseElement (element);
   }
   tail_ = NULL;
}

void bf::ClauseList::Swap(bf::ClauseList& list)
{
   Element * buf_head = head_;
   Element * buf_tail = tail_;
   head_ = list.head_;
   tail_ = list.tail_;
   list.head_ = buf_head;
   list.tail_ = buf_tail;
}

void bf::ClauseList::Print (FILE * file)
{
   fputs ("<ClauseList>\n", file);
   for (Element * element = head_;
         element != NULL;
         element = element->next())
   {
      element->clause()->Print(file);
   }
   fputs ("</ClauseList>\n", file);
}
