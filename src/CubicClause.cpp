//
//  bf::CubicClause.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 29.09.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#include <limits>

#include "CubicClause.h"
#include "bfexception.h"
#include "LiteralInfo.h"

bf::CubicClause::~CubicClause () {}

void bf::CubicClause::InitWithNFClause (int a_cid, Clause * a_nfclause)
{
#ifndef NDEBUG
   if (a_nfclause == NULL)
   {
      fputs ("[bf::CubicClause::InitWithNFClause] a_nfclause is NULL.\n", stderr);
      throw bf::UnexpectedNULLException ();
   }
   if (a_nfclause->NumberOfLiterals () > 3)
   {
      fputs ("[bf::CubicClause::InitWithNFClause] Given clause contains more than three literals.\n", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif

   setCid(a_cid);;
   literals_[0] = literals_[1] = literals_[2] = kNoLiteral;
   watch_list_[0] = watch_list_[1] = watch_list_[2] = kNoLiteral;

   int index = 0;
   for (Clause::LiteralSet::const_iterator lit
         = a_nfclause->literals ().begin ();
         lit != a_nfclause->literals ().end ();
         ++ lit)
   {
      literals_[index] = watch_list_[index] = *lit;
      ++index;
   }

}

void bf::CubicClause::InitWithLiterals (int a_cid, int * a_literals,
      size_t a_size)
{
#ifndef NDEBUG
   if (a_literals == NULL)
   {
      fputs ("[bf::CubicClause::InitWithLiterals] a_literals is NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (a_size > 2)
   {
      fputs ("[bf::CubicClause::InitWithLiterals] Set of literals contains more than two literals.\n", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif
   setCid(a_cid);;
   literals_[0] = literals_[1] = literals_[2] = kNoLiteral;
   watch_list_[0] = watch_list_[1] = watch_list_[2] = kNoLiteral;
   while (a_size > 0)
   {
      -- a_size;
      watch_list_[a_size] = literals_[a_size] = a_literals[a_size];
   }
}

void bf::CubicClause::InitWithThreeLiterals (int a_cid,
      int a_literal, int b_literal, int c_literal)
{
   setCid(a_cid);;
   watch_list_[0] = literals_[0] = a_literal;
   watch_list_[1] = literals_[1] = b_literal;
   watch_list_[2] = literals_[2] = c_literal;
}

void bf::CubicClause::InitWithClause (int a_cid, ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs ("[bf::CubicClause::InitWithClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (a_clause->size () > 3)
   {
      fputs ("[bf::CubicClause::InitWithClause] Given clause contains more than two literals.\n", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif
   setCid(a_cid);;
   watch_list_[0] = literals_[0] = a_clause->literal (0);
   watch_list_[1] = literals_[1] = a_clause->literal (1);
   watch_list_[2] = literals_[2] = a_clause->literal (2);
}

void bf::CubicClause::InitWithCapacity(int a_cid, size_t a_capacity)
{
#ifndef NDEBUG
   if (a_capacity != 3)
   {
      fputs("[bf::CubicClause::InitWithCapacity] a_capacity != 2.\n", stderr);
      throw BadParameterException();
   }
#endif
   setCid(a_cid);;
   literals_[0] = literals_[1] = literals_[2] = kNoLiteral;
   watch_list_[0] = watch_list_[1] = watch_list_[2] = kNoLiteral;
}

void bf::CubicClause::Clear ()
{
   setCid(0);
   literals_[0] = literals_[1] = literals_[2] = kNoLiteral;
   watch_list_[0] = watch_list_[1] = watch_list_[2] = kNoLiteral;
}

size_t bf::CubicClause::size ()
{
   return 3;
}

size_t bf::CubicClause::capacity ()
{
   return 3;
}

int bf::CubicClause::literal (size_t lit_index)
{
#ifndef NDEBUG
   if (lit_index > 2)
   {
      fputs ("[bf::CubicClause::literal] lit_index > 2\n", stderr);
      throw UnexpectedNULLException ();
   }
#endif
   return literals_[lit_index];
}

size_t bf::CubicClause::IndexOfLiteral (int lit, size_t from_index)
{
   for (size_t index=from_index; index<3; ++index)
   {
      if (literals_[index] == lit)
      {
         return index;
      }
   }
   return std::numeric_limits<size_t>::max();
}

void bf::CubicClause::AddLiteral (int lit)
{
   size_t index = 0;
   while (literals_[index]!=kNoLiteral)
   {
      ++index;
   }
   if (index < 3)
   {
      literals_[index] = lit;
   }
   else
   {
      fputs ("[bf::CubicClause::AddLiteral] Clause is already full.\n", stderr);
      throw IndexOutOfBoundsException ();
   }
}

void bf::CubicClause::SetLiteralsFromClause (ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs ("[bf::CubicClause::SetLiteralsFromClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (a_clause->size () > 3)
   {
      fputs ("[bf::CubicClause::SetLiteralsFromClause] Given clause contains more than two literals.\n", stderr);
      throw bf::IndexOutOfBoundsException ();
   }
#endif
   watch_list_[0] = literals_[0] = a_clause->literal (0);
   watch_list_[1] = literals_[1] = a_clause->literal (1);
   watch_list_[2] = literals_[2] = a_clause->literal (2);
}

int bf::CubicClause::WatchedLiteralA ()
{
   return watch_list_[0];
}

int bf::CubicClause::WatchedLiteralB ()
{
   return watch_list_[1];
}

bool bf::CubicClause::CheckState (bf::LiteralInfo * literal_info,
      bf::ClauseInterface::ClauseState * state)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::CubicClause::CheckState] literal_info == NULL\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (state == NULL)
   {
      fputs ("[bf::CubicClause::CheckState] state == NULL\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (literals_[2] == kNoLiteral)
   {
      fputs ("[bf::CubicClause::CheckState] This clause is not cubic\n", stderr);
      throw IndexOutOfBoundsException ();
   }
#endif
   *state = DryCheckState(literal_info);
   bool watch_changed = false;
   if (literal_info[watch_list_[2]].value_ != kFalseValue)
   {
      if (literal_info[watch_list_[0]].value_ == kFalseValue)
      {
         int swap = watch_list_[0];
         watch_list_[0] = watch_list_[2];
         watch_list_[2] = swap;
         watch_changed = true;
      }
      else if (literal_info[watch_list_[1]].value_ == kFalseValue)
      {
         int swap = watch_list_[1];
         watch_list_[1] = watch_list_[2];
         watch_list_[2] = swap;
         watch_changed = true;
      }
   }
   if (literal_info[watch_list_[0]].value_ == kFalseValue
         && literal_info[watch_list_[1]].value_ == kUnknownValue)
   {
      int swap = watch_list_[0];
      watch_list_[0] = watch_list_[1];
      watch_list_[1] = swap;
   }
#if 0
   fputs ("[bf::CubicClause::CheckState] ", stderr);
   Print(stderr);
   fputc('\n', stderr);
   fprintf (stderr, "%d %d %d %d\n",
         literal_info[literals_[0]].value_,
         literal_info[literals_[1]].value_,
         literal_info[literals_[2]].value_,
         static_cast<int>(*state));
#endif
   return watch_changed;
}

   bf::ClauseInterface::ClauseState
bf::CubicClause::DryCheckState (bf::LiteralInfo * literal_info)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::CubicClause::DryCheckState] literal_info == NULL\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (literals_[2] == kNoLiteral)
   {
      fputs ("[bf::CubicClause::DryCheckState] This clause is not cubic\n", stderr);
      throw IndexOutOfBoundsException ();
   }
#endif
   if (literal_info[literals_[0]].value_ == kTrueValue
         || literal_info[literals_[1]].value_ == kTrueValue 
         || literal_info[literals_[2]].value_ == kTrueValue)
   {
      return kSatisfied;
   }
   int number_of_unknowns = 0;
   for (size_t index = 0; index < 3; ++index)
   {
      if (literal_info[literals_[index]].value_ == kUnknownValue)
      {
         ++ number_of_unknowns;
      }
   }
   switch (number_of_unknowns)
   {
      case 0:
         return kUnsatisfied;
      case 1:
         return kUnit;
      case 2:
         return kBinary;
      default:
         break;
   }
   return kUnresolved;
}

bool bf::CubicClause::UpdateWatches (bf::LiteralInfo * literal_info)
{
#ifndef NDEBUG
   if (literal_info == NULL)
   {
      fputs ("[bf::CubicClause::UpdateWatches] literal_info == NULL\n", stderr);
      throw UnexpectedNULLException ();
   }
   if (literals_[2] == kNoLiteral)
   {
      fputs ("[bf::CubicClause::UpdateWatches] This clause is not cubic\n", stderr);
      throw IndexOutOfBoundsException ();
   }
#endif
   bool watch_changed = false;
   if (literal_info[watch_list_[2]].value_ != kFalseValue)
   {
      if (literal_info[watch_list_[0]].value_ == kFalseValue)
      {
         int swap = watch_list_[0];
         watch_list_[0] = watch_list_[2];
         watch_list_[2] = swap;
         watch_changed = true;
      }
      else if (literal_info[watch_list_[1]].value_ == kFalseValue)
      {
         int swap = watch_list_[1];
         watch_list_[1] = watch_list_[2];
         watch_list_[2] = swap;
         watch_changed = true;
      }
   }
   if (literal_info[watch_list_[0]].value_ == kFalseValue
         && literal_info[watch_list_[1]].value_ == kUnknownValue)
   {
      int swap = watch_list_[0];
      watch_list_[0] = watch_list_[1];
      watch_list_[1] = swap;
   }
   return watch_changed;
}

void bf::CubicClause::SetWatchIndices(size_t watch_a, size_t watch_b)
{
#ifndef NDEBUG
   if (watch_a > 2)
   {
      fputs("[bf::ClauseInterface::SetWatchIndices] watch_a is out of bounds.\n", stderr);
      throw IndexOutOfBoundsException();
   }
   if (watch_b > 2)
   {
      fputs("[bf::ClauseInterface::SetWatchIndices] watch_b is out of bounds.\n", stderr);
      throw IndexOutOfBoundsException();
   }
   if (watch_a == watch_b)
   {
      fputs ("[bf::CubicClause::SetWatchIndices] watch_a == watch_b.\n", stderr);
      throw BadParameterException();
   }
#endif
   watch_list_[0]=literals_[watch_a];
   watch_list_[1]=literals_[watch_b];
   watch_list_[2]=literals_[3-watch_a-watch_b];
}

