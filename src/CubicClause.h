//
//  CubicClause.h
//  bfmanip++
//
//  Created by Petr Kučera on 29.09.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef CUBICCLAUSE_H_
#define CUBICCLAUSE_H_

#include "ClauseInterface.h"

namespace bf {
   class CubicClause: public bf::ClauseInterface {
      friend class ClauseInterface;
      private:
         int literals_[4];
         int watch_list_[3];
      protected:
         inline CubicClause ()
         {
            literals_[0] = literals_[1] = kNoLiteral;
            literals_[2] = literals_[3] = kNoLiteral;
            watch_list_[0] = watch_list_[1] = watch_list_[2] = kNoLiteral;
         }
         virtual void InitWithNFClause (int a_cid, Clause * a_nfclause);
         virtual void InitWithLiterals (int a_cid, int * a_literals,
               size_t a_size);
         virtual void InitWithThreeLiterals (int a_cid,
               int a_literal, int b_literal, int c_literal);
         virtual void InitWithClause (int a_cid, ClauseInterface * a_clause);
         virtual void InitWithCapacity (int a_cid, size_t a_capacity);

      public:
         virtual ~CubicClause ();
         virtual void Clear ();
         virtual size_t size ();
         virtual size_t capacity ();
         virtual int literal (size_t lit_index);
         virtual size_t IndexOfLiteral (int lit, size_t from_index);
         virtual void AddLiteral (int lit);
         virtual void SetLiteralsFromClause (ClauseInterface * a_clause);
         virtual int WatchedLiteralA ();
         virtual int WatchedLiteralB ();
         virtual bool CheckState(LiteralInfo * literal_info,
               ClauseState * state);
         virtual ClauseState DryCheckState(LiteralInfo * literal_info);
         virtual bool UpdateWatches (LiteralInfo * literal_info);
         virtual void SetWatchIndices(size_t watch_a, size_t watch_b);
      private:
         CubicClause (CubicClause&);
         void operator= (const CubicClause&);
   };

} /* namespace bf */

#endif /* CUBICCLAUSE_H_ */
