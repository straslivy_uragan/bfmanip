/*
 * IntHeap.cpp
 *
 *  Created on: 31.10.2011
 *      Author: su
 */
#include <cstdio>

#include "IntHeap.h"
#include "bfexception.h"

bf::IntHeap::~IntHeap() {
   if (elements_ != NULL)
   {
      delete [] elements_;
      elements_ = NULL;
   }
}

void bf::IntHeap::InitWithCapacity(size_t a_capacity)
{
   if (elements_ != NULL)
   {
      delete [] elements_;
      elements_ = NULL;
   }
   capacity_ = a_capacity;
   size_ = 0;
   elements_ = new Element[a_capacity];
}

void bf::IntHeap::Insert(int a_value, int a_key)
{
#ifndef NDEBUG
   if (size_ >= capacity_)
   {
      fputs("[IntHeap::Insert] Heap is already full", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   elements_[size_].value_ = a_value;
   elements_[size_].key_ = a_key;
   ++ size_;
   Up(size_-1);
}

void bf::IntHeap::DeleteMin(int * a_value, int * a_key)
{
#ifndef NDEBUG
   if (a_value == NULL)
   {
      fputs ("[IntHeap::DeleteMin] Parameter a_value is NULL.", stderr);
      throw bf::UnexpectedNULLException();
   }
   if (a_key == NULL)
   {
      fputs ("[IntHeap::DeleteMin] Parameter a_key is NULL.", stderr);
      throw bf::UnexpectedNULLException();
   }
   if (size_ == 0)
   {
      fputs ("[IntHeap::DeleteMin] Heap is empty.", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   *a_value = elements_[0].value_;
   *a_key = elements_[0].key_;
   -- size_;
   elements_[0].value_ = elements_[size_].value_;
   elements_[0].key_ = elements_[size_].key_;
   Down(0);
}

void bf::IntHeap::Min(int * a_value, int * a_key)
{
#ifndef NDEBUG
   if (a_value == NULL)
   {
      fputs ("[IntHeap::Min] Parameter a_value is NULL.", stderr);
      throw bf::UnexpectedNULLException();
   }
   if (a_key == NULL)
   {
      fputs ("[IntHeap::Min] Parameter a_key is NULL.", stderr);
      throw bf::UnexpectedNULLException();
   }
   if (size_ == 0)
   {
      fputs ("[IntHeap::Min] Heap is empty.", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   *a_value = elements_[0].value_;
   *a_key = elements_[0].key_;
}


void bf::IntHeap::Up(size_t position)
{
   while (position > 0)
   {
      size_t parent = position / 2;
      if (elements_[parent].key_ <= elements_[position].key_)
      {
         break;
      }
      int swap = elements_[parent].value_;
      elements_[parent].value_ = elements_[position].value_;
      elements_[position].value_ = swap;
      swap = elements_[parent].key_;
      elements_[parent].key_ = elements_[position].key_;
      elements_[position].key_ = swap;
      position = parent;
   }
}

void bf::IntHeap::Down(size_t position)
{
   while (position < size_)
   {
      size_t left = position * 2;
      size_t right = position * 2 + 1;
      size_t smaller = left;
      if (right < size_
            && elements_[right].key_ < elements_[left].key_)
      {
         smaller = right;
      }
      if (smaller >= size_ 
            || elements_[smaller].key_ >= elements_[position].key_)
      {
         break;
      }
      int swap = elements_[smaller].value_;
      elements_[smaller].value_ = elements_[position].value_;
      elements_[position].value_ = swap;
      swap = elements_[smaller].key_;
      elements_[smaller].key_ = elements_[position].key_;
      elements_[position].key_ = swap;
      position = smaller;
   }
}

