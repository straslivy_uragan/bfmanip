/*
 * IntHeap.h
 *
 *  Created on: 31.10.2011
 *      Author: su
 */

#ifndef INTHEAP_H_
#define INTHEAP_H_

#include <cstddef>

namespace bf {
/**@brief Class implementing a heap where both data and keys are int
 * values.
 *
 * Note, that safety checks (for NULL parameters, for emptiness in Min
 * or DeleteMin) are performed only if NDEBUG macro is undefined.
 */
class IntHeap {
   /**@brief Structure describing a heap element.
    */
   struct Element {
      /**@brief Value stored in this element. */
      int value_;
      /**@brief Key associated with the value stored in this
       * element. */
      int key_;
   };

   /**@brief Capacity of this heap.
    *
    * The allocated size of elements_ array.
    */
   size_t capacity_;
   /**@brief Size of this heap.
    *
    * The number of elements stored in this heap.
    */
   size_t size_;
   /**@brief Array with elements representing data stored in this
    * heap.
    */
   Element * elements_;

   /**@brief Repairs heap invariants which are not possibly satisfied
    * by an element at given position.
    *
    * Moves element at give position up in the binary tree to satisfy
    * property that children of a tree node have greater or equal
    * key values.
    */
   void Up(size_t position);
   /**@brief Repairs heap invariants which are not possibly satisfied
    * by an element at given position.
    *
    * Moves element at give position down in the binary tree to satisfy
    * property that children of a tree node have greater or equal
    * key values.
    */
   void Down(size_t position);

public:
   /**@brief A constructor initializes an empty heap with zero
    * capacity.
    */
	IntHeap();
   /**@brief Destructor deallocates memory used by this heap.
    */
	virtual ~IntHeap();

   /**@brief Initializes the heap with given capacity.
    *
    * If elements_ array is not NULL, it is deleted first thus
    * emptying the heap.
    * 
    * @param a_capacity Number of elements to allocate.
    */
   void InitWithCapacity(size_t a_capacity);
   /**@brief Returns capacity of this heap.
    *
    * Capacity is the number of allocated elements in the elements_
    * array.
    */
   size_t capacity();
   /**@brief Returns number of elements in this heap.
    */
   size_t size();

   /**@brief Allows to access the value at given index.
    *
    * @param index index at which the value should be accessed.
    *
    * @return Value at index @p index.
    */
   int ValueAt(size_t index);

   /**@brief Checks whether this heap is empty.
    *
    * @return True, if size_==0, false otherwise.
    */
   bool IsEmpty();
   /**@brief Inserts new data into this heap.
    *
    * @param a_value Value of the data to be stored in the heap.
    * @param a_key Key associated with the value to be stored in the
    * heap.
    */
   void Insert(int a_value, int a_key);
   /**@brief Deletes the element with the minimum value of key.
    *
    * @param a_value In this output parameter the value of the data
    * with minimum key is returned.
    * @param a_key In this output parameter the key associated with
    * minimum key is returned.
    *
    * @throw bf::UnexpectedNULLException if one of the parameters is
    * NULL.
    * @throw bf::IndexOutOfBoundsException if the heap is already
    * empty.
    */
   void DeleteMin(int * a_value, int * a_key);
   /**@brief Returns the element with the minimum value of key.
    *
    * @param a_value In this output parameter the value of the data
    * with minimum key is returned.
    * @param a_key In this output parameter the key associated with
    * minimum key is returned.
    *
    * @throw bf::UnexpectedNULLException if one of the parameters is
    * NULL.
    * @throw bf::IndexOutOfBoundsException if the heap is empty.
    */
   void Min(int * a_value, int * a_key);

   /**@brief Empties the heap by setting its size to 0. */
   void Clear();
};

inline IntHeap::IntHeap()
   : capacity_(0), size_(0), elements_(NULL)
{}

inline size_t IntHeap::capacity()
{
   return capacity_;
}

inline size_t IntHeap::size()
{
   return size_;
}

inline bool IntHeap::IsEmpty()
{
   return (size_==0);
}

inline void IntHeap::Clear()
{
   size_ = 0;
}

inline int IntHeap::ValueAt(size_t index)
{
   return elements_[index].value_;
}

} /* namespace bf */

#endif /* INTHEAP_H_ */
