//
//  LiteralInfo.h
//  bfmanip++
//
//  Created by Petr Kučera on 06.10.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef LITERALINFO_H_
#define LITERALINFO_H_

#include "ClauseInterface.h"
#include "input.h"
#include "ClauseList.h"

namespace bf {
   /**@brief Structure used for storing information about a
    * single literal.
    *
    * It has only a basic constructor and destructor, otherwise
    * it is only a structure for storing data thus all elements
    * are public.
    *
    * There is no field for storing value of the literal as it
    * is stored in the assignment array.
    */
   struct LiteralInfo {
      /**@brief Index of antecedent clause.
       *
       * Equals to NULL if this literal has no antecedent
       * assigned.
       */
      ClauseInterface * antecedent_;
      /**@brief Decision level of assignment.
       *
       * Equals to -1, if this literal is not assigned a value.
       */
      int level_;

      /**@brief Current value of this literal. */
      PIValues value_;

      /**@brief Order in which this literal has been assigned a value.
       */
      int64_t assignment_order_;
      /**@brief VSIDS value of this literal.
       *
       * The VSIDS (or decaying sum) is stored as long double.
       * For exactly how this value is computed and used see
       * descriptions of CDCL::ConflictAnalysis, CDCL::Decay,
       * CDCL::Run, and CDCL::PickBranchingLiteral method
       * descriptions.
       */
      long double vsids_;
      /**@brief Number of appearances of this literal in
       * quadratic clauses.
       *
       * At the beginning and after backtrack this value is set
       * to 0. When we encounter a quadratic clause is with this
       * literal after assigning a value to another variable,
       * this value is increased.
       */
      int bin_occurrences_;
      /**@brief List of clauses in which this literal is being
       * watched.
       */
      ClauseList watched_clauses_;
      /**@brief Basic constructor initializes values to implicit
       * ones.
       */
      inline LiteralInfo () 
         : antecedent_ (NULL), level_ (-1), value_(kUnknownValue), assignment_order_ (-1),
         vsids_ (0.0),
         bin_occurrences_ (0),
         watched_clauses_ () {}
      /**@brief Destructor.
       *
       * It clears list of watched clauses and assigns NULL to
       * #antecedent_.
       */
      inline ~LiteralInfo () {
         antecedent_ = NULL;
         watched_clauses_.Clear();
      }

      private:
      /**@brief Copy constructor is private to prevent its
       * invocation.
       */
      LiteralInfo(LiteralInfo&);
      /**@brief Assignment operator is private to prevent its
       * invocation.
       */
      void operator=(LiteralInfo&);
   };
}

#endif
