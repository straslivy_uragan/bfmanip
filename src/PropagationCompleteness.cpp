/*
 * PropagationCompleteness.cpp
 *
 *  Created on: 17.12.2011
 *      Author: su
 */
#include "PropagationCompleteness.h"
#include "bfexception.h"

namespace bf
{
   PropagationCompleteness::~PropagationCompleteness()
   {
      ClearImplicateList();
   }

   bool
   PropagationCompleteness::IsPropagationComplete()
   {
      Clause implicate;
      unsigned int emp_literal = 0;
      return (FindEmpoweringImplicate(&implicate, &emp_literal, false) > 0);
   }

   void
   PropagationCompleteness::ClearImplicateList()
   {
      while (!closed_implicates_.empty())
      {
         std::pair<Clause*, int> implicate_desc = closed_implicates_.back();
         closed_implicates_.pop_back();
         Clause* clause = implicate_desc.first;
         delete clause;
      }
      while (!open_implicates_.empty())
      {
         std::pair<Clause*, int> implicate_desc = open_implicates_.back();
         open_implicates_.pop_back();
         Clause* clause = implicate_desc.first;
         delete clause;
      }
   }

   void
   PropagationCompleteness::InitializeImplicateList()
   {
      ClearImplicateList();
      const CNF::ElementList& formula_clauses = formula_.elements();
      for (CNF::ElementList::const_iterator clause_ptr =
            formula_clauses.begin(); clause_ptr != formula_clauses.end(); ++clause_ptr)
      {
         const Clause * clause = static_cast<Clause*>(*clause_ptr);
         Clause * clause_copy = new Clause(*clause);
         std::pair<Clause*, int> implicate_desc(clause_copy, 0);
         open_implicates_.push_back(implicate_desc);
      }
   }

   int
   bf::PropagationCompleteness::FindEmpoweringImplicateBounded(bf::Clause* emp_implicate,
         unsigned int* emp_literal, int max_level, bool log_resolutions)
   {
      if (emp_implicate == NULL)
      {
         fputs("[PropagationCompleteness::FindEmpoweringImplicate] emp_implicate == NULL\n", stderr);
         throw UnexpectedNULLException(); 
      }
      if (emp_literal == NULL)
      {
         fputs("[PropagationCompleteness::FindEmpoweringliteral] emp_literal == NULL\n", stderr);
         throw UnexpectedNULLException(); 
      }
      InitializeImplicateList();
      int level = 0;
      bool found = false;
      while (!open_implicates_.empty() && level <= max_level && !found)
      {
         std::pair<Clause*, int> implicate_desc = open_implicates_.front();
         level = implicate_desc.second;
         open_implicates_.pop_front();
         Clause* implicate = implicate_desc.first;
         bool found_in_closed = false;
         for (ImplicateList::const_iterator closed_implicate_clause
               = closed_implicates_.begin();
               !found_in_closed 
               && closed_implicate_clause != closed_implicates_.end();
               ++ closed_implicate_clause)
         {
             Clause* closed_implicate = (*closed_implicate_clause).first;
            found_in_closed = closed_implicate->IsEqual(*implicate);
         }
         if (found_in_closed)
         {
            delete implicate;
            continue;
         }
         Clause resolvent;
         for (ImplicateList::const_iterator closed_implicate_clause
               = closed_implicates_.begin();
               closed_implicate_clause != closed_implicates_.end();
               ++ closed_implicate_clause
             )
         {
            Clause * closed_implicate = (*closed_implicate_clause).first;
            if (implicate->Resolve(*closed_implicate, &resolvent) == 1)
            {
               if (log_resolutions)
               {
                  std::string out_string = "";
                  resolvent.DIMACSString(&out_string);
                  std::cout << "Adding resolvent " << out_string;
                  out_string = "";
                  implicate->DIMACSString(&out_string);
                  std::cout << " = R(" << out_string;
                  out_string = "";
                  closed_implicate->DIMACSString(&out_string);
                  std::cout << ", " << out_string << ")";
                  std::cout << " level=" << level + 1
                     << std::endl;

               }
               std::pair<Clause *, int> resolvent_desc(
                     new Clause(resolvent), implicate_desc.second + 1);
               open_implicates_.push_back(resolvent_desc);
            }
         }
         closed_implicates_.push_back(implicate_desc);
         if (implicate_desc.second > 0)
         {
            if (IsEmpoweringClause(*implicate, emp_literal))
            {
               emp_implicate -> InitWithElement(*implicate);
               found = true;
            }
         }
      }
      return (found ? level : 0);
   }

   bool
   PropagationCompleteness::IsEmpoweringClause(const Clause & clause,
         unsigned int *emp_literal)
   {
 //     fputs("[PropagationCompleteness::IsEmpoweringClause] start\n", stderr);
      PartialInput part_input(formula_.NumberOfVariables());
      part_input.assign(formula_.NumberOfVariables(), bf::kUnknownValue);
      const Clause::LiteralSet& literals = clause.literals();
      for (Clause::LiteralSet::const_iterator literal = literals.begin();
            literal != literals.end(); ++literal)
     {
         clause.PartialInputFromElement(&part_input);
         part_input[(*literal) / 2] = kUnknownValue;
         bool no_empty_clause = formula_.UnitPropagation(&part_input);
         if (no_empty_clause
               && part_input[(*literal) / 2] == kUnknownValue)
         {
            *emp_literal = *literal;
            return true;
         }
      }
//      fputs("[PropagationCompleteness::IsEmpoweringClause] end\n", stderr);
      return false;
   }

} /* namespace bf */
