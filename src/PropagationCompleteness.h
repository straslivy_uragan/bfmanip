/*
 * PropagationCompleteness.h
 *
 *  Created on: 17.12.2011
 *      Author: su
 */

#ifndef PROPAGATIONCOMPLETENESS_H_
#define PROPAGATIONCOMPLETENESS_H_

#include <limits>
#include <utility>
#include <list>

#include "nfelement.h"
#include "normalform.h"

namespace bf
{
   class PropagationCompleteness
   {
      private:
         typedef std::list<std::pair<Clause*, int> > ImplicateList;
         const CNF& formula_;
         ImplicateList closed_implicates_;
         ImplicateList open_implicates_;

         PropagationCompleteness();

         void InitializeImplicateList();

         void ClearImplicateList();

      public:
         inline
            PropagationCompleteness(const CNF& a_formula) :
               formula_(a_formula),
               closed_implicates_(), open_implicates_()
            {
            }
         virtual
            ~PropagationCompleteness();

         bool
            IsPropagationComplete();

         /**@brief Looks for an empowering implicate by resolution method.
          *
          * The level is always nonnegative, it is 0, if no empowering
          * implicate has been found, it is positive, if an empowering
          * implicate has been found, in that case the level is returned.
          *
          * @param implicate in this variable an empowering implicate is
          * returned, if it was found.
          *
          * @return level of resolution at which an empowering implicate
          * was found, 0, if no empowering implicate has been found.
          */
         inline int
            FindEmpoweringImplicate(Clause* emp_implicate,
                  unsigned int* emp_literal, bool log_resolutions)
            {
               return FindEmpoweringImplicateBounded(emp_implicate,
                     emp_literal, std::numeric_limits<int>::max(), log_resolutions);
            }

         int
            FindEmpoweringImplicateBounded(Clause* emp_implicate,
                  unsigned int* emp_literal, int max_level,
                  bool log_resolutions);

         /**@brief
          */
         bool
            IsEmpoweringClause(const Clause& clause, unsigned int* emp_literal);
   };

} /* namespace bf */
#endif /* PROPAGATIONCOMPLETENESS_H_ */
