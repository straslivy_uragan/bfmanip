/*
 * bf::UnitClause.cpp
 *
 *  Created on: 23.9.2011
 *      Author: su
 */

#include <cstddef>
#include <cstdio>
#include <limits>

#include "bfexception.h"

#include "UnitClause.h"
#include "LiteralInfo.h"

void bf::UnitClause::InitWithNFClause (int a_cid, bf::Clause * a_nfclause)
{
#ifndef NDEBUG
   if (a_nfclause == NULL)
   {
      fputs ("[bf::UnitClause::InitWithNFClause] a_nfclause is NULL.\n", stderr);
      throw bf::UnexpectedNULLException();
   }
   if (a_nfclause->NumberOfLiterals() > 1)
   {
      fputs ("[bf::UnitClause::InitWithNFClause] Given clause is not unit.\n", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   setCid(a_cid);;
   if (a_nfclause->NumberOfLiterals() == 0)
   {
      literal_ = kNoLiteral;
   }
   else
   {
      //const std::set<unsigned int>& literals = a_nfclause->literals();
      //literal_ = *(literals.begin());
      literal_ = *(a_nfclause->literals().begin());
   }
}

void bf::UnitClause::InitWithLiterals (int a_cid, int * a_literals,
      size_t a_size)
{
#ifndef NDEBUG
   if (a_literals == NULL)
   {
      fputs ("[bf::UnitClause::InitWithLiterals] a_literals is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if (a_size > 1)
   {
      fputs ("[bf::UnitClause::InitWithLiterals] Set of literals contains more than one literal.\n", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   setCid(a_cid);;
   if (a_size == 0)
   {
      literal_ = kNoLiteral;
   }
   else
   {
      literal_ = a_literals[0];
   }
}

void bf::UnitClause::InitWithOneLiteral (int a_cid, int a_literal)
{
   setCid(a_cid);;
   literal_ = a_literal;
}

void bf::UnitClause::InitWithClause (int a_cid, ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs("[bf::UnitClause::InitWithClause] a_clause is NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if (a_clause->size() > 1)
   {
      fputs ("[bf::UnitClause::InitWithClause] Given clause is not unit.\n", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   setCid(a_cid);;
   if (a_clause->size() == 0)
   {
      literal_ = kNoLiteral;
   }
   else
   {
      literal_ = a_clause->literal(0);
   }
}

void bf::UnitClause::InitWithCapacity(int a_cid, size_t a_capacity)
{
#ifndef NDEBUG
   if (a_capacity != 1)
   {
      fputs("[bf::UnitClause::InitWithCapacity] a_capacity != 1.\n", stderr);
      throw BadParameterException();
   }
#endif
   setCid(a_cid);;
   literal_ = kNoLiteral;
}

bf::UnitClause::~UnitClause() {}

void bf::UnitClause::Clear ()
{
   literal_ = kNoLiteral;
}

size_t bf::UnitClause::size()
{
   return 1;
}

size_t bf::UnitClause::capacity ()
{
   return 1;
}

int bf::UnitClause::literal (size_t lit_index)
{
   return (lit_index == 0 ? literal_ : kNoLiteral);
}

size_t bf::UnitClause::IndexOfLiteral(int lit, size_t)
{
   return (lit == literal_ ? 0 : std::numeric_limits<size_t>::max());
}

void bf::UnitClause::AddLiteral (int lit)
{
#ifndef NDEBUG
   if (literal_ != kNoLiteral)
   {
      fputs ("[bf::UnitClause::AddLiteral] Clause is full.\n", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   literal_ = lit;
}

void bf::UnitClause::SetLiteralsFromClause (ClauseInterface * a_clause)
{
#ifndef NDEBUG
   if (a_clause == NULL)
   {
      fputs("[bf::UnitClause::SetLiteralsFromClause] a_clause is NULL\n", stderr);
      throw UnexpectedNULLException();
   }
   if (a_clause->size() > 1)
   {
      fputs ("[bf::UnitClause::SetLiteralsFromClause] Given clause is not unit.\n", stderr);
      throw bf::IndexOutOfBoundsException();
   }
#endif
   literal_ = a_clause->literal(0);
}

int bf::UnitClause::WatchedLiteralA ()
{
   return literal_;
}

int bf::UnitClause::WatchedLiteralB ()
{
   return kNoLiteral;
}

bool bf::UnitClause::CheckState (LiteralInfo * literal_info,
      bf::ClauseInterface::ClauseState * state)
{
   *state = DryCheckState(literal_info);
   return false;
}

bf::ClauseInterface::ClauseState
bf::UnitClause::DryCheckState (LiteralInfo * literal_info)
{
#ifndef NDEBUG
   if (literal_ == kNoLiteral)
   {
      fputs("[bf::UnitClause::DryCheckState] literal_ == kNoLiteral\n\n", stderr);
      return kUnsatisfied;
   }
   if (literal_info == NULL)
   {
      fputs ("[bf::UnitClause::DryCheckState] literal_info == NULL\n", stderr);
      throw UnexpectedNULLException();
   }
#endif
   switch (literal_info[literal_].value_)
   {
      case kFalseValue:
         return kUnsatisfied;
      case kTrueValue:
         return kSatisfied;
      case kUnknownValue:
      default:
         return kUnit;
   }
}

bool bf::UnitClause::UpdateWatches(LiteralInfo *)
{
   return false;
}
