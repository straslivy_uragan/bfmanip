/*
 * UnitClause.h
 *
 *  Created on: 23.9.2011
 *      Author: su
 */

#ifndef UNITCLAUSE_H_
#define UNITCLAUSE_H_

#include "ClauseInterface.h"

namespace bf {

   /**@brief Class implementing unit clause.
    *
    * In case no literal is present in clause, literal_ equals to
    * kNoLiteral, if this is the case, then some functions may not
    * neccessarily work properly as this is not checked. What always
    * works is adding a literal and intialization procedures, these
    * should be called from the factory functions. Size on the other
    * hand is always considered to be equal to 1. Almost no safety
    * checks are performed, if NDEBUG is defined.
    */
   class UnitClause: public bf::ClauseInterface {
      friend class ClauseInterface;
      private:
         int literal_;
      protected:
         inline UnitClause() 
            : literal_ (kNoLiteral) {}

         virtual void InitWithNFClause (int a_cid, Clause * a_nfclause);
         virtual void InitWithLiterals (int a_cid, int * a_literals,
               size_t a_size);
         virtual void InitWithOneLiteral (int a_cid, int a_literal);
         virtual void InitWithClause (int a_cid, ClauseInterface * a_clause);
         virtual void InitWithCapacity (int a_cid, size_t a_capacity);

      public:
         virtual ~UnitClause();
         virtual void Clear ();
         virtual size_t size ();
         virtual size_t capacity ();
         virtual int literal (size_t lit_index);
         virtual size_t IndexOfLiteral(int lit, size_t from_index);
         virtual void AddLiteral (int lit);
         virtual void SetLiteralsFromClause (ClauseInterface * a_clause);
         virtual int WatchedLiteralA ();
         virtual int WatchedLiteralB ();
         virtual bool CheckState(LiteralInfo * literal_info,
               ClauseState * state);
         virtual ClauseState DryCheckState(LiteralInfo * literal_info);
         virtual bool UpdateWatches(LiteralInfo * literal_info);

      private:
         UnitClause (UnitClause&);
         void operator=(const UnitClause&);

   };

} /* namespace bf */
#endif /* UNITCLAUSE_H_ */
