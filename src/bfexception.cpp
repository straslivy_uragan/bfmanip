//
//  bfexception.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 24.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include "bfexception.h"

const char * bf::IndexOutOfBoundsException::what () const throw ()
{
   return "Index out of bounds";
}

const char * bf::UnexpectedNULLException::what () const throw ()
{
   return "Unexpected NULL";
}

const char * bf::UnknownEnumConstantException::what () const throw ()
{
   return "Unknown enum constant";
}

const char * bf::BadParameterException::what () const throw ()
{
   return "Bad parameter";
}

const char * bf::BadMethodCallException::what () const throw ()
{
   return "Bad method call";
}

const char * bf::InvariantFailedException::what () const throw ()
{
   return "Invariant failed";
}

const char * bf::ConfigFileReadException::what () const throw ()
{
   return "Configuration file could not be read";
}


