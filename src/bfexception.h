//
//  bfexception.h
//  bfmanip++
//
//  Created by Petr Kučera on 24.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef BFEXCEPTION_H
#define BFEXCEPTION_H

/**@file bfexception.h
 * @brief File declaring exceptions for use in functions and classes
 * in bf namespace.
 */

#include <string>
#include <exception>

namespace bf {

   /**@brief An exception which is thrown in case some index gets out
    * of bounds.
    */
   class IndexOutOfBoundsException : public std::exception
   {
      public:
         /**@brief String description of this error.
          */
         virtual const char * what () const throw ();
   };
  
   /**@brief An exception which is thrown in case of an unexpected
    * NULL value.
    */
   class UnexpectedNULLException : public std::exception
   {
      public:
         /**@brief String description of this error.
          */
         virtual const char * what () const throw ();
   };

   /**@brief An exception which is thrown in case that some variable
    * of an enum type has value out of it.
    */
   class UnknownEnumConstantException : public std::exception
   {
     public:
        /**@brief String description of this error.
         */
        virtual const char * what () const throw ();
   };

   /**@brief An exception which is thrown in case of a bad parameter
    * value.
    */
   class BadParameterException : public std::exception
   {
      public:
         /**@brief String description of this error.
          */
         virtual const char * what () const throw ();
   };

   /**@brief An exception called when a method which is not properly
    * implemented is called.
    */
   class BadMethodCallException : public std::exception
   {
      public:
         /**@brief String description of this error.
          */
         virtual const char * what () const throw ();
   };

   /**@brief An exception called when some invariant does not hold.
    */
   class InvariantFailedException : public std::exception
   {
      public:
         /**@brief String description of this error.
          */
         virtual const char * what () const throw ();
   };

   /**@brief An exception called when configuration file could not be
    * read.
    */
   class ConfigFileReadException : public std::exception
   {
      public:
         /**@brief String description of this error.
          */
         virtual const char * what () const throw ();
   };

   class SBException : public std::exception
   {
      std::string mWhatMsg;

      public:

      explicit SBException(const std::string &func, const std::string &msg) throw()
         : mWhatMsg()
      {
         mWhatMsg += "[";
         mWhatMsg += func;
         mWhatMsg += "] ";
         mWhatMsg += msg;
      }
      explicit SBException(const std::string &what_arg) throw()
         : mWhatMsg(what_arg)
      {}
      virtual const char* what() const throw()
      {
         return mWhatMsg.c_str();
      }

      virtual ~SBException() throw() {}
   };

   class NullPointerException : public SBException
   {
      public:
         explicit NullPointerException(const std::string &func, const std::string &msg)
            : SBException(func, msg) {}
         explicit NullPointerException(const std::string& what_arg)
            : SBException(what_arg) {}
   };

   class BadInputException : public SBException
   {
      public:
         explicit BadInputException(const std::string &func, const std::string &msg)
            : SBException(func, msg) {}
         explicit BadInputException(const std::string& what_arg)
            : SBException(what_arg) {}
   };


}

#endif
