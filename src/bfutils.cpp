//
//  bfutils.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 17.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include "bfutils.h"

#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>

int bf::TimevalSubstract (timeval * result, timeval * x, timeval * y)
{
   /* Perform the carry for the later subtraction by updating y. */
   if (x->tv_usec < y->tv_usec) {
      int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
      y->tv_usec -= 1000000 * nsec;
      y->tv_sec += nsec;
   }
   if (x->tv_usec - y->tv_usec > 1000000) {
      int nsec = (x->tv_usec - y->tv_usec) / 1000000;
      y->tv_usec += 1000000 * nsec;
      y->tv_sec -= nsec;
   }

   /* Compute the time remaining to wait.
      tv_usec is certainly positive. */
   result->tv_sec = x->tv_sec - y->tv_sec;
   result->tv_usec = x->tv_usec - y->tv_usec;

   /* Return 1 if result is negative. */
   return x->tv_sec < y->tv_sec;
}

void bf::DumpRelativeResourceUsage(FILE * outfile, rusage * start_usage)
{
   rusage end_usage;
   getrusage (RUSAGE_SELF, &end_usage);
   fputs ("Relative resource usage: \n", outfile);
   timeval tv;
   TimevalSubstract(&tv, &(end_usage.ru_utime), &(start_usage->ru_utime));
   fprintf (outfile, "\tUser time used: %ld.%06ld\n",
         tv.tv_sec, static_cast<long int>(tv.tv_usec));
   timeval total_time;
   total_time.tv_sec = tv.tv_sec;
   total_time.tv_usec = tv.tv_usec;
   TimevalSubstract(&tv, &(end_usage.ru_stime), &(start_usage->ru_stime));
   fprintf (outfile, "\tSystem time used: %ld.%06ld\n",
         tv.tv_sec, static_cast<long int>(tv.tv_usec));
   total_time.tv_sec += tv.tv_sec;
   total_time.tv_usec += tv.tv_usec;
   total_time.tv_sec += total_time.tv_usec / 1000000;
   total_time.tv_usec = total_time.tv_usec % 1000000;
   fprintf (outfile, "\tTotal time used: %ld.%06ld\n",
         total_time.tv_sec, static_cast<long int>(total_time.tv_usec));
   fprintf (outfile, "\tIntegral max resident set size: %ld\n", end_usage.ru_maxrss-start_usage->ru_maxrss);
   // fprintf (outfile, "\tIntegral shared text memory size: %ld\n", end_usage.ru_ixrss-start_usage->ru_ixrss);
   // fprintf (outfile, "\tIntegral unshared data size: %ld\n", end_usage.ru_idrss-start_usage->ru_idrss);
   // fprintf (outfile, "\tIntegral unshared stack size: %ld\n", end_usage.ru_isrss-start_usage->ru_isrss);
   // fprintf (outfile, "\tPage reclaims: %ld\n", end_usage.ru_minflt-start_usage->ru_minflt);
   // fprintf (outfile, "\tPage faults: %ld\n", end_usage.ru_majflt-start_usage->ru_majflt);
   // fprintf (outfile, "\tSwaps: %ld\n", end_usage.ru_nswap-start_usage->ru_nswap);
   // fprintf (outfile, "\tBlock input operations: %ld\n", end_usage.ru_inblock-start_usage->ru_inblock);
   // fprintf (outfile, "\tBlock output operations: %ld\n", end_usage.ru_oublock-start_usage->ru_oublock);
   // fprintf (outfile, "\tMessages sent: %ld\n", end_usage.ru_msgsnd-start_usage->ru_msgsnd);
   // fprintf (outfile, "\tMessages received: %ld\n", end_usage.ru_msgrcv-start_usage->ru_msgrcv);
   // fprintf (outfile, "\tSignals received: %ld\n", end_usage.ru_nsignals-start_usage->ru_nsignals);
   // fprintf (outfile, "\tVoluntary context switches: %ld\n", end_usage.ru_nvcsw-start_usage->ru_nvcsw);
   // fprintf (outfile, "\tInvoluntary context switches: %ld\n", end_usage.ru_nivcsw-start_usage->ru_nivcsw);
   fflush (outfile);
}

void bf::DumpResourceUsage(FILE * outfile)
{
   rusage res_usage;
   getrusage (RUSAGE_SELF, &res_usage);
   fputs ("Resource usage: \n", outfile);
   fprintf (outfile, "User time used: %ld.%06ld\n",
         res_usage.ru_utime.tv_sec, static_cast<long int>(res_usage.ru_utime.tv_usec));
   fprintf (outfile, "System time used: %ld.%06ld\n",
         res_usage.ru_stime.tv_sec, static_cast<long int>(res_usage.ru_stime.tv_usec));
   timeval total_time;
   total_time.tv_sec = res_usage.ru_utime.tv_sec + res_usage.ru_stime.tv_sec;
   total_time.tv_usec = res_usage.ru_utime.tv_usec + res_usage.ru_stime.tv_usec;
   total_time.tv_sec += total_time.tv_usec / 1000000;
   total_time.tv_usec = total_time.tv_usec % 1000000;
   fprintf (outfile, "Total time used: %ld.%06ld\n",
         total_time.tv_sec, static_cast<long int>(total_time.tv_usec));
   fprintf (outfile, "Integral max resident set size: %ld\n", res_usage.ru_maxrss);
   // fprintf (outfile, "Integral shared text memory size: %ld\n", res_usage.ru_ixrss);
   // fprintf (outfile, "Integral unshared data size: %ld\n", res_usage.ru_idrss);
   // fprintf (outfile, "Integral unshared stack size: %ld\n", res_usage.ru_isrss);
   // fprintf (outfile, "Page reclaims: %ld\n", res_usage.ru_minflt);
   // fprintf (outfile, "Page faults: %ld\n", res_usage.ru_majflt);
   // fprintf (outfile, "Swaps: %ld\n", res_usage.ru_nswap);
   // fprintf (outfile, "Block input operations: %ld\n", res_usage.ru_inblock);
   // fprintf (outfile, "Block output operations: %ld\n", res_usage.ru_oublock);
   // fprintf (outfile, "Messages sent: %ld\n", res_usage.ru_msgsnd);
   // fprintf (outfile, "Messages received: %ld\n", res_usage.ru_msgrcv);
   // fprintf (outfile, "Signals received: %ld\n", res_usage.ru_nsignals);
   // fprintf (outfile, "Voluntary context switches: %ld\n", res_usage.ru_nvcsw);
   // fprintf (outfile, "Involuntary context switches: %ld\n", res_usage.ru_nivcsw);
   fflush (outfile);
}
