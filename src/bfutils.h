//
//  bfutils.h
//  bfmanip++
//
//  Created by Petr Kučera on 17.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#ifndef BFUTILS_H
#define BFUTILS_H

#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>

namespace bf {
   /**@brief Defines return values of SAT algorithms. */ 
   enum Result {
      /**@brief Return value with meaning, that CNF is
       * unsatisfiable.
       */
      kUnsatisfiable = 0,
      /**@brief Return value with meaning, that CNF is satisfiable.
       */
      kSatisfiable = 1,
      /**@brief Return value with meaning, that the algorithm
       * failed.
       */
      kFailed = 2,
      /**@brief Return value with meaning, that some function
       * received bad parameters.
       */
      kBadParameters = 3,
      /**@brief Return value with meaning, that the value is not
       * yet determined.
       */
      kUndetermined = 4
   };

   /**@brief Result of comparison. */
   enum ComparisonResult {
      kCompareEqual,
      kCompareBigger,
      kCompareSmaller,
      kIncomparable
   };

   /* Subtract the `struct timeval' values X and Y,
      storing the result in RESULT.
      Return 1 if the difference is negative, otherwise 0.  */
   int TimevalSubstract (timeval * result, timeval * x, timeval * y);
   void DumpRelativeResourceUsage(FILE * outfile, rusage * start_usage);
   void DumpResourceUsage(FILE * outfile);
}

#endif
