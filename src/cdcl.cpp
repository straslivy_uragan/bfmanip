//
//  cdcl.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 30.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <assert.h>

#include "cdcl.h"

bf::CDCL::~CDCL ()
{
   ReleaseDataStructures ();
}

bf::Result bf::CDCL::BuildDataStructures ()
{
   statistics_.Reset ();
   parameters_.next_cid_ = 0;
   bin_clauses_literals_size_ = 0;
   bin_clauses_literals_ = new size_t[2*statistics_.num_variables_];
   // Variable info
   literal_info_ = new LiteralInfo [2*statistics_.num_variables_];
   // Assignment stack
   assignment_stack_ = new size_t[statistics_.num_variables_];
   for (size_t literal = 0; literal < 2*statistics_.num_variables_; ++ literal)
   {
      literal_info_[literal].value_ = kUnknownValue;
   }
   // Clause list and watched literals.
   for (CNF::ElementList::const_iterator element=cnf_.elements().begin();
         element!=cnf_.elements().end();
         ++ element)
   {
      if ((*element)->NumberOfLiterals () == 0)
      {
         return kUnsatisfiable;
      }
      ClauseInterface * clause =
         ClauseInterface::CreateClauseWithNFClause((int)parameters_.next_cid_++,
               *element);
#if 0
      std::string out_string;
      (*element)->DIMACSString(&out_string);
      fputs("a: ", stderr);
      fputs(out_string.c_str(), stderr);
      fputc('\n', stderr);
      fputs("b: ", stderr);
      clause->Print(stderr);
      fputc('\n', stderr);
#endif
      if (clause->size () > statistics_.max_length_original_clause_)
      {
         statistics_.max_length_original_clause_ = clause->size();
      }
      ClauseInterface::ClauseState state;
      clause->CheckState (literal_info_, &state);
      if (clause->size() == 1)
      {
         unit_clauses_.PushBack (clause);
         unary_clauses_.PushBack (clause);
      }
      else if (config_.branching.branching_heuristic == CDCLConfig::Branching::kBranchBinaryWithVSIDS
            && clause->size() == 2)
      {
         binary_clauses_.PushBack (clause);
         if (!literal_info_[clause->WatchedLiteralA()].bin_occurrences_)
         {
            bin_clauses_literals_[bin_clauses_literals_size_++] = clause->WatchedLiteralA();
         }
         ++ literal_info_[clause->WatchedLiteralA()].bin_occurrences_;
         if (!literal_info_[clause->WatchedLiteralB()].bin_occurrences_)
         {
            bin_clauses_literals_[bin_clauses_literals_size_++] = clause->WatchedLiteralB();
         }
         ++ literal_info_[clause->WatchedLiteralB()].bin_occurrences_;
      }
      literal_info_[clause->WatchedLiteralA()].watched_clauses_.
         PushBack(clause);
      if (clause->WatchedLiteralB() != ClauseInterface::kNoLiteral
          && clause->WatchedLiteralB() != clause->WatchedLiteralA())
      {
         literal_info_[clause->WatchedLiteralB()].watched_clauses_.
            PushBack(clause);
      }
      original_clauses_.PushBack (clause);
      if (clause->NeedsBacktrackNotification())
      {
         backtrack_notification_listeners_.PushBack(clause);
      }
   }
   parameters_.clause_length_bound_ = static_cast<int64_t>(
         config_.restarts.init_clause_length_factor
         * statistics_.max_length_original_clause_);
   learned_buf_ = ClauseInterface::CreateBufferClause(statistics_.num_variables_);
   resolvent_ = ClauseInterface::CreateBufferClause(statistics_.num_variables_);
   variable_marks_ = new VariableMarks [statistics_.num_variables_];
   for (size_t var = 0; var < statistics_.num_variables_; ++ var)
   {
      variable_marks_ [var] = kVarUnmarked;
   }
   num_marked_variables_ = 0;
   marked_variables_ = new size_t [statistics_.num_variables_];
   ties_ = new size_t [2*statistics_.num_variables_];

   return kUndetermined;
}

void bf::CDCL::ReleaseDataStructures ()
{
   unit_clauses_.Clear();
   backtrack_notification_listeners_.Clear();
   original_clauses_.DeleteAllElements();
   learned_clauses_.DeleteAllElements();
   conflict_clause_ = NULL;
   if (bin_clauses_literals_ != NULL)
   {
      delete [] bin_clauses_literals_;
   }
   if (assignment_stack_ != NULL)
   {
      delete [] assignment_stack_;
      assignment_stack_ = NULL;
   }
   if (literal_info_ != NULL)
   {
      delete [] literal_info_;
      literal_info_ = NULL;
   }
   ClauseList::Element::DeleteFreeElements ();
   if (learned_buf_ != NULL)
   {
      delete learned_buf_;
      learned_buf_ = NULL;
   }
   if (resolvent_ != NULL)
   {
      delete resolvent_;
      resolvent_ = NULL;
   }
   if (variable_marks_ != NULL)
   {
      delete [] variable_marks_;
      variable_marks_ = NULL;
   }
   if (marked_variables_ != NULL)
   {
      delete [] marked_variables_;
      marked_variables_ = NULL;
   }
   if (ties_ != NULL)
   {
      delete [] ties_;
      ties_ = NULL;
   }
}

bf::Result bf::CDCL::UnitPropagation ()
{
//   fprintf (stderr, "<UnitPropagation level=%d>\n", decision_level_);
   Result result = kUndetermined;
   while (!unit_clauses_.Empty())
   {
      //   unit_clauses_.Print(stderr);
      ClauseInterface * unit_clause = unit_clauses_.PopFront();
      ClauseInterface::ClauseState state;
      int old_watch_a = unit_clause->WatchedLiteralA();
      int old_watch_b = unit_clause->WatchedLiteralB();
      if (unit_clause->CheckState(literal_info_, &state))
      {
         if (unit_clause->WatchedLiteralA() != old_watch_a
               && unit_clause->WatchedLiteralA() != old_watch_b)
         {
            if (unit_clause->size() > parameters_.clause_length_bound_)
            {
               literal_info_[unit_clause->WatchedLiteralA()].
                  watched_clauses_.PushBack(unit_clause);
            }
            else
            {
               literal_info_[unit_clause->WatchedLiteralA()].
                  watched_clauses_.PushFront(unit_clause);
            }
         }
         if (unit_clause->WatchedLiteralB() != old_watch_a
               && unit_clause->WatchedLiteralB() != old_watch_b
               && unit_clause->WatchedLiteralB() != unit_clause->WatchedLiteralA())
         {
           if (unit_clause->size() > parameters_.clause_length_bound_)
            {
               literal_info_[unit_clause->WatchedLiteralB()].
                  watched_clauses_.PushBack(unit_clause);
            }
            else
            {
               literal_info_[unit_clause->WatchedLiteralB()].
                  watched_clauses_.PushFront(unit_clause);
            }
         }
      }
      if (state == ClauseInterface::kUnit)
      {
         int literal = unit_clause->WatchedLiteralA();
         int neg_literal = (literal % 2 ? literal-1 : literal+1);
         assert (literal_info_[literal].value_ == kUnknownValue);
         assert (static_cast<size_t>(neg_literal) < 2*statistics_.num_variables_);
         if (literal_info_[literal].antecedent_ == NULL
               || unit_clause->size() < literal_info_[literal].antecedent_->size())
         {
            literal_info_[literal].antecedent_=unit_clause;
            literal_info_[neg_literal].antecedent_=unit_clause;
         }
         if (literal_info_[literal].value_ == kUnknownValue)
         {
            ++ statistics_.num_unitprop_;
            result = AddValueAssignment(unit_clause->WatchedLiteralA());
            if (result != kUndetermined)
            {
               break;
            }
         }
      }
   }
   //fputs ("</UnitPropagation>\n", stderr);

   return result;
}

bool bf::CDCL::AllVariablesAssigned ()
{
   return (assigned_variables_ >= statistics_.num_variables_);
}

void bf::CDCL::PickBranchingLiteral (size_t * literal)
{
   ++ statistics_.num_decisions_;
   if(config_.branching.branching_heuristic==CDCLConfig::Branching::kBranchWithFixedOrder)
   {
      PickBranchingLiteralWithFixedOrder(literal);
      return;
   }
   if ((unsigned)(rand () % 1000) < config_.branching.chance_of_random_pick)
   {
      // Choose randomly
      ++ statistics_.num_rand_decisions_;
      size_t picked = rand () % (2*(statistics_.num_variables_ - assigned_variables_));
      for (size_t lit = 0; lit < 2*statistics_.num_variables_; ++ lit)
      {
         if (literal_info_[lit].value_ == kUnknownValue)
         {
            if (picked == 0)
            {
               *literal = lit;
               return;
            }
            else
            {
               -- picked;
            }
         }
      }
   }
   else
   {
      *literal= 0;

      switch (config_.branching.branching_heuristic)
      {
         case CDCLConfig::Branching::kBranchBinaryWithVSIDS:
            FillTiesWithBinaryHeuristic ();
            break;
         case CDCLConfig::Branching::kBranchWithVSIDS:
         default:
            FillTiesWithVSIDSHeuristic ();
            break;
      }

      assert (num_ties_ > 0);
      assert (num_ties_ <= 2 * statistics_.num_variables_);
      if (num_ties_ == 1)
      {
         *literal = ties_ [0];
      }
      else
      {
         *literal = ties_ [rand () % num_ties_];
         assert (*literal < 2 * statistics_.num_variables_);
      }
   }
}

   void
bf::CDCL::PickBranchingLiteralWithFixedOrder(size_t *literal)
{
   size_t i=(fixed_order_stack_.empty() ? 0 : fixed_order_stack_.top()+1);
   while(i<config_.branching.literal_order.size())
   {
      if(literal_info_[config_.branching.literal_order[i]].level_<0)
      {
         *literal=config_.branching.literal_order[i];
         fixed_order_stack_.push(i);
         //std::cerr<<"Literal picked: "<<*literal<<std::endl;
         return;
      }
      ++i;
   }
   throw BadInputException("CDCL::PickBranchingLiteralWithFixedOrder",
         "All literals in the fixed order are already assigned.");
}

void bf::CDCL::FillTiesWithVSIDSHeuristic ()
{
   long double max_vsids = -1.0;
   num_ties_ = 0;
   for (size_t lit = 0; lit < 2*statistics_.num_variables_; ++lit)
   {
      if (literal_info_[lit].value_ == kUnknownValue)
      {
         if (literal_info_[lit].vsids_ > max_vsids)
         {
            max_vsids = literal_info_[lit].vsids_;
            ties_ [0] = lit;
            num_ties_ = 1;
         }
         else if (literal_info_[lit].vsids_ == max_vsids)
         {
            ties_ [num_ties_ ++] = lit;
         }
      }
   }
}

void bf::CDCL::FillTiesWithBinaryHeuristic ()
{
   long double max_vsids = -1.0;
   int max_quad = 0;
   num_ties_ = 0;
   for (size_t lit = 0;
         lit < 2 * statistics_.num_variables_; //quad_index < bin_clauses_literals_size_;
         ++lit)
   {
      //int lit = bin_clauses_literals_[quad_index];
      size_t neg_lit = (lit % 2 ? lit - 1 : lit + 1);
      assert (neg_lit < 2*statistics_.num_variables_);
      if (literal_info_[lit].value_ == kUnknownValue)
      {
         if (literal_info_[neg_lit].bin_occurrences_ > max_quad)
         {
            max_quad = literal_info_[neg_lit].bin_occurrences_;
            max_vsids = literal_info_[lit].vsids_;
            ties_ [0] = lit;
            num_ties_ = 1;
         }
         else if (literal_info_[neg_lit].bin_occurrences_ == max_quad)
         {
            if (literal_info_[lit].vsids_ > max_vsids)
            {
               max_vsids = literal_info_[lit].vsids_;
               ties_ [0] = lit;
               num_ties_ = 1;
            }
            else if (literal_info_[lit].vsids_ == max_vsids)
            {
               ties_ [num_ties_ ++] = lit;
            }
         }
      }
   }
   if (max_quad > 0)
   {
      ++ statistics_.num_bin_decisions_;
   }
}

bf::Result bf::CDCL::AddValueAssignment (size_t literal)
{
   Result result = kUndetermined;
   //fprintf (stderr, "<AddValueAssignment literal=%d, level=%d>\n",
   //      (literal%2?-literal/2:literal/2), decision_level_);
   if (literal_info_[literal].value_ != kUnknownValue)
   {
      if (literal_info_[literal].value_ == kFalseValue)
      {
         return kFailed;
      }
      return kUndetermined;
   }
   int negated_literal = (int)(literal % 2 ? literal-1 : literal+1);
   assert (static_cast<size_t>(negated_literal) < 2*statistics_.num_variables_);
   literal_info_[literal].level_ = decision_level_;
   literal_info_[literal].assignment_order_ = assigned_variables_;
   literal_info_[negated_literal].level_ = decision_level_;
   literal_info_[negated_literal].assignment_order_ = assigned_variables_;
   assignment_stack_[assigned_variables_ ++] = literal;
   literal_info_[literal].value_ = kTrueValue;
   literal_info_[negated_literal].value_ = kFalseValue;
   // Watched clauses.
   ClauseInterface * watched_clause = NULL;
   ClauseList watched_list;
   watched_list.Swap (literal_info_[negated_literal].watched_clauses_);
   while (! watched_list.Empty())
   {
      watched_clause = watched_list.PopFront();
      if (watched_clause != NULL
            && (watched_clause->WatchedLiteralA() == negated_literal
            || watched_clause->WatchedLiteralB() == negated_literal))
      {
         int watched_a = watched_clause->WatchedLiteralA();
         int watched_b = watched_clause->WatchedLiteralB();
         ClauseInterface::ClauseState state;
         watched_clause->CheckState(literal_info_, &state);
         int new_watched_a = watched_clause->WatchedLiteralA();
         int new_watched_b = watched_clause->WatchedLiteralB();
         if (new_watched_a == negated_literal
               || (new_watched_a != watched_a
                  && new_watched_a != watched_b))
         {
            if (watched_clause->size() > parameters_.clause_length_bound_)
            {
               literal_info_[new_watched_a].watched_clauses_.
                  PushBack(watched_clause);
            }
            else
            {
               literal_info_[new_watched_a].watched_clauses_.
                  PushFront(watched_clause);
            }
         }
         if (new_watched_b != new_watched_a
            && (new_watched_b == negated_literal
               || (new_watched_b != watched_b
                  && new_watched_b != watched_a)))
         {
            if (watched_clause->size() > parameters_.clause_length_bound_)
            {
               literal_info_[new_watched_b].watched_clauses_.
                  PushBack(watched_clause);
            }
            else
            {
               literal_info_[new_watched_b].watched_clauses_.
                  PushFront(watched_clause);
            }
         }
         if (state == ClauseInterface::kUnsatisfied)
         {
            conflict_clause_ = watched_clause;
            result = kUnsatisfiable;
         }
         else if (state == ClauseInterface::kUnit)
         {
   //         fputs ("Adding unit clause\n", stderr);
   //         watched_clause->Print(stderr);
             assert (literal_info_[watched_clause->WatchedLiteralA()].value_ == kUnknownValue);
             assert (literal_info_[watched_clause->WatchedLiteralB()].value_ == kFalseValue);
            unit_clauses_.PushBack (watched_clause);
         }
         else if (config_.branching.branching_heuristic == CDCLConfig::Branching::kBranchBinaryWithVSIDS
               && state == ClauseInterface::kBinary)
         {
            if(!literal_info_[watched_clause->WatchedLiteralA()].bin_occurrences_)
            {
               bin_clauses_literals_[bin_clauses_literals_size_++]
                  = watched_clause->WatchedLiteralA();
            }
            ++ literal_info_[watched_clause->WatchedLiteralA()].bin_occurrences_;
            if(!literal_info_[watched_clause->WatchedLiteralB()].bin_occurrences_)
            {
               bin_clauses_literals_[bin_clauses_literals_size_++]
                  = watched_clause->WatchedLiteralB();
            }
            ++ literal_info_[watched_clause->WatchedLiteralB()].bin_occurrences_;
         }
      }
   }
   return result;
}

void bf::CDCL::ConflictAnalysis (int * bt_level)
{
   //fputs ("[bf::CDCL::ConflictAnalysis]\n", stderr);
   ++ statistics_.num_conflicts_;
   // We use clause learning with stopping at the first UIP
   // Construction of a clause to be learned.
   if (conflict_clause_ == NULL)
   {
      fprintf (stderr, "[bf::CDCL::ConflictAnalysis] ConflictAnalysis called when conflict_clause_ is NULL at level %d\n", decision_level_);
      return;
   }
   /*fputs ("Conflict clause:\n", stderr);
   conflict_clause_->Print(stderr);
   fputc ('\n', stderr);*/
   /*
   if(conflict_clause_->DryCheckState(literal_info_) != ClauseInterface::kUnsatisfied)
   {
      fputs ("Conflict clause:\n", stderr);
      conflict_clause_->Print(stderr);
      fputc ('\n', stderr);
   }
   */
   assert (conflict_clause_->DryCheckState(literal_info_) == ClauseInterface::kUnsatisfied);
   learned_buf_->SetLiteralsFromClause(conflict_clause_);
   assert (learned_buf_->DryCheckState(literal_info_) == ClauseInterface::kUnsatisfied);
   int implied_literal = ClauseInterface::kNoLiteral;
   while (! IsUIP (learned_buf_, &implied_literal))
   {
      //learned_buf_->Print(stderr);
      assert (implied_literal != ClauseInterface::kNoLiteral);
      assert (learned_buf_->DryCheckState(literal_info_) == ClauseInterface::kUnsatisfied);
      //literal_info_[implied_literal].antecedent_->Print (stderr);
      if (!learned_buf_->Resolve(literal_info_[implied_literal].antecedent_,
               resolvent_))
      {
         fputs ("[bf::CDCL::ConflictAnalysis] Resolution failed\n", stderr);
         fputs ("[...] First resolvent: ", stderr);
         learned_buf_->Print(stderr);
         fputc ('\n', stderr);
         fputs ("[...] Second resolvent: ", stderr);
         literal_info_[implied_literal].antecedent_->Print(stderr);
         fputc ('\n', stderr);
         fprintf (stderr, "Implied literal: %d\n",
               (implied_literal%2 ? -implied_literal/2 : implied_literal/2));
         fprintf (stderr, "Value of implied_literal: %d negation: %d\n",
                  static_cast<int>(literal_info_[implied_literal].value_),
                      static_cast<int>(literal_info_[(implied_literal%2?implied_literal-1:implied_literal+1)].value_));
         abort();
      }
      ClauseInterface * sw_clause = learned_buf_;
      learned_buf_ = resolvent_;
      resolvent_ = sw_clause;
   }

   // minimize learned clause
   // Check, if it is neccessary to learn this clause, i.e. if there
   // is not a clause subsuming the learned one.
   //ClauseInterface * learned_clause = NULL;
   //if (ClauseMinimization ())
   //{
   //fprintf(stderr, "Learned buf before minimization (uip: %lu)\n", implied_literal);
   //learned_buf_->Print(stderr);
   ClauseMinimization ();
   //learned_buf_->Print(stderr);
   if (learned_buf_->size() == 0)
   {
      *bt_level = -1;
      return;
   }
   ClauseInterface * learned_clause =
      ClauseInterface::CreateClauseWithClause((int)parameters_.next_cid_++,
            learned_buf_);

// Uncomment the following lines to list learned clauses to stderr (prefixed with "L: ", brackets at the end contain current watched literals)!
   /*
   fputs ("L: ", stderr);
   conflict_clause_->Print(stderr);
   fputc ('\n', stderr);
   */

   if (learned_clause->size() > parameters_.clause_length_bound_)
   {
      learned_clauses_.PushBack(learned_clause);
      if (learned_clause->NeedsBacktrackNotification())
      {
         backtrack_notification_listeners_.PushBack(learned_clause);
      }
   }
   else
   {
      learned_clauses_.PushFront(learned_clause);
      if (learned_clause->NeedsBacktrackNotification())
      {
         backtrack_notification_listeners_.PushFront(learned_clause);
      }
   }

   if (learned_clause->size() == 1)
   {
      unary_clauses_.PushBack (learned_clause);
   }
   else if (config_.branching.branching_heuristic == CDCLConfig::Branching::kBranchBinaryWithVSIDS
         && learned_clause->size () == 2)
   {
      binary_clauses_.PushBack (learned_clause);
   }
   if (learned_clause->size() > statistics_.max_length_learned_clause_)
   {
      statistics_.max_length_learned_clause_ = learned_clause->size();
   }
   ++ statistics_.num_learned_clauses_;
   size_t uip_index = 0;
   while (uip_index < learned_clause->size()
         && literal_info_[learned_clause->literal(uip_index)].level_ < decision_level_)
   {
      ++ uip_index;
   }
   assert (uip_index < learned_clause->size());

   /*fputs ("Learned clause:\n", stderr);
   learned_clause->Print(stderr);
   for (int ll = 0; ll < learned_clause->size(); ++ll)
   {
      int lit = learned_clause->literal(ll);
      fprintf (stderr, "[li:%d, as:%d, le:%d, ant:%s]\n",
            (lit%2?-lit/2:lit/2), literal_info_[lit].value_,
            literal_info_[lit].level_,
            (literal_info_[lit].antecedent_ == NULL
             ? "NULL" : "not NULL"));
   }*/
   /*}
   else
   {
      learned_clause = learned_buf_;
   }*/

   // Determining backtrack level
   // Also literal statistics (number of occurrences) are updated
   // during the process.
   size_t watched_literal_b = uip_index;
   if (learned_clause->size() == 1)
   {
      /* If we learned unit literal, then it can be propagated right
       * from the beginning, i.e. level 0, in an unlikely case, it
       * already has been assigned (the opposite) value at level 0, we
       * can see, that the formula must be unsatisfiable. (I think,
       * this should not happen, but I am too lazy to prove it
       * formally.)
       */
      size_t literal = learned_clause->literal(0);
      if (literal_info_[literal].level_ == 0)
      {
         *bt_level = -1;
      }
      else
      {
         *bt_level = 0;
      }
   }
   else
   {
      *bt_level = -1;
      for (size_t literal_ind = 0;
            literal_ind < learned_clause->size();
            ++literal_ind)
      {
         size_t literal = learned_clause->literal(literal_ind);
         if (literal_info_[literal].level_ < decision_level_
               && literal_info_[literal].level_ > *bt_level)
         {
            *bt_level = literal_info_[literal].level_;
            watched_literal_b = literal_ind;
         }
         literal_info_[literal].vsids_ += parameters_.vsids_add_;
      }
      parameters_.vsids_add_ *= config_.branching.vsids_bump;
   }
   // TODO: Watched literals should be set to UIP and the one
   // determining backtrack level.
   unit_clauses_.PushBack(learned_clause);
   learned_clause->SetWatchIndices (uip_index, watched_literal_b);
   if (learned_clause->size() > parameters_.clause_length_bound_)
   {
      literal_info_[learned_clause->WatchedLiteralA()].watched_clauses_.
         PushBack(learned_clause);
   }
   else
   {
      literal_info_[learned_clause->WatchedLiteralA()].watched_clauses_.
         PushFront(learned_clause);
   }
   if (learned_clause->WatchedLiteralB() != ClauseInterface::kNoLiteral
         && learned_clause->WatchedLiteralB() != learned_clause->WatchedLiteralA())
   {
      if (learned_clause->size() > parameters_.clause_length_bound_)
      {
         literal_info_[learned_clause->WatchedLiteralB()].watched_clauses_.
            PushBack(learned_clause);
      }
      else
      {
         literal_info_[learned_clause->WatchedLiteralB()].watched_clauses_.
            PushFront(learned_clause);
      }
   }
   //fprintf (stderr, "Level: %d, bt_level: %d\n", decision_level_, *bt_level);
   conflict_clause_ = NULL;
}

bool bf::CDCL::ClauseMinimization ()
{
   if (learned_buf_->size() > config_.learning.clause_minimization_threshold)
   {
      switch (config_.learning.minimization_method)
      {
         case CDCLConfig::Learning::kNoMinimization:
            break;
         case CDCLConfig::Learning::kMinimizeLocallyByResolution:
            return ClauseMinimizationLocallyByResolution ();
         case CDCLConfig::Learning::kMinimizeLocallyByMarking:
            return ClauseMinimizationLocallyByMarking ();
         case CDCLConfig::Learning::kMinimizeGlobally:
            return ClauseMinimizationGlobally ();
         default:
            break;
      }
   }
   return true;
}

bool bf::CDCL::ClauseMinimizationLocallyByResolution ()
{
   size_t * antecedent_candidates = new size_t [statistics_.num_variables_];
   size_t ant_size = 0;
   for (size_t literal_index = 0;
         literal_index < learned_buf_->size();
         ++ literal_index)
   {
      size_t literal = learned_buf_->literal(literal_index);
      if (literal_info_[literal].level_>0)
      {
         //        learned_storage->AddLiteralWithoutCheck(literal);
         if (literal_info_[literal].antecedent_ != NULL
               && literal_info_[literal].level_ < decision_level_)
         {
            antecedent_candidates[ant_size++] = literal;
         }
      }
   }
   while (ant_size > 0)
   {
      if (learned_buf_->
            SubsumingResolve(literal_info_[antecedent_candidates[--ant_size]].antecedent_,
               resolvent_))
      {
         ClauseInterface * swap_clause = learned_buf_;
         learned_buf_ = resolvent_;
         resolvent_ = swap_clause;
      }
   }
   delete[] antecedent_candidates;
   return true;
}

bool bf::CDCL::ClauseMinimizationLocallyByMarking ()
{
   // We shall use resolvent_ as a buffer to store new minimized clause.
   resolvent_->Clear ();
   // Mark variables.
   for (size_t literal_index = 0;
         literal_index < learned_buf_->size();
         ++ literal_index)
   {
      size_t literal = learned_buf_->literal(literal_index);
      variable_marks_ [literal / 2] = kVarInClause;
   }
   for (size_t literal_index = 0;
         literal_index < learned_buf_->size();
         ++ literal_index)
   {
      // Check whether all variables in antecedent are marked.
      size_t literal = learned_buf_->literal(literal_index);
      ClauseInterface * antecedent = literal_info_[literal].antecedent_;
      if (antecedent == NULL
            || literal_info_[literal].level_ == decision_level_
            )
      {
         resolvent_->AddLiteral((int)literal);
      }
      else if (literal_info_[literal].level_ == 0)
      {
         variable_marks_ [literal / 2] = kVarUnmarked;
      }
      else
      {
         bool all_marked = true;
         for (size_t literal_ind = 0;
               all_marked && literal_ind < antecedent->size();
               ++ literal_ind)
         {
            if (variable_marks_[antecedent->literal(literal_ind)/2]
                  == kVarUnmarked)
            {
               all_marked = false;
            }
         }
         if (all_marked)
         {
            variable_marks_[literal/2] = kVarUnmarked;
         }
         else
         {
            resolvent_->AddLiteral((int)literal);
         }
      }
   }
   ClauseInterface * swap = resolvent_;
   resolvent_ = learned_buf_;
   learned_buf_ = swap;
   // Unmark variables.
   for (size_t literal_index = 0;
         literal_index < learned_buf_->size();
         ++ literal_index)
   {
      size_t literal = learned_buf_->literal(literal_index);
      variable_marks_ [literal / 2] = kVarUnmarked;
   }
   return true;
}

void bf::CDCL::LearnedClauseMarking(size_t variable)
{
   //fputs ("<LearnedClauseMarking>\n", stderr);
   ClauseInterface * antecedent = literal_info_[variable*2].antecedent_;
   assert (variable_marks_[variable] == kVarUnmarked
         || variable_marks_[variable] == kVarInClause);
   if (antecedent == NULL)
   {
      if (variable_marks_[variable] == kVarInClause)
      {
         variable_marks_[variable] = kVarUnremovable;
      }
      else
      {
         variable_marks_[variable] = kVarCannotBeResolvedOut;
      }
   }
   else
   {
      bool all_pathes_ok = true;
      for (size_t literal_ind = 0;
            literal_ind < antecedent->size();
            ++ literal_ind)
      {
         size_t var = antecedent->literal(literal_ind)/2;
         if (var == variable)
         {
            continue;
         }
         if (variable_marks_[var] == kVarUnmarked
               || variable_marks_[var] == kVarInClause)
         {
            LearnedClauseMarking (var);
         }
         if (variable_marks_[var] == kVarCannotBeResolvedOut)
         {
            all_pathes_ok = false;
         }
      }
      if (all_pathes_ok)
      {
         if (variable_marks_[variable] == kVarUnmarked)
         {
            variable_marks_[variable] = kVarCanBeResolvedOut;
            marked_variables_ [num_marked_variables_ ++] = variable;
         }
         else
         {
            variable_marks_[variable] = kVarRemovable;
         }
      }
      else
      {
         if (variable_marks_[variable] == kVarUnmarked)
         {
            variable_marks_[variable] = kVarCannotBeResolvedOut;
            marked_variables_ [num_marked_variables_ ++] = variable;
         }
         else
         {
            variable_marks_[variable] = kVarUnremovable;
         }
      }
   }
//   fputs ("</LearnedClauseMarking>\n", stderr);
}

bool bf::CDCL::ClauseMinimizationGlobally ()
{
 //  fputs ("<ClauseMinimizationGlobally>\n", stderr);
   // We assume, that all variables are unmarked at the beginning,
   // We will make sure at the end, it is still true.
   // Mark variables in clause.
   for (size_t literal_ind = 0;
         literal_ind < learned_buf_->size();
         ++ literal_ind)
   {
      size_t literal = learned_buf_->literal(literal_ind);
      marked_variables_[num_marked_variables_ ++] = literal/2;
      if (literal_info_[literal].level_ == decision_level_)
      {
         variable_marks_ [literal/2] = kVarUnremovable;
      }
      else if (literal_info_[literal].level_ == 0)
      {
         variable_marks_[literal/2] = kVarRemovable;
      }
      else
      {
         variable_marks_ [literal/2] = kVarInClause;
      }
   }
   // Start global marking procedure for every literal.
   for (size_t literal_ind = 0;
         literal_ind < learned_buf_->size();
         ++ literal_ind)
   /*for (size_t literal_ind = learned_buf_->size();
         literal_ind > 0;
         -- literal_ind)*/
   {
      size_t literal = learned_buf_->literal(literal_ind);
      if (variable_marks_ [literal/2] == kVarInClause)
      {
         LearnedClauseMarking (literal/2);
      }
   }
   // Copy the remaining literals to resolvent_
   resolvent_->Clear ();
   for (size_t literal_ind = 0;
         literal_ind < learned_buf_->size();
         ++ literal_ind)
   {
      size_t literal = learned_buf_->literal(literal_ind);
      if (variable_marks_ [literal/2] == kVarUnremovable)
      {
         resolvent_->AddLiteral((int)literal);
      }
   }
   // And swap resolvent with learned buf.
   ClauseInterface * swap = resolvent_;
   resolvent_ = learned_buf_;
   learned_buf_ = swap;
   // Remove marks, right now it goes over all variables,
   // later we can remember touched variables somewhere and remove
   // marks only on these variables.
   while (num_marked_variables_ > 0)
   {
      variable_marks_[marked_variables_[--num_marked_variables_]]
         = kVarUnmarked;
   }
//   fputs ("</ClauseMinimizationGlobally>\n", stderr);
   return true;
}

void bf::CDCL::Backtrack (int bt_level)
{
   // Remove assignments up to bt_level
   // Leave all assignments on bt_level or earlier untouched.
   while (assigned_variables_ > 0
         && literal_info_[assignment_stack_[assigned_variables_-1]].level_
         > bt_level)
   {
      size_t variable = assignment_stack_[--assigned_variables_]/2;
      literal_info_[2*variable].value_ = kUnknownValue;
      literal_info_[2*variable+1].value_ = kUnknownValue;
      literal_info_[2*variable].level_ = -1;
      literal_info_[2*variable].assignment_order_ = -1;
      literal_info_[2*variable+1].level_ = -1;
      literal_info_[2*variable+1].assignment_order_ = -1;
      literal_info_[2*variable].antecedent_ = NULL;
      literal_info_[2*variable+1].antecedent_ = NULL;
   }
   if (config_.branching.branching_heuristic == CDCLConfig::Branching::kBranchBinaryWithVSIDS)
   {
      for (size_t quad_index = 0; quad_index < bin_clauses_literals_size_; ++quad_index)
      {
         literal_info_[bin_clauses_literals_[quad_index]].bin_occurrences_ = 0;
      }
      bin_clauses_literals_size_ = 0;
   }
   // Notify backtrack notification listeners.
   for (ClauseList::Element * element
         = backtrack_notification_listeners_.head();
         element != NULL;
         element = element->next())
   {
      ClauseInterface * clause = element->clause();
      clause->Backtrack(bt_level);
   }
   // unit_clauses_.Clear();
   while(!fixed_order_stack_.empty()
         && literal_info_[config_.branching.literal_order[fixed_order_stack_.top()]].level_<0)
   {
      fixed_order_stack_.pop();
   }
}

void bf::CDCL::Restart ()
{
   decision_level_ = 0;
   assigned_variables_ = 0;
   //parameters_.vsids_add_ = 1.0;
   for (size_t literal = 0; literal < 2*statistics_.num_variables_; ++ literal)
   {
      literal_info_[literal].value_ = kUnknownValue;
      literal_info_[literal].level_ = -1;
      literal_info_[literal].assignment_order_ = -1;
      literal_info_[literal].bin_occurrences_ = 0;
      //literal_info_[literal].vsids_ = 0;
      literal_info_[literal].antecedent_ = NULL;
      // Remove long clauses from watched ones.
      // Long clauses are stored at the back of the list.
      while (literal_info_[literal].watched_clauses_.tail() != NULL
            && literal_info_[literal].watched_clauses_.tail()
            ->clause()->size() > parameters_.clause_length_bound_)
      {
         literal_info_[literal].watched_clauses_.PopBack();
      }
   }
   // Remove long clauses from learned clauses
   while (backtrack_notification_listeners_.tail() != NULL
         && backtrack_notification_listeners_.tail()
         ->clause()->size() > parameters_.clause_length_bound_
         && backtrack_notification_listeners_.tail()->clause()->cid()
         >= (int)statistics_.num_original_clauses_)
   {
      backtrack_notification_listeners_.PopBack();
   }
   if(config_.restarts.prune_learned_clauses)
   {
      while (learned_clauses_.tail() != NULL
            && learned_clauses_.tail()->clause()->size()
            > parameters_.clause_length_bound_)
      {
         ClauseInterface * clause = learned_clauses_.PopBack();
         ++ statistics_.num_deleted_clauses_;
         delete clause;
      }
   }
   while(!fixed_order_stack_.empty())
   {
      fixed_order_stack_.pop();
   }
   ++ statistics_.num_restarts_;
   PrintShortStatistics(stdout, (statistics_.num_restarts_ == 1), false);

   // Notify about backtrack
   for (ClauseList::Element * element
         = backtrack_notification_listeners_.head();
         element != NULL;
         element = element->next())
   {
      ClauseInterface * clause = element->clause();
      clause->Backtrack(0);
   }

   // Collect unit and binary clauses
   unit_clauses_.Clear();
   bin_clauses_literals_size_ = 0;
   for (ClauseList::Element * element = unary_clauses_.head();
         element != NULL;
         element = element->next())
   {
      ClauseInterface * clause = element->clause();
      unit_clauses_.PushBack (clause);
   }
   if (config_.branching.branching_heuristic == CDCLConfig::Branching::kBranchBinaryWithVSIDS)
   {
      for (ClauseList::Element * element = binary_clauses_.head();
            element != NULL;
            element = element->next())
      {
         ClauseInterface * clause = element->clause();
         if (!literal_info_[clause->WatchedLiteralA()].bin_occurrences_)
         {
            bin_clauses_literals_[bin_clauses_literals_size_++] = clause->WatchedLiteralA();
         }
         ++ literal_info_[clause->WatchedLiteralA()].bin_occurrences_;
         if (!literal_info_[clause->WatchedLiteralB()].bin_occurrences_)
         {
            bin_clauses_literals_[bin_clauses_literals_size_++] = clause->WatchedLiteralB();
         }
         ++ literal_info_[clause->WatchedLiteralB()].bin_occurrences_;
      }
   }
   size_t new_clause_length_bound_ = static_cast<int64_t>(
            parameters_.clause_length_bound_ * config_.restarts.clause_length_bump);
   if (new_clause_length_bound_ <= parameters_.clause_length_bound_)
   {
      ++ new_clause_length_bound_;
   }
   parameters_.clause_length_bound_ = new_clause_length_bound_;
}

bool bf::CDCL::IsUIP (bf::ClauseInterface * clause, int * implied_literal)
{
   if (clause == NULL)
   {
      return false;
   }
   //size_t uip_lit = 0;
   *implied_literal = ClauseInterface::kNoLiteral;
   int num_level_lit = 0;
   for (size_t literal_ind = 0;
         literal_ind < clause->size() && num_level_lit < 2;
         ++literal_ind)
   {
      int literal = clause->literal(literal_ind);
      if (literal_info_[literal].level_ == decision_level_)
      {
         //uip_lit = literal;
         if (literal_info_[literal].antecedent_ != NULL
               && (*implied_literal == ClauseInterface::kNoLiteral
                  || literal_info_[*implied_literal].assignment_order_
                  < literal_info_[literal].assignment_order_))
         {
            *implied_literal = literal;
         }
         ++ num_level_lit;
      }
   }
   return (num_level_lit == 1);
}

void bf::CDCL::Decay ()
{
   // Find maximum vsids
   long double max_vsids = 0;
   for (size_t literal = 0; literal < 2*statistics_.num_variables_; ++ literal)
   {
      if (literal_info_[literal].vsids_ > max_vsids)
      {
         max_vsids = literal_info_[literal].vsids_;
      }
   }
   for (size_t literal = 0; literal < 2*statistics_.num_variables_; ++ literal)
   {
      literal_info_[literal].vsids_ /= max_vsids;
   }
   parameters_.vsids_add_ = 1.0;
}

bf::Result bf::CDCL::Run (bf::Input * input)
{
   if(config_.random.set_seed)
   {
      std::cout<<"Setting seed to given value. "<<std::endl;
   }
   else
   {
      std::cout<<"Setting seed to current time. "<<std::endl;
      config_.random.seed=(unsigned)time(NULL);
   }
   std::cout<<"Using seed: "<<config_.random.seed<<std::endl;
   srand(config_.random.seed);
   Result result = BuildDataStructures ();
   puts("Data structures built");
   if (result != kUndetermined)
   {
      return result;
   }
   result = UnitPropagation ();
   int64_t conflict_bound = config_.restarts.first_restart_conflicts;
   while (result == kUndetermined)
   {
      result = SearchWithConflictBound (conflict_bound);
      if (result == kUndetermined)
      {
         Restart ();
      }
      conflict_bound = static_cast<int64_t>(
            conflict_bound * config_.restarts.restart_conflict_bump);
   }
   ++ statistics_.num_restarts_;
   PrintShortStatistics(stdout, (statistics_.num_restarts_ == 1), true);

   if (result == kSatisfiable && input != NULL)
   {
      input->assign(statistics_.num_variables_, false);
      for (size_t var = 0; var < statistics_.num_variables_; ++ var)
      {
         (* input)[var] = (literal_info_[2*var].value_ == kTrueValue);
      }
   }
   return result;
}

bf::Result bf::CDCL::SearchWithConflictBound (int64_t conflict_bound)
{
   Result result = UnitPropagation ();
   if (result == kUndetermined)
   {
      decision_level_ = 0;
      size_t literal = 0;
      while (! AllVariablesAssigned () && conflict_bound > 0)
      {
         if (parameters_.vsids_add_ >= config_.branching.max_vsids_add)
         {
            Decay ();
         }
         PickBranchingLiteral(&literal);
         ++ decision_level_;
         if (decision_level_ > statistics_.max_level_)
         {
            statistics_.max_level_ = decision_level_;
         }
         result = AddValueAssignment (literal);
         if (result == kUndetermined)
         {
            result = UnitPropagation ();
         }
         if (result == kFailed)
         {
            return result;
         }
         /* A conflict clause after backtrack becomes unit clause,
          * which has to be assigned the right value using unit
          * propagation. If this unit propagation leads to another
          * conflict, it must be analysed. This continues, until no
          * conflict arises from unit propagation.
          */
         while (result != kUndetermined)
         {
            assert(result == kUnsatisfiable);
            int new_decision_level = 0;
            ConflictAnalysis (&new_decision_level);
            -- conflict_bound;
            if (new_decision_level < 0)
            {
               return kUnsatisfiable;
            }
            Backtrack (new_decision_level);
            decision_level_ = new_decision_level;
            result = UnitPropagation();
         }
      }
      if (AllVariablesAssigned ())
      {
         result = kSatisfiable;
      }
   }
   return result;
}

void bf::CDCL::PrintShortStatistics (FILE * file,
      bool print_header, bool print_footer)
{
   if (file == NULL)
   {
      file = stdout;
   }
   if (print_header)
   {
      fputs ("┌───────┬─────────┬─────────┬───────┬───────┬──────┬──────┐\n", file);
      fputs ("│restart│conflicts│decisions│learned│deleted│mlevel│cllenb│\n", file);
      fputs ("├───────┼─────────┼─────────┼───────┼───────┼──────┼──────┤\n", file);
   }
   fprintf (file, "│%7ld│%9ld│%9ld│%7lu│%7lu│%6lu│%6lu│\n",
         static_cast<long>(statistics_.num_restarts_),
         static_cast<long>(statistics_.num_conflicts_),
         static_cast<long>(statistics_.num_decisions_),
         static_cast<unsigned long>(statistics_.num_learned_clauses_),
         static_cast<unsigned long>(statistics_.num_deleted_clauses_),
         static_cast<unsigned long>(statistics_.max_level_),
         parameters_.clause_length_bound_);
   if (print_footer)
   {
      fputs ("└───────┴─────────┴─────────┴───────┴───────┴──────┴──────┘\n", file);
   }
   fflush(file);
}

void bf::CDCL::PrintLongStatistics (FILE * file)
{
   if (file == NULL)
   {
      file = stdout;
   }
   fprintf (file, "Number of original variables: %lu\n", statistics_.num_variables_);
   fprintf (file, "Number of original clauses: %lu\n", statistics_.num_original_clauses_);
   fprintf (file, "Max level reached: %d\n", max_level());
   fprintf (file, "Number of learned clauses: %lu\n", num_learned_clauses());
   fprintf (file, "Max length of original clause: %lu\n",
         static_cast<unsigned long>(max_length_original_clause()));
   fprintf (file, "Max length of learned clause: %lu\n",
         static_cast<unsigned long>(max_length_learned_clause()));
   fprintf (file, "Number of decisions: %d (quadratic: %d, random: %d)\n",
         num_decisions(), num_bin_decisions(), num_rand_decisions ());
   fprintf (file, "Number of unit propagations: %d\n", num_unitprop());
   fprintf (file, "Number of restarts: %ld\n", static_cast<long>(num_restarts ()));
   fflush(file);
}

