//
//  cdcl.h
//  bfmanip++
//
//  Created by Petr Kučera on 30.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
/**@file cdcl.h
  @brief Declaration of CDCL class implementing a CDCL sat solver.
 */

/*TODO:
 * - Note, that if in Run, we find learn a clause (a_1 v a_2 v ... v
 * a_k) in ConflictAnalysis, backtrack, so that a_k remains only
 * unassigned variable, and it satisfying leads to conflict, then when
 * we learn a clause then, we can resolve it and remove a_k, thus we
 * actually can learn only one clause in the inner cycle, not as many
 * as number of repeats). Although it would be probably better to
 * combine it with assignment stack shrinking.
 * - Add comments
 * - Assignment stack shrinking, conflict clause minimization.
 * - Or another way of controlling size of learned clause.
 * - Learned clause deleting strategy.
 * - Restarts.
 */
#ifndef CDCL_H
#define CDCL_H

#include "input.h"
#include "normalform.h"
#include "bfutils.h"
#include "ClauseInterface.h"
#include "ClauseList.h"
#include "LiteralInfo.h"
#include "cdclconfig.h"
#include <stack>

namespace bf {
   /**@brief Class encapsulating an instance of CDCL kind of a sat
    * solver together with neccessary data structures.
    *
    * An instance of CDCL class is associated with a cnf object of
    * class CNF. The actual run of the sat solver is implemented via
    * CDCL::Run(Input*) function.
    */
   class CDCL {
      public:
         enum VariableMarks {
            kVarUnmarked,
            kVarInClause,
            kVarUnremovable,
            kVarRemovable,
            kVarCanBeResolvedOut,
            kVarCannotBeResolvedOut
         };

         /**@brief Structure containing various statistics of the
          * search.
          */
         struct Statistics {
            /**@brief Number of variables in the input CNF.
             */
            size_t num_variables_;
            /**@brief Number of clauses in the input CNF.
             */
            size_t num_original_clauses_;
            /**@brief Number of learned clauses.
             */
            size_t num_learned_clauses_;
            size_t num_deleted_clauses_;
            /**@brief Number of decisions, i.e. number of literal
             * picking.
             *
             * It is increased during each invocation of
             * #PickBranchingLiteral method.
             */
            int num_decisions_;
            /**@brief Number of literals picked based on binary clause
             * heuristic.
             *
             * See #PickBranchingLiteral method description for more
             * details.
             */
            int num_bin_decisions_;
            int num_rand_decisions_;
            int num_unitprop_;
            /**@brief Number of implied assignments during unit
             * propagation.
             */
            int64_t num_conflicts_;
            int64_t num_restarts_;
            size_t max_length_original_clause_;
            /**@brief Length of the longest learned clause.
             */
            size_t max_length_learned_clause_;
            /**@brief Maximum level reached during search for satisfying
             * assignment.
             */
            int max_level_;

            Statistics (size_t n_vars, size_t n_clauses) :
               num_variables_ (n_vars),
               num_original_clauses_ (n_clauses),
               num_learned_clauses_ (0),
               num_deleted_clauses_ (0),
               num_decisions_ (0),
               num_bin_decisions_ (0),
               num_rand_decisions_ (0),
               num_unitprop_ (0),
               num_conflicts_ (0),
               num_restarts_ (0),
               max_length_original_clause_ (0),
               max_length_learned_clause_ (0),
               max_level_ (0) {}

            void Reset ();

            private:
            Statistics ();
            Statistics (Statistics&);
            void operator=(Statistics&);
         };

         struct Parameters {
            /**@brief Next clause identification number to be used.
             */
            int64_t next_cid_;
            /**@brief Value used to increase VSIDS of a literal in a
             * learned clause.
             *
             * For more detail about VSIDS usage see #ConflictAnalysis,
             * #PickBranchingLiteral, #Decay, and #Run method
             * descriptions.
             */
            long double vsids_add_;

            size_t clause_length_bound_;

            Parameters () :
               next_cid_ (0),
               vsids_add_ (1.0),
               clause_length_bound_ (0) {}

            void Reset ();
         };

      private:
         // Member variables.
         /**@brief Reference to associated CNF.
          */
         const CNF& cnf_;

         // Statistics and state
         /**@brief Current decision level.
          */
         int decision_level_;

         Statistics statistics_;
         Parameters parameters_;
         CDCLConfig config_;

         /**@brief Array with information about each literal.
          */
         LiteralInfo * literal_info_;
         /**@brief Stack of literal assignments.
          *
          * When a literal is assigned a value, it is put at the end
          * of this stack, so that during backtrack we can reset this
          * assignment.
          */
         size_t * assignment_stack_;
         /**@brief Number of variables with assigned value.
          *
          * It serves two purposes, for one it helps determine,
          * whether all variables have assigned values and search can
          * thus stop, it also determines the size of used part of
          * #assignment_stack_.
          */
         size_t assigned_variables_;
         size_t * ties_;
         size_t num_ties_;
         std::stack<size_t> fixed_order_stack_;
         /**@brief Variable marks used for clause minimization to mark
          * variables in minimized clause.
          *
          * Initially all marks are set to false.
          */
         VariableMarks * variable_marks_;
         size_t * marked_variables_;
         size_t num_marked_variables_;
         /**@brief Literals which have been found in a binary clause.
          *
          * When value LiteralInfo::bin_occurrences_ changes from 0 to
          * 1 for some literal, this literal is put into this array.
          * This happens, when by assigning a value to a literal, a
          * binary clause is produced. Then
          * LiteralInfo::bin_occurrences_ is increased for this
          * literal. See also #AddValueAssignment and
          * #PickBranchingLiteral method descriptions.
          */
         size_t * bin_clauses_literals_;
         /**@brief Number of literals in binary clauses.
          *
          * Contains number of literals stored in #assigned_variables_
          * array.
          */
         size_t bin_clauses_literals_size_;

         /**@brief List with original clauses.
          *
          * First there are the original clauses, learned clauses are
          * added after them. The original clauses can be recognized
          * by observing, that they have cid_ < num_original_clauses_.
          */
         ClauseList original_clauses_;
         ClauseList learned_clauses_;
         /**@brief List with unit clauses.
          *
          * When a unit clause is produced during variable assignment,
          * it is added into this list, which is then used during unit
          * propagation. See #AddValueAssignment and #UnitPropagation
          * method descriptions.
          */
         ClauseList unit_clauses_;

         /**@brief List of clauses which really have size one.
          */
         ClauseList unary_clauses_;
         /**@brief List of clauses which really have size two.
          *
          * This list is managed only if pick heuristic is using
          * binary clauses.
          */
         ClauseList binary_clauses_;

         /**@brief List of clauses, which need to be notified about
          * backtrack, when it occurs.
          *
          * This is especially the case of big clauses, in which
          * assigned values are stored in a separate data structure
          * and thus when bactrack occurs, they must be informed so
          * that they can return unassigned values back to the
          * watch list.
          */
         ClauseList backtrack_notification_listeners_;
         /**@brief Contains a conflict clause in case a conflict has
          * been encountered.
          *
          * See also #ConflictAnalysis and #AddValueAssignment method
          * desriptions.
          */
         ClauseInterface * conflict_clause_;
         /**@brief Auxiliary clause used as a buffer for conflict
          * analysis.
          *
          * We have it as a global variable, so that we do not have to
          * allocate it each time #ConflictAnalysis is invoked.
          */
         ClauseInterface * learned_buf_;
         /**@brief Auxiliary clause used as a buffer for resolvent in
          * conflict analysis.
          *
          * We have it as a global variable, so that we do not have to
          * allocate it each time #ConflictAnalysis is invoked.
          */
         ClauseInterface * resolvent_;

         // Member functions.
         /**@brief Allocates and initializes data members of this
          * object.
          *
          * Among other things it reads associated cnf (#cnf_) and
          * initializes list of clauses and literal info. It also
          * looks for unit clauses which are later used by unit
          * propagation.
          *
          * @return Result value as described in #Result enum.
          * In particular, it returns #kUnsatisfiable, if an empty
          * clause is encountered in #cnf_, and #kUndetermined if
          * everything went O.K. If some error occurred, it can return
          * #kFailed.
          */
         Result BuildDataStructures ();
         void RecomputeLiteralOrder();
         /**@brief Frees and deletes memory used by data members of
          * this object.
          */
         void ReleaseDataStructures ();

         /**@brief Performs unit propagation on current cnf.
          *
          * Until list of unit clauses is nonempty, it picks up a unit
          * clause and assigns its sole literal to satisfy it. If a
          * conflict is encountered, it is reflected in the result.
          * Each decision made based on unit propagation marks an
          * antecedent to the corresponding literal. If a literal is
          * already assigned consistently (i.e. if a literal is in
          * more than one unit clause in the list), then from the two
          * possible antecedents the shorter one is chosen.
          *
          * @return See #Result enum for meaning of possible result
          * values.
          */
         Result UnitPropagation ();
         /**@brief Checks whether all variables already have a value
          * assigned.
          *
          * This simply means checking, whether
          * #assigned_variables_==#num_variables_.
          *
          * @return True, if all variables have a value assigned,
          * false otherwise.
          */
         bool AllVariablesAssigned ();
         /**@brief Picks next branching literal.
          *
          * The picked literal is returned in @p literal parameter.
          * Return value is not used to reserve it for possible future
          * #Result value.
          *
          * For literal picking we use a combination of two
          * heuristics, first criterion used is the number of
          * binary clauses, in which literal is at the present. The
          * second criterion is value of literal VSIDS. For deciding
          * based on binary clauses, we remember literals appearing in
          * at least one binary clause in #bin_clauses_literals_
          * stack. If this stack is nonempty, the literal with highest
          * number of binary clauses, in which it appears, is chosen
          * and if more literals have the same number of binary
          * occurrences, then the one with higher LiteralInfo::vsids_
          * is chosen.
          * <em>We used to compare number of
          * binary occurrences of negations of these literals with the
          * idea, that by satisfying the chosen literal, every binary clause
          * with its negation becomes unit and hence it will
          * be unit propagated. However, we could observe, that using
          * directly number of binary occurrences leads to a slightly
          * better running time, although to a bit bigger max search
          * depth. In the future we should try more experiments with
          * it.</em>
          * Highest LiteralInfo::vsids_ is used to take
          * advantage of locality. If no literal is found to be in a
          * binary clause (which is always true e.g. after backtrack),
          * then an unassigned literal with highest
          * LiteralInfo::vsids_ is returned.
          *
          * Let us now describe, how number of binary occurences of a
          * literal is maintained dynamically. At the beginning each
          * LiteralInfo::bin_occurrences_ is set to 0. When we add an
          * assignment in #AddValueAssignment, we check, if a clause,
          * in which we falsify a literal, becomes binary. If so, then
          * we increase number of binary occurrences of the two
          * unassigned literals in which it is present (these are
          * always watched in case of a binary clause). Note, that
          * until we backtrack, a binary clause can only become unit,
          * empty, or satisfied, but it will never become bigger than
          * binary. In case it becomes satisfied, we do not decrease
          * the number of occurrences, as it would take some time. In
          * case it becomes empty, conflict is encountered with
          * subsequent backtrack, and in case it becomes unit, the
          * remaining literal is unit propagated and it is not
          * considered for picking later on. When we backtrack, we
          * reset all the occurrences to 0 again. The reason behind
          * using binary clauses is the same as in case of DPLL, we
          * can hope for a conflict to be encountered earlier and for
          * more often use of unit propagation and less number of
          * branching decisions.
          *
          * @param literal Here the picked literal is returned.
          */
         void PickBranchingLiteral (size_t * literal);
         void PickBranchingLiteralWithFixedOrder (size_t *literal);
         void FillTiesWithVSIDSHeuristic ();
         void FillTiesWithBinaryHeuristic ();

         /**@brief Adds new assignment of variable in given literal,
          * so that given literal is satisfied.
          *
          * Watched clauses of negation of @p satisfied_literal are
          * traversed and their state is checked, if unit clause is
          * encountered in the process, it is added into
          * #unit_clauses_ list. If a binary clause is encountered,
          * its member literals have their
          * LiteralInfo::bin_occurrences_ values increased.
          *
          * @param satisfied_literal Literal to be satisfied by the
          * newly added assignment.
          * @return See #Result for meaning of possible return values.
          */
         Result AddValueAssignment (size_t satisfied_literal);
         /**@brief Learns a clause from the conflict and determines
          * the level, at which solver should backtrack.
          *
          * In the conflict analysis, we use first uip heuristic, that
          * is until the conflict clause contains UIP at current
          * decision level, an implied literal from it is chosen and
          * it is then resolved with its antecedent. The resulting
          * clause is then learned, which means it is added into its
          * watched literals lists nad into a clause list. If its size
          * is 1 (it is a truly unit clause), backtrack level is set
          * to 0 (or -1, if it already was 0), if the size of learned
          * clause is bigger than 1, then backtrack level is set to be
          * the highest level of the remaining literals.
          *
          * @param bt_level Output variable, in which the determined
          * backtrack level is stored at the end.
          */
         void ConflictAnalysis (int * bt_level);

         /**@brief Tries to minimize given clause for learning.
          *
          * Clause to be learned is stored in #learned_buf_,
          * #resolvent_ is used as an other buffer in this method.
          *
          * This function goes over all clauses in the clause list and
          * for every clause (@p list_clause) it checks, whether one of the given
          * conditions is satisfied:
          * <ol>
          * <li>@p clause is subsumed by @p list_clause, in which case we
          * decide not to learn clause at all. We return @p
          * list_clause in @p minimized_clause in this case.</li>
          * <li>@p list_clause is subsumed by @p clause, in which case
          * we can safely remove @p list clause from the list if we
          * learn @p clause (removing clauses will be implemented
          * later).</li>
          * <li>@p clause and @p list_clause are resolvable and their
          * resolvent subsumes @p clause, in this case we replace @p
          * clause by the resolvent.</li>
          * </ol>
          * At the end, @p minimized_clause is filled with the last
          * claus, or @p list_clause from the first item, in case we
          * decide not to learn a clause. Return value determines,
          * whether @p minimized_clause contains a clause to be
          * learned or not.
          *
          * Note, that the clause is not learned in this method, i.e.
          * it is not added to clause list. Although it may remove
          * clauses under the assumption, that this clause will be
          * learned.
          *
          * @return True, if clause returned in @p minimized_clause
          * should be learned, false otherwise.
          */
         bool ClauseMinimization ();

         bool ClauseMinimizationLocallyByResolution ();
         bool ClauseMinimizationLocallyByMarking ();
         bool ClauseMinimizationGlobally ();
         void LearnedClauseMarking (size_t variable);

         /**@brief Backtrack of decision made at level after the given
          * one.
          *
          * Values of literals in the #assignment_stack_ are set to
          * #kUnknownValue, their levels to -1, their antecedents to
          * NULL. All that for literals with LiteralInfo::level_
          * bigger than @p bt_level. Then
          * LiteralInfo::bin_occurrences_ is set to 0 for every
          * literal, and #bin_clauses_literals_ are emptied by setting
          * #bin_clauses_literals_size_ to 0.
          */
         void Backtrack (int bt_level);

         void Restart ();

         /**@brief Checks, if given clause is a UIP, i.e. it contains
          * only one literal decided at current level.
          *
          * It also finds an implied literal if clause is not a uip.
          *
          * @param clause Clause on which test should be performed.
          * @param implied_literal Index of an implied literal in
          * #literals_ array. Will be -1, if no literal at current
          * decision level with non-NULL antecedent is present in
          * given clause.
          * @return True, if the clause is UIP, false otherwise.
          */
         bool IsUIP (ClauseInterface * clause, int * implied_literal);
         /**@brief Normalise vsids values of literals to have the
          * biggest one equal to 1.0.
          *
          * All LiteralInfo::vsids_ values are divided by the biggest
          * one, so that after decay, their proportions are more or
          * less preserved, but the highest vsids is then 1.0.
          */
         void Decay ();

         Result SearchWithConflictBound (int64_t conflict_bound);

         /**@brief Constructor without parameters is private so that
          * it cannot be invoked.
          */
         CDCL ();
         /**@brief Copy constructor is private so that it cannot be
          * invoked.
          */
         CDCL (CDCL&);
         /**@brief Assignment operator is private so that it cannot be
          * invoked.
          */
         void operator=(CDCL&);

      public:
         /**@brief Initializes solver.
          *
          * It does not call #BuildDataStructures however, this
          * function is then called during #Run method.
          */
         explicit inline CDCL (const CNF& a_cnf, const CDCLConfig &cfg) :
            cnf_ (a_cnf),
            decision_level_ (0),
            statistics_ (a_cnf.NumberOfVariables (), a_cnf.NumberOfElements ()),
            parameters_ (),
            config_ (cfg),
            literal_info_ (NULL),
            assignment_stack_ (NULL),
            assigned_variables_ (0),
            ties_ (NULL),
            num_ties_ (0),
            variable_marks_ (NULL),
            marked_variables_ (NULL),
            num_marked_variables_ (0),
            bin_clauses_literals_(NULL),
            bin_clauses_literals_size_(0),
            original_clauses_ (),
            learned_clauses_ (),
            unit_clauses_ (),
            unary_clauses_ (),
            binary_clauses_ (),
            conflict_clause_ (NULL),
            learned_buf_ (NULL),
            resolvent_ (NULL) {}

         /**@brief Releases memory used by the solver.
          *
          * Calls #ReleaseDataStructures to free memory used by the
          * solver object.
          */
         ~CDCL ();

         /**@brief Invokes the search for satisfying assignment.
          *
          * The basic structure of the Run method follows the
          * structure of a CDCL solver. First data structures are
          * built using #BuildDataStructures method. On the other
          * hand, Run itself does not call #ReleaseDataStructures when
          * it finishes work, but it is later called in destructor.
          * Then unit propagation (#UnitPropagation) is called to
          * resolve unit clauses which have been found during build
          * phase in #BuildDataStructures. If everything was O.K. and
          * unit propagation ended with #kUndetermined result, main
          * cycle follows.
          *
          * The main cycle starts with decision level 0 and finishes
          * when all variables have assigned values, or failure was
          * detected at some point, or it was determined in
          * #ConflictAnalysis, that in fact, this cnf is
          * unsatisfiable. Condition of the cycle however consits of a
          * test, whether all variables have assigned values, which is
          * performed using #AllVariablesAssigned method.
          *
          * The cycle itself starts by checking, if decay is
          * neccessary, it happens, if value #Parameters::vsids_add_ is greater or
          * equal to #kMaxVSIDSAdd, in that case #Decay is called.
          * Then next branching literal is chosen by calling
          * #PickBranchingLiteral method and current decision level is
          * increased. Variable in the literal is then assigned value
          * satisfying chosen literal (#AddValueAssignment). During
          * this assignment some unit clauses may have been produced,
          * thus another unit propagation (#UnitPropagation) follows.
          * If the result of unit propagation is, that a conflict has
          * been encountered, it is dealt with in #ConflictAnalysis,
          * where a clause may be learned and new decision level is
          * computed. After bactrack to new decision level by
          * #Backtrack is performed, learned clause becomes unit, and
          * thus unit propagation must be called after backtrack to
          * maintain invariant, that at the beginning of the main
          * cycle before picking branching literal, current cnf does
          * not contain unit clauses. During subsequent unit
          * propagation another conflict may occur, that is why we run
          * the conflict analysis in a cycle, until we end up with a
          * cnf, which satisfies the above mentioned invariant. If we
          * would decide during conflict analysis, that we should
          * backtrack to a negative level, it means, that associated
          * cnf is unsatisfiable. Note, that if a unit clause (here we
          * mean absolutely unit, i.e. with size==1, not relatively
          * with respect to current assignment) is learned, then we
          * backtrack to level 0, which corresponds to the fact, that
          * the unit clause learned must be satisfied in every
          * satisfying assignment, which forces a value for
          * corresponding literal at every level.
          *
          * At the end, if satisfying assignment has been found, it is
          * stored in the @p input parameter.
          *
          * @param input Variable, in which the satisfying assignment
          * will be stored, if the result of #kSatisfiable.
          *
          * @return Result as described in #Result enum.
          */
         Result Run (bf::Input * input);

         void PrintShortStatistics (FILE * file,
               bool print_header, bool print_footer);
         void PrintLongStatistics (FILE * file);

         /**@brief Returns maximum level reached during search for
          * satisfying assignment.
          *
          * @return Value of #max_level_ member variable.
          */
         inline int max_level ()
         {
            return statistics_.max_level_;
         }

         /**@brief Returns number of implied assignments during unit
          * propagation.
          *
          * @return Value of #num_unitprop_ member variable.
          */
         inline int num_unitprop ()
         {
            return statistics_.num_unitprop_;
         }

         /**@brief Returns number of decisions, i.e. number of literal
          * picking.
          *
          * @return Value of #num_decisions_ member variable.
          */
         inline int num_decisions ()
         {
            return statistics_.num_decisions_;
         }

         /**@brief Number of literals picked based on binary clause
          * heuristic.
          *
          * @return Value of #num_bin_decisions_ member variable.
          */
         inline int num_bin_decisions ()
         {
            return statistics_.num_bin_decisions_;
         }

         inline int num_rand_decisions ()
         {
            return statistics_.num_rand_decisions_;
         }

         /**@brief Returns length of the longest learned clause.
          *
          * @return Value of #Statistics::max_length_learned_clause_ member
          * variable.
          */
         inline size_t max_length_learned_clause ()
         {
            return statistics_.max_length_learned_clause_;
         }

         /**@brief Returns length of the longest original clause.
          *
          * @return Value of #Statistics::max_length_original_clause_ member
          * variable.
          */
         inline size_t max_length_original_clause ()
         {
            return statistics_.max_length_original_clause_;
         }

         /**@brief Returns number of learned clauses.
          *
          * @return Value of #num_learned_clauses_ member variable.
          */
         inline size_t num_learned_clauses ()
         {
            return statistics_.num_learned_clauses_;
         }

         inline int64_t num_restarts ()
         {
            return statistics_.num_restarts_;
         }
   };
}

#endif

