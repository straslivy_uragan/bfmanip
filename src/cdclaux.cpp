//
//  cdclaux.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 30.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

/*
 * Implementation of auxiliary data structures in CDCL class.
 * Like functions of Clause class etc.
 * 
 */

#include <assert.h>
#include <string.h>

#include "cdcl.h"

void bf::CDCL::Statistics::Reset ()
{
   num_learned_clauses_ = 0;
   num_deleted_clauses_ = 0;
   num_decisions_ = 0;
   num_bin_decisions_ = 0;
   num_rand_decisions_ = 0;
   num_unitprop_ = 0;
   num_conflicts_ = 0;
   num_restarts_ = 0;
   max_length_learned_clause_ = 0;
   max_level_ = 0;
}

void bf::CDCL::Parameters::Reset ()
{
   next_cid_ = 0;
   vsids_add_ = 1.0;
}
