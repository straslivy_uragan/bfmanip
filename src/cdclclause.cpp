//
//  cdclclause.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 21.07.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include <iostream>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include "cdclclause.h"
#include "bfexception.h"

bf::CDCLClause::~CDCLClause ()
{
   if (literals_ != NULL)
   {
      delete literals_;
   }
   literals_ = NULL;
   size_ = 0;
   watch_a_ = 0;
   watch_b_ = 0;
}

void bf::CDCLClause::InitWithCapacity (int64_t a_cid, size_t a_capacity)
{
   cid_ = a_cid;
   size_ = 0;
   capacity_ = a_capacity;
   watch_a_ = 0;
   watch_b_ = 0;
   literals_ = new size_t[capacity_];
}

void bf::CDCLClause::InitWithNFClause (int64_t a_cid, bf::Clause * a_nfclause)
{
   cid_ = a_cid;
   size_ = 0;
   capacity_ = 0;
   watch_a_ = 0;
   watch_b_ = 0;
   if (a_nfclause != NULL
         && a_nfclause->NumberOfLiterals() > 0)
   {
      capacity_ = a_nfclause->NumberOfLiterals();
      literals_ = new size_t[capacity_];
      for (Clause::LiteralSet::const_iterator lit=a_nfclause->literals().begin();
            lit != a_nfclause->literals().end();
            ++ lit)
      {
         literals_[size_ ++] = *lit;
      }
      if (capacity_ > 1)
      {
         watch_b_ = 1;
      }
   }
}

void bf::CDCLClause::InitWithLiterals (int64_t a_cid, int * a_literals, size_t a_size) 
{
   cid_ = a_cid;
   size_ = a_size;
   capacity_ = a_size;
   watch_a_ = 0;
   watch_b_ = 0;
   if (a_literals != NULL && a_size > 0)
   {
      literals_ = new size_t[a_size];
      memcpy (literals_, a_literals, sizeof(size_t)*a_size);
      if (a_size > 1)
      {
         watch_b_ = 1;
      }
   }
}

void bf::CDCLClause::InitWithClause (int64_t a_cid, bf::CDCLClause *a_clause)
{
   cid_ = a_cid;
   size_ = 0;
   capacity_ = 0;
   watch_a_ = 0;
   watch_b_ = 0;
   if (a_clause != NULL && a_clause->size())
   {
      literals_ = new size_t[a_clause->size()];
      capacity_ = size_ = a_clause->size();
      memcpy (literals_, a_clause->literals_, size_ * sizeof(size_t));
      if (size_ > 1)
      {
         watch_b_ = 1;
      }
   }
}

void bf::CDCLClause::AddLiteral(size_t lit)
{
   if (size_ >= capacity_)
   {
      fputs ("[bf::CDCLClause::AddLiteral] size_ >= capacity_.\n", stderr);
      throw IndexOutOfBoundsException();
   }
   if (size_ > 0 && literals_[size_ - 1] > lit)
   {
      fputs ("[bf::CDCLClause::AddLiteral] Last literal in clause is bigger than literal to be added.\n", stderr);
      throw BadParameterException();
   }
   if (size_ == 0 || literals_[size_ - 1] < lit)
   {
      literals_[size_ ++] = lit;
      if (watch_a_ == watch_b_
            && size_ > 1)
      {
         watch_b_ = (watch_a_ + 1) % size_;
      }
   }
}

void bf::CDCLClause::SetLiteralsFromClause (bf::CDCLClause * a_clause)
{
   if (a_clause == NULL)
   {
      fputs ("[bf::CDCLClause::SetLiteralsFromClause] a_clause == NULL.\n", stderr);
      throw UnexpectedNULLException();
   }
   if (a_clause->size() > capacity_)
   {
      fputs ("[bf::CDCLClause::SetLiteralsFromClause] a_clause->size() > capacity_.\n", stderr);
      throw IndexOutOfBoundsException();
   }
   size_ = a_clause->size();
   memcpy (literals_, a_clause->literals_, size_ * sizeof(size_t));
   watch_a_ = watch_b_ = 0;
   if (size_ > 1)
   {
      watch_b_ = 1;
   }
}

bool bf::CDCLClause::CheckStateUnit (bf::PIValues * an_assignment,
      bf::CDCLClause::ClauseState * state)
{
   switch (an_assignment[WatchedLiteralA()])
   {
      case kFalseValue:
         *state = kUnsatisfied;
         break;
      case kTrueValue:
         *state = kSatisfied;
         break;
      case kUnknownValue:
      default:
         *state = kUnit;
         break;
   }
   return false;
}

bool bf::CDCLClause::CheckStateBinary (bf::PIValues * an_assignment,
      bf::CDCLClause::ClauseState * state)
{
   if (an_assignment[WatchedLiteralA()] == kTrueValue
         || an_assignment[WatchedLiteralB()] == kTrueValue)
   {
      *state = kSatisfied;
   }
   else if (an_assignment[WatchedLiteralA()] == kFalseValue)
   {
      if (an_assignment[WatchedLiteralB()] == kUnknownValue)
      {
         *state = kUnit;
         size_t w = watch_a_;
         watch_a_ = watch_b_;
         watch_b_ = w;
      }
      else
      {
         *state = kUnsatisfied;
      }
   }
   else // an_assignment[WatchedLiteralA()] == kUnknownValue
   {
      *state = (an_assignment[WatchedLiteralB()] == kUnknownValue
            ? kBinary : kUnit);
   }
   return false;
}

bool bf::CDCLClause::CheckStateTernary (bf::PIValues * an_assignment,
      bf::CDCLClause::ClauseState * state)
{
   bool watch_changed = false;
   size_t third_index = 3 - (watch_a_ + watch_b_);
   if (an_assignment[literals_[0]] == kTrueValue
         || an_assignment[literals_[1]] == kTrueValue
         || an_assignment[literals_[2]] == kTrueValue)
   {
      *state = kSatisfied;
   }
   else if (an_assignment[WatchedLiteralA()] == kFalseValue)
   {
      if (an_assignment[literals_[third_index]] == kUnknownValue)
      {
         watch_a_ = third_index;
         watch_changed = true;
         *state = (an_assignment[WatchedLiteralB()] == kFalseValue
               ? kUnit : kBinary);
      }
      else if (an_assignment[WatchedLiteralB()] == kFalseValue)
      {
         *state = kUnsatisfied;
      }
      else
      {
         *state = kUnit;
         size_t w = watch_a_;
         watch_a_ = watch_b_;
         watch_b_ = w;
      }
   }
   else // an_assignment[WatchedLiteralA()] == kUnknownValue
   {
      if (an_assignment[WatchedLiteralB()] == kFalseValue)
      {
         if (an_assignment[literals_[third_index]] == kFalseValue)
         {
            *state = kUnit;
         }
         else
         {
            watch_b_ = third_index;
            watch_changed = true;
            *state = kBinary;
         }
      }
      else // an_assignment[WatchedLiteralB()] == kUnknownValue
      {
         *state = (an_assignment[literals_[third_index]] == kFalseValue
               ? kBinary : kUnresolved);
      }
   }
   return watch_changed;
}

bool bf::CDCLClause::CheckStateGeneral (bf::PIValues * an_assignment,
      bf::CDCLClause::ClauseState * state)
{
   size_t watch_cand_a = size_;
   size_t watch_cand_b = size_;
   size_t number_of_unassigned = 0;
   for (size_t literal_ind = 0; literal_ind < size_; ++literal_ind)
   {
      if (an_assignment[literals_[literal_ind]] == kTrueValue)
      {
         *state = kSatisfied;
         return false;
      }
      if (an_assignment[literals_[literal_ind]] == kUnknownValue)
      {
         ++ number_of_unassigned;
         if (literal_ind != watch_a_
               && literal_ind != watch_b_)
         {
            if (watch_cand_a == size_)
            {
               watch_cand_a = literal_ind;
            }
            else //if (watch_cand_b == size_)
            {
               watch_cand_b = literal_ind;
            }
         }
      }
   }
   bool watch_changed = false;
   if (an_assignment[WatchedLiteralA()] == kFalseValue
         && watch_cand_a < size_)
   {
      watch_a_ = watch_cand_a;
      watch_changed = true;
   }
   if (an_assignment[WatchedLiteralB()] == kFalseValue)
   {
      if (!watch_changed && watch_cand_a < size_)
      {
         watch_b_ = watch_cand_a;
         watch_changed = true;
      }
      else if (watch_cand_b < size_)
      {
         watch_b_ = watch_cand_b;
         watch_changed = true;
      }
   }
   switch (number_of_unassigned)
   {
      case 0:
         *state = kUnsatisfied;
         break;
      case 1:
         *state = kUnit;
         if (an_assignment[WatchedLiteralA()] == kFalseValue
               && an_assignment[WatchedLiteralB()] == kUnknownValue)
         {
            size_t swap = watch_a_;
            watch_a_ = watch_b_;
            watch_b_ = swap;
         }
         break;
      case 2:
         *state = kBinary;
         break;
      default:
         break;
   }
   return watch_changed;
}

bool bf::CDCLClause::CheckState (bf::PIValues * an_assignment,
      bf::CDCLClause::ClauseState * state)
{
   bool watch_changed = false;
   switch (size_)
   {
      case 0:
         *state = kUnsatisfied;
         break;
      case 1:
         watch_changed = CheckStateUnit (an_assignment, state);
         break;
      case 2:
         watch_changed = CheckStateBinary (an_assignment, state);
         break;
      case 3:
         watch_changed = CheckStateTernary (an_assignment, state);
         break;
      default:
         watch_changed = CheckStateGeneral (an_assignment, state);
         break;
   }
   return watch_changed;
}


bf::CDCLClause::ClauseState 
bf::CDCLClause::DryCheckState (PIValues * an_assignment)
{
   int unassigned = 0;
   for (size_t lit_index = 0; lit_index < size_; ++ lit_index)
   {
      switch (an_assignment[literals_[lit_index]])
      {
         case kTrueValue:
            return kSatisfied;
         case kUnknownValue:
            ++ unassigned;
            break;
         default:
            break;
      }
   }
   switch (unassigned)
   {
      case 0:
         return kUnsatisfied;
      case 1:
         return kUnit;
      case 2:
         return kBinary;
      default:
         return kUnresolved;
   }
   return kUnresolved;
}

bool bf::CDCLClause::Resolve (bf::CDCLClause * other_parent,
      bf::CDCLClause * resolvent)
{
   assert (other_parent != NULL);
   assert (resolvent != NULL);

   resolvent->Clear ();
   size_t my_literal_ind = 0;
   size_t other_literal_ind = 0;
   bool found_conflict = false;
   while (my_literal_ind < size_
         && other_literal_ind < other_parent->size())
   {
      if (literals_[my_literal_ind] / 2
            == other_parent->literal(other_literal_ind) / 2)
      {
         if (literals_[my_literal_ind] 
               != other_parent->literal(other_literal_ind))
         {
            if (found_conflict)
            {
               return false;
            }
            found_conflict = true;
         }
         else
         {
            resolvent->AddLiteralWithoutCheck(literals_[my_literal_ind]);
         }
         ++ my_literal_ind;
         ++ other_literal_ind;

      }
      else if (literals_[my_literal_ind]
            < other_parent->literal(other_literal_ind))
      {
         resolvent->AddLiteralWithoutCheck(literals_[my_literal_ind++]);
      }
      else
      {
         resolvent->AddLiteralWithoutCheck(other_parent->literal(other_literal_ind++));
      }
   }
   if (!found_conflict)
   {
      return false;
   }
   while (my_literal_ind < size_)
   {
      resolvent->AddLiteralWithoutCheck(literals_[my_literal_ind++]);
   }
   while (other_literal_ind < other_parent->size())
   {
      resolvent->AddLiteralWithoutCheck(other_parent->literal(other_literal_ind++));
   }
   resolvent->watch_a_ = 0;
   resolvent->watch_a_ = (resolvent->size_ > 1 ? 1 : 0);
   return true;
}

bool bf::CDCLClause::SubsumingResolve (bf::CDCLClause * other_parent,
      bf::CDCLClause * resolvent)
{
   assert (other_parent != NULL);
   assert (resolvent != NULL);

   if (other_parent->size()>size_)
   {
      return false;
   }
   resolvent->size_ = 0;
   size_t my_literal_ind = 0;
   size_t other_literal_ind = 0;
   bool found_conflict = false;
   while (my_literal_ind < size_
         && other_literal_ind < other_parent->size())
   {
      if (literals_[my_literal_ind] / 2
            == other_parent->literal(other_literal_ind) / 2)
      {
         if (literals_[my_literal_ind] 
               != other_parent->literal(other_literal_ind))
         {
            if (found_conflict)
            {
               return false;
            }
            found_conflict = true;
         }
         else
         {
            resolvent->AddLiteralWithoutCheck(literals_[my_literal_ind]);
         }
         ++ my_literal_ind;
         ++ other_literal_ind;

      }
      else if (literals_[my_literal_ind]
            < other_parent->literal(other_literal_ind))
      {
         resolvent->AddLiteralWithoutCheck(literals_[my_literal_ind++]);
      }
      else //literals_[my_literal_ind]>other_parent->literal(other_literal_ind)
      {
         return false;
      }
   }
   if (!found_conflict || other_literal_ind < other_parent->size())
   {
      return false;
   }
   while (my_literal_ind < size_)
   {
      resolvent->AddLiteralWithoutCheck(literals_[my_literal_ind++]);
   }
   resolvent->watch_a_ = 0;
   resolvent->watch_a_ = (resolvent->size_ > 1 ? 1 : 0);
   return true;
}

void bf::CDCLClause::Print (FILE * file)
{
   for (size_t literal_ind = 0; literal_ind < size_; ++ literal_ind)
   {
      if (literals_[literal_ind] % 2)
      {
         fprintf (file, "-%lu ", literals_[literal_ind] / 2);
      }
      else
      {
         fprintf (file, "%lu ", literals_[literal_ind] / 2);
      }
   }
   fprintf (file, "0 (%s%lu, %s%lu)\n",
         (WatchedLiteralA() % 2 ? "-" : ""),
         WatchedLiteralA()/2,
         (WatchedLiteralB() % 2 ? "-" : ""),
         WatchedLiteralB()/2);

}
