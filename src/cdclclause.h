//
//  cdclclause.h
//  bfmanip++
//
//  Created by Petr Kučera on 21.07.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef CDCLCLAUSE_H
#define CDCLCLAUSE_H

#include <limits>

#include "input.h"
#include "normalform.h"

namespace bf {
   /**@brief Class for storing a clause.
    */
   class CDCLClause {
      public:
         /**@brief Constants for determining state of a clause.
          *
          * These constants are returned by
          * #CheckState(PIValues*,ClauseState*) method.
          */
         enum ClauseState {
            /**@brief Clause is satisfied.
             *
             * It has at least one literal which evaluate to 1.
             */
            kSatisfied,
            /**@brief Clause is unsatisfied.
             *
             * All its literals evaluate to 0.
             */
            kUnsatisfied,
            /**@brief Clause is unit.
             *
             * It has only one unassigned literal and all other
             * literals are falsified.
             */
            kUnit,
            /**@brief Clause is binary.
             *
             * It has two unassigned literals and all other
             * literals are falsified.
             */
            kBinary,
            /**@brief Clause is unresolved.
             *
             * It has three or more unassigned literals and all
             * other literals are falsified.
             */
            kUnresolved
         };
      private:
         /**@brief Unique clause identification number.
          *
          * If cid_ < num_original_clauses_, then it is an
          * original clause.
          */
         int64_t cid_;
         /**@brief Clause literals.
          *
          * An array of length stored in #capacity_ variable,
          * which has to be at least equal to #size_.
          * A positive literal on variable <em>i</em> is stored
          * as 2*<em>i</em> and a negative literal on variable
          * <em>i</em>
          * is stored as 2*<em>i</em>+1. In order for resolution
          * to work properly, it is neccessary, that literals in
          * this array are stored in increasing order by their
          * numbers.
          *
          */
         size_t * literals_;
         /**@brief Number of literals in clause.
          */
         size_t size_;
         /**@brief Capacity of this clause.
          * 
          * Size of #literals_ array.
          */
         size_t capacity_;
         /**@brief First watched literal.
          *
          * The value in this variable is an index into literals. */
         size_t watch_a_;
         /**@brief Second watched literal.
          *
          * The value in this variable is an index into literals. */
         size_t watch_b_;

         /**@brief This method checks state and update watched
          * literals of a unit clause.
          *
          * This method is called from
          * #CheckState(PIValues*,ClauseState*) method if the
          * clause is unit, i.e. its #size_ is 1. For further
          * description see #CheckState(PIValues*,ClauseState*).
          *
          * @param an_assignment Assignment with respect to
          * which the state should be checked.
          * @param state Output parameter in which the state of
          * this clause is stored.
          * @return True if watched literals changed during
          * status check, false otherwise.
          */
         bool CheckStateUnit (PIValues * an_assignment, ClauseState * state);
         /**@brief This method checks state and update watched
          * literals of a binary clause.
          *
          * This method is called from
          * #CheckState(PIValues*,ClauseState*) method if the
          * clause is binary, i.e. its #size_ is 2. For further
          * description see #CheckState(PIValues*,ClauseState*).
          *
          * @param an_assignment Assignment with respect to
          * which the state should be checked.
          * @param state Output parameter in which the state of
          * this clause is stored.
          * @return True if watched literals changed during
          * status check, false otherwise.
          */
         bool CheckStateBinary (PIValues * an_assignment, ClauseState * state);
         /**@brief This method checks state and update watched
          * literals of a ternary clause.
          *
          * This method is called from
          * #CheckState(PIValues*,ClauseState*) method if the
          * clause is ternary (also called cubic), i.e. its
          * #size_ is 3. For further description see
          * #CheckState(PIValues*,ClauseState*).
          *
          * @param an_assignment Assignment with respect to
          * which the state should be checked.
          * @param state Output parameter in which the state of
          * this clause is stored.
          * @return True if watched literals changed during
          * status check, false otherwise.
          */
         bool CheckStateTernary (PIValues * an_assignment, ClauseState * state);
         /**@brief This method checks state and update watched
          * literals of a general clause.
          *
          * This method is called from
          * #CheckState(PIValues*,ClauseState*) method if the
          * clause has #size_ bigger than 3. For further
          * description see #CheckState(PIValues*,ClauseState*).
          *
          * @param an_assignment Assignment with respect to
          * which the state should be checked.
          * @param state Output parameter in which the state of
          * this clause is stored.
          * @return True if watched literals changed during
          * status check, false otherwise.
          */
         bool CheckStateGeneral (PIValues * an_assignment, ClauseState * state);

      public:
         /**@brief Constructor initializing an empty clause.
          *
          * Array #literals_ is not allocated in the
          * constructor, but in one of initialization functions
          * (InitWithXXX), through which further initialization
          * should be performed. 
          */
         explicit inline CDCLClause () :
            cid_ (0), literals_ (NULL), size_ (0), capacity_(0),
            watch_a_ (0), watch_b_ (0) {}
         /**@brief Initializes clause with given identification
          * number and capacity.
          *
          * Allocates #literals_ array to capacity @p
          * a_capacity, but otherwise keeps #size_ equal to 0,
          * thus keeping it empty.
          *
          *
          * @param a_cid Clause identification number to be
          * assigned to #cid_.
          * @param a_capacity New capacity of this clause.
          */
         void InitWithCapacity (int64_t a_cid, size_t a_capacity);
         /**@brief Initializes clause to be a copy of given
          * clause given as NFElement.
          *
          *
          * @param a_cid Clause identification number to be
          * assigned to #cid_.
          * @param a_nfclause A clause to be copied.
          */
         void InitWithNFClause (int64_t a_cid, Clause * a_nfclause);
         /**@brief Initializes clause with given array of
          * literals.
          *
          * To make it fast, memcpy is used to copy @p
          * a_literals to #literals_ and it is not checked,
          * whether literals are stored in increasing order in
          * the input array. It is responsibility of caller to
          * ensure validity of this property.
          *
          * @param a_cid Clause identification number to be
          * assigned to #cid_.
          * @param a_literals Array with literals.
          * @param a_size Number of literals in @p a_literals
          * array.
          */
         void InitWithLiterals (int64_t a_cid, int * a_literals,
               size_t a_size);
         /**@brief Initializes clause as a copy of given clause
          * with given identification number. 
          *
          * Identification number is not taken from
          * @p a_clause, but from @p a_cid parameter.
          *
          * @param a_cid Clause identification number to be
          * assigned to #cid_.
          * @param a_clause A clause to be copied.
          */
         void InitWithClause (int64_t a_cid, CDCLClause *a_clause);

         /**@brief Empties the clause without freeing
          * literals array. */
         inline void Clear () {
            size_ = 0;
            watch_a_ = watch_b_ = 0;
         }

         /**@brief Destructor deletes literals_ array if it is
          * not NULL.
          */
         ~CDCLClause ();

         /**@brief Returns clause identification number.
          *
          * Returns value of #cid_.
          *
          * @return Clause identification number.
          */
         inline int64_t cid () {return cid_;}
         /**@brief Returns number of literals in this clause.
          *
          * Returns value of #size_.
          * 
          * @return Number of literals in this clause.
          */
         inline size_t size () {return size_;}
         /**@brief Returns literal at given index.
          *
          * Since the literals in #literals_ array are stored in
          * increasing order, literal with <tt>lit_index</tt>-th
          * smallest number is returned.
          * 
          * @param lit_index index to #literals_ of a literal to
          * be returned.
          *
          * @return Literal at index @p lit_index.
          */
         inline size_t literal (size_t lit_index)
         {
            return (lit_index < size_ ? literals_[lit_index] : std::numeric_limits<size_t>::max());
         }
         /**@brief Adds given literal to this clause.
          *
          * Although in #literals_ array we require, that
          * literals are sorted in increasing order by their
          * numbers, this function checks this property only
          * with respect to the last literal in the clause,
          * which means, that if only this method is used to add
          * literals, then the property is checked. However, if
          * it is not satisfied, only an exception
          * BadParameterException is thrown. In particular, this
          * exception is thrown only if @p literal is
          * strictly smaller than the last literal in the
          * clause, if @p literal is equal to the last literal
          * in the clause, then no literal is added and no
          * exception is thrown.
          *
          * @param lit Literal to be added
          * @throw IndexOutOfBoundsException If size_ is already
          * equal to capacity_.
          * @throw BadParameterException If literal is 
          * strictly smaller than the last current literal.
          */
         void AddLiteral(size_t lit);

         /**@brief Adds literals without checking, if it
          * satisfies increasing property and whether it fits
          * within capacity.
          *
          * This is a quick version intended to be used as a
          * quick interface to buffers. No watch is updated
          * either.
          */
         inline void AddLiteralWithoutCheck (size_t lit) {
            literals_[size_++] = lit;
         }

         /**@brief Sets literals in this clause to be the same
          * as in given clause.
          *
          * Using memcpy copies #literals_ from @p a_clause.
          *
          * @param a_clause Clause literals of which should be
          * copied.
          * @throw UnexpectedNULLException If @p a_clause is
          * NULL.
          * @throw IndexOutOfBoundsException If number of
          * literals in @p a_clause exceeds capacity of this
          * clause.
          */
         void SetLiteralsFromClause (CDCLClause * a_clause);

         /**@brief First watched literal as literal number.
          *
          * For the sake of speed, it is not checked, whether
          * #literals_ array is NULL or whether the #size_ is at
          * least 1.
          */
         inline size_t WatchedLiteralA ()
         {
            return literals_[watch_a_];
         }
         /**@brief Second watched literal as literal number.
          *
          * For the sake of speed, it is not checked, whether
          * #literals_ array is NULL or whether the #size_ is at
          * least 1.
          */
         inline size_t WatchedLiteralB ()
         {
            return literals_[watch_b_];
         }

         /**@brief Allows to set watched literals.
          *
          * This should be used with caution. When we learn a
          * clause, we need this to set watched literals
          * correctly. Parameters denote new values of watch_a_
          * and watch_b_ directly, i.e. they are indices into
          * @p literals_ array.
          */
         inline void SetWatchIndices (size_t watched_literal_a,
               size_t watched_literal_b)
         {
            watch_a_ = watched_literal_a;
            watch_b_ = watched_literal_b;
         }

         /**@brief Updates the watched literals according to given
          * assignment.
          *
          * It checks the state and updates watched literals if
          * neccessary. Note, that afterwards sender should
          * check, whether some watch has changed in order to
          * update it in its configuration. If returned state is
          * kUnit, then watch_a_ allways points to the
          * unassigned literal. This method actually only checks
          * size of a clause and then it uses specialized
          * versions #CheckStateUnit, #CheckStateBinary,
          * #CheckStateTernary, or #CheckStateGeneral.
          *
          * @param an_assignment The assignment should have length
          * twice the number of variables and it should have value
          * for every literal (i.e. both positive and negative).
          * @param state Final state of the clause.
          * 
          * @return True, if one of the watched literals has
          * changed. False otherwise.
          */
         bool CheckState (PIValues * an_assignment, ClauseState * state);

         /**@brief Checks the state of the clause without
          * modifying watches. 
          *
          * Mainly used for debugging.
          */
         ClauseState DryCheckState (PIValues * an_assignment);

         /**@brief Creates resolvent of this clause with given
          * clause.
          *
          * This function assumes, that in clause literals are
          * sorted in increasing order by their numbers. If this is
          * satisfied, this property holds for resolvent, too.
          * The result is stored in the @p resolvent, it must have
          * enough space in literals array to hold all variables
          * from both clauses. This size bound is not checked (it
          * would be too slow considering how often resolution is
          * computed and that we use literal_buf_ and resolvent_
          * with enough preallocated capacity for resolution).
          *
          * @return True, if resolution was successfull, false
          * otherwise, i.e. if no conflict or more than one
          * conflict have been encountered. In case of false
          * result, contents of resolvent are undefined.
          */
         bool Resolve (CDCLClause * other_parent, CDCLClause * resolvent);

         /**@brief Creates subsuming resolvent of this clause with given
          * clause.
          *
          * This function performs normal resolution, but it only
          * succeeds, if the result in resolvent form a subclause of
          * this clause.
          */
         bool SubsumingResolve (CDCLClause * other_parent, CDCLClause * resolvent);

         /**@brief Prints description of this clause to given
          * file.
          *
          * It first outputs list of literals in the clause
          * ended with 0, then in brackets watched literals
          * follow.
          *
          * @param file File to which description of this clause
          * should be written.
          */
         void Print (FILE * file);
      private:
         /**@brief The copy constructor - is private to prevent
          * calling it.
          */
         CDCLClause (CDCLClause&);
         /**@brief The assignment operator - is private to
          * prevent calling it.
          */
         void operator=(const CDCLClause&);
   };
}

#endif
