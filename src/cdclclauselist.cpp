//
//  cdclclauselist.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 21.07.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include <iostream>
#include <assert.h>
#include "cdclclauselist.h"
#include "bfexception.h"

bf::CDCLClauseList::Element * bf::CDCLClauseList::Element::free_elements_stack_ = NULL;

bf::CDCLClauseList::Element::~Element ()
{
   clause_ = NULL;
   prev_ = next_ = NULL;
}

bf::CDCLClauseList::Element * bf::CDCLClauseList::Element::NewElement ()
{
   Element * element = NULL;
   if (free_elements_stack_ != NULL)
   {
      element = free_elements_stack_;
      free_elements_stack_ = free_elements_stack_ -> next_;
      if (free_elements_stack_ != NULL)
      {
         free_elements_stack_->prev_ = NULL;
      }
      element->next_ = element->prev_ = NULL;
      element->clause_ = NULL;
   }
   else
   {
      element = new Element ();
   }
   return element;
}

void bf::CDCLClauseList::Element::ReleaseElement (bf::CDCLClauseList::Element * element)
{
   element->next_ = free_elements_stack_;
   element->prev_ = NULL;
   element->clause_ = NULL;
   free_elements_stack_ = element;
}

void bf::CDCLClauseList::Element::DeleteFreeElements ()
{
   while (free_elements_stack_ != NULL)
   {
      Element * element = free_elements_stack_;
      free_elements_stack_ = free_elements_stack_->next_;
      delete element;
   }
}

void bf::CDCLClauseList::Element::DeleteClause ()
{
   if (clause_ != NULL)
   {
      delete clause_;
      clause_ = NULL;
   }
}

bf::CDCLClauseList::~CDCLClauseList ()
{
   Clear ();
}

void bf::CDCLClauseList::PushBack (bf::CDCLClause * a_clause)
{
   Element * element = Element::NewElement ();
   assert (element != NULL);
   element->setClause(a_clause);
   if (head_== NULL)
   {
      head_ = tail_ = element;
   }
   else
   {
      tail_->setNext(element);
      element->setPrev(tail_);
      tail_ = element;
   }
}

void bf::CDCLClauseList::PushFront (bf::CDCLClause * a_clause)
{
   Element * element = Element::NewElement ();
   assert (element != NULL);
   element->setClause(a_clause);
   if (head_== NULL)
   {
      head_ = tail_ = element;
   }
   else
   {
      head_->setPrev(element);
      element->setNext(head_);
      head_ = element;
   }
}

bf::CDCLClause * bf::CDCLClauseList::PopBack ()
{
   CDCLClause * clause = NULL;
   if (tail_ != NULL)
   {
      clause = tail_->clause();
      tail_->setClause(NULL);
      Element * element = tail_;
      tail_ = tail_->prev();
      if (tail_ != NULL)
      {
         tail_->setNext(NULL);
      }
      else
      {
         head_ = NULL;
      }
      Element::ReleaseElement (element);
   }
   return clause;
}

bf::CDCLClause * bf::CDCLClauseList::PopFront ()
{
   CDCLClause * clause = NULL;
   if (head_ != NULL)
   {
      clause = head_->clause();
      head_->setClause(NULL);
      Element * element = head_;
      head_ = head_->next();
      if (head_ != NULL)
      {
         head_->setPrev(NULL);
      }
      else
      {
         tail_ = NULL;
      }
      Element::ReleaseElement (element);
   }
   return clause;
}

void bf::CDCLClauseList::Clear ()
{
   while (head_ != NULL)
   {
      Element * element = head_;
      head_ = head_->next();
      Element::ReleaseElement (element);
   }
   tail_ = NULL;
}

void bf::CDCLClauseList::DeleteAllElements ()
{
   while (head_ != NULL)
   {
      Element * element = head_;
      head_ = head_->next();
      element->DeleteClause();
      Element::ReleaseElement (element);
   }
   tail_ = NULL;
}

void bf::CDCLClauseList::Swap(bf::CDCLClauseList& list)
{
   Element * buf_head = head_;
   Element * buf_tail = tail_;
   head_ = list.head_;
   tail_ = list.tail_;
   list.head_ = buf_head;
   list.tail_ = buf_tail;
}

void bf::CDCLClauseList::Print (FILE * file)
{
   fputs ("<CDCLClauseList>\n", file);
   for (Element * element = head_;
         element != NULL;
         element = element->next())
   {
      element->clause()->Print(file);
   }
   fputs ("</CDCLClauseList>\n", file);
}
