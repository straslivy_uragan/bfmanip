//
//  cdclclauselist.h
//  bfmanip++
//
//  Created by Petr Kučera on 21.07.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#ifndef CDCLCLAUSELIST_H
#define CDCLCLAUSELIST_H

#include "cdclclause.h"

namespace bf {
   
   /**@brief Class implementing a doubly linked list of clauses.
    */
   class CDCLClauseList {
      public:
         /**@brief Class implementing a single element of list of
          * clauses.
          *
          * This class also allows to manage list of free elements to
          * avoid often reallocating of object of this class. You can
          * use this feature through static element
          * #free_elements_stack_ and functions #NewElement,
          * #ReleaseElement, and #DeleteFreeElements.
          */
         class Element {
            private:
               /**@brief Clause stored in this list element. */
               CDCLClause * clause_;
               /**@brief Pointer to the previous element in the list.
                */
               Element * prev_;
               /**@brief Pointer to the next element in the list. */
               Element * next_;

            public:
               /**@brief Stack of free elements.
                *
                * Elements which have been allocated and later
                * released using #ReleaseElement are stored in this
                * stack to be used later by #NewElement.
                */
               static Element * free_elements_stack_;
               /**@brief Allocates and creates new element.
                *
                * This function first looks at stack
                * #free_elements_stack_ and if it is nonempty, an
                * element is popped from it and returned as new
                * element. If no free element is stored in this stack,
                * then a new element is allocated by calling @p new
                * operator.
                */
               static Element * NewElement ();
               /**@brief Resets element content and stores the
                * element in the stack of free elements.
                *
                * Memory used by the element is not freed, rather this
                * element is stored in #free_elements_stack_ to be
                * used later by futuger #NewElement allocator
                * function.
                *
                * @param element Element to be released.
                */
               static void ReleaseElement (Element * element);
               /**@brief Immediately frees all elements in the stack of
                * free elements and empties this stack.
                *
                * Until the stack #free_elements_stack_ is not empty,
                * an element is popped from it and deleted using @p
                * delete operator.
                */
               static void DeleteFreeElements ();

               /**@brief Returns clause stored in this element.
                *
                * @return Value of #clause_ variable.
                */
               inline CDCLClause * clause () {return clause_;}
               /**@brief Returns pointer to the previous element.
                *
                * @return Value of #prev_ variable.
                */
               inline Element * prev () {return prev_;}
               /**@brief Returns pointer to the next element.
                *
                * @return Value of #next_ variable.
                */
               inline Element * next () {return next_;}
               /**@brief Sets stored clause to given value.
                *
                * @param a_clause New value for stored clause.
                */
               inline void setClause (CDCLClause * a_clause) {clause_ = a_clause;}
               /**@brief Sets pointer to previous element in the list
                * to given value.
                *
                * @param a_prev New pointer to previous element.
                */
               inline void setPrev (Element * a_prev) {prev_ = a_prev;}
               /**@brief Sets pointer to next element in the list
                * to given value.
                *
                * @param a_next New pointer to next element.
                */
               inline void setNext (Element * a_next) {next_ = a_next;}

               /**@brief Deletes stored clause by delete operator.
                *
                * If #clause_ is not NULL, calls @p delete #clause_ to
                * free memory used by it. Note, that normaly element
                * is not considered as the owner of stored clause as
                * this clause can be in many lists. Thus if you want
                * to delete clauses stored in the list, you have to do
                * it explicitely e.g. by this function.
                */
               void DeleteClause();

               /**@brief Constructor initializes an empty list
                * element.
                */
               inline Element ()
                  : clause_(NULL), prev_(NULL), next_(NULL) {}

               /**@brief Resets the element and deletes memory used by
                * it.
                *
                * The clause is not considered as being owned by the
                * list element, and thus it is not deleted in this
                * destructor.
                */
               ~Element ();

            private:
               /**@brief Copy constructor is made private to prevent
                * it from being called.
                */
               Element (Element&);
               /**@brief Assignment operator is made private to
                * prevent it from being called.
                */
               void operator=(const Element&);
         };
      private:


      /**@brief Pointer to the head of the list.
       */
      Element * head_;
      /**@brief Pointer to the tail of the list.
       */
      Element * tail_;

      public:
      /**@brief Constructor initializing an empty list.
       */
      inline CDCLClauseList () : head_(NULL), tail_(NULL) {}
      /**@brief Deletes list and its content.
       *
       * Calls #Clear to clear contents of the list.
       */
      ~CDCLClauseList ();
      /**@brief Appends given clause to the end of the list.
       *
       * @param a_clause A clause to be added at the end of the
       * list.
       */
      void PushBack (CDCLClause * a_clause);
      /**@brief Inserts given clause at the beginning of the
       * list.
       *
       * @param a_clause A clause to be added at the beginning
       * of the list.
       */
      void PushFront (CDCLClause * a_clause);
      /**@brief Removes the first element from the list and
       * returns its content.
       *
       * @return Clause stored in the former first element of the list,
       * or NULL, if the list was empty.
       */
      CDCLClause * PopFront ();
      /**@brief Removes the last element from the list and
       * returns its content.
       *
       * @return Clause stored in the former last element of the
       * list, or NULL, if the list was empty.
       */
      CDCLClause * PopBack ();
      /**@brief Returns head of the list.
       *
       * @return The head, i.e. the first element of the list.
       */
      inline Element * head () {return head_;}
      /**@brief Returns tail of the list.
       *
       * @return The tail, i.e. the last element of the list.
       */
      inline Element * tail () {return tail_;}

      /**@brief Swaps contents of the list with given one.
       *
       * Swaps #head_ and #tail_ values with head and tail of @p
       * list. This function finishes in constant time, since it
       * consists of constant number of assigning of pointers.
       *
       * @param list List with which the head and tail should be
       * exchanged.
       */
      void Swap (CDCLClauseList& list);

      /**@brief Checks, whether the list contains some element.
       *
       * @return True, if the list is empty, i.e. #head_ is
       * NULL, false otherwise.
       */
      inline bool Empty () {return (head_ == NULL);}
      /**@brief Removes all clauses from the list without
       * deleting them.
       * 
       * This function only removes all elements but it does not
       * delete clauses contained in them.
       */
      void Clear ();
      /**@brief Removes all clauses from the list with deleting
       * them using delete operator.
       *
       * This function removes all elements of the list and it
       * also deletes the clauses contained in them using delete
       * operator.
       */
      void DeleteAllElements ();

      /**@brief Writes descriptions of the clauses stored in
       * this list to given file.
       *
       * On every clause stored in the list, its
       * CDCLClause::Print(FILE*) method is called.
       */
      void Print (FILE * file);

      private:
      /**@brief Copy constructor is private to prevent its
       * invocation.
       */
      CDCLClauseList(CDCLClauseList&);
      /**@brief Assignment operator is private to prevent its
       * invocation.
       */
      void operator=(const CDCLClauseList&);
   };
}

#endif
