//
// Project BFManip++
// 2017, Petr Kučera
//
/**@file cdclconfig.cpp
 * @brief Structure representing a configuration of CDCL solver.
 */

#include "cdclconfig.h"

#include <fstream>

#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/error/en.h>

#include "bfexception.h"

const char *bf::CDCLConfig::Branching::branching_heuristic_values[kBranchingHeuristicSize]=
{
   "binary_with_vsids",
   "vsids",
   "fixed_order"
};
const char *bf::CDCLConfig::Learning::minimization_methods_values[kMinimizationMethodsSize]=
{
   "none",
   "locally_by_resolution",
   "locally_by_marking",
   "globally"
};
const char *bf::CDCLConfig::Random::kJSONObjectName="random";
const char *bf::CDCLConfig::Random::kJSONSetSeed="set_seed";
const char *bf::CDCLConfig::Random::kJSONSeed="seed";

const char *bf::CDCLConfig::Branching::kJSONObjectName="branching";
const char *bf::CDCLConfig::Branching::kJSONVSIDSBump="vsids_bump";
const char *bf::CDCLConfig::Branching::kJSONMaxVSIDSAdd="max_vsids_add";
const char *bf::CDCLConfig::Branching::kJSONChanceOfRandomPick="chance_of_random_pick";
const char *bf::CDCLConfig::Branching::kJSONBranchingHeuristic="branching_heuristic";
const char *bf::CDCLConfig::Branching::kJSONLiteralOrderFile="literal_order_file";

const char *bf::CDCLConfig::Learning::kJSONObjectName="learning";
const char *bf::CDCLConfig::Learning::kJSONClauseMinimizationThreshold="clause_minimization_threshold";
const char *bf::CDCLConfig::Learning::kJSONMinimizationMethod="minimization_method";

const char *bf::CDCLConfig::Restarts::kJSONObjectName="restarts";
const char *bf::CDCLConfig::Restarts::kJSONUseRestarts="use_restarts";
const char *bf::CDCLConfig::Restarts::kJSONFirstRestartConflict="first_restart_conflicts";
const char *bf::CDCLConfig::Restarts::kJSONRestartConflictBump="restart_conflict_bump";
const char *bf::CDCLConfig::Restarts::kJSONClauseLengthBump="clause_length_bump";
const char *bf::CDCLConfig::Restarts::kJSONInitClauseLengthFactor="init_clause_length_factor";
const char *bf::CDCLConfig::Restarts::kJSONPruneLearnedClauses="prune_learned_clauses";

bf::CDCLConfig::CDCLConfig(std::istream &json)
{
   if(!ReadJSON(json))
   {
      throw ConfigFileReadException();
   }
}

   bool
bf::CDCLConfig::ReadJSON(std::istream &json)
{
   rapidjson::Document d;
   rapidjson::IStreamWrapper isw(json);
   d.ParseStream(isw);
   if(d.HasParseError())
   {
      std::cerr<<"Config file JSON parse error: "<<
         rapidjson::GetParseError_En(d.GetParseError())<<
         " ("<<d.GetErrorOffset()<<")"<<std::endl;
      return false;
   }
   if(!d.IsObject())
   {
      std::cerr<<"Config file JSON parse error: Document root is not an object"<<std::endl;
      return false;
   }
   if(d.HasMember(Random::kJSONObjectName)
         &&!random.SetJSON(d[Random::kJSONObjectName]))
   {
      return false;
   }
   if(d.HasMember(Branching::kJSONObjectName)
         &&!branching.SetJSON(d[Branching::kJSONObjectName]))
   {
      return false;
   }
   if(d.HasMember(Learning::kJSONObjectName)
         &&!learning.SetJSON(d[Learning::kJSONObjectName]))
   {
      return false;
   }
   if(d.HasMember(Restarts::kJSONObjectName)
         &&!restarts.SetJSON(d[Restarts::kJSONObjectName]))
   {
      return false;
   }
   return true;
}

   void
bf::CDCLConfig::WriteJSON(std::ostream &json) const
{
   rapidjson::Document d;
   GenerateJSON(&d, d.GetAllocator());
   rapidjson::OStreamWrapper osw(json);
   rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(osw);
   d.Accept(writer);
}

   void
bf::CDCLConfig::Dump(std::ostream &out) const
{
   WriteJSON(out);
   if(branching.branching_heuristic==Branching::kBranchWithFixedOrder)
   {
      out<<"\n\nLiteral order:";
      for(std::vector<unsigned>::const_iterator u=branching.literal_order.begin();
            u!=branching.literal_order.end();
            ++u)
      {
         out<<' ';
         if((*u)%2==1)
         {
            out<<'-';
         }
         out<<((*u)/2);
      }
   }
   out<<std::endl;
}

   bool
bf::CDCLConfig::Random::SetJSON(const rapidjson::Value &v)
{
   if(!v.IsObject())
   {
      std::cerr<<"Config file JSON parse error: Property \"random\" is not an object"<<std::endl;
      return false;
   }
   if(v.HasMember(kJSONSetSeed))
   {
      if(!v[kJSONSetSeed].IsBool())
      {
         std::cerr<<"Config file JSON parse error: \""<<
            kJSONSetSeed<<"\" is not bool"<<std::endl;
         return false;
      }
      set_seed=v[kJSONSetSeed].GetBool();
   }
   if(v.HasMember(kJSONSeed))
   {
      if(!v[kJSONSeed].IsUint())
      {
         std::cerr<<"Config file JSON parse error: \""
            <<kJSONSeed<<"\" is not unsigned integer"<<std::endl;
         return false;
      }
      seed=v[kJSONSeed].GetUint();
   }
   return true;
}

   bool
bf::CDCLConfig::Branching::SetJSON(const rapidjson::Value &v)
{
   if(!v.IsObject())
   {
      std::cerr<<"Config file JSON parse error: Property \"branching\" is not an object"<<std::endl;
      return false;
   }
   if(v.HasMember(kJSONVSIDSBump))
   {
      if(!v[kJSONVSIDSBump].IsDouble())
      {
         std::cerr<<"Config file JSON parse error: \"vsids_bump\" is not a double"<<std::endl;
         return false;
      }
      vsids_bump=v[kJSONVSIDSBump].GetDouble();
   }
   if(v.HasMember(kJSONChanceOfRandomPick))
   {
      if(!v[kJSONChanceOfRandomPick].IsUint())
      {
         std::cerr<<"Config file JSON parse error: \"chance_of_random_pick\" is not an unsigned integer"<<std::endl;
         return false;
      }
      chance_of_random_pick=v[kJSONChanceOfRandomPick].GetUint();
   }
   if(v.HasMember(kJSONBranchingHeuristic))
   {
      if(!v[kJSONBranchingHeuristic].IsString())
      {
         std::cerr<<"Config file JSON parse error: \"branching_heuristic\" is not a string"<<std::endl;
         return false;
      }
      std::string bh=v[kJSONBranchingHeuristic].GetString();
      branching_heuristic=kBranchingHeuristicSize;
      for(int i=0; i<kBranchingHeuristicSize; ++i)
      {
         if(bh==branching_heuristic_values[i])
         {
            branching_heuristic=static_cast<BranchingHeuristic>(i);
            break;
         }
      }
      if(branching_heuristic==kBranchingHeuristicSize)
      {
         std::cerr<<"Config file JSON parse error: \""<<bh
            <<"\" is not a valid name of a branching heuristic."<<std::endl;
         return false;
      }
   }
   if(branching_heuristic==kBranchWithFixedOrder)
   {
      if(!v.HasMember(kJSONLiteralOrderFile)
            || !v[kJSONLiteralOrderFile].IsString())
      {
         std::cerr<<"Config file JSON parse error: No literal order file provided with fixed order literal pick"<<std::endl;
         branching_heuristic=kBranchBinaryWithVSIDS;
         return false;
      }
      literal_order_file=v[kJSONLiteralOrderFile].GetString();
      if(!ReadLiteralOrder())
      {
         std::cerr<<"Config file: Failed to read literal order from file "
            <<v[kJSONLiteralOrderFile].GetString()<<std::endl;
         branching_heuristic=kBranchBinaryWithVSIDS;
         return false;
      }
   }
   return true;
}

   bool
bf::CDCLConfig::Branching::ReadLiteralOrder()
{
   std::ifstream in(literal_order_file.c_str());
   literal_order.clear();
   if(in.fail())
   {
      std::cerr<<"Failed to open the file with literal order \"";
      std::cerr<<literal_order_file;
      std::cerr<<"\""<<std::endl;
      return false;
   }
   while(!in.eof())
   {
      int lit=0;
      in>>lit;
      if(in.fail())
      {
         break;
      }
      unsigned ulit=2*abs(lit)+(lit<0 ? 1 : 0);
      literal_order.push_back(ulit);
   }
   return true;
}

   bool
bf::CDCLConfig::Learning::SetJSON(const rapidjson::Value &v)
{
   if(!v.IsObject())
   {
      std::cerr<<"Config file JSON parse error: Property \"learning\" is not an object"<<std::endl;
      return false;
   }
   if(v.HasMember(kJSONClauseMinimizationThreshold))
   {
      if(!v[kJSONClauseMinimizationThreshold].IsUint())
      {
         std::cerr<<"Config file JSON parse error: \"clause_minimization_threshold\" is not an unsigned integer"<<std::endl;
         return false;
      }
      clause_minimization_threshold=v[kJSONClauseMinimizationThreshold].GetUint();
   }
   if(v.HasMember(kJSONMinimizationMethod))
   {
      if(!v[kJSONMinimizationMethod].IsString())
      {
         std::cerr<<"Config file JSON parse error: \"minimization_method\" is not a string."<<std::endl;
         return false;
      }
      std::string mm=v[kJSONMinimizationMethod].GetString();
      minimization_method=kMinimizationMethodsSize;
      for(int i=0; i<kMinimizationMethodsSize; ++i)
      {
         if(mm==minimization_methods_values[i])
         {
            minimization_method=static_cast<MinimizationMethods>(i);
            break;
         }
      }
      if(minimization_method==kMinimizationMethodsSize)
      {
         std::cerr<<"Config file JSON parse error: Unknown minimization method \""
            <<mm<<"\"."<<std::endl;
         return false;
      }
   }
   return true;
}

   bool
bf::CDCLConfig::Restarts::SetJSON(const rapidjson::Value &v)
{
   if(!v.IsObject())
   {
      std::cerr<<"Config file JSON parse error: Property \"restarts\" is not an object"<<std::endl;
      return false;
   }
   if(v.HasMember(kJSONUseRestarts))
   {
      if(!v[kJSONUseRestarts].IsBool())
      {
         std::cerr<<"Config file JSON parse error: \"use_restarts\" is not bool"<<std::endl;
         return false;
      }
      use_restarts=v[kJSONUseRestarts].GetBool();
   }
   if(v.HasMember(kJSONFirstRestartConflict))
   {
      if(!v[kJSONFirstRestartConflict].IsUint())
      {
         std::cerr<<"Config file JSON parse error: \"first_restart_conflicts\" is not an unsigned integer"<<std::endl;
         return false;
      }
      first_restart_conflicts=v[kJSONFirstRestartConflict].GetUint();
   }
   if(v.HasMember(kJSONRestartConflictBump))
   {
      if(!v[kJSONRestartConflictBump].IsDouble())
      {
         std::cerr<<"Config file JSON parse error: \"restart_conflict_bump\" is not a double"<<std::endl;
         return false;
      }
      restart_conflict_bump=v[kJSONRestartConflictBump].GetDouble();
   }
   if(v.HasMember(kJSONClauseLengthBump))
   {
      if(!v[kJSONClauseLengthBump].IsDouble())
      {
         std::cerr<<"Config file JSON parse error: \"clause_length_bump\" is not a double"<<std::endl;
         return false;
      }
      clause_length_bump=v[kJSONClauseLengthBump].GetDouble();
   }
   if(v.HasMember(kJSONInitClauseLengthFactor))
   {
      if(!v[kJSONInitClauseLengthFactor].IsDouble())
      {
         std::cerr<<"Config file JSON parse error: \"init_clause_length_factor\" is not a double"<<std::endl;
         return false;
      }
      init_clause_length_factor=v[kJSONInitClauseLengthFactor].GetDouble();
   }

   if(v.HasMember(kJSONPruneLearnedClauses))
   {
      if(!v[kJSONPruneLearnedClauses].IsBool())
      {
         std::cerr<<"Config file JSON parse error: \"prune_learned_clauses\" is not bool"<<std::endl;
         return false;
      }
      prune_learned_clauses=v[kJSONPruneLearnedClauses].GetBool();
   }
   return true;
}

   void
bf::CDCLConfig::GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const
{
   if (d == NULL)
   {
      throw NullPointerException("CDCLConfig::GenerateJSON", "d == NULL");
   }
   d->SetObject();
   rapidjson::Value random_json;
   random.GenerateJSON(&random_json, allocator);
   d->AddMember(rapidjson::StringRef(Random::kJSONObjectName), random_json, allocator);
   rapidjson::Value branching_json;
   branching.GenerateJSON(&branching_json, allocator);
   d->AddMember(rapidjson::StringRef(Branching::kJSONObjectName), branching_json, allocator);
   rapidjson::Value learning_json;
   learning.GenerateJSON(&learning_json, allocator);
   d->AddMember(rapidjson::StringRef(Learning::kJSONObjectName), learning_json, allocator);
   rapidjson::Value restarts_json;
   restarts.GenerateJSON(&restarts_json, allocator);
   d->AddMember(rapidjson::StringRef(Restarts::kJSONObjectName), restarts_json, allocator);
}

   void
bf::CDCLConfig::Random::GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const
{
   if (d == NULL)
   {
      throw NullPointerException("CDCLConfig::Random::GenerateJSON", "d == NULL");
   }
   d->SetObject();
   d->AddMember(rapidjson::StringRef(kJSONSetSeed), set_seed, allocator);
   d->AddMember(rapidjson::StringRef(kJSONSeed), seed, allocator);
}

   void
bf::CDCLConfig::Branching::GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const
{
   if (d == NULL)
   {
      throw NullPointerException("CDCLConfig::Branching::GenerateJSON", "d == NULL");
   }
   d->SetObject();
   d->AddMember(rapidjson::StringRef(kJSONVSIDSBump), (double)vsids_bump, allocator);
   d->AddMember(rapidjson::StringRef(kJSONMaxVSIDSAdd), (double)max_vsids_add, allocator);
   d->AddMember(rapidjson::StringRef(kJSONChanceOfRandomPick), (unsigned)chance_of_random_pick, allocator);
   rapidjson::Value v;
   v.SetString(branching_heuristic_values[branching_heuristic], allocator);
   d->AddMember(rapidjson::StringRef(kJSONBranchingHeuristic), v, allocator);
   v.SetString(literal_order_file.c_str(), allocator);
   d->AddMember(rapidjson::StringRef(kJSONLiteralOrderFile), v, allocator);
}

   void
bf::CDCLConfig::Learning::GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const
{
   if (d == NULL)
   {
      throw NullPointerException("CDCLConfig::Learning::GenerateJSON", "d == NULL");
   }
   d->SetObject();
   d->AddMember(rapidjson::StringRef(kJSONClauseMinimizationThreshold), clause_minimization_threshold, allocator);
   rapidjson::Value v;
   v.SetString(minimization_methods_values[minimization_method], allocator);
   d->AddMember(rapidjson::StringRef(kJSONMinimizationMethod), v, allocator);
}

   void
bf::CDCLConfig::Restarts::GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const
{
   if (d == NULL)
   {
      throw NullPointerException("CDCLConfig::Restarts::GenerateJSON", "d == NULL");
   }
   d->SetObject();
   d->AddMember(rapidjson::StringRef(kJSONUseRestarts), use_restarts, allocator);
   d->AddMember(rapidjson::StringRef(kJSONFirstRestartConflict), (unsigned)first_restart_conflicts, allocator);
   d->AddMember(rapidjson::StringRef(kJSONRestartConflictBump), (double)restart_conflict_bump, allocator);
   d->AddMember(rapidjson::StringRef(kJSONClauseLengthBump), (double)clause_length_bump, allocator);
   d->AddMember(rapidjson::StringRef(kJSONInitClauseLengthFactor), (double)init_clause_length_factor, allocator);
   d->AddMember(rapidjson::StringRef(kJSONPruneLearnedClauses), prune_learned_clauses, allocator);
}
