//
// Project BFManip++
// 2017, Petr Kučera
//
/**@file cdclconfig.h
 * @brief Structure representing a configuration of CDCL solver.
 */

#ifndef __CDCLCONFIG_H
#define __CDCLCONFIG_H

#include <vector>
#include <iostream>
#include <rapidjson/document.h>

namespace bf
{
   /**@brief Structure representing a configuration of a CDCL solver.
    *
    * Can be read from JSON using RapidJSON
    */
   struct CDCLConfig
   {

      struct Random
      {
         static const char *kJSONObjectName;
         static const char *kJSONSetSeed;
         static const char *kJSONSeed;

         bool set_seed;
         unsigned seed;

         Random()
            : set_seed(false)
         {
            seed=(unsigned)time(NULL);
         }

         bool SetJSON(const rapidjson::Value &v);
         void GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const;
      };
      struct Branching
      {
         static const char *kJSONObjectName;
         enum BranchingHeuristic {
            kBranchBinaryWithVSIDS,
            kBranchWithVSIDS,
            kBranchWithFixedOrder,
            kBranchingHeuristicSize
         };

         static const char *branching_heuristic_values[];
         static const char *kJSONVSIDSBump;
         static const char *kJSONMaxVSIDSAdd;
         static const char *kJSONChanceOfRandomPick;
         static const char *kJSONBranchingHeuristic;
         static const char *kJSONLiteralOrderFile;

         /**@brief Constant determining multiplication bump of vsids
          * addition value.
          *
          * This is a value, by which #Parameters::vsids_add_ is multiplied each
          * time a clause is learned. For more details see
          * #PickBranchingLiteral(int*) and #ConflictAnalysis(int*).
          * The value of this constant is 2.0.
          */
         long double vsids_bump;
         /**@brief Constant determining maximum value of vsids addition
          * value before decay is performed.
          *
          * This is a value, which determines maximum value of
          * #vsids_add_, when this value is reached, #Decay() is
          * called. For more detail see also #Run(Input*).
          * The value of this constant is <tt>LDBL_MAX/16.0</tt>, where
          * @p LDBL_MAX is the maximum value of a <tt>long double</tt> variable.
          */
         long double max_vsids_add;
         /**@brief Chance of picking the next literal by random
          * instead of using branching heuristic, in per mille.
          */
         unsigned chance_of_random_pick;
         BranchingHeuristic branching_heuristic;
         std::string literal_order_file;
         std::vector<unsigned> literal_order;

         Branching()
            : vsids_bump(2.0),
            max_vsids_add(std::numeric_limits<double>::max()/16.0),
            chance_of_random_pick(10),
            branching_heuristic(kBranchBinaryWithVSIDS)
         { }

         bool SetJSON(const rapidjson::Value &v);
         bool ReadLiteralOrder();
         void GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const;
      };
      struct Learning
      {
         static const char *kJSONObjectName;
         enum MinimizationMethods {
            kNoMinimization,
            kMinimizeLocallyByResolution,
            kMinimizeLocallyByMarking,
            kMinimizeGlobally,
            kMinimizationMethodsSize
         };

         static const char *minimization_methods_values[];
         static const char *kJSONClauseMinimizationThreshold;
         static const char *kJSONMinimizationMethod;

         /**@brief A clause must have length bigger than this bound to
          * be minimized.
          */
         unsigned clause_minimization_threshold;
         MinimizationMethods minimization_method;

         Learning()
            : clause_minimization_threshold(4),
            minimization_method(kMinimizeGlobally)
         { }

         bool SetJSON(const rapidjson::Value &v);
         void GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const;
      };
      struct Restarts
      {
         static const char *kJSONObjectName;
         static const char *kJSONUseRestarts;
         static const char *kJSONFirstRestartConflict;
         static const char *kJSONRestartConflictBump;
         static const char *kJSONClauseLengthBump;
         static const char *kJSONInitClauseLengthFactor;
         static const char *kJSONPruneLearnedClauses;

         bool use_restarts;
         long unsigned int first_restart_conflicts;
         long double restart_conflict_bump;
         long double clause_length_bump;
         long double init_clause_length_factor;
         bool prune_learned_clauses;

         Restarts()
            : use_restarts(true),
            first_restart_conflicts(128),
            restart_conflict_bump(1.5),
            clause_length_bump(1.1),
            init_clause_length_factor(1.5),
            prune_learned_clauses(true)
         { }

         bool SetJSON(const rapidjson::Value &v);
         void GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const;
      };

      Random random;
      Branching branching;
      Learning learning;
      Restarts restarts;

      CDCLConfig() {}
      CDCLConfig(std::istream &json);

      bool ReadJSON(std::istream &json);
      void WriteJSON(std::ostream &json) const;
      void Dump(std::ostream &out) const;
      void GenerateJSON(rapidjson::Value *d,
               rapidjson::Document::AllocatorType &allocator) const;
   };
};

#endif
