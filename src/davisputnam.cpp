//
//  davisputnam.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 11.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include "davisputnam.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

bool bf::DavisPutnam::AddClauseToBucket (bf::DavisPutnam::DPClause * clause)
{
   // Simplest way, we add at the end and we don't care about
   // duplicities and subclauses.
   // Also for the sake of speed we don't check whether clause is
   // NULL, whether clause is empty,
   // or whether bucket is not out of bounds, caller must make
   // sure that.
   int bucket = clause -> literals[0];
   return var_buckets_[bucket].clause_set.insert(clause).second;
}

bf::DavisPutnam::DPClause * bf::DavisPutnam::DPClauseFromClause (
      const bf::Clause * clause)
{
   if (clause == NULL 
         || clause->NumberOfLiterals() == 0)
   {
      return NULL;
   }
   std::set<unsigned int> literals;
   if (variable_order_ == NULL)
   {
      literals.insert (clause->literals().begin(), clause->literals().end());
   }
   else
   {
      for (std::set<unsigned int>::const_iterator lit = clause->literals().begin();
            lit != clause->literals().end();
            ++ lit)
      {
         literals.insert (2 * variable_order_[(* lit) / 2] + (* lit) % 2);
      }
   }
   DPClause * dpclause = (DPClause *) malloc (sizeof (DPClause));
   if (dpclause == NULL)
   {
      return NULL;
   }
   dpclause->literals = (int*) malloc (literals.size()*sizeof(int));
   if (dpclause->literals == NULL)
   {
      free (dpclause);
      return NULL;
   }
   dpclause->next = NULL;
   dpclause->positive_parent = NULL;
   dpclause->negative_parent = NULL;
   dpclause->parent_variable = 0;
   dpclause->number_of_literals = static_cast<int>(literals.size());
   int lit_ind = 0;
   for (std::set<unsigned int>::const_iterator lit = literals.begin();
         lit != literals.end();
         ++ lit)
   {
      dpclause->literals[lit_ind++] = (*lit);
   }
   return dpclause;
}

bf::Result bf::DavisPutnam::BuildDataStructures ()
{
   std::string output_string;
   cnf.ToString(NULL, &output_string);
   std::cout<<output_string<<std::endl;
   // var_buckets_ = (VarBucket *) malloc (2 * cnf.NumberOfVariables () * sizeof (VarBucket));
   var_buckets_ = new VarBucket[2 * cnf.NumberOfVariables()];
#if 0
   for (int i = 0;
         i < static_cast<int>(2 * cnf.NumberOfVariables ());
         ++ i)
   {
      var_buckets_ [i].head = NULL;
      var_buckets_ [i].tail = NULL;
   }
#endif
   for (CNF::ElementList::const_iterator element
         = cnf.elements ().begin ();
         element != cnf.elements ().end ();
         ++ element)
   {
      output_string.clear();
      (* element)->ToString(NULL, &output_string);
      std::cout<<output_string<<std::endl;
      DPClause * clause = DPClauseFromClause ((* element));
      if (clause == NULL)
      {
         fputs ("[bf::DavisPutnam::BuildDataStructures] Failed to allocate clause when building data structures", stderr);
         return kFailed;
      }
      if (clause->number_of_literals == 0)
      {
         free (clause);
      }
      else
      {
         AddClauseToBucket (clause);
      }
   }
   //Allocate buffer
   merge_buffer_ = (int *) malloc (2 * cnf.NumberOfVariables ());
   if (merge_buffer_ == NULL)
   {
      fputs ("[bf::DavisPutnam::BuildDataStructures] Failed to allocate merge_buffer", stderr);
      return kFailed;
   }
   return kUndetermined;
}

void bf::DavisPutnam::ReleaseDataStructures ()
{
   if (merge_buffer_ != NULL)
   {
      free (merge_buffer_);
      merge_buffer_ = NULL;
   }
   if (var_buckets_ != NULL)
   {
      for (int bucket = 0;
            bucket < static_cast<int>(2 * cnf.NumberOfVariables());
            ++ bucket)
      {
         for (DPClauseSet::iterator clause 
               = var_buckets_[bucket].clause_set.begin();
               clause != var_buckets_[bucket].clause_set.end();
               ++ clause)
         {
            free (*clause);
         }
#if 0
         while (var_buckets_[bucket].head != NULL)
         {
            DPClause * clause = var_buckets_[bucket].head;
            var_buckets_[bucket].head = var_buckets_[bucket].head->next;
            free (clause->literals);
            free (clause);
         }
         var_buckets_[bucket].tail = NULL;
#endif
      }
   }
   delete[] var_buckets_;
   //free (var_buckets_);
   var_buckets_ = NULL;
}

bf::Result bf::DavisPutnam::TestSatisfiability (bf::Input * input)
{
   Result result = BuildDataStructures ();
//   for (int bucket = 0; bucket < static_cast<int>(cnf.NumberOfVariables()) * 2; ++ bucket)
//      WriteBucket(bucket, stderr);
   int variable = 0;
   for (variable = 0;
         result == kUndetermined && variable < static_cast<int>(cnf.NumberOfVariables()) - 1;
         ++ variable)
   {
      result = ProcessVariable (variable);
   }
   if (result == kUndetermined)
   {
      if (var_buckets_[2 * variable].clause_set.size() != 0
            && var_buckets_[2 * variable + 1].clause_set.size () != 0)
      {
         result = kUnsatisfiable;
      }
      else
      {
         result = kSatisfiable;
      }
   }
   fputc('\n', stderr);
//   for (int bucket = 0; bucket < static_cast<int>(cnf.NumberOfVariables()) * 2; ++ bucket)
//      WriteBucket(bucket, stderr);
   if (result == kSatisfiable)
   {
      bf::PartialInput partial_input;
      result = ExtractSatisfyingAssignment (&partial_input);
      if (variable_order_ == NULL)
      {
         for (variable = 0;
               variable < static_cast<int>(partial_input.size());
               ++ variable)
         {
            input->push_back(partial_input[variable] == bf::kTrueValue);
         }
      }
      else
      {
         input->assign(cnf.NumberOfVariables(), false);
         for (variable = 0;
               variable < static_cast<int>(partial_input.size());
               ++ variable)
         {
            (*input)[variable] = 
               (partial_input[variable_order_[variable]] == bf::kTrueValue);
         }
      }
   }
   ReleaseDataStructures ();
   return result;
}

bf::Result bf::DavisPutnam::ProcessVariable(int variable)
{
   std::cerr << "Processing variable " << variable << "." << std::endl;
   std::cerr << "Size of positive bucket: " << var_buckets_[2*variable].clause_set.size() << std::endl;
   std::cerr << "Size of negative bucket: " << var_buckets_[2*variable+1].clause_set.size() << std::endl;
   int number_of_resolvents = 0;
   for (DPClauseSet::iterator positive = var_buckets_[2 * variable].clause_set.begin();
         positive != var_buckets_[2 * variable].clause_set.end();
         ++ positive)
   {
      for (DPClauseSet::iterator negative = var_buckets_[2 * variable + 1].clause_set.begin();
            negative != var_buckets_[2 * variable + 1].clause_set.end();
            ++ negative)
      {
         // Try to merge these clauses except the first literal into
         // merge_buffer_.
         int literals = 0;
         int literal_in_pos = 1;
         int literal_in_neg = 1;
         bool found_conflict = false;
         while (literal_in_pos < (*positive)->number_of_literals
               && literal_in_neg < (*negative)->number_of_literals)
         {
            if ((*positive)->literals[literal_in_pos] / 2
                  == (*negative)->literals[literal_in_neg] / 2)
            {
               if ((*positive)->literals[literal_in_pos]
                   != (*negative)->literals[literal_in_neg])
               {
                  found_conflict = true;
                  break;
               }
               else
               {
                  merge_buffer_[literals++] =
                     (*positive)->literals[literal_in_pos++];
                  ++literal_in_neg;
               }
            }
            else if ((*positive)->literals[literal_in_pos]
                  < (*negative)->literals[literal_in_neg])
            {
               merge_buffer_[literals ++] =
                  (*positive)->literals[literal_in_pos ++];
            }
            else
            {
               merge_buffer_[literals ++] =
                  (*negative)->literals[literal_in_neg ++];
            }
         }
         if (found_conflict)
         {
            continue;
         }
         while (literal_in_pos < (*positive)->number_of_literals)
         {
            merge_buffer_[literals ++] = (*positive)->literals[literal_in_pos ++];
         }
         while (literal_in_neg < (*negative)->number_of_literals)
         {
            merge_buffer_[literals ++] = (*negative)->literals[literal_in_neg ++];
         }
         if (literals == 0)
         {
            return kUnsatisfiable;
         }
         DPClause * resolvent = (DPClause *) malloc (sizeof(DPClause));
         if (resolvent == NULL)
         {
            fputs ("[bf::DavisPutnam::ProcessVariable] Failed to allocate resolvent", stderr);
            return kFailed;
         }
         resolvent -> next = NULL;
         resolvent -> positive_parent = (*positive);
         resolvent -> negative_parent = (*negative);
         resolvent -> parent_variable = variable;
         resolvent -> number_of_literals = literals;
         resolvent -> literals = (int *)malloc (literals * sizeof (int));
         if (resolvent ->literals == NULL)
         {
            fputs ("[bf::DavisPutnam::ProcessVariable] Failed to allocate resolvent->literals", stderr);
            free (resolvent);
            return kFailed;
         }
         memcpy (resolvent->literals, merge_buffer_, literals * sizeof(int));
         number_of_resolvents += AddClauseToBucket(resolvent);
      }
   }

   std::cerr << "Number of resolvents added: " << number_of_resolvents << std::endl;
   return kUndetermined;
}

bf::Result bf::DavisPutnam::ExtractSatisfyingAssignment(bf::PartialInput * partial_input)
{
   partial_input->assign(cnf.NumberOfVariables(), bf::kUnknownValue);
   for (int variable = cnf.NumberOfVariables() - 1;
         variable >= 0;
         -- variable)
   {
      bool positive_satisfied = true;
      for (DPClauseSet::iterator positive = var_buckets_[2 * variable].clause_set.begin();
            positive != var_buckets_[2 * variable].clause_set.end() && positive_satisfied;
            ++ positive)
      {
         bf::PIValues value = EvaluateDPClause (*positive, partial_input);
         if (value == kUnknownValue)
         {
            positive_satisfied = false;
         }
         else if (value == kFalseValue)
         {
            // There should not be any falsely evaluated clause,
            // because every clause in the list contains variable,
            // which has not been assigned a value yet.
            std::cerr <<
               "[bf::DavisPutnam::ExtractSatisfyingAssignment] A\
               clause in positive list evaluated to false." << std::endl;
            std::cerr << "[...] Value: " << value << std::endl;
            std::string output_string;
            PartialInputToString(*partial_input, &output_string);
            std::cerr << "[...] Input: " << output_string << std::endl;
            std::cerr << "[...] Clause: ";
            for (int lind = 0;
                  lind < (*positive)->number_of_literals;
                  lind ++)
            {
               if ((*positive)->literals[lind] % 2)
               {
                  std::cerr << '-';
               }
               std::cerr << (*positive)->literals[lind] / 2 << " ";
            }
            std::cerr << std::endl;
            return kFailed;
         }
      }
      if (positive_satisfied)
      {
         (*partial_input)[variable] = bf::kFalseValue;
      }
      else
      {
         (*partial_input)[variable] = bf::kTrueValue;
         // The following is here mainly for debugging purposes, 
         // it should not be neccessary.
         bool negative_satisfied = true;
         for (DPClauseSet::iterator negative =
               var_buckets_[2 * variable + 1].clause_set.begin();
               negative != var_buckets_[2 * variable + 1].clause_set.end()
               && negative_satisfied;
               ++ negative)
         {
            bf::PIValues value = EvaluateDPClause ((*negative), partial_input);
            if (value == kUnknownValue)
            {
               negative_satisfied = false;
            }
            else if (value == kFalseValue)
            {
               // There should not be any falsely evaluated clause,
               // because every clause in the list contains variable,
               // which has not been assigned a value yet.
               std::cerr << "[bf::DavisPutnam::ExtractSatisfyingAssignment] A clause in negative list evaluated to false." << std::endl;
               std::cerr << "[...] Value: " << value << std::endl;
               std::string output_string;
               PartialInputToString(*partial_input, &output_string);
               std::cerr << "[...] Input: " << output_string << std::endl;
               std::cerr << "[...] Clause: ";
               for (int lind = 0;
                     lind < (*negative)->number_of_literals;
                     lind ++)
               {
                  if ((*negative)->literals[lind] % 2)
                  {
                     std::cerr << '-';
                  }
                  std::cerr << (*negative)->literals[lind] / 2 << " ";
               }
               std::cerr << std::endl;
               return kFailed;
            }
         }
         if (! negative_satisfied)
         {
            std::cerr << "[bf::DavisPutnam::ExtractSatisfyingAssignment] In both lists an unsatisfied clause was found." << std::endl;
            return kFailed;
         }
      }
   }
   return kSatisfiable;
}

bf::PIValues bf::DavisPutnam::EvaluateDPClause (bf::DavisPutnam::DPClause * clause,
      bf::PartialInput * partial_input)
{
   PIValues value = kFalseValue;
 
   for (int literal_ind = 0;
         value != kTrueValue && literal_ind < clause->number_of_literals;
         ++ literal_ind)
   {
      if ((*partial_input)[clause->literals[literal_ind] / 2] == kUnknownValue)
      {
         value = kUnknownValue;
      }
      else if (
            (clause->literals[literal_ind] % 2 == 0)
            == 
            ((*partial_input)[clause->literals[literal_ind] / 2] == bf::kTrueValue))
      {
         value = kTrueValue;
      }
   }
   return value;
}

void bf::DavisPutnam::WriteBucket (int bucket, FILE * file)
{
   if (bucket % 2)
   {
      fprintf (file, "bucket %3d: ", - bucket / 2);
   }
   else
   {
      fprintf (file, "bucket %3d: ", bucket / 2);
   }
   for (DPClauseSet::iterator clause = var_buckets_[bucket].clause_set.begin();
         clause != var_buckets_[bucket].clause_set.end();
         ++ clause)
   {
      fputc ('(', file);
      for (int vp = 0; vp<(*clause)->number_of_literals; ++vp)
      {
         if ((*clause)->literals[vp] % 2)
         {
            fputc('-', file);
         }
         fprintf (file, "%d", (*clause)->literals[vp] / 2);
         if (vp + 1 < (*clause)->number_of_literals)
         {
            fputc(' ', file);
         }
      }
      fputc (')', file);
   }
   fputc ('\n', file);
}


bool  bf::DavisPutnam::CompareDPClauses::operator() (const bf::DavisPutnam::DPClause * cl1, const bf::DavisPutnam::DPClause * cl2) const
{
   int lit1 = 0;
   int lit2 = 0;
   if (cl1->number_of_literals < cl2->number_of_literals)
   {
      return false;
   }
   if (cl1->number_of_literals > cl2->number_of_literals)
   {
      return true;
   }
   while (lit1 < cl1->number_of_literals
         && lit2 < cl2->number_of_literals)
   {
      if (cl1->literals[lit1++]>cl2->literals[lit2++])
      {
         return true;
      }
   }
   return false;
}
