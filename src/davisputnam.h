//
//  davisputnam.h
//  bfmanip++
//
//  Created by Petr Kučera on 11.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
/**@file davisputnam.h
 * @brief Contains Davis Putnam satisfiability algorithm, which is
 * based on directed resolution.
 */
#ifndef DAVISPUTNAM_H
#define DAVISPUTNAM_H

#include <set>

#include "normalform.h"
#include "bfutils.h"

namespace bf {
   /**@brief Class encapsulating an instance of Davis Putnam
    * algorithm based on directed resolution.
    */
   class DavisPutnam {
    public:
      struct DPClause {
         DPClause * next;
         DPClause * positive_parent;
         DPClause * negative_parent;
         int parent_variable;
         int number_of_literals;
         int * literals;
      };

      struct CompareDPClauses 
      {
         bool operator () (const DPClause * cl1, const DPClause * cl2) const;
      };

      typedef std::set<DPClause *, CompareDPClauses> DPClauseSet;

      struct VarBucket {
         DPClauseSet clause_set;
         inline VarBucket() : clause_set() {}
      };

    private:
     
      /**@brief CNF object on which we shall test satisfiability.
       *
       * It is a constant reference, which can be initialized only in
       * constructor.
       */
      const CNF&  cnf;

      /**@brief Variable ordering.
       *
       * It is an array of integers of lengths cnf.NumberOfVariables()
       * defining permutation of variables.
       */
      int * variable_order_;

      /**@brief A bucket for every literal.
       *
       * Positive literals are at even position (2 * (variable - 1)),
       * negative at even positions (2 * (variable - 1) + 1).
       */
      VarBucket * var_buckets_;

      /**@brief Buffer for merging clauses. */
      int * merge_buffer_;

    private:
      /**@brief Constructor without parameters.
       *
       * It is not allowed to call it due to the presence of cnf
       * reference variable.
       */
      DavisPutnam ();

      /**@brief Initalizes data structures.
       *
       * @return True, if building was successfull, false otherwise.
       */
      Result BuildDataStructures ();

      /**@brief Frees memory allocated for data structures.
       */
      void ReleaseDataStructures ();

      /**@brief Adds given clause to given bucket.
       *
       * It is added to the right bucket, if clause is nonempty.
       */
      bool AddClauseToBucket (DPClause * clause);

      /**@brief Creates newly allocated clause structure from given
       * element.
       *
       * To save number of allocations a bit, DPClause structure is
       * allways allocated at once together clause structure and array
       * with literals, pointer to which is set accordingly. Only one
       * free() call is then needed to release memory. Variables are
       * renumbered according to stored ordering, i.e. number of
       * variable var is changed to variable_order_[var]. If
       * variable_order_ is NULL, then numbers of variables are left
       * untouched. Literals in clause are ordered by their new
       * numbers.
       */
      DPClause * DPClauseFromClause (const Clause * clause);

      /**@brief By considering buckets for positive and negative
       * literal with given variable performs all resolutions over
       * given variable.
       */
      Result ProcessVariable (int variable);

      /**@brief Extracts satisfying assignment from the data
       * structures.
       */
      Result ExtractSatisfyingAssignment (PartialInput * input);

      /**@brief Evaluates a clause on given partial input.
       */
      PIValues EvaluateDPClause (DPClause * clause, PartialInput * partial_input);
     
      DavisPutnam(const DavisPutnam&);               
      void operator=(const DavisPutnam&);

      void WriteBucket (int bucket, FILE * file);
    public:
      /**@brief Constructor.
       *
       * @param a_cnf Reference to cnf we shall test satisfiability
       * of. Number of variables is taken from cnf.NumberOfVariables,
       * thus make sure, it is set appropriately. Indices of variables
       * in cnf must be between [0...cnf.NumberOfVariables()-1].
       * @param var_order Variable ordering. It can be NULL, in which
       * case ordering imposed by variable indices is used. If it is
       * not NULL, then it must point to an array of (at least)
       * cnf.NumberOfVariables() and it must define permutation of
       * variables.
       */
      inline DavisPutnam (const CNF& a_cnf,
            int * var_order) :
         cnf (a_cnf),
         variable_order_ (var_order),
         var_buckets_ (NULL),
         merge_buffer_ (NULL)
      {}

      /**@brief Destroys all data structure.
       */
      inline ~DavisPutnam ()
      {
         ReleaseDataStructures ();
      }

      /**@brief Runs the satisfiability tester and returns the result.
       *
       * If the associated CNF is satisfiable, then input is filled
       * with satisfying input.
       *
       * @param input Output variable for storing satisfying input in
       * case associated CNF is satisfiable.
       *
       * @return Appropriate constant giving result of the algorithm.
       * I.e. kUnsatisfiable in case of unsatisfiable CNF,
       * kSatisfiable in case of satisfiable CNF, kFailed in case of
       * failure (mostly due to the lack of memory), kBadParameters in
       * case of bad parameters (here or during construction).
       */
      Result TestSatisfiability (bf::Input * input);
   };
}

#endif
