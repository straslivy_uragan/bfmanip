//
//  dpll.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 17.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include <stdlib.h>
#include <string.h>

#include "dpll.h"

bf::Result bf::DPLL::AllocateLevel (int level)
{
   level_data_[level].assigned_values  = 
      (PIValues *) malloc (number_of_variables_ * sizeof (PIValues));
   if (level_data_[level].assigned_values == NULL)
   {
      return kFailed;
   }
   level_data_[level].clauses =
      (DPLLClause *) malloc (number_of_clauses_ * sizeof (DPLLClause));
   if (level_data_[level].clauses == NULL)
   {
      return kFailed;
   }
   for (int clause = 0; clause < number_of_clauses_; ++ clause)
   {
      level_data_[level].clauses[clause].literals = NULL;
   }
   for (int clause = 0; clause < number_of_clauses_; ++ clause)
   {
      level_data_[level].clauses[clause].literals =
         (int *) malloc (number_of_variables_ * sizeof (int));
      if (level_data_[level].clauses[clause].literals == NULL)
      {
         return kFailed;
      }
   }
   level_data_[level].unit_clauses =
      (int *) malloc (number_of_clauses_ * sizeof (int));
   if (level_data_[level].unit_clauses == NULL)
   {
      return kFailed;
   }
   level_data_[level].num_unit_clauses = 0;
   level_data_[level].num_clauses = 0;
   level_data_[level].chosen_literal = 0;
   level_data_[level].literal_info =
      (LiteralInfo *) malloc (number_of_literals_ * sizeof(LiteralInfo));
   if (level_data_[level].literal_info == NULL)
   {
      return kFailed;
   }
   for (int literal = 0; literal < number_of_literals_; ++ literal)
   {
      level_data_[level].literal_info[literal].clause_list = NULL;
      level_data_[level].literal_info[literal].number_of_clauses = 0;
      level_data_[level].literal_info[literal].number_of_quadratic = 0;
   }
   for (int literal = 0; literal < number_of_literals_; ++ literal)
   {
      level_data_[level].literal_info[literal].clause_list = 
         (int *) malloc (number_of_clauses_ * sizeof (int));
   }
   return kUndetermined;
}

bf::Result bf::DPLL::BuildDataStructures ()
{
   max_level_ = 0;
   number_of_processed_literals_ = 0;
   satisfying_assignment_ = (PIValues *) malloc (
         number_of_variables_ * sizeof (PIValues));
   if (satisfying_assignment_ == NULL)
   {
      return kFailed;
   }
   level_data_ = (LevelData *) malloc (number_of_variables_ * sizeof (LevelData));
   if (level_data_ == NULL)
   {
      return kFailed;
   }
   for (int level = 0; level < number_of_variables_; ++ level)
   {
      level_data_[level].assigned_values  = NULL;
      level_data_[level].clauses = NULL;
      level_data_[level].unit_clauses = NULL;
      level_data_[level].literal_info = NULL;
   }
   if (AllocateLevel (0) == kFailed)
   {
      return kFailed;
   }
  
   // Initialize first level.
   for (int variable = 0; variable < number_of_variables_; ++ variable)
   {
      level_data_[0].assigned_values[variable] = kUnknownValue;
   }
   for (int literal = 0; literal < number_of_literals_; ++ literal)
   {
      level_data_[0].literal_info[literal].number_of_clauses = 0;
      level_data_[0].literal_info[literal].number_of_quadratic = 0;
   }
   level_data_[0].chosen_literal = 0; // This value is uninportant - there is no chosen literal at level 0.
   level_data_[0].num_unit_clauses = 0;
   level_data_[0].num_clauses = 0;
   int clause_ind = 0;
   for (CNF::ElementList::const_iterator element = cnf_.elements().begin();
         element != cnf_.elements().end();
         ++ element)
   {
      if ((*element)->NumberOfLiterals () == 0)
      {
         return kUnsatisfiable;
      }
      if ((*element)->ContainsConflictOccurrence())
      {
         continue;
      }
      int literal_ind = 0;
      for (std::set<unsigned int>::const_iterator elem_lit 
            = (*element)->literals().begin();
            elem_lit != (*element)->literals().end();
            ++ elem_lit)
      {
         level_data_[0].clauses[clause_ind].literals[literal_ind ++]
            = (*elem_lit);
         int clind = level_data_[0].literal_info[(*elem_lit)].number_of_clauses ++;
         level_data_[0].literal_info[(*elem_lit)].clause_list[clind] = clause_ind;
         if ((*element)->NumberOfLiterals() == 2)
         {
            ++ level_data_[0].literal_info[(*elem_lit)].number_of_quadratic;
         }
      }
      level_data_[0].clauses[clause_ind].number_of_literals = literal_ind;
      if (literal_ind == 1)
      {
         level_data_[0].unit_clauses[level_data_[0].num_unit_clauses ++] = level_data_[0].clauses[clause_ind].literals[0];
      }
      ++ clause_ind;
   }
   level_data_[0].num_clauses = clause_ind;
   return kUndetermined;
}

void bf::DPLL::ReleaseDataStructures ()
{
   if (level_data_ != NULL)
   {
      for (int level = 0; level < number_of_variables_; ++ level)
      {
         if (level_data_[level].assigned_values != NULL)
         {
            free (level_data_[level].assigned_values);
            level_data_[level].assigned_values = NULL;
         }
         if (level_data_[level].clauses != NULL)
         {
            for (int clause = 0; clause < number_of_clauses_; ++ clause)
            {
               if (level_data_[level].clauses[clause].literals != NULL)
               {
                  free (level_data_[level].clauses[clause].literals);
                  level_data_[level].clauses[clause].literals = NULL;
               }
            }
            free (level_data_[level].clauses);
            level_data_[level].clauses = NULL;
         }
         if (level_data_[level].unit_clauses != NULL)
         {
            free (level_data_[level].unit_clauses);
            level_data_[level].unit_clauses = NULL;
         }
         if (level_data_[level].literal_info != NULL)
         {
            for (int literal=0; literal<number_of_literals_; ++literal)
            {
               if (level_data_[level].literal_info[literal].clause_list != NULL)
               {
                  free (level_data_[level].literal_info[literal].clause_list);
                  level_data_[level].literal_info[literal].clause_list = NULL;
               }
            }
            free (level_data_[level].literal_info);
            level_data_[level].literal_info = NULL;
         }
      }
      free (level_data_);
   }
}

bf::Result bf::DPLL::ChooseLiteral (int level, int * chosen_literal)
{
   // We choose the literal with the biggest number of quadratic
   // clauses it is present in.
   // First find first unassigned literal.
   int var_index= 0;
   for (var_index = 0;
         var_index < number_of_variables_;
         ++ var_index)
   {
  //    fprintf (stderr, "%d=%d\n", var_index, level_data_[level].assigned_values[var_index]);
      if (level_data_[level].assigned_values[var_index] == kUnknownValue)
      {
         break;
      }
   }
   if (var_index == number_of_variables_)
   {
      //fprintf (stderr, "%d\n", number_of_variables_);
      fprintf (stderr, "[bf::DPLL::ChooseLiteral] All variables are already assigned a value at level %d.\n", level);
      return kFailed;
   }
   (*chosen_literal) = 2 * var_index;
   for (int lit_index = (*chosen_literal) + 1;
         lit_index < number_of_literals_;
         ++ lit_index)
   {
      if (level_data_[level].assigned_values[lit_index/2] == kUnknownValue
            && (level_data_[level].literal_info[lit_index].number_of_quadratic >
               level_data_[level].literal_info[(*chosen_literal)].number_of_quadratic))
      {
         (*chosen_literal) = lit_index;
      }
   }
   return kUndetermined;
}

bf::Result bf::DPLL::UnitPropagation (int level)
{
   while (level_data_[level].num_unit_clauses > 0)
   {
      int unit_literal = level_data_[level].unit_clauses[
         --level_data_[level].num_unit_clauses];
      //fprintf (stderr, "[bf::DPLL::UnitPropagation] L:%d - Considering %d\n", level,
      //      unit_literal);
      if (level_data_[level].assigned_values[unit_literal/2] != kUnknownValue)
      {
         continue;
      }
      level_data_[level].assigned_values[unit_literal/2] = 
         (unit_literal % 2 ? kFalseValue: kTrueValue);
      //fprintf (stderr, "[bf::DPLL::UnitPropagation] L:%d - Assigning value to var %d=%d\n",
      //      level,
      //      unit_literal / 2, level_data_[level].assigned_values[unit_literal/2]);
      /* All clauses, in which unit_literal is satisfied shall be
       * removed. Removal would be complicated however, thus we make
       * them empty and we leave them in CNF, thus denoting, that they
       * are in fact satisfied (though empty). The clause is not
       * removed from the lists of other literals, if later these
       * literals are used for unit propagation, then we take it into
       * account.
       */
      for (int clause_in_list = 0;
            clause_in_list < level_data_[level].literal_info[unit_literal].number_of_clauses;
            ++ clause_in_list)
      {
         DPLLClause * clause = 
            level_data_[level].clauses +
            level_data_[level].literal_info[unit_literal].clause_list[clause_in_list];
         clause->number_of_literals = 0;
      }
      int neg_literal = 2 * (unit_literal / 2) + (1 - unit_literal % 2);
      for (int clause_in_list = 0;
            clause_in_list < level_data_[level].literal_info[neg_literal].number_of_clauses;
            ++ clause_in_list)
      {
         DPLLClause * clause = 
            level_data_[level].clauses +
            level_data_[level].literal_info[neg_literal].clause_list[clause_in_list];
         int literal_ind = 0;
         while (literal_ind < clause->number_of_literals)
         {
            if (clause->literals[literal_ind] == neg_literal)
            {
               break;
            }
            ++ literal_ind;
         }
         if (literal_ind < clause->number_of_literals)
         {
            
            -- clause->number_of_literals;
            if (literal_ind < clause->number_of_literals)
            {
               clause->literals[literal_ind] =
                  clause->literals[clause->number_of_literals];
               clause->literals[clause->number_of_literals] = neg_literal;
            }
            if (clause->number_of_literals == 0)
            {
               return kUnsatisfiable;
            }
            if (clause->number_of_literals == 1)
            {
               level_data_[level].unit_clauses[
                  level_data_[level].num_unit_clauses ++] = clause->literals[0];
            }
         }
      }
   }
   // Test whether some nontrivial clause remains
   int clause = 0;
   for (clause = 0; clause < level_data_[level].num_clauses; ++ clause)
   {
      if (level_data_[level].clauses[clause].number_of_literals)
      {
         break;
      }
   }
   if (clause == level_data_[level].num_clauses)
   {
      memcpy (satisfying_assignment_, level_data_[level].assigned_values,
            number_of_variables_ * sizeof (PIValues));
      return kSatisfiable;
   }
   return kUndetermined;
}

bf::Result bf::DPLL::PrepareLevel (int level, int literal)
{
   // Copy the structure from given level to the next one while
   // assigning literal the satisfying value.
   // Assigned values.
   if (level + 1 > max_level_
         && AllocateLevel (level + 1) == kFailed)
   {
      return kFailed;
   }

   memcpy (level_data_[level+1].assigned_values,
         level_data_[level].assigned_values, number_of_variables_*sizeof(PIValues));
   level_data_[level+1].assigned_values[literal/2] = 
      (literal % 2 ? kFalseValue : kTrueValue);
   // Clauses and literals.
   level_data_[level+1].num_clauses = 0;
   level_data_[level+1].num_unit_clauses = 0;
   for (int lit_ind = 0; lit_ind < number_of_literals_; ++lit_ind)
   {
      level_data_[level+1].literal_info[lit_ind].number_of_clauses = 0;
      level_data_[level+1].literal_info[lit_ind].number_of_quadratic = 0;
   }
   int new_clause_it = 0;
   for (int old_clause_it = 0; old_clause_it < level_data_[level].num_clauses; ++old_clause_it)
   {
      /* If a clause is empty and we did not report it during previous
       * unit propagation, then it is because it is in fact satisfied
       * but we could not simply remove it from the list during unit
       * propagation, because it would cause renumbering all clauses.
       */
      DPLLClause * old_clause = level_data_[level].clauses + old_clause_it;
      if (old_clause->number_of_literals == 0)
      {
         continue;
      }
      bool clause_sat = false;
      /* First we check, if clause is satisfied, i.e. whether it shall
       * be included in copy. */
      for (int old_literal= 0;
            !clause_sat && old_literal < old_clause->number_of_literals;
            ++ old_literal)
      {
         clause_sat = (old_clause->literals[old_literal] == literal);
      }
      if (clause_sat)
      {
         continue;
      }
      int last_literal = 0;
      DPLLClause * new_clause = level_data_[level+1].clauses + new_clause_it;
      for (int old_literal = 0;
            old_literal < old_clause->number_of_literals;
            ++ old_literal)
      {
         if (old_clause->literals[old_literal]/2 != literal / 2)
         {
            int lc=old_clause->literals[old_literal];
            new_clause->literals[last_literal++]=lc;
            LiteralInfo * lc_info = level_data_[level+1].literal_info + lc;
            lc_info->clause_list[lc_info->number_of_clauses++]=new_clause_it;
         }
      }
      new_clause->number_of_literals = last_literal;
      if (last_literal == 0)
      {
         return kUnsatisfiable;
      }
      if (last_literal == 1)
      {
         // fprintf (stderr, "Found unit literal %d.\n", new_clause->literals[0]);
         level_data_[level+1].unit_clauses[
            level_data_[level+1].num_unit_clauses++]=new_clause->literals[0];
      }
      else if (last_literal == 2)
      {
         int first_literal = new_clause->literals[0];
         int second_literal = new_clause->literals[1];
         level_data_[level+1].literal_info[first_literal].number_of_quadratic++;
         level_data_[level+1].literal_info[second_literal].number_of_quadratic++;
      }
      ++ new_clause_it;
   }
   level_data_[level+1].num_clauses = new_clause_it;
   // fprintf (stderr, "level_data_[%d].num_clauses=%d\n",
   //      level+1, level_data_[level+1].num_clauses);
   return kUndetermined;
}

bf::Result bf::DPLL::ProcessLevel (int level)
{
   if (level > max_level_)
   {
      max_level_ = level;
   }
   ++ number_of_processed_literals_;
   Result result = UnitPropagation (level);
   if (result != kUndetermined)
   {
      return result;
   }
   int literal;
   result = ChooseLiteral (level, &literal);
   if (result != kUndetermined)
   {
      return result;
   }
   result = PrepareLevel (level, literal);
   if (result != kUndetermined)
   {
      return result;
   }
   result = ProcessLevel (level + 1);
   if (result != kUnsatisfiable)
   {
      return result;
   }
   if (literal % 2)
   {
      -- literal;
   }
   else
   {
      ++ literal;
   }
   result = PrepareLevel (level, literal);
   if (result != kUndetermined)
   {
      return result;
   }
   result = ProcessLevel (level + 1);
   return result;
}

bf::Result bf::DPLL::Run (bf::Input * input)
{
   Result result = BuildDataStructures ();
   if (result == kUndetermined)
   {
      result = ProcessLevel (0);
   }
   if (result == kSatisfiable
         && input != NULL)
   {
      input->assign(number_of_variables_, false);
      for (int variable = 0; variable < number_of_variables_; ++ variable)
      {
         if (satisfying_assignment_ [variable] == kTrueValue)
         {
            (*input)[variable] = true;
         }
      }
   }
   ReleaseDataStructures ();
   return result;
}
