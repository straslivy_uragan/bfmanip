//
//  dpll.h
//  bfmanip++
//
//  Created by Petr Kučera on 17.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#ifndef DPLL_H
#define DPLL_H

#include "input.h"
#include "normalform.h"
#include "bfutils.h"

/**@file dpll.h
 * @brief Contains DPLL satisfiability algorithm.
 */
namespace bf {
   /**@brief Class encapsulating an instance of DPLL algorithm
    * together with neccessary data structures.
    */
   class DPLL {
      public:
         /**@brief Structure describing a clause for purposes of this
          * implementation of DPLL algorithm.
          */
         struct DPLLClause {
            /**@brief Number of literals in the clause. */
            int number_of_literals;
            /**@brief Array with literals in this clause. */
            int * literals;
         };

         /**@brief Structure describing a literals for purposes of
          * this implementation of DPLL algorithm.
          */
         struct LiteralInfo {
            /**@brief List of clauses in which this literal is
             * contained.
             *
             * It is implemented as an array of indices into
             * LevelData::clauses array.
             */
            int * clause_list;
            /**@brief Number of clauses in which this literal appears.
             */
            int number_of_clauses;
            /**@brief Number of quadratic clauses in which this
             * literal appears.
             *
             * This value is used for a simple heuristic used for
             * choosing next literal. A literal which appears in
             * largest number of quadratic clauses is used for the
             * next step.
             */
            int number_of_quadratic;
         };

         /**@brief Structure describing data connected with one level
          * of recursive calls of DPLL algorithm.
          */
         struct LevelData {
            /**@brief Values of variables, which have been assigned to
             * them.
             *
             * The values are from PIValues, in particular if a value
             * has not been determined yet, it is set to
             * kUnknownValue.
             */
            PIValues * assigned_values;
            /**@brief Array with clauses at this level.
             */
            DPLLClause * clauses;
            /**@brief Array with literals present in a unit literal at
             * this level.
             */
            int * unit_clauses;
            /**@brief Number of unit clauses at this level.
             */
            int num_unit_clauses;
            /**@brief Number of clauses at this level.
             */
            int num_clauses;
            /**@brief Array with information about every literal.
             */
            LiteralInfo * literal_info;
            /**@brief Literal chosen for this level to be satisfied.
             */
            int chosen_literal;
         };

      private:
         /**@brief CNF object on which we shall test satisfiability.
          *
          * It is a constant reference, which can be initialized only in
          * constructor.
          */
         const CNF&  cnf_;

         /**@brief Number of literals (twice the number of variables)
          * in the associated CNF.
          *
          * It is stored as an integer constant initialized in the
          * constructor using values from CNF.
          */
         const int number_of_literals_;
         /**@brief Number of variables in the associated CNF.
          *
          * It is stored as an integer constant initialized in the
          * constructor using values from CNF.
          */
         const int number_of_variables_;
         /**@brief Number of clauses in the associated CNF.
          *
          * It is stored as an integer constant initialized in the
          * constructor using values from CNF.
          */
         const int number_of_clauses_;

         /**@brief Array of structures with data for every level.
          */
         LevelData * level_data_;

         /**@brief At the end of search, here we store satisfying
          * assignment.
          *
          * It is stored as a partial assignment, in case some
          * variable is still undetermined when we arrive to a
          * satisfiable formula.
          */
         PIValues * satisfying_assignment_;

         /**@brief Maximum level reached during recursive search.
          */
         int max_level_;
         /**@brief Total number of instantions of ProcessLevel
          * procedure during the search.
          */
         int number_of_processed_literals_;

         /**@brief Chooses next literal to branch on.
          *
          * It chooses a literal with largest number of quadratic
          * clauses it is present in. If a positive literal is chosen,
          * then positive value should be tried first, if a negative
          * literal is chosen, then negative value should be tried
          * first.
          *
          * @param level Level at which next literal should be found.
          * @param chosen_literal Place to store chosen literal.
          *
          * @return kFailed if all variables have already assigned
          * value, kUndetermined otherwise.
          */
         Result ChooseLiteral (int level, int * chosen_literal);
         /**@brief Performs unit propagation at given level.
          *
          * At the end, when no unit clause remains at this level, we
          * check, whether all clauses are already satisfied. This is
          * reflected in the return value kSatisfiable. If an empty
          * clause is generated during unit propagation, then
          * kUnsatisfiable is returned. If a clause is satisfied, i.e.
          * some of its literals is evaluated to true, then this
          * clause remains in the CNF, but it is emptied, i.e. the
          * number of literals is set to 0. Which means that after
          * unit propagation finishes with kUndetermined result, there
          * may be empty clauses, which are however considered as
          * satisfied, not as unsatisfied. If real empty clause is
          * generated, kUnsatisfiable is returned. In case
          * kSatisfiable is returned, satisfying_assignment_ is filled
          * appropriately.
          *
          * @param level Level at which unit propagation should be
          * performed.
          *
          * @return Return value is described above, if an error
          * occurs, kFailed is returned.
          */
         Result UnitPropagation (int level);
         /**@brief Allocates memory needed to store information and
          * structures at given level.
          *
          * At the begining only level 0 is allocated, afterwards a
          * level is allocated only if it is reached during search. If
          * it is reached more than once, the same memory is reused.
          *
          * @return kUndetermined if all went O.K., kFailed if the
          * function failed to allocate some structure.
          */
         Result AllocateLevel (int level);
         /**@brief Allocates level 0 and other structures and
          * initializes level 0 with a copy of associated CNF.
          *
          * @return kUndetermined if all went O.K., kFailed otherwise.
          */
         Result BuildDataStructures ();
         /**@brief Releases all memory allocated by this object.
          */
         void ReleaseDataStructures ();
         /**@brief Processes given level, i.e. after unit propagation
          * it chooses a literal and then tries to consecutively
          * satisfy and falsify.
          *
          * @param level Level to process.
          *
          * @return Result of processing, it can be any result value.
          */
         Result ProcessLevel (int level);
         /**@brief Prepares level following after the given one with
          * satisfying given literal.
          *
          * If necessary, it allocates memory for the next level, i.e.
          * @p (level+1) using AllocateLevel method, then it copies data
          * from level @p level to @p (level+1), during this process,
          * partial assignment with setting given literal to true is
          * performed. Also empty clauses, which remain from previous
          * unit propagation as satisfied clauses, are removed.
          */
         Result PrepareLevel (int level, int literal);

         /**@brief Private constructor, is not allowed to be called. */
         inline DPLL (); 
         /**@brief Private copy constructor, is not allowed to be
          * called. */
         DPLL(const DPLL&);
         /**@brief Private assignment operator, is not allowed to be
          * called. */
         void operator=(const DPLL&);

      public:
         /**@brief Initializes object with given cnf.
          *
          * @param a_cnf CNF passed as a constant reference which is tied with
          * constant member variable cnf_.
          */
         explicit inline DPLL (const CNF& a_cnf) :
            cnf_ (a_cnf),
            number_of_literals_(static_cast<int>(a_cnf.NumberOfVariables()) * 2),
            number_of_variables_(static_cast<int>(a_cnf.NumberOfVariables())),
            number_of_clauses_(static_cast<int>(a_cnf.NumberOfElements())),
            level_data_(NULL),
            satisfying_assignment_ (NULL),
            max_level_ (0),
            number_of_processed_literals_ (0) {}

         /**@brief Runs the satisfiability tester on stored CNF.
          *
          * Each run consist of building data structures, processing
          * level 0 and releasing all memory after finish. During
          * computation log information is written to standard output,
          * errors and warnings to standard error output. If the
          * result is that the formula is satisfiable and given
          * input is not NULL, it is filled with satisfying
          * assignment.
          *
          * @param input Output variable in which satisfying
          * assignment is stored, if found. If input is NULL, then no
          * satisfying assignment is returned.
          */
         Result Run (bf::Input * input);

         /**@brief Returns maximum reached level of recursion. */
         inline int max_level ()
         {
            return max_level_;
         }
         /**@brief Returns total number of invocation of ProcessLevel
          * method. */
         inline int number_of_processed_literals ()
         {
            return number_of_processed_literals_;
         }
   };

}

#endif
