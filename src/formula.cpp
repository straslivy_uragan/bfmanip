//
//  formula.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 24.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#include <stdio.h>

#include "formula.h"
#include "bfexception.h"

bf::Formula::Node::Node() :
      node_type_(kFalseNode), left_op_(NULL), right_op_(NULL), variable_index_(
            0)
{
}

bf::Formula::Node::~Node()
{
   if (left_op_ != NULL)
   {
      delete left_op_;
      left_op_ = NULL;
   }
   if (right_op_ != NULL)
   {
      delete right_op_;
      right_op_ = NULL;
   }
   node_type_ = kTrueNode;
}

bf::Formula::Node::Node(NodeType a_type, unsigned int a_variable_index) :
      node_type_(a_type), left_op_(NULL), right_op_(NULL), variable_index_(
            a_variable_index)

{
}

bf::Formula::Node::Node(NodeType a_type, Node * a_left_op, Node * a_right_op) :
      node_type_(a_type), left_op_(a_left_op), right_op_(a_right_op), variable_index_(
            0)

{
}

bf::Formula::Node *
bf::Formula::Node::Copy()
{
   if (IsLeaf())
   {
      return new Node(node_type_, variable_index_);
   }

   Node * left_op_copy = NULL;
   if (left_op_ != NULL)
   {
      left_op_copy = left_op_->Copy();
   }

   Node * right_op_copy = NULL;
   if (right_op_ != NULL)
   {
      right_op_copy = right_op_->Copy();
   }
   return new Node(node_type_, left_op_copy, right_op_copy);
}

void
bf::Formula::Node::ToString(const std::vector<std::string> * variable_names,
      std::string * output_string) const
{
   if (output_string == NULL)
   {
      std::cerr
            << "[bf::Formula::Node::ToString] Parameter output_string is NULL."
            << std::endl;
      return;
   }
   switch (node_type_)
   {
      case kFalseNode:
         (*output_string) += '0';
         break;
      case kTrueNode:
         (*output_string) += '1';
         break;
      case kVariableNode:
         if (variable_names == NULL
               || variable_index_ >= variable_names->size())
         {
            char index_str[32] = "";
            snprintf(index_str, 32, "%u", variable_index_);
            (*output_string) += index_str;
         }
         else
         {
            (*output_string) += (*variable_names)[variable_index_];
         }
         break;
      case kNotNode:
      {
         if (unary_op() == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ToString] Unary (left) child of not operator is NULL."
                  << std::endl;
            return;
         }

         (*output_string) += '-';
         if (!unary_op()->IsLeaf())
         {
            (*output_string) += '(';
         }
         unary_op()->ToString(variable_names, output_string);
         if (!unary_op()->IsLeaf())
         {
            (*output_string) += ')';
         }

         break;
      }
      case kAndNode:
      {
         if (left_op() == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ToString] Left child of an and operator is NULL."
                  << std::endl;
            return;
         }
         if (right_op() == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ToString] Right child of an and operator is NULL."
                  << std::endl;
            return;
         }

         if (left_op()->node_type() == kOrNode)
         {
            (*output_string) += '(';
         }
         left_op()->ToString(variable_names, output_string);
         if (left_op()->node_type() == kOrNode)
         {
            (*output_string) += ')';
         }
         if (left_op()->node_type() != kOrNode
               && right_op()->node_type() != kOrNode)
         {
            (*output_string) += '&';
         }
         if (right_op()->node_type() == kOrNode)
         {
            (*output_string) += '(';
         }
         right_op()->ToString(variable_names, output_string);
         if (right_op()->node_type() == kOrNode)
         {
            (*output_string) += ')';
         }
         break;
      }
      case kOrNode:
      {
         if (left_op() == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ToString] Left child of an or operator is NULL."
                  << std::endl;
            return;
         }
         if (right_op() == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ToString] Right child of an or operator is NULL."
                  << std::endl;
            return;
         }

         if (left_op()->node_type() == kAndNode)
         {
            (*output_string) += '(';
         }
         left_op()->ToString(variable_names, output_string);
         if (left_op()->node_type() == kAndNode)
         {
            (*output_string) += ')';
         }
         (*output_string) += '|';
         if (right_op()->node_type() == kAndNode)
         {
            (*output_string) += '(';
         }
         right_op()->ToString(variable_names, output_string);
         if (right_op()->node_type() == kAndNode)
         {
            (*output_string) += ')';
         }
         break;
      }
      default:
         std::cerr << "[bf::Formula::Node::ToString] Bad node type"
               << std::endl;
         break;

   }
}

bool
bf::Formula::Node::ValueForInput(const Input& input)
{
   bool output_value = false;
   switch (node_type_)
   {
      case kFalseNode:
         output_value = false;
         break;
      case kTrueNode:
         output_value = true;
         break;
      case kVariableNode:
         if (variable_index_ >= input.size())
         {
            std::cerr
                  << "[bf::Formula::Node::ValueForInput] Variable index exceeds input size"
                  << std::endl;
            throw bf::IndexOutOfBoundsException();
         }
         output_value = input[variable_index_];
         break;
      case kNotNode:
         if (unary_op() == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ValueForInput] Unary (left) child of negation operator is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         output_value = !(unary_op()->ValueForInput(input));
         break;
      case kAndNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ValueForInput] Left child of AND operator is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ValueForInput] right child of AND operator is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         output_value = left_op_->ValueForInput(input)
               && right_op_->ValueForInput(input);
         break;
      case kOrNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ValueForInput] Left child of OR operator is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ValueForInput] right child of OR operator is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         output_value = left_op_->ValueForInput(input)
               || right_op_->ValueForInput(input);
         break;
      default:
         std::cerr << "[bf::Formula::Node::ValueForInput] Unknown node type";
         break;
   }
   return output_value;
}

void
bf::Formula::Node::PropagateNegations(bool negate)
{
   switch (node_type_)
   {
      case kFalseNode:
         if (negate)
         {
            node_type_ = kTrueNode;
         }
         break;
      case kTrueNode:
         if (negate)
         {
            node_type_ = kFalseNode;
         }
         break;
      case kVariableNode:
         if (negate)
         {
            Node * negatedNode = this->Copy();
            node_type_ = kNotNode;
            left_op_ = negatedNode;
            right_op_ = NULL;
         }
         break;
      case kNotNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::PropagateNegations] Unary (left) operand of negation is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (left_op_->node_type() == kVariableNode)
         {
            if (negate)
            {
               node_type_ = left_op_->node_type();
               variable_index_ = left_op_->variable_index();
               right_op_ = NULL;
               delete left_op_;
               left_op_ = NULL;
            }
         }
         else
         {
            left_op_->PropagateNegations(!negate);
            node_type_ = left_op_->node_type();
            variable_index_ = left_op_->variable_index();
            right_op_ = left_op_->right_op();
            Node * node_to_delete = left_op_;
            left_op_ = left_op_->left_op();
            node_to_delete->left_op_ = NULL;
            node_to_delete->right_op_ = NULL;
            delete node_to_delete;
         }
         break;
      case kAndNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::PropagateNegations] Left operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::PropagateNegations] Right operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         left_op_->PropagateNegations(negate);
         right_op_->PropagateNegations(negate);
         if (negate)
         {
            node_type_ = kOrNode;
         }
         break;
      case kOrNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::PropagateNegations] Left operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::PropagateNegations] Right operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         left_op_->PropagateNegations(negate);
         right_op_->PropagateNegations(negate);
         if (negate)
         {
            node_type_ = kAndNode;
         }
         break;
      default:
         std::cerr
               << "[bf::Formula::Node::PropagateNegations] Unknown node type";
         break;
   }
}

void
bf::Formula::Node::ApplyPartialAssignment(
      const bf::PartialInput& partial_assignment, bool simplify)
{
   switch (node_type_)
   {
      case kFalseNode:
         break;
      case kTrueNode:
         break;
      case kVariableNode:
         if (variable_index_ > partial_assignment.size())
         {
            std::cerr
                  << "[bf::Formula::Node::ApplyPartialAssignment] Variable index exceeds size of the assignment."
                  << std::endl;
            throw bf::IndexOutOfBoundsException();
         }
         if (partial_assignment[variable_index_] == kTrueValue)
         {
            node_type_ = kTrueNode;
         }
         else if (partial_assignment[variable_index_] == kFalseValue)
         {
            node_type_ = kFalseNode;
         }
         break;
      case kNotNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ApplyPartialAssignment] Unary (left) operand of negation is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         left_op_->ApplyPartialAssignment(partial_assignment, simplify);
         if (simplify)
         {
            if (unary_op()->node_type() == kTrueNode)
            {
               node_type_ = kFalseNode;
               delete left_op_;
               left_op_ = NULL;
            }
            else if (unary_op()->node_type() == kFalseNode)
            {
               node_type_ = kTrueNode;
               delete left_op_;
               left_op_ = NULL;
            }
         }
         break;
      case kAndNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ApplyPartialAssignment] Left operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ApplyPartialAssignment] Right operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         left_op_->ApplyPartialAssignment(partial_assignment, simplify);
         right_op_->ApplyPartialAssignment(partial_assignment, simplify);
         if (simplify)
         {
            if (left_op_->node_type() == kFalseNode
                  || right_op_->node_type() == kFalseNode)
            {
               node_type_ = kFalseNode;
               delete left_op_;
               left_op_ = NULL;
               delete right_op_;
               right_op_ = NULL;
            }
            else if (left_op_->node_type() == kTrueNode)
            {
               node_type_ = right_op_->node_type();
               variable_index_ = right_op_->variable_index();
               delete left_op_;
               left_op_ = right_op_->left_op_;
               Node * node_to_delete = right_op_;
               right_op_ = right_op_->right_op_;
               node_to_delete->right_op_ = NULL;
               node_to_delete->left_op_ = NULL;
               delete node_to_delete;
            }
            else if (right_op_->node_type() == kTrueNode)
            {
               node_type_ = left_op_->node_type();
               variable_index_ = left_op_->variable_index();
               delete right_op_;
               right_op_ = left_op_->right_op_;
               Node * node_to_delete = left_op_;
               left_op_ = left_op_->left_op_;
               node_to_delete->left_op_ = NULL;
               node_to_delete->right_op_ = NULL;
               delete node_to_delete;
            }
         }
         break;
      case kOrNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ApplyPartialAssignment] Left operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::ApplyPartialAssignment] Right operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         left_op_->ApplyPartialAssignment(partial_assignment, simplify);
         right_op_->ApplyPartialAssignment(partial_assignment, simplify);
         if (simplify)
         {
            if (left_op_->node_type() == kTrueNode
                  || right_op_->node_type() == kTrueNode)
            {
               node_type_ = kTrueNode;
               delete left_op_;
               left_op_ = NULL;
               delete right_op_;
               right_op_ = NULL;
            }
            else if (left_op_->node_type() == kFalseNode)
            {
               node_type_ = right_op_->node_type();
               variable_index_ = right_op_->variable_index();
               delete left_op_;
               left_op_ = right_op_->left_op_;
               Node * node_to_delete = right_op_;
               right_op_ = right_op_->right_op_;
               node_to_delete->right_op_ = NULL;
               node_to_delete->left_op_ = NULL;
               delete node_to_delete;
            }
            else if (right_op_->node_type() == kFalseNode)
            {
               node_type_ = left_op_->node_type();
               variable_index_ = left_op_->variable_index();
               delete right_op_;
               right_op_ = left_op_->right_op_;
               Node * node_to_delete = left_op_;
               left_op_ = left_op_->left_op_;
               node_to_delete->left_op_ = NULL;
               node_to_delete->right_op_ = NULL;
               delete node_to_delete;
            }
         }
         break;
      default:
         std::cerr
               << "[bf::Formula::Node::ApplyPartialAssignment] Unknown node type";
         break;
   }
}

bool
bf::Formula::Node::IsCNF() const
{
   bool answer = false;
   switch (node_type_)
   {
      case kFalseNode:
      case kTrueNode:
      case kVariableNode:
         answer = true;
         break;
      case kNotNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsCNF] Unary (left) operand of negation is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsLeaf();
         break;
      case kAndNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsCNF] Left operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsCNF] Right operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsCNF() && right_op_->IsCNF();
         break;
      case kOrNode:
         answer = IsClause();
         break;
      default:
         std::cerr << "[bf::Formula::Node::IsCNF] Unknown node type";
         break;
   }
   return answer;
}

bool
bf::Formula::Node::IsDNF() const
{
   bool answer = false;
   switch (node_type_)
   {
      case kFalseNode:
      case kTrueNode:
      case kVariableNode:
         answer = true;
         break;
      case kNotNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsDNF] Unary (left) operand of negation is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsLeaf();
         break;
      case kAndNode:
         answer = IsTerm();
         break;
      case kOrNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsDNF] Left operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsDNF] Right operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsDNF() && right_op_->IsDNF();
         break;
      default:
         std::cerr << "[bf::Formula::Node::IsDNF] Unknown node type";
         break;
   }
   return answer;
}

bool
bf::Formula::Node::IsClause() const
{
   bool answer = false;
   switch (node_type_)
   {
      case kFalseNode:
      case kTrueNode:
      case kVariableNode:
         answer = true;
         break;
      case kNotNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsClause] Unary (left) operand of negation is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsLeaf();
         break;
      case kAndNode:
         answer = false;
         break;
      case kOrNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsClause] Left operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsClause] Right operand of disjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsClause() && right_op_->IsClause();
         break;
      default:
         std::cerr << "[bf::Formula::Node::IsClause] Unknown node type";
         break;
   }
   return answer;
}

bool
bf::Formula::Node::IsTerm() const
{
   bool answer = false;
   switch (node_type_)
   {
      case kFalseNode:
      case kTrueNode:
      case kVariableNode:
         answer = true;
         break;
      case kNotNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsTerm] Unary (left) operand of negation is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsLeaf();
         break;
      case kAndNode:
         if (left_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsTerm] Left operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         if (right_op_ == NULL)
         {
            std::cerr
                  << "[bf::Formula::Node::IsTerm] Right operand of conjunction is NULL."
                  << std::endl;
            throw bf::UnexpectedNULLException();
         }
         answer = left_op_->IsTerm() && right_op_->IsTerm();
         break;
      case kOrNode:
         answer = false;
         break;
      default:
         std::cerr << "[bf::Formula::Node::IsTerm] Unknown node type";
         break;
   }
   return answer;
}

bool
bf::Formula::Node::TransformToCNF(bf::CNF * out_cnf) const
{
   if (out_cnf == NULL)
   {
      fputs("[bf::Formula::Node::TransformToCNF] Parameter out_cnf is NULL.\n",
            stderr);
      throw bf::UnexpectedNULLException();
   }
   if (node_type_ == kTrueNode)
   {
      return true;
   }
   else if (node_type_ == kFalseNode)
   {
      Clause empty_clause;
      out_cnf->AddElement(empty_clause);
      return true;
   }
   else if (node_type_ == kVariableNode
         || node_type_ == kNotNode
         || node_type_ == kOrNode)
   {
      Clause add_clause;
      bool satisfied = false;
      bool answer = TransformToClause(&add_clause, &satisfied);
      if (answer && !satisfied)
      {
         out_cnf->AddElement(add_clause);
      }
      return answer;
   }
   else if (node_type_ == kAndNode)
   {
      if (left_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToCNF] Left operand of conjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      if (right_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToCNF] Right operand of conjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      bool answer = left_op_->TransformToCNF(out_cnf)
            && right_op_->TransformToCNF(out_cnf);
      return answer;
   }
   fputs("[bf::Formula::Node::TransformToCNF] Unknown node type\n", stderr);
   return false;
}

bool
bf::Formula::Node::TransformToDNF(bf::DNF * out_dnf) const
{
   if (out_dnf == NULL)
   {
      fputs("[bf::Formula::Node::TransformToDNF] Parameter out_dnf is NULL.\n",
            stderr);
      throw bf::UnexpectedNULLException();
   }
   if (node_type_ == kTrueNode)
   {
      return true;
   }
   else if (node_type_ == kFalseNode)
   {
      Term empty_term;
      out_dnf->AddElement(empty_term);
      return true;
   }
   else if (node_type_ == kVariableNode || node_type_ == kNotNode
         || node_type_ == kAndNode)
   {
      Term add_term;
      bool falsified = false;
      bool answer = TransformToTerm(&add_term, &falsified);
      if (answer && !falsified)
      {
         out_dnf->AddElement(add_term);
      }
      return answer;
   }
   else if (node_type_ == kOrNode)
   {
      if (left_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToDNF] Left operand of disjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      if (right_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToDNF] Right operand of disjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      bool answer = left_op_->TransformToDNF(out_dnf)
            && right_op_->TransformToDNF(out_dnf);
      return answer;
   }
   fputs("[bf::Formula::Node::TransformToDNF] Unknown node type\n", stderr);
   return false;
}

bool
bf::Formula::Node::TransformToClause(bf::Clause * out_clause,
      bool * satisfied) const
{
   if (out_clause == NULL)
   {
      fputs(
            "[bf::Formula::Node::TransformToClause] Parameter out_clause is NULL.\n",
            stderr);
      throw bf::UnexpectedNULLException();
   }
   if (satisfied == NULL)
   {
      fputs(
            "[bf::Formula::Node::TransformToClause] Parameter satisfied is NULL.\n",
            stderr);
      throw bf::UnexpectedNULLException();
   }
   if (node_type_ == kTrueNode)
   {
      *satisfied = true;
      return true;
   }
   else if (node_type_ == kFalseNode)
   {
      return true;
   }
   else if (node_type_ == kVariableNode)
   {
      out_clause->AddLiteral(variable_index_, false);
      return true;
   }
   else if (node_type_ == kNotNode)
   {
      if (left_op_ != NULL && left_op_->node_type() == kVariableNode)
      {
         out_clause->AddLiteral(left_op_->variable_index(), true);
         return true;
      }
      else
      {
         return false;
      }
   }
   else if (node_type_ == kAndNode)
   {
      return false;
   }
   else if (node_type_ == kOrNode)
   {
      if (left_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToClause] Left operand of disjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      if (right_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToClause] Right operand of disjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      bool answer = left_op_->TransformToClause(out_clause, satisfied)
            && right_op_->TransformToClause(out_clause, satisfied);
      return answer;
   }
   fputs("[bf::Formula::Node::TransformToClause] Unknown node type\n", stderr);
   return false;
}

bool
bf::Formula::Node::TransformToTerm(bf::Term * out_term, bool * falsified) const
{
   if (out_term == NULL)
   {
      fputs(
            "[bf::Formula::Node::TransformToTerm] Parameter out_term is NULL.\n",
            stderr);
      throw bf::UnexpectedNULLException();
   }
   if (falsified == NULL)
   {
      fputs(
            "[bf::Formula::Node::TransformToTerm] Parameter falsified is NULL.\n",
            stderr);
      throw bf::UnexpectedNULLException();
   }
   if (node_type_ == kTrueNode)
   {
      return true;
   }
   else if (node_type_ == kFalseNode)
   {
      *falsified = true;
      return true;
   }
   else if (node_type_ == kVariableNode)
   {
      out_term->AddLiteral(variable_index_, false);
      return true;
   }
   else if (node_type_ == kNotNode)
   {
      if (left_op_ != NULL && left_op_->node_type() == kVariableNode)
      {
         out_term->AddLiteral(left_op_->variable_index(), true);
         return true;
      }
      else
      {
         return false;
      }
   }
   else if (node_type_ == kOrNode)
   {
      return false;
   }
   else if (node_type_ == kAndNode)
   {
      if (left_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToTerm] Left operand of disjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      if (right_op_ == NULL)
      {
         fputs(
               "[bf::Formula::Node::TransformToTerm] Right operand of disjunction is NULL.\n",
               stderr);
         throw bf::UnexpectedNULLException();
      }
      bool answer = left_op_->TransformToTerm(out_term, falsified)
            && right_op_->TransformToTerm(out_term, falsified);
      return answer;
   }
   fputs("[bf::Formula::Node::TransformToTerm] Unknown node type\n", stderr);
   return false;
}

bf::Formula::Formula() :
      names_of_variables_(NULL), formula_root_(NULL), number_of_variables_(0)
{
}

bf::Formula::~Formula()
{
   if (names_of_variables_ != NULL)
   {
      delete names_of_variables_;
      names_of_variables_ = NULL;
   }
   if (formula_root_ != NULL)
   {
      delete formula_root_;
      formula_root_ = NULL;
   }
}

bf::Formula::Formula(unsigned int new_number_of_variables,
      std::vector<std::string> * new_names_of_variables) :
      names_of_variables_(new_names_of_variables), formula_root_(NULL), number_of_variables_(
            new_number_of_variables)
{
}

bf::Formula::Formula(const std::string & formula_string) :
      names_of_variables_(NULL), formula_root_(NULL), number_of_variables_(0)
{
   InitWithString(formula_string);
}

unsigned int
bf::Formula::InitWithString(const std::string & formula_string)
{
   if (formula_root_ != NULL)
   {
      delete formula_root_;
      formula_root_ = NULL;
   }
   if (names_of_variables_ != NULL)
   {
      delete names_of_variables_;
      names_of_variables_ = NULL;
   }
   names_of_variables_ = new std::vector<std::string>();
   unsigned int position = 0;
   AtomType last_atom_type = kNoAtom;
   size_t last_atom_location = 0;
   size_t last_atom_length = 0;
   formula_root_ = ParseDisjunction(formula_string, &position, &last_atom_type,
         &last_atom_location, &last_atom_length);
   if (formula_root_ == NULL)
   {
      return position;
   }
   number_of_variables_ = (unsigned int)names_of_variables_->size();
   return 0;
}

   unsigned int
bf::Formula::InitWithFile(FILE * in)
{
   std::string formula_string;
   // Read file to string.
   char *line = NULL;
   size_t line_capacity = 0;

   while (getline(&line, &line_capacity, in) > 0)
   {
      int i = 0;
      while (line[i] != '\0'
            && isspace(line[i]))
      {
         ++i;
      }
      if (line[i] != '#' && line[i] != '\0' && line[i] != '\n')
      {
         while(line[i] != '\0'
               && line[i] != '\n')
         {
            ++i;
         }
         if (line[i] == '\n')
         {
            line[i] = '\0';
         }
         formula_string.append(line);
      }
   }
   if (line != NULL)
   {
      free(line);
   }
   InitWithString(formula_string);
   return 0;
}

bf::Formula *
bf::Formula::Copy() const
{
   Formula * formula_copy = new Formula(number_of_variables_,
         names_of_variables_);
   if (formula_root_ != NULL)
   {
      formula_copy->formula_root_ = formula_root_->Copy();
   }
   return formula_copy;
}

void
bf::Formula::SetNamesOfVariables(
      const std::vector<std::string> * new_names_of_variables)
{
   if (new_names_of_variables == names_of_variables_)
   {
      return;
   }
   if (names_of_variables_ != NULL)
   {
      delete names_of_variables_;
      names_of_variables_ = NULL;
   }
   if (new_names_of_variables != NULL)
   {
      names_of_variables_ = new std::vector<std::string>(
            *new_names_of_variables);
   }
}

bool
bf::Formula::ValueForInput(const bf::Input & input) const
{
   if (formula_root_ == NULL)
   {
      return false;
   }
   return formula_root_->ValueForInput(input);
}

void
bf::Formula::ApplyPartialAssignment(const bf::PartialInput & partial_assignment,
      bool simplify)
{
   if (formula_root_ != NULL)
   {
      formula_root_->ApplyPartialAssignment(partial_assignment, simplify);
   }
}

void
bf::Formula::ToString(std::string * output_string) const
{
   if (formula_root_ != NULL)
   {
      formula_root_->ToString(names_of_variables_, output_string);
   }
}

void
bf::Formula::Simplify()
{
   if (formula_root_ != NULL)
   {
      bf::PartialInput partial_input(number_of_variables_, bf::kUnknownValue);
      ApplyPartialAssignment(partial_input, true);
   }
}

void
bf::Formula::Negate()
{
   if (formula_root_ != NULL)
   {
      formula_root_ = new Node(Node::kNotNode, formula_root_, NULL);
   }
}

void
bf::Formula::SetToFalse()
{
   if (formula_root_ != NULL && formula_root_->node_type() != Node::kFalseNode)
   {
      delete formula_root_;
      formula_root_ = NULL;
   }
   if (formula_root_ == NULL)
   {
      formula_root_ = new Node(Node::kFalseNode, 0);
   }
}

void
bf::Formula::SetToTrue()
{
   if (formula_root_ != NULL && formula_root_->node_type() != Node::kTrueNode)
   {
      delete formula_root_;
      formula_root_ = NULL;
   }
   if (formula_root_ == NULL)
   {
      formula_root_ = new Node(Node::kTrueNode, 0);
   }
}

void
bf::Formula::SetToVariable(unsigned int variable_index)
{
   if (formula_root_ != NULL)
   {
      if (formula_root_->node_type() == Node::kVariableNode
            && formula_root_->variable_index() == variable_index)
      {
         return;
      }
      delete formula_root_;
      formula_root_ = NULL;
   }
   formula_root_ = new Node(Node::kVariableNode, variable_index);
}

void
bf::Formula::MakeConjunction(const bf::Formula & formula)
{
   if (formula_root_ == NULL)
   {
      if (!formula.IsEmpty())
      {
         formula_root_ = formula.formula_root_->Copy();
      }
   }
   else
   {
      if (!formula.IsEmpty())
      {
         formula_root_ = new Node(Node::kAndNode, formula_root_,
               formula.formula_root_);
      }
   }
}

void
bf::Formula::MakeDisjunction(const bf::Formula & formula)
{
   if (formula_root_ == NULL)
   {
      if (!formula.IsEmpty())
      {
         formula_root_ = formula.formula_root_->Copy();
      }
   }
   else
   {
      if (!formula.IsEmpty())
      {
         formula_root_ = new Node(Node::kOrNode, formula_root_,
               formula.formula_root_);
      }
   }
}

void
bf::Formula::PropagateNegations()
{
   if (formula_root_ != NULL)
   {
      formula_root_->PropagateNegations(false);
   }
}

bool
bf::Formula::IsCNF() const
{
   return (formula_root_ == NULL || formula_root_->IsCNF());
}

bool
bf::Formula::IsDNF() const
{
   return (formula_root_ == NULL || formula_root_->IsDNF());
}

bool
bf::Formula::IsClause() const
{
   return (formula_root_ == NULL || formula_root_->IsClause());
}

bool
bf::Formula::IsTerm() const
{
   return (formula_root_ == NULL || formula_root_->IsTerm());
}

bool
bf::Formula::TransformToCNF(bf::CNF * out_cnf) const
{
   if (out_cnf == NULL)
   {
      fputs("[Formula::TransformToCNF] out_cnf is NULL\n", stderr);
      throw UnexpectedNULLException();
   }
   out_cnf->RemoveAllElements();
   out_cnf->SetNumberOfVariables(number_of_variables_);
   return (formula_root_ == NULL || formula_root_->TransformToCNF(out_cnf));
}

bool
bf::Formula::TransformToDNF(bf::DNF * out_dnf) const
{
   if (out_dnf == NULL)
   {
      fputs("[Formula::TransformToDNF] out_dnf is NULL\n", stderr);
      throw UnexpectedNULLException();
   }
   out_dnf->RemoveAllElements();
   out_dnf->SetNumberOfVariables(number_of_variables_);
   return (formula_root_ == NULL || formula_root_->TransformToDNF(out_dnf));
}

bool
bf::Formula::TransformToClause(bf::Clause * out_clause) const
{
   if (out_clause == NULL)
   {
      fputs("[Formula::TransformToCNF] out_clause is NULL\n", stderr);
      throw UnexpectedNULLException();
   }
   out_clause->RemoveAllLiterals();
   bool satisfied;
   return (formula_root_ == NULL || formula_root_->TransformToClause(out_clause, &satisfied));

}

bool
bf::Formula::TransformToTerm(bf::Term * out_term) const
{
   if (out_term == NULL)
   {
      fputs("[Formula::TransformToCNF] out_term is NULL\n", stderr);
      throw UnexpectedNULLException();
   }
   out_term->RemoveAllLiterals();
   bool falsified;
   return (formula_root_ == NULL || formula_root_->TransformToTerm(out_term, &falsified));
}

bf::Formula::Node *
bf::Formula::ParseDisjunction(const std::string & formula_string,
      unsigned int * position, bf::Formula::AtomType * last_atom_type,
      size_t * last_atom_location, size_t * last_atom_length)
{
   if (position == NULL)
   {
      std::cerr << "[bf::Formula::ParseConjunction] parameter position is NULL";
      return NULL;
   }
   if (last_atom_type == NULL)
   {
      std::cerr << "[Formula ParseConjunction] parameter last_atom_type is NULL"
            << std::endl;
      return NULL;
   }
   if (last_atom_location == NULL)
   {
      std::cerr
            << "[Formula ParseConjunction] parameter last_atom_location is NULL"
            << std::endl;
      return NULL;
   }
   if (last_atom_length == NULL)
   {
      std::cerr
            << "[Formula ParseConjunction] parameter last_atom_length is NULL"
            << std::endl;
      return NULL;
   }

   Node * my_node = ParseConjunction(formula_string, position, last_atom_type,
         last_atom_location, last_atom_length);

   if (my_node == NULL)
   {
      std::cerr
            << "[bf::Formula::ParseDisjunction] Message ParseConjunction returned a NULL value for my_node."
            << std::endl;
      return NULL;
   }

   if ((*last_atom_type) == kNoAtom)
   {
      if (!ReadAtom(formula_string, position, last_atom_type,
            last_atom_location, last_atom_length))
      {
         return NULL;
      }
   }

   while ((*last_atom_type) != kNoAtom && (*last_atom_type) != kRightBracketAtom)
   {
      if ((*last_atom_type) != kOrAtom)
      {
         std::cerr << "[bf::Formula::ParseDisjunction] Missing OR operator"
               << std::endl;
         return NULL;
      }
      (*last_atom_type) = kNoAtom;
      Node * right_node = ParseConjunction(formula_string, position,
            last_atom_type, last_atom_location, last_atom_length);

      if (right_node == NULL)
      {
         std::cerr
               << "[bf::Formula::ParseDisjunction] Message ParseConjunction returned a NULL value for right_node."
               << std::endl;
         return NULL;
      }
      if ((*last_atom_type) == kNoAtom)
      {
         if (!ReadAtom(formula_string, position, last_atom_type,
               last_atom_location, last_atom_length))
         {
            return NULL;
         }
      }

      my_node = new Node(Node::kOrNode, my_node, right_node);
   }

   return my_node;
}

bf::Formula::Node *
bf::Formula::ParseConjunction(const std::string & formula_string,
      unsigned int * position, bf::Formula::AtomType * last_atom_type,
      size_t * last_atom_location, size_t * last_atom_length)
{
   if (position == NULL)
   {
      std::cerr << "[bf::Formula::ParseConjunction] parameter position is NULL";
      return NULL;
   }
   if (last_atom_type == NULL)
   {
      std::cerr << "[Formula ParseConjunction] parameter last_atom_type is NULL"
            << std::endl;
      return NULL;
   }
   if (last_atom_location == NULL)
   {
      std::cerr
            << "[Formula ParseConjunction] parameter last_atom_location is NULL"
            << std::endl;
      return NULL;
   }
   if (last_atom_length == NULL)
   {
      std::cerr
            << "[Formula ParseConjunction] parameter last_atom_length is NULL"
            << std::endl;
      return NULL;
   }

   Node * my_node = ParseNegation(formula_string, position, last_atom_type,
         last_atom_location, last_atom_length);
   if (my_node == NULL)
   {
      std::cerr
            << "[bf::Formula::ParseConjunction] Call of ParseNegation returned a NULL value for my_node."
            << std::endl;
      return NULL;
   }

   if ((*last_atom_type) == kNoAtom)
   {
      if (!ReadAtom(formula_string, position, last_atom_type,
            last_atom_location, last_atom_length))
      {
         return NULL;
      }
   }

   while ((*last_atom_type) != kNoAtom && (*last_atom_type) != kOrAtom
         && (*last_atom_type) != kRightBracketAtom)
   {
      // A conjunction sign can be omitted, it can be implicitly given
      // as an empty string between parentheses or between literals.
      if ((*last_atom_type) == kAndAtom)
      {
         (*last_atom_type) = kNoAtom;
      }
      Node * right_node = ParseNegation(formula_string, position,
            last_atom_type, last_atom_location, last_atom_length);

      if ((*last_atom_type) == kNoAtom)
      {
         if (!ReadAtom(formula_string, position, last_atom_type,
               last_atom_location, last_atom_length))
         {
            return NULL;
         }
      }

      if (right_node == NULL)
      {
         std::cerr
               << "[bf::Formula::ParseConjunction] Call of ParseNegation returned a NULL value for right_node."
               << std::endl;
      }

      my_node = new Node(Node::kAndNode, my_node, right_node);
   }

   return my_node;
}

bf::Formula::Node *
bf::Formula::ParseNegation(const std::string & formula_string,
      unsigned int * position, bf::Formula::AtomType * last_atom_type,
      size_t * last_atom_location, size_t * last_atom_length)
{
   if (position == NULL)
   {
      std::cerr << "[bf::Formula::ParseNegation] parameter position is NULL";
      return NULL;
   }
   if (last_atom_type == NULL)
   {
      std::cerr << "[Formula ParseNegation] parameter last_atom_type is NULL"
            << std::endl;
      return NULL;
   }
   if (last_atom_location == NULL)
   {
      std::cerr
            << "[Formula ParseNegation] parameter last_atom_location is NULL"
            << std::endl;
      return NULL;
   }
   if (last_atom_length == NULL)
   {
      std::cerr << "[Formula ParseNegation] parameter last_atom_length is NULL"
            << std::endl;
      return NULL;
   }

   if ((*last_atom_type) == kNoAtom)
   {
      if (!ReadAtom(formula_string, position, last_atom_type,
            last_atom_location, last_atom_length))
      {
         return NULL;
      }
   }

   if ((*last_atom_type) == kNoAtom)
   {
      std::cerr
            << "[bf::Formula::ParseNegation] No atom found, where it was expected."
            << std::endl;
      return NULL;
   }

   bool is_negated = ((*last_atom_type) == kNotAtom);
   if (is_negated)
   {
      if (!ReadAtom(formula_string, position, last_atom_type,
            last_atom_location, last_atom_length))
      {
         return NULL;
      }

      if ((*last_atom_type) == kNoAtom)
      {
         std::cerr
               << "[bf::Formula::ParseNegation] No atom found, where it was expected."
               << std::endl;
         return NULL;
      }
   }

   Node * my_node = NULL;

   switch ((*last_atom_type))
   {
      case kTrueAtom:
         my_node = new Node(Node::kTrueNode, 0);
         (*last_atom_type) = kNoAtom;
         break;

      case kFalseAtom:
         my_node = new Node(Node::kFalseNode, 0);
         (*last_atom_type) = kNoAtom;
         break;

      case kVariableAtom:
      {
         size_t index_of_variable = 0;
         while (index_of_variable < names_of_variables_->size()
               && formula_string.compare(*last_atom_location, *last_atom_length,
                     (*names_of_variables_)[index_of_variable]))
         {
            ++index_of_variable;
         }
         if (index_of_variable >= names_of_variables_->size())
         {
            names_of_variables_->push_back(
                  formula_string.substr(*last_atom_location,
                        *last_atom_length));
            index_of_variable = names_of_variables_->size() - 1;
         }
         my_node = new Node(Node::kVariableNode,
               static_cast<unsigned int>(index_of_variable));
         (*last_atom_type) = kNoAtom;
         break;
      }
      case kNotAtom:
         (*last_atom_type) = kNoAtom;
         my_node = ParseNegation(formula_string, position, last_atom_type,
               last_atom_location, last_atom_length);
         if (my_node == NULL)
         {
            return NULL;
         }
         my_node = new Node(Node::kNotNode, my_node, NULL);
         break;

      case kLeftBracketAtom:
         (*last_atom_type) = kNoAtom;
         my_node = ParseDisjunction(formula_string, position, last_atom_type,
               last_atom_location, last_atom_length);
         if (my_node == NULL)
         {
            return NULL;
         }
         if ((*last_atom_type) != kRightBracketAtom)
         {
            std::cerr << "[bf::Formula::ParseNegation] Unmatched bracket."
                  << std::endl;
         }
         (*last_atom_type) = kNoAtom;
         break;

      default:
         if (is_negated)
         {
            std::cerr
                  << "[bf::Formula::ParseNegation] Negation operator was not followed by suitable atom"
                  << std::endl;
         }
         break;
   }

   if (is_negated)
   {
      my_node = new Node(Node::kNotNode, my_node, NULL);
   }
   return my_node;
}

bool
bf::Formula::ReadAtom(const std::string & formula_string,
      unsigned int * position, bf::Formula::AtomType * atom_type,
      size_t * atom_location, size_t * atom_length)
{
   if (position == NULL)
   {
      std::cerr << "[bf::Formula::ReadAtom] position == NULL" << std::endl;
      return false;
   }
   if (atom_type == NULL)
   {
      std::cerr << "[bf::Formula::ReadAtom] atom_type == NULL" << std::endl;
      return false;
   }
   if (atom_location == NULL)
   {
      std::cerr << "[bf::Formula::ReadAtom] atom_location == NULL" << std::endl;
      return false;
   }
   if (atom_length == NULL)
   {
      std::cerr << "[bf::Formula::ReadAtom] atom_length == NULL" << std::endl;
      return false;
   }
   (*atom_type) = kNoAtom;

   while ((*position) < formula_string.length()
         && isspace(formula_string[*position]))
   {
      (*position)++;
   }

   if ((*position) >= formula_string.length())
   {
      return true;
   }

   (*atom_location) = (*position);

   switch (formula_string[*position])
   {
      case '(':
         (*atom_type) = kLeftBracketAtom;
         break;
      case ')':
         (*atom_type) = kRightBracketAtom;
         break;
      case '-':
         (*atom_type) = kNotAtom;
         break;
      case '+':
         (*atom_type) = kOrAtom;
         break;
      case '*':
         (*atom_type) = kAndAtom;
         break;
      case '&':
         (*atom_type) = kAndAtom;
         if ((*position) + 1 < formula_string.length()
               && formula_string[(*position) + 1] == '&')
         {
            (*position)++;
         }
         break;
      case '|':
         (*atom_type) = kOrAtom;
         if ((*position) + 1 < formula_string.length()
               && formula_string[(*position) + 1] == '|')
         {
            (*position)++;
         }
         break;
      case '0':
         if ((*position) + 1 < formula_string.length()
               && (isalnum(formula_string[(*position) + 1])
                     || formula_string[(*position) + 1] == '_'))
         {
            std::cerr
                  << "[Formula::readAtom] Variable name cannot start with a digit."
                  << std::endl;
            return false;
         }
         (*atom_type) = kFalseAtom;
         break;
      case '1':
         if ((*position) + 1 < formula_string.length()
               && (isalnum(formula_string[(*position) + 1])
                     || formula_string[(*position) + 1] == '_'))
         {
            std::cerr
                  << "[Formula::readAtom] Variable name cannot start with a digit."
                  << std::endl;
            return false;
         }
         (*atom_type) = kTrueAtom;
         break;
      default:
         break;
   }

   if ((*atom_type) != kNoAtom)
   {
      (*position)++;
      (*atom_length) = (*position) - (*atom_location);
      return true;
   }

   if (!isalpha(formula_string[*position]) && formula_string[*position] != '_')
   {
      std::cerr
            << "[Formula::readAtom] A variable has to start with a letter or a _ character."
            << std::endl;
   }

   while ((*position) < formula_string.length()
         && (isalnum(formula_string[*position])
               || formula_string[*position] == '_'))
   {
      (*position)++;
   }

   (*atom_length) = (*position) - (*atom_location);

   if ((*atom_length) == 2 && tolower(formula_string[*atom_location]) == 'o'
         && tolower(formula_string[(*atom_location) + 1]) == 'r')
   {
      (*atom_type) = kOrAtom;
   }
   else if ((*atom_length) == 3
         && tolower(formula_string[*atom_location]) == 'a'
         && tolower(formula_string[(*atom_location) + 1]) == 'n'
         && tolower(formula_string[(*atom_location) + 2]) == 'd')
   {
      (*atom_type) = kAndAtom;
   }
   else if ((*atom_length) == 4
         && tolower(formula_string[*atom_location]) == 't'
         && tolower(formula_string[(*atom_location) + 1]) == 'r'
         && tolower(formula_string[(*atom_location) + 2]) == 'u'
         && tolower(formula_string[(*atom_location) + 3]) == 'e')
   {
      (*atom_type) = kTrueAtom;
   }
   else if ((*atom_length) == 5
         && tolower(formula_string[*atom_location]) == 'f'
         && tolower(formula_string[(*atom_location) + 1]) == 'a'
         && tolower(formula_string[(*atom_location) + 2]) == 'l'
         && tolower(formula_string[(*atom_location) + 3]) == 's'
         && tolower(formula_string[(*atom_location) + 4]) == 'e')
   {
      (*atom_type) = kFalseAtom;
   }
   else if ((*atom_length) == 3
         && tolower(formula_string[*atom_location]) == 'n'
         && tolower(formula_string[(*atom_location) + 1]) == 'o'
         && tolower(formula_string[(*atom_location) + 2]) == 't')
   {
      (*atom_type) = kNotAtom;
   }
   else
   {
      (*atom_type) = kVariableAtom;
   }

   return true;
}

