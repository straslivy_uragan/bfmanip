//
//  formula.h
//  bfmanip++
//
//  Created by Petr Kučera on 24.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#ifndef FORMULA_H
#define FORMULA_H
/**@file Formula.h
 * @brief File with declaration of class Formula, this includes also
 * declaration of Formula::Node as inner structure.
 */

#include <vector>
#include <string>

#include "input.h"
#include "nfelement.h"
#include "normalform.h"

namespace bf {

   /**@brief Class representing a general formula.
    *
    * A formula is a tree, where inner vertices are labeled by
    * connectives and leaves are labeled by variables and constants (0,
    * 1).
    *
    * In fact, in the implementation we do not really check, whether one
    * node is not present twice in the structure, therefore it is
    * possible, that if we look at the pointer structure of the nodes,
    * then we do not necessarily see a real tree, but a general acyclic
    * graph, i.e. a circuit. However, when manipulating and especially
    * evaluating a BFFormula object, we treat it as a tree, i.e. if a
    * node is present twice (or more times) in the structure, then it is
    * evaluated twice (or more times), i.e. as if this really would be a
    * tree. Also when we transform the structure to a string
    * representation, we get a formula, not a circuit.
    *
    * Most logic of the formula is stored in the Formula::Node nested
    * class, Formula itself serves as an encapsulation. It also
    * contains some functions which need to work globally.
    */
   class Formula
   {
      protected:

         /**@brief Represents a node in a tree structure representing formula.
          *
          * This class defines properties of both inner nodes (i.e. operators)
          * and leaf nodes (i.e. constants and variables). Negation is a unary
          * operator and it can appear anywhere in a formula, not just in front
          * of variables. 
          *
          * Note, that child nodes are considered to be owned by the
          * parent node. Thus if a node is deleted, then it also
          * deletes possible child nodes. Also if type of a node
          * changes, then it may cause deletion of child nodes.
          */
         class Node {
            public:
               /**@brief Possible types of nodes in a formula tree.
                */
               enum NodeType {
                  /**@brief Leaf node representing constant 0 (false).
                   */
                  kFalseNode,
                  /**@brief Leaf node representing constant 1 (true).
                   */
                  kTrueNode,
                  /**@brief Leaf node representing a variable.
                   */
                  kVariableNode,
                  /**@brief Inner node representing a unary negation operator.
                   */
                  kNotNode,
                  /**@brief Leaf node representing binary conjunction.
                   */
                  kAndNode,
                  /**@brief Leaf node representing binary disjunction.
                   */
                  kOrNode
               }; // NodeType
            private: 
               /**@brief NodeType of this node.
                */
               NodeType node_type_;
               /**@brief Left child of this node. 
                *
                * In case of a leaf node, the value of this variable is
                * NULL. In case of negation, the left child is the root
                * of the negated formula and the right child is nil.
                */
               Node * left_op_;
               /**@brief Right child of this node. 
                *
                * In case of a leaf node, the value of this variable is
                * NULL. In case of negation, the left child is the root
                * of the negated formula and the right child is nil.
                */
               Node * right_op_;
               /**@brief In case of a leaf node representing a variable, this
                * variable stores its index.
                */
               unsigned int variable_index_;

               Node(const Node&);               
               void operator=(const Node&);
            public:

               /**@brief Creates an empty node representing constant 0
                * (false).
                */
               Node ();

               /**@brief Deletes this node.
                *
                * Note, that child nodes are considered to be owned by
                * the parent node, so this method deletes possible
                * child nodes, too.
                */
               ~Node ();
               
               /**@brief Initializes node as a leaf node.
                *
                * Note, that if @p aType is not actually a leaf kind
                * of type, then this method successfully finishises,
                * but child nodes are set tu NULL.
                *
                * @param a_type NodeType of the new node.
                * @param a_variable_index Index of variable if the type is
                * kVariableNode, otherwise, it can be arbitrary.
                */
               Node (NodeType a_type, unsigned int a_variable_index);

               /**@brief Initializes node as an operator of given
                * type.
                *
                * The right operand is NULL by default, so that it is
                * not neccessary to provide it in case of negation.
                *
                * @param a_type NodeType of the new node.
                * @param a_left_op Left operand.
                * @param a_right_op Right operand, should be NULL in case
                * of negation.
                */
               Node (NodeType a_type, Node * a_left_op, Node * a_right_op);

               /**@brief Copies the node.
                *
                * As node gets the ownerwhip of its children, this
                * method actually performs a deep copy, i.e. both
                * child nodes are recursively copied as well.
                */
               Node * Copy ();

               /**@brief Checks whether this is a leaf node.
                *
                * A node is a leaf node if it is a constant or a
                * variable.
                *
                * @return YES, if this is a leaf node, NO otherwise.
                */
               inline bool IsLeaf () const
               {
                  return (node_type_ == kFalseNode 
                        || node_type_ == kTrueNode
                        || node_type_ == kVariableNode);
               }

               /**@brief Returns type of this node.
                */
               inline NodeType node_type () const
               {
                  return node_type_;
               }

               /**@brief Returns index of variable stored in this
                * node.
                */
               inline unsigned int variable_index () const
               {
                  return variable_index_;
               }

               /**@brief Returns left child of this node.
                */
               inline Node * left_op () const
               {
                  return left_op_;
               }

               /**@brief Returns right child of this node.
                */
               inline Node * right_op () const
               {
                  return right_op_;
               }

               /**@brief Returns left child of this node.
                *
                * This method is to provide easy access to a child
                * node in case of unary negation operator.
                */
               inline Node * unary_op () const
               {
                  return left_op_;
               }

               /**@brief Appends string representation of a subformula
                * represented by this node to given string.
                *
                * @param variable_names Names of variables to be
                * used in the string representation. Although it is an
                * input variable, it is given as a pointer not by
                * a constant reference, because it is called from
                * bf::Formula ToString method and member variable
                * bf::Formula::names_of_variables_ is passed here.
                * This value can be NULL, in which case index numbers
                * are used instead of names.
                * @param output_string String, to which the string
                * representation of this subformula will be appended.
                */
               void ToString (const std::vector<std::string> * variable_names,
                     std::string * output_string) const;

               /**@brief Evaluates subtree rooted in this node on
                * given input.
                *
                * @param input Input Boolean vector.
                * 
                * @return Value of a subtree rooted in this node on
                * given input.
                */
               bool ValueForInput (const Input& input);

               /**@brief Propagates negation operators in the subtree
                * rooted at this node, so that they negate variables
                * only.
                *
                * This method also allows to negate the subtree during
                * the process.
                *
                * @param negate Whether the subtree should be also
                * negated.
                */
               void PropagateNegations (bool negate);

               /**@brief Applies given partial assignment to the
                * formula rooted in this node.
                *
                * If the @p simplify parameter is set to true, then
                * the formula is then simplified so that it does not
                * contain trivially constant subformulas.
                *
                * @param partial_assignment Partial assignment to be
                * applied to the formula.
                * @param simplify Determines whether simplification
                * should be also performed during the process.
                */
               void ApplyPartialAssignment(const bf::PartialInput& partial_assignment,
                     bool simplify);

               /**@brief Checks, whether the subtree rooted at this
                * node represents a formula in conjunctive normal
                * form.
                */
               bool IsCNF () const;

               /**@brief Checks, whether the subtree rooted at this
                * node represents a formula in conjunctive normal
                * form.
                */
               bool IsDNF () const;

               /**@brief Checks, whether the subtree rooted at this
                * node represents a formula in conjunctive normal
                * form.
                */
               bool IsClause () const;

               /**@brief Checks, whether the subtree rooted at this
                * node represents a formula in conjunctive normal
                * form.
                */
               bool IsTerm () const;

               /**@brief Transforms the formula represented by the
                * subtree of this node into a CNF.
                *
                * This method can only be successful, if the formula
                * itself is a CNF (i.e. if #IsCNF() would return
                * true). This is determined by the return value. The
                * output CNF must be preallocated (i.e. the parameter
                * should not be NULL), at the beginning
                * the given CNF structure is not emptied, thus new
                * clauses are added to this structure.
                * If the formula is not in fact a CNF, then false is
                * returned and out_cnf is cleared.
                *
                * @param out_cnf Output parameter in which the
                * resulting cnf is stored.
                *
                * @return True, if the transformation was successful,
                * i.e. if the formula is a CNF, false otherwise.
                */
               bool TransformToCNF (CNF * out_cnf) const;

               /**@brief Transforms the formula represented by the
                * subtree of this node into a DNF.
                *
                * This method can only be successful, if the formula
                * itself is a DNF (i.e. if #IsDNF() would return
                * true). This is determined by the return value. The
                * output DNF must be preallocated, at the beginning
                * the given DNF structure is not emptied, thus new
                * terms are added to this structure. If the formula is
                * not in fact a DNF, then false is returned and
                * out_cnf is cleared.
                *
                * @param out_cnf Output parameter in which the
                * resulting dnf is stored.
                *
                * @return True, if the transformation was successful,
                * i.e. if the formula is a DNF, false otherwise.
                */
               bool TransformToDNF (DNF * out_dnf) const;

               /**@brief Transforms the formula represented by the
                * subtree of this node into a Clause.
                *
                * This method can only be successful, if the formula
                * itself is a Clause (i.e. if #IsClause() would return
                * true). This is determined by the return value. The
                * output Clause must be preallocated, at the beginning
                * the given Clause structure is not emptied, thus new
                * literals are added to this structure.
                * If the formula is not in fact a Clause, then false is
                * returned and out_cnf is cleared.
                *
                * @param out_cnf Output parameter in which the
                * resulting clause is stored.
                * @param satisfied Output parameter, it is set to
                * true, if this clause is already satisfied by
                * constant 1 present in the clause. Otherwise this
                * parameter is not set in this method.
                *
                * @return True, if the transformation was successful,
                * i.e. if the formula is a Clause, false otherwise.
                */
               bool TransformToClause (Clause * out_clause, bool * satisfied) const;

               /**@brief Transforms the formula represented by the
                * subtree of this node into a Term.
                *
                * This method can only be successful, if the formula
                * itself is a Term (i.e. if #IsTerm() would return
                * true). This is determined by the return value. The
                * output Term must be preallocated, at the beginning
                * the given Term structure is not emptied, thus new
                * literals are added to this structurere.
                * If the formula is not in fact a Term, then false is
                * returned and out_cnf is cleared.
                *
                * @param out_cnf Output parameter in which the
                * resulting term is stored.
                * @param falsified Output parameter, it is set to
                * true, if this term is already falsified by constant
                * 1 present in the clause. Otherwise this parameter is
                * not set in this method.
                *
                * @return True, if the transformation was successful,
                * i.e. if the formula is a Term, false otherwise.
                */
               bool TransformToTerm (Term * out_term, bool * falsified) const;

         }; // Node

      private:
         /**@brief Array with names of variables of this formula.
          *
          * This member variable is not supposed to be owned by the
          * formula object, i.e. when freeing a Formula object, this
          * variable is not deleted.
          */
         std::vector<std::string> * names_of_variables_;

         /**@brief Formula root.
          *
          * This member variable is supposed to be owned by the
          * formula object, i.e. when freeing a Formula object, this
          * variable is deleted.
          */
         Node * formula_root_;

         /**@brief Number of variables of this formula. */
         unsigned int number_of_variables_;

         Formula(const Formula&);               
         void operator=(const Formula&);
      public:

         /**@brief Creates an empty formula. */
         Formula();

         /**@brief Destroys the object.
          *
          * Note, that as formula root is considered to be owned by
          * the Formula object, it is deleted. 
          * The same holds for names_of_variables.
          */
         ~Formula ();

         /**@brief Constructs a formula with given number of variables
          * and possibly their names.
          *
          * @param new_number_of_variables Number of variables in a
          * formula to be constructed.
          * @param new_names_of_variables Names of variables in this
          * formula. Can be NULL. It can even contain less variables
          * than the number of all variables, in which case only
          * provided names are used (and the indices are used
          * instead of others).
          */
         Formula (unsigned int new_number_of_variables,
               std::vector<std::string> * new_names_of_variables);

         /**@brief Parses the given string and creates a
          * formula which is described by it.
          *
          * @param formula_string A string to be parsed as a description of a
          * formula.
          */
         explicit Formula (const std::string & formula_string);

         /**@brief Parses the given string and creates a formula which
          * is described by it.
          *
          * This equals to reinitialization of the whole formula.
          *
          * @param formula_string string to be parsed.
          *
          * @return 0 if parsing was successfull, index of character
          * at which a syntax error was encountered otherwise.
          */
         unsigned int InitWithString (const std::string & formula_string);

         /**@brief Parses given file and creates a formula which is
          * described by it.
          *
          * The file is assumed to contain just one formula, which can
          * be split over several lines. Comment lines starting with
          * # are possible.
          *
          * @param in Input file to read.
          *
          * @return 0 if parsing was successfull, nonzero value
          * otherwise.
          */
         unsigned int InitWithFile (FILE * in);
      
     
         /**@brief Creates a copy of this object.
          * 
          * The copy returned is newly allocated and formula root is
          * copied as well, it is a deep copy.
          */
         Formula * Copy () const;

         /**@brief Returns number of variables of this function.
          */
         inline unsigned int NumberOfVariables () const
         {
            return number_of_variables_;
         }

         /**@brief Sets names of variables to given vectors of strings.
          */
         void SetNamesOfVariables (const std::vector<std::string> * new_names_of_variables);

         std::vector<std::string> * NamesOfVariables()
         {
            return names_of_variables_;
         }

         /**@brief Returns the result of applying of given input to the
          * representation.
          *
          * If the formula is empty, then value NO (false) is returned.
          * Note, that emptiness of formula can be tested using
          * #IsEmpty() method.
          *
          * @param input Input, for which we want to evaluate result.
          *
          * @return Value of function which is represented by this structure on
          * @p input.
          */
         bool ValueForInput (const bf::Input & input) const;

         /**@brief Applies given partial assignment to this formula.
          *
          * This consists of setting all variables to the appropriate
          * values, i.e. replacing corresponding variable nodes by
          * appropriate constants. If @p simplify is true, then the
          * formula is then simplified on the way up (i.e. it is
          * partially evaluated in a sense (x || 1) = 1 and such.
          *
          * @param partial_assignment Partial assignment to be
          * applied.
          * @param simplify Determines whether formula should be
          * simplified after setting variables to given values.
          */
         void ApplyPartialAssignment (
               const bf::PartialInput & partial_assignment,
               bool simplify);

         /**@brief Appends string representation of the formula into
          * given string.
          *
          * @param output_string String to which string description
          * should be appended.
          */
         void ToString (std::string * output_string) const;

         /**@brief Simplifies this formula.
          *
          * By traversing the formula tree, this method evaluates every node,
          * whose value is constant and can be easily determined as
          * such. (Like (x || 1)=1.
          */
         void Simplify ();

         /**@brief Negates this formula.
          *
          * This method creates new node representing a negation, with former
          * root node as its single child. This new node is then set to be the
          * new root node. An empty formula is left unchanged. Note,
          * that simplification or propagation of negations are not
          * parts of this method.
          */
         void Negate ();

         /**@brief Sets formula to represent only a false (0) constant
          * function.
          */
         void SetToFalse ();

         /**@brief Sets formula to represent only a true (1) constant
          * function.
          */
         void SetToTrue ();

         /**@brief Sets formula to represent only a function
          * consisting of a single positive occurrence of given
          * variable.
          *
          * @param variable_index Index of variable, this formula
          * should represent.
          */
         void SetToVariable (unsigned int variable_index);

         /**@brief Modify this formula to represent conjunction of the original
          * formula with the given one.
          *
          * Creates new node representing conjunction with the original formula
          * as the left child and @p formula as the right child. This new node
          * is then set to be the new root node. If before this operation the
          * formula is empty, then this method simply sets root node of @p
          * formula as the new root node.
          *
          * @param formula A formula we want to make conjunction with.
          */
         void MakeConjunction (const Formula & formula);

         /**@brief Modify this formula to represent disjunction of the original
          * formula with the given one.
          *
          * Creates new node representing disjunction with the original formula
          * as the left child and @p formula as the right child. This new node
          * is then set to be the new root node. If before this operation the
          * formula is empty, then this method simply sets root node of @p
          * formula as the new root node.
          *
          * @param formula A formula we want to make disjunction with.
          */
         void MakeDisjunction (const Formula & formula);

         /**@brief Propagates all negation operators so that they appear only
          * in front of variables.
          *
          * After this method finishes, negation operations have all only a
          * leaf node representing a variable as a child.
          */
         void PropagateNegations ();

         /**@brief Checks, whether this formula is in conjunctive normal form.
          */
         bool IsCNF () const;

         /**@brief Checks, whether this formula is in disjunctive normal form.
          */
         bool IsDNF () const;

         /**@brief Checks, whether this formula is a clause, i.e. disjunction
          * of literals.
          */
         bool IsClause () const;

         /**@brief Checks, whether this formula is a term, i.e. conjunction of
          * literals.
          */
         bool IsTerm () const;

         /**@brief Checks, whether this formula is empty, i.e. its
          * formula root is NULL.
          */
         inline bool IsEmpty () const
         {
            return (formula_root_ == NULL);
         }

         /**@brief Transforms this formula into a CNF.
          *
          * This method can only be successful, if the formula
          * itself is a CNF (i.e. if #IsCNF() would return
          * true). This is determined by the return value. The
          * output CNF must be preallocated (i.e. the parameter
          * should not be NULL), at the beginning
          * the given CNF structure is reinitialized as empty.
          * If the formula is not in fact a CNF, then false is
          * returned and out_cnf is cleared.
          *
          * @param out_cnf Output parameter in which the
          * resulting cnf is stored.
          *
          * @return True, if the transformation was successful,
          * i.e. if the formula is a CNF, false otherwise.
          */
         bool TransformToCNF (CNF * out_cnf) const;

         /**@brief Transforms this formula into a DNF.
          *
          * This method can only be successful, if the formula
          * itself is a DNF (i.e. if #IsDNF() would return
          * true). This is determined by the return value. The
          * output DNF must be preallocated, at the beginning
          * the given DNF structure is reinitialized as empty.
          * If the formula is not in fact a DNF, then false is
          * returned and out_cnf is cleared.
          *
          * @param out_cnf Output parameter in which the
          * resulting cnf is stored.
          *
          * @return True, if the transformation was successful,
          * i.e. if the formula is a DNF, false otherwise.
          */
         bool TransformToDNF (DNF * out_dnf) const;

         /**@brief Transforms this formula into a Clause.
          *
          * This method can only be successful, if the formula
          * itself is a Clause (i.e. if #IsClause() would return
          * true). This is determined by the return value. The
          * output Clause must be preallocated, at the beginning
          * the given Clause structure is reinitialized as empty.
          * If the formula is not in fact a Clause, then false is
          * returned and out_cnf is cleared.
          *
          * @param out_cnf Output parameter in which the
          * resulting cnf is stored.
          *
          * @return True, if the transformation was successful,
          * i.e. if the formula is a Clause, false otherwise.
          */
         bool TransformToClause (Clause * out_clause) const;

         /**@brief Transforms this formula into a Term.
          *
          * This method can only be successful, if the formula
          * itself is a Term (i.e. if #IsTerm() would return
          * true). This is determined by the return value. The
          * output Term must be preallocated, at the beginning
          * the given Term structure is reinitialized as empty.
          * If the formula is not in fact a Term, then false is
          * returned and out_cnf is cleared.
          *
          * @param out_cnf Output parameter in which the
          * resulting cnf is stored.
          *
          * @return True, if the transformation was successful,
          * i.e. if the formula is a Term, false otherwise.
          */
         bool TransformToTerm (Term * out_term) const;

      private:
         /**@brief NodeType of an atom within the formula.
          *
          * This is part of the implementation of parsing of the
          * formula from its string representation, and that's why it
          * is private.
          */
         enum AtomType {
            /**@brief No atom to remember. */
            kNoAtom,
            /**@brief A constant true or 1. */
            kTrueAtom,
            /**@brief A constant false or 0. */
            kFalseAtom,
            /**@brief A variable. */
            kVariableAtom,
            /**@brief A negation operator. */
            kNotAtom,
            /**@brief A conjunction operator. */
            kAndAtom,
            /**@brief A disjunction operator. */
            kOrAtom,
            /**@brief A left bracket '('. */
            kLeftBracketAtom,
            /**@brief A right bracket ')'. */
            kRightBracketAtom
         };

         /**@brief Parses given string at the level of disjunction.
          *
          * @param formula_string A string to parse.
          * @param position From this position the parsing will start
          * in given string. This value is updated during parsing.
          * @param last_atom_type When an atom was read, but it has
          * not been used yet, it is stored in this output variable.
          * If no such atom needs to be stored, then kNoAtom is stored
          * in this variable.
          * @param last_atom_location When some atom is stored in @p
          * last_atom_type, then a position where this atom starts in
          * @p formula_string is given in this output variable.
          * @param last_atom_length When some atom is stored in @p
          * last_atom_type, then its length within
          * formula_string is given in this output variable.
          *
          * @return If parsing successfully finished, then in the
          * return value you can find the node representing the part
          * of string which was parsed.
          */
         Node * ParseDisjunction (const std::string & formula_string,
               unsigned int * position,
               AtomType * last_atom_type,
               size_t * last_atom_location,
               size_t * last_atom_length);

         /**@brief Parses given string at the level of conjunction.
          *
          * @param formula_string A string to parse.
          * @param position From this position the parsing will start
          * in given string. This value is updated during parsing.
          * @param last_atom_type When an atom was read, but it has
          * not been used yet, it is stored in this output variable.
          * If no such atom needs to be stored, then kNoAtom is stored
          * in this variable.
          * @param last_atom_location When some atom is stored in @p
          * last_atom_type, then a position where this atom starts in
          * @p formula_string is given in this output variable.
          * @param last_atom_length When some atom is stored in @p
          * last_atom_type, then its length within
          * formula_string is given in this output variable. 
          *
          * @return If parsing successfully finished, then in the
          * return value you can find the node representing the part
          * of string which was parsed.
          */
         Node * ParseConjunction (const std::string & formula_string,
               unsigned int * position,
               AtomType * last_atom_type,
               size_t * last_atom_location,
               size_t * last_atom_length);

         /**@brief Parses given string at the level of negation.
          *
          * @param formula_string A string to parse.
          * @param position From this position the parsing will start
          * in given string. This value is updated during parsing.
          * @param last_atom_type When an atom was read, but it has
          * not been used yet, it is stored in this output variable.
          * If no such atom needs to be stored, then kNoAtom is stored
          * in this variable.
          * @param last_atom_location When some atom is stored in @p
          * last_atom_type, then a position where this atom starts in
          * @p formula_string is given in this output variable.
          * @param last_atom_length When some atom is stored in @p
          * last_atom_type, then its length within
          * formula_string is given in this output variable. 
          *
          * @return If parsing successfully finished, then in the
          * return value you can find the node representing the part
          * of string which was parsed.
          */
         Node * ParseNegation (const std::string & formula_string,
               unsigned int * position,
               AtomType * last_atom_type,
               size_t * last_atom_location,
               size_t * last_atom_length);

         /**@brief Parses given string to read a single atom from it.
          *
          * @param formula_string A string to parse.
          * @param position From this position the parsing will start
          * in given string. This value is updated during parsing.
          * @param atom_type In this variable a type of atom which was
          * read is stored. If no such atom needs to be stored, then
          * kNoAtom is stored in this variable. It does not
          * neccessarily mean, that a syntax error occurred.
          * @param atom_location When some atom is stored in @p
          * atom_type, then a position where this atom starts in
          * @p formula_string is given in this output variable.
          * @param atom_length When some atom is stored in @p
          * last_atom_type, then its length within
          * formula_string is given in this output variable. 
          *
          * @return The return value determines whether parsing of an
          * atom was successful, if so, true is returned, false
          * otherwise, i.e. in case of a syntax error.
          */
         bool ReadAtom (const std::string & formula_string,
               unsigned int * position,
               AtomType * atom_type,
               size_t * atom_location,
               size_t * atom_length);
               
   
   }; // Formula

}
#endif // FORMULA_H
