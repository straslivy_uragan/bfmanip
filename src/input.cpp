//
//  input.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 27.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#include "input.h"

bool bf::InputIncrement (bf::Input * input)
{
   if (input == NULL)
   {
      return false;
   }
   size_t index = 0;
   for (index = input -> size ();
        index > 0;
        index --)
   {
      if (! (* input) [index - 1])
      {
         break;
      }
   }
   bool overflow_detected = (index == 0);
   if (index > 0)
   {
      (* input) [index - 1] = true;
   }
   while (index < input -> size ())
   {
      (* input) [index ++] = false;
   }
   return overflow_detected;
}

void bf::PartialInputToString (const bf::PartialInput & partial_input,
      std::string * output_string)
{
   if (output_string == NULL)
   {
      return;
   }
   for (bf::PartialInput::const_iterator iterator = partial_input.begin ();
         iterator < partial_input.end ();
         iterator ++)
   {
      switch (* iterator)
      {
         case kFalseValue:
            (* output_string) += '0';
            break;
         case kTrueValue:
            (* output_string) += '1';
            break;
         case kUnknownValue:
            (* output_string) += '*';
            break;
         default:
            (* output_string) += '?';
            break;
      }
   }
}

void bf::InputToString (const bf::Input & input, std::string * output_string)
{
   if (output_string == NULL)
   {
      return;
   }
   for (Input::const_iterator iterator = input.begin ();
         iterator < input.end ();
         iterator ++)
   {
      (* output_string) += ((* iterator) ? '1' : '0');
   }
}

size_t bf::StringToPartialInput (const std::string& input_string,
      bf::PartialInput * partial_input )
{
   bool finish = false;
   size_t number_of_characters = 0;
   for (std::string::const_iterator input_string_iterator = input_string.begin();
         input_string_iterator != input_string.end()
         && ! finish;
         ++ input_string_iterator, ++ number_of_characters)
   {
      switch (* input_string_iterator)
      {
         case '0':
            partial_input->push_back(bf::kFalseValue);
            break;
         case '1':
            partial_input->push_back(bf::kTrueValue);
            break;
         case '*':
            partial_input->push_back(bf::kUnknownValue);
            break;
         default:
            finish = true;
      }
   }

   return number_of_characters;
}

size_t bf::StringToInput (const std::string& input_string,
      Input * input)
{
   bool finish = false;
   size_t number_of_characters = 0;
   for (std::string::const_iterator input_string_iterator = input_string.begin();
         input_string_iterator != input_string.end()
         && ! finish;
         ++ input_string_iterator, ++ number_of_characters)
   {
      switch (* input_string_iterator)
      {
         case '0':
            input->push_back(false);
            break;
         case '1':
            input->push_back(true);
            break;
         default:
            finish = true;
      }
   }

   return number_of_characters;
}

