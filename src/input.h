//
//  input.h
//  bfmanip++
//
//  Created by Petr Kučera on 27.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
#ifndef INPUT_H
#define INPUT_H
/**@file input.h
 * @brief File with declaration of types representing partial input
 * and boolean input.
 */

#include <vector>
#include <bitset>
#include <iostream>

/**@namespace bf
 * @brief Namespace used for classes and functions representing and
 * working with Boolean functions.
 */
namespace bf {
   
   /**@brief Possible values within partial input.
    */
   enum PIValues {
      /**@brief Value false or 0. */
      kFalseValue,
      /**@brief Value true or 1. */
      kTrueValue,
      /**@brief Unknown value or *. */
      kUnknownValue
   };

   /**@brief Partial input is defined as a vector of PIValues.
    */
   typedef std::vector<PIValues> PartialInput;

   /**@brief Input is defined as a boolean vector. */
   typedef std::vector<bool> Input;

   /**@brief Increments given input by one.
    *
    * Considers input as an array of boolean values representing a
    * natural number in binary and performs increment operation. The
    * return value determines whether overflow occurred. The least
    * significant bit is considered to be on the last position.
    * 
    * @param input Input vector to be incremented.
    *
    * @return True if overflow ocurred during incrementation, false
    * otherwise.
    */
   bool InputIncrement (Input * input);

   /**@brief Outputs a string description of given partial input.
    *
    * It appends a string description to given string.
    */
   void PartialInputToString (const bf::PartialInput & partial_input, std::string * output_string);

   /**@brief Outputs a string description of given input.
    *
    * It appends a string description to given string.
    */
   void InputToString (const bf::Input & input, std::string * output_string);

   /**@brief Parses given string and produces partial input whose
    * representation it contains.
    *
    * The return value is equal to number of characters used in the
    * input string.
    *
    * @param input_string String with partial input representation.
    * @param partial_input Into this partial input the new values are
    * added using push_back, i.e. they are appended at the end of the
    * underlying vector. 
    *
    * @return Number of characters actually read from @p input_string.
    */
   size_t StringToPartialInput (const std::string& input_string, 
         PartialInput * partial_input);
   
   /**@brief Parses given string and produces input whose
    * representation it contains.
    *
    * The return value is equal to number of characters used in the
    * input string.
    *
    * @param input_string String with partial input representation.
    * @param input Into this input the new values are added using
    * push_back, i.e. they are appended at the end of the underlying
    * vector. 
    *
    * @return Number of characters actually read from @p input_string.
    */
   size_t StringToInput (const std::string& input_string,
         Input * input);
}

#endif // INPUT_H
