//
//  main.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 24.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>
#include <iostream>
#include <fstream>

#include <sys/time.h>
#include <sys/resource.h>

#include "input.h"
#include "formula.h"
#include "nfelement.h"
#include "normalform.h"
#include "davisputnam.h"
#include "dpll.h"
#include "bfutils.h"
#include "cdcl.h"
#include "PropagationCompleteness.h"
#include "cdclconfig.h"

int FormulaTest()
{
   bf::Input input (4, false);

   bool overflow = false;
   std::string output_string;
   while (! overflow)
   {
      bf::InputToString(input, & output_string);
      std::cout << output_string << std::endl;
      output_string.clear ();
      overflow = bf::InputIncrement (& input);
   }


   bf::Formula formula ("(a or b)(c or d)");
   output_string = "";
   formula.ToString (& output_string);
   std::cout << output_string << std::endl;

   input[0]=false;
   input[1]=false;
   input[2]=true;
   input[3]=true;
   std::cout << "Value " << formula.ValueForInput(input) << std::endl;
   std::cout << "Is cnf: " << formula.IsCNF () << std::endl;

   formula.Negate();
   output_string = "";
   formula.ToString(& output_string);
   std::cout << output_string << std::endl;
   formula.PropagateNegations();
   output_string = "";
   formula.ToString(& output_string);
   std::cout << output_string << std::endl;

   bf::PartialInput partial_input;
   StringToPartialInput("0**1", &partial_input);
   output_string = "";
   partial_input.clear();
   PartialInputToString(partial_input, &output_string);
   std::cout << "Input " << output_string << std::endl;
   formula.ApplyPartialAssignment(partial_input, true);
   output_string = "";
   formula.ToString(& output_string);
   std::cout << output_string << std::endl;
   return 0;
}

int NFElementTest()
{
   std::vector<std::string> variable_names;
   variable_names.push_back ("a");
   variable_names.push_back ("b");
   variable_names.push_back ("c");
   variable_names.push_back ("d");
   variable_names.push_back ("e");
   variable_names.push_back ("f");
   variable_names.push_back ("g");
   bf::Clause clause1;
   clause1.AddLiteral(2, true);
   clause1.AddLiteral(2, true);
   clause1.AddLiteral(3, false);
   clause1.AddLiteral(1, false);
   std::string output_string;
   clause1.ToString(&variable_names, &output_string);
   std::cout << output_string;
   std::cout << "(neg: " << static_cast<unsigned int>(clause1.NumberOfNegativeLiterals ()) << ")" << std::endl;

   bf::PartialInput partial_input;
   bf::StringToPartialInput("001**1", &partial_input);
   bf::Clause clause2;
   clause2.InitWithPartialInput(partial_input);
   output_string.clear();
   clause2.ToString(&variable_names, &output_string);
   std::cout << output_string;
   std::cout << "(neg: " << static_cast<unsigned int>(clause2.NumberOfNegativeLiterals ()) << ")" << std::endl;
   output_string = "";
   PartialInputToString(partial_input, &output_string);
   std::cout << "Input " << output_string << std::endl;
   clause1.MergeWithElement(clause2);
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "Merged: " << output_string;
   std::cout << "(neg: " << static_cast<unsigned int>(clause1.NumberOfNegativeLiterals ()) << ")" << std::endl;
   clause1.ChangePolarity(3);
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "Changed d: " << output_string << std::endl;
   clause1.ChangePolarityOfAllLiterals();
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "Changed all: " << output_string << std::endl;
   clause1.RemoveVariable(3);
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "Remove d: " << output_string << std::endl;

   clause1.RemoveAllLiterals();
   clause2.RemoveAllLiterals();
   partial_input.clear();
   StringToPartialInput("00*1*1", &partial_input);
   clause1.InitWithPartialInput(partial_input);
   partial_input.clear();
   StringToPartialInput("**0*1*", &partial_input);
   clause2.InitWithPartialInput(partial_input);
   std::cout << "Merge (";
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << output_string;
   std::cout << ") with (";
   output_string.clear();
   clause2.ToString(&variable_names, &output_string);
   std::cout << output_string;
   unsigned int conflicts = clause1.NumberOfConflicts (clause2);
   clause1.MergeWithElement(clause2);
   std::cout << ") to (";
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << output_string << ")" << std::endl;
   std::cout << "Number of conflicts: " << conflicts << std::endl;

   clause1.RemoveAllLiterals();
   clause2.RemoveAllLiterals();
   partial_input.clear();
   StringToPartialInput("00*1*1", &partial_input);
   clause1.InitWithPartialInput(partial_input);
   partial_input.clear();
   StringToPartialInput("01110*", &partial_input);
   clause2.InitWithPartialInput(partial_input);
   std::cout << "Resolvent of (";
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << output_string;
   std::cout << ") and (";
   output_string.clear();
   clause2.ToString(&variable_names, &output_string);
   std::cout << output_string;
   bf::Clause resolvent;
   conflicts = clause1.Resolve (clause2, &resolvent);
   std::cout << ") is (";
   output_string.clear();
   resolvent.ToString(&variable_names, &output_string);
   std::cout << output_string << ")" << std::endl;
   std::cout << "Number of conflicts: " << conflicts << std::endl;

   partial_input.clear();
   resolvent.PartialInputFromElement(&partial_input);
   output_string.clear();
   PartialInputToString(partial_input, &output_string);
   std::cout << output_string << std::endl;
   std::cout << "Value: " << resolvent.ValueForPartialInput (partial_input) << std::endl;
   partial_input[3] = bf::kFalseValue;
   std::cout << "Value ([3] -> 0): " << resolvent.ValueForPartialInput (partial_input) << std::endl;
   partial_input[3] = bf::kUnknownValue;
   std::cout << "Value ([3] -> *): " << resolvent.ValueForPartialInput (partial_input) << std::endl;
   resolvent.ApplyPartialAssignment(partial_input);
   output_string.clear();
   resolvent.ToString(&variable_names, &output_string);
   std::cout << "Applied the last: " << output_string << std::endl;

   bf::Clause * clause1_copy = static_cast<bf::Clause *>(clause1.Copy());
   output_string.clear();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "NFClause 1: " << output_string << std::endl;
   output_string.clear();
   clause1_copy->ToString(&variable_names, &output_string);
   std::cout << "Copy of clause 1: " << output_string << std::endl;

   std::cout << "Equality: " << clause1.IsEqual(*clause1_copy) << std::endl;
   clause1_copy->RemoveVariable(5);
   output_string.clear();
   clause1_copy->ToString(&variable_names, &output_string);
   std::cout << "Copy of clause 1 without f: " << output_string << std::endl;
   std::cout << "Equality: " << clause1.IsEqual(*clause1_copy) << std::endl;
   std::cout << "Subelement: " << clause1_copy->IsSubelementOf(clause1) << std::endl;
   clause1_copy->ChangePolarity(3);
   output_string.clear();
   clause1_copy->ToString(&variable_names, &output_string);
   std::cout << "... inverted d: " << output_string << std::endl;
   std::cout << "Subelement: " << clause1_copy->IsSubelementOf(clause1) << std::endl;

   return 0;
}

int NormalFormTest()
{
   std::vector<std::string> variable_names;
   variable_names.push_back ("a");
   variable_names.push_back ("b");
   variable_names.push_back ("c");
   variable_names.push_back ("d");
   variable_names.push_back ("e");
   variable_names.push_back ("f");
   variable_names.push_back ("g");

   bf::Clause clause;
   bf::PartialInput partial_input;
   bf::CNF cnf;
   StringToPartialInput("00*1*1", &partial_input);
   clause.InitWithPartialInput(partial_input);
   cnf.AddElement(clause);
   partial_input.clear();
   StringToPartialInput("01110*", &partial_input);
   clause.InitWithPartialInput(partial_input);
   cnf.AddElement(clause);

   std::string output_string;
   cnf.ToString (&variable_names, &output_string);
   std::cout << "CNF: " << output_string << std::endl;
   bf::CNF cnf_copy;
   cnf_copy.InitWithNF (cnf);
   output_string.clear();
   cnf_copy.ToString (&variable_names, &output_string);
   std::cout << "CNF copy: " << output_string << std::endl;

   partial_input.clear();
   StringToPartialInput("01110*", &partial_input);
   std::cout << "Value on [0110*]: ";
   std::cout << cnf.ValueForPartialInput(partial_input) << std::endl;

   cnf.ApplyPartialAssignment(partial_input);
   output_string.clear();
   cnf.ToString (&variable_names, &output_string);
   std::cout << "CNF (pa [0110*]): " << output_string << std::endl;

   output_string.clear();
   cnf_copy.ToString (&variable_names, &output_string);
   std::cout << "CNF copy: " << output_string << std::endl;
   partial_input.clear();
   StringToPartialInput("1*110*", &partial_input);
   std::cout << "Value on [1*110*]: ";
   std::cout << cnf_copy.ValueForPartialInput(partial_input) << std::endl;

   bf::Input input;
   bf::StringToInput ("001101", &input);
   std::cout << "Value on [001101]: ";
   std::cout << cnf_copy.ValueForInput(input) << std::endl;

   bf::Clause clause1 (cnf_copy.FirstElement());
   output_string.clear ();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "NFClause " << output_string;
   std::cout << (cnf.ContainsElement (clause1)
         ? "is"
         : "is not");
   std::cout << " contained in the cnf." << std::endl;

   clause1.AddLiteral(2, false);
   output_string.clear ();
   clause1.ToString(&variable_names, &output_string);
   std::cout << "NFClause " << output_string;
   std::cout << (cnf.ContainsElement (clause1)
         ? "is"
         : "is not");
   std::cout << " contained in the cnf." << std::endl;
   std::cout << "Subelement of clause " << output_string;
   std::cout << (cnf.ContainsSubelement (clause1)
         ? "is"
         : "is not");
   std::cout << " contained in the cnf." << std::endl;

   cnf_copy.AddElement (clause1);
   output_string.clear();
   cnf_copy.ToString (&variable_names, &output_string);
   std::cout << "CNF copy (added): " << output_string << std::endl;
   cnf_copy.RemoveElement (clause1);
   output_string.clear();
   cnf_copy.ToString (&variable_names, &output_string);
   std::cout << "CNF copy (removed): " << output_string << std::endl;

   return 0;
}

int DimacsTest (int argc, char * argv[])
{
   if (argc > 1)
   {
      bf::CNF cnf;
      FILE * dimacs_file = fopen (argv [1], "r");
      cnf.InitWithDIMACSFile (dimacs_file);
      fclose(dimacs_file);
      std::string output_string;
      cnf.ToString(NULL, &output_string);
      std::cout << output_string << std::endl;
      printf ("Number of variables: %u\n", static_cast<unsigned int>(cnf.NumberOfVariables()));
      printf ("Number of clauses: %u\n", static_cast<unsigned int>(cnf.NumberOfElements ()));
      printf ("Number of trivial elements: %u\n", cnf.RemoveTrivialElements());
      if (argc > 2)
      {
         //cnf.InitRandomly(20, 30, 3);
         FILE * out_file = fopen (argv[2], "w");
         cnf.WriteDIMACSFile(out_file);
         fclose (out_file);
      }
   }
   return 0;
}

int DPTest (int , char *argv[])
{
   bf::CNF cnf;
   time_t seed = time(NULL);
   srand(static_cast<int>(seed));
   //cnf.InitRandomly (50, 80, 3);
   FILE * dimacs_file = fopen (argv [1], "r");
   cnf.InitWithDIMACSFile (dimacs_file);
   fclose(dimacs_file);

   cnf.RemoveTrivialElements();
   cnf.WriteDIMACSFile(stdout);
   bf::Input input;
   int * var_order = (int*)malloc(cnf.NumberOfVariables() * sizeof (int));
   fprintf (stderr, "seed: %d\n", static_cast<int>(seed));

   for (int var = 0; var < static_cast<int>(cnf.NumberOfVariables()); ++ var)
   {
      var_order[var]=var; //cnf.NumberOfVariables()-var-1;
   }
   for (int var = cnf.NumberOfVariables()-1; var > 0; -- var)
   {
      int other = rand() % (var + 1);
      if (other != var)
      {
         int buf = var_order[var];
         var_order[var] = var_order [other];
         var_order[other] = buf;
      }
   }
   fputs ("order: ", stderr);
   for (int var = 0; var < static_cast<int>(cnf.NumberOfVariables()); ++ var)
   {
      fprintf (stderr, "%d ", var_order[var]);
   }
   fputc ('\n', stderr);
   struct rusage start_usage;
   getrusage(RUSAGE_SELF, &start_usage);
   bf::DumpResourceUsage(stdout);
   bf::DavisPutnam dp(cnf, var_order);
   bf::Result result = dp.TestSatisfiability(&input);
   printf ("Sat test ended with result: %d (time: %lu)\n", result, time(NULL)-seed);
   if (result == bf::kSatisfiable)
   {
      std::string output_string;
      bf::InputToString(input, &output_string);
      printf ("Found satisfying assignment %s\n", output_string.c_str());
      printf ("Value of cnf on this assignment: %d\n", cnf.ValueForInput(input));
   }
   free (var_order);
   bf::DumpRelativeResourceUsage(stdout, &start_usage);
   return 0;
}

int runDPLL (int, char * argv[])
{
   bf::CNF cnf;
   time_t seed = time(NULL);
   fprintf (stderr, "seed: %d\n", static_cast<int>(seed));
   srand(static_cast<int>(seed));
   //cnf.InitRandomly (50, 80, 3);
   FILE * dimacs_file = fopen (argv [2], "r");
   cnf.InitWithDIMACSFile (dimacs_file);
   fclose(dimacs_file);

   cnf.RemoveTrivialElements();
   cnf.WriteDIMACSFile(stdout);
   bf::Input input;

   struct rusage start_usage;
   getrusage(RUSAGE_SELF, &start_usage);
   bf::DumpResourceUsage(stdout);
   bf::DPLL dpll(cnf);
   bf::Result result = dpll.Run (&input);
   printf ("Sat test ended with result: %d\n", result);
   if (result == bf::kSatisfiable)
   {
      std::string output_string;
      bf::InputToString(input, &output_string);
      printf ("Found satisfying assignment %s\n", output_string.c_str());
      printf ("Value of cnf on this assignment: %d\n", cnf.ValueForInput(input));
   }
   printf ("Max level reached: %d\n", dpll.max_level());
   printf ("Number of processed literals: %d\n", dpll.number_of_processed_literals());
   bf::DumpRelativeResourceUsage(stdout, &start_usage);
   return 0;
}

int runCDCL (int argc, char * argv[])
{
   bf::CNF cnf;
   //int seed=1482240485;
   int seed = (int)time(NULL);
   int f_index=2;
   bf::CDCLConfig cfg;
   if(argc>4 && strcmp(argv[2], "-c")==0)
   {
      try {
         std::ifstream in(argv[3]);
         bf::CDCLConfig cfgfile(in);
         cfg=cfgfile;
      }
      catch(std::exception &e)
      {
         std::cerr<<"Failed to read the configuration file: "<<
            e.what()<<std::endl;
         std::cerr<<"I will use the default configuration instead."<<std::endl;
      }
      f_index=4;
   }
   std::cout<<"Current configuration:"<<std::endl;
   cfg.Dump(std::cout);
   srand(seed);
   //cnf.InitRandomly (50, 80, 3);
   FILE * dimacs_file = fopen (argv [f_index], "r");
   if (dimacs_file == NULL)
   {
       fprintf (stderr, "Could not open input file %s\n", argv[2]);
       return 1;
   }
   cnf.InitWithDIMACSFile (dimacs_file);
   fclose(dimacs_file);
   puts("File read");

   cnf.RemoveTrivialElements();
   puts("Trivial elements removed");
//   cnf.WriteDIMACSFile(stdout);
   bf::Input input;

   struct rusage start_usage;
   getrusage(RUSAGE_SELF, &start_usage);
   //bf::DumpResourceUsage(stdout);
   bf::CDCL cdcl(cnf, cfg);
   puts("CDCL instance initialized");
   bf::Result result = cdcl.Run (&input);
   const char * result_string;
   switch (result)
   {
      case bf::kUnsatisfiable:
         result_string = "UNSATISFIABLE";
         break;
      case bf::kSatisfiable:
         result_string = "SATISFIABLE";
         break;
      case bf::kFailed:
         result_string = "FAILED";
         break;
      case bf::kBadParameters:
         result_string = "BAD PARAMETERS";
         break;
      case bf::kUndetermined:
         result_string = "UNDETERMINED";
         break;
      default:
         result_string = "UNKNOWN";
         break;
   }
   printf ("Sat test ended with result: %d (%s)\n", result, result_string);
   if (result == bf::kSatisfiable)
   {
      std::string output_string;
      bf::InputToString(input, &output_string);
      printf ("Found satisfying assignment %s\n", output_string.c_str());
      printf ("Value of cnf on this assignment: %d\n", cnf.ValueForInput(input));
   }
   cdcl.PrintLongStatistics(stdout);
   bf::DumpRelativeResourceUsage(stdout, &start_usage);
   return 0;
}

int propagation_completeness_test(int argc, char *argv[])
{
   if (argc != 3)
   {
      std::cout << "Usage: bfmanip pctest input.cnf" << std::endl;
      return 1;
   }
   bf::CNF cnf;

   FILE * dimacs_file = fopen (argv [2], "r");
   if (dimacs_file == NULL)
   {
       fprintf (stderr, "Could not open input file %s\n", argv[2]);
       return 1;
   }
   cnf.InitWithDIMACSFile (dimacs_file);
   fclose(dimacs_file);
   puts("File read");

   bf::PropagationCompleteness pctester(cnf);
   bf::Clause emp_implicate;
   unsigned int emp_literal;
   int level = pctester.FindEmpoweringImplicate(&emp_implicate, &emp_literal, false);
   std::string output_string;
   emp_implicate.DIMACSString(&output_string);
   if (level > 0)
   {
      std::cout << "Found empowering implicate " << output_string;
      std::cout << " at level " << level;
      std::cout << " with empowering literal " << (emp_literal%2 ? "-" : "");
      std::cout << emp_literal/2+1 << std::endl;
   }
   else
   {
      std::cout << "No empowering implicate found." << std::endl;
   }

   return 0;
}

int propagation_completeness_find(int argc, char *argv[])
{
   srand((unsigned)time(NULL));
   int level = 0;
   int num_variables = 8;
   int num_clauses = 5;
   int literals_per_clause = 3;
   int max_level = 3;
   if (argc != 6)
   {
      fputs ("Usage: bfmanip pcfind num_variables num_clauses literals_per_clause max_level\n", stderr);
      return 1;
   }
   if (sscanf(argv[2], "%d", &num_variables) == 0)
   {
      fputs ("Usage: bfmanip pcfind num_variables num_clauses literals_per_clause max_level\n", stderr);
      return 1;
   }
   if (sscanf(argv[3], "%d", &num_clauses) == 0)
   {
      fputs ("Usage: bfmanip pcfind num_variables num_clauses literals_per_clause max_level\n", stderr);
      return 1;
   }
   if (sscanf(argv[4], "%d", &literals_per_clause) == 0)
   {
      fputs ("Usage: bfmanip pcfind num_variables num_clauses literals_per_clause max_level\n", stderr);
      return 1;
   }
   if (sscanf(argv[5], "%d", &max_level) == 0)
   {
      fputs ("Usage: bfmanip pcfind num_variables num_clauses literals_per_clause max_level\n", stderr);
      return 1;
   }
   fprintf(stdout, "bfmanip %d %d %d %d\n", num_variables,
         num_clauses, literals_per_clause, max_level);
   
   while (level < max_level)
   {
      bf::CNF cnf;
      cnf.InitRandomly(num_variables, num_clauses, literals_per_clause);
      std::cout << "CNF: " << std::endl;
      cnf.WriteDIMACSFile(stdout);
      bf::PropagationCompleteness pctester(cnf);
      bf::Clause emp_implicate;
      unsigned int emp_literal;
      level = pctester.FindEmpoweringImplicateBounded(&emp_implicate,
            &emp_literal, max_level, false);
      std::string output_string;
      emp_implicate.DIMACSString(&output_string);
      if (level > 0)
      {
         std::cout << "Found empowering implicate " << output_string;
         std::cout << " at level " << level;
         std::cout << " with empowering literal " << (emp_literal%2 ? "-" : "");
         std::cout << emp_literal << std::endl;
      }
      else
      {
         std::cout << "No empowering implicate found." << std::endl;
      }
   }
   return 0;
}

void print_usage()
{
   fputs ("Usage: Use one of the following forms.\n", stderr);
   fputs ("\nbfmanip dpll dimacs_file\n", stderr);
   fputs ("\t... Run DPLL on a given DIMACS input file.\n", stderr);
   fputs ("\nbfmanip cdcl [-c config_file] dimacs_file\n", stderr);
   fputs ("\t... Run CDCL on a given DIMACS input file. A configuration file can be passed via -c option.\n", stderr);
   fputs ("\nbfmanip pctest input.cnf\n", stderr);
   fputs ("\t... Check if the input DIMACS file describes a propagation complete formula,\n", stderr);
   fputs ("\t    or it admits an empowering clause (which is returned in this case).\n", stderr);
   fputs ("\nbfmanip pcfind num_variables num_clauses literals_per_clause max_level\n", stderr);
   fputs ("\t... Try to find a propagation complete formula with a given number of variables,\n", stderr);
   fputs ("\t    clauses and literals per clause. The formula is chosen randomly.\n", stderr);
   fputs ("\nbfmanip canon input.cnf [output.cnf]\n", stderr);
   fputs ("\t... Generate a canonical formula for a given CNF in DIMACS format,\n", stderr);
   fputs ("\nbfmanip pccompile input.cnf [compiled.cnf]\n", stderr);
   fputs ("\t... Attempt to compile a formula (CNF in DIMACS format).\n", stderr);
   fputs ("\t    It generates a canonical formula and for each generated implicate\n", stderr);
   fputs ("\t    it checks, if it is empowering.\n", stderr);
}

bool print_clause (const bf::Clause &clause)
{
   std::string out;
   clause.DIMACSString(&out);
   std::cout<<out<<std::endl;
   return true;
}

int
generate_canonical_nf(int argc, char *argv[])
{
   if (argc < 3 || argc > 4)
   {
      print_usage();
      return 1;
   }
 
   bf::CNF cnf;
   FILE * dimacs_file = fopen (argv [2], "r");
   if (dimacs_file == NULL)
   {
       fprintf (stderr, "Could not open input file %s\n", argv[2]);
       return 1;
   }
   cnf.InitWithDIMACSFile (dimacs_file);
   fclose(dimacs_file);
   puts("File read");
   
   bf::CNF canonical;
   cnf.CreateCanonical(&canonical, print_clause);

   FILE *output_file=stdout;
   if (argc==4)
   {
      output_file=fopen(argv[3], "w");
      if(output_file==NULL)
      {
         fprintf(stderr, "Could not open output file %s\n", argv[3]);
         output_file=stdout;
      }
   }
   std::cout << "Finished" << std::endl;
   canonical.WriteDIMACSFile(output_file);
  
   if(argc==4)
   {
      fclose(output_file);
   }
   return 0;
}

bf::CNF *pc_compile_hook_cnf = NULL;

bool pc_compile_hook (const bf::Clause &clause)
{
   if(pc_compile_hook_cnf!=NULL)
   {
      bf::PropagationCompleteness pctester(*pc_compile_hook_cnf);
      unsigned int emp_literal=0;
      if(pctester.IsEmpoweringClause(clause, &emp_literal))
      {
         pc_compile_hook_cnf->AddElement(clause);
         std::string out;
         clause.DIMACSString(&out);
         std::cout<<out<<" ("<<(emp_literal%2==1?"-":"")<<emp_literal/2+1<<")"<<std::endl;
      }
   }
   return true;
}

int propagation_completeness_compile(int argc, char *argv[])
{
   if (argc < 3 || argc > 4)
   {
      print_usage();
      return 1;
   }
   bf::CNF cnf;

   FILE * dimacs_file = fopen (argv [2], "r");
   if (dimacs_file == NULL)
   {
       fprintf (stderr, "Could not open input file %s\n", argv[2]);
       return 1;
   }
   cnf.InitWithDIMACSFile (dimacs_file);
   fclose(dimacs_file);
   puts("File read");

   bf::CNF compiled;
   compiled.InitWithNF(cnf);
   pc_compile_hook_cnf=&compiled;

   bf::CNF canonical;
   cnf.CreateCanonical(&canonical, pc_compile_hook);

   compiled.RemoveSubsumptions();

   FILE *output_file=stdout;
   if(argc==4)
   {
      output_file=fopen(argv[3], "w");
      if(output_file==NULL)
      {
         fprintf(stderr, "Could not open output file %s\n", argv[3]);
         output_file=stdout;
      }
   }

   std::cout << "Finished" << std::endl;
   compiled.WriteDIMACSFile(output_file);
  
   if(argc==4)
   {
      fclose(output_file);
   }
 
   return 0;
}

int main(int argc, char *argv[])
{
   if (argc < 2)
   {
      fputs ("bfmanip: You must tell me, what to do.\n", stderr);
      print_usage();
      return 1;
   }
   // return FormulaTest();
   // return NFElementTest();
   // return NormalFormTest();
   // return DimacsTest (argc, argv);
   if (strcmp (argv[1], "dpll") == 0)
   {
      return runDPLL (argc, argv);
   }
   else if (strcmp (argv[1], "cdcl") == 0)
   {
      return runCDCL (argc, argv);
   }
   else if (strcmp (argv[1], "pctest") == 0)
   {
      return propagation_completeness_test(argc, argv);
   }
   else if (strcmp (argv[1], "pcfind") == 0)
   {
      return propagation_completeness_find(argc, argv);
   }
   else if (strcmp (argv[1], "canon") == 0)
   {
      return generate_canonical_nf(argc, argv);
   }
   else if (strcmp (argv[1], "pccompile") == 0)
   {
      return propagation_completeness_compile(argc, argv);
   }
   fputs ("bfmanip: I don't understand, what did you mean?\n", stderr);
   print_usage();
   return 1;
}
