//
//  nfelement.h
//  bfmanip++
//
//  Created by Petr Kučera on 27.4.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
/**@file nfelement.h
 * @brief File containing declaration of classes NFElement describing
 * common properties of elements of normal forms.
 *
 * This class is used as a base for classes NFClause and NFTerm representing
 * clauses and terms respectively.
 */
#ifndef NFELEMENT_H
#define NFELEMENT_H

#include <set>
#include <cstdio>
#include <cstdlib>

#include "input.h"
#include "bfexception.h"

namespace bf {
   /**@brief Class NFElement with common properties of elements of
    * normal forms.
    *
    * @param DETERMINING_VALUE Contains truth value, which if a literal in this
    * element is evaluated to, whole element is determined to have
    * the same value.
    * In case of clauses, this value is true, in case of terms, it
    * is false. It also determines other important values of the
    * element, i.e. value of empty element is !determining_value
    * (i.e. 0 for clause, 1 for term).
    * An element is a set of literals, where we allow to have both
    * positive and negative literals with the same variable within the
    * set.
    *
    * @param Operator string to be used when
    * representing this element by a string between literals.
    */
   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
      class NFElement
   {
      public:
         /**@brief Constants determining literal polarity.
          */
         enum LiteralPolarity {
            kNoOccurrence,
            kNegativeOccurrence,
            kPositiveOccurrence,
            kBothOccurences 
         };

         /**@brief Constants describing a state of a element after
          * applying a partial assignment. */
         enum ElementState {
            kUnsatisfied,
            kSatisfied,
            kUnit,
            kQuadratic,
            kCubic,
            kUndetermined
         };

         /**@brief Type of literal set.
          */
         typedef std::set<unsigned int> LiteralSet;
      private:
      /**@brief Set of literals.
       *
       * Given variable with index <i>i</i>, positive literal would be
       * stored as 2*<i>i</i> and negative literal would be stored as
       * 2*<i>i</i>+1. Using upper and lower bounds in std::set this
       * allows us to answer most queries about a single literal or
       * variable by one query to set of literals.
       */
      LiteralSet literals_;

      /**@brief The number of negative literals in the element.
       */
      size_t number_of_negative_;

   public:
      /**@brief Creates an empty element.
       */
      inline NFElement<DETERMINING_VALUE, OPERATOR_SIGN> () :
         literals_ (),
         number_of_negative_ (0)
      {}

      /**@brief Copy constructor.
       *
       * Due to the presence of constant member variables, we can not
       * rely on a default copy constructor. And we want to store
       * NFElement objects in a list (within NormalForm), thus we make
       * NFElement copyable.
       */
      inline NFElement<DETERMINING_VALUE, OPERATOR_SIGN>(
            const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element) :
          literals_ (element.literals_),
          number_of_negative_ (element.number_of_negative_)
      {}

      /**@brief Assign operator.
       *
       * Due to the presence of constant member variables, we can not
       * rely on a default assign operator. And we want to store
       * NFElement objects in a list (within NormalForm), thus we make
       * NFElement copyable.
       */
      inline NFElement<DETERMINING_VALUE, OPERATOR_SIGN>&
         operator=(const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element)
      {
         number_of_negative_ = element.number_of_negative_;
         literals_ = element.literals_;
         return (* this);
      }

      /**@brief Frees all memory needed by element.
       */
      virtual inline ~NFElement<DETERMINING_VALUE, OPERATOR_SIGN> () {}

      /**@brief Initializes the element with given partial input.
       *
       * After this method finishes, this element contains exactly all
       * variables which are assigned value by @p partial_input and
       * no literal in this element is given determining value.
       */
      void InitWithPartialInput (const PartialInput& partial_input);

      /**@brief A copy initialization function.
       *
       * @param element An element, which we shall make a copy of.
       */
      inline void
         InitWithElement (
               const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element)
      {
         literals_ = element.literals ();
         number_of_negative_ = element.NumberOfNegativeLiterals ();
      }

      /**@brief Generates random element with given number of literals
       * over given number of variables.
       *
       * It is not checked, whether one variable is generated more
       * than once in any polarity, thus the resulting element can
       * contain less literals or it can be trivial if a variable is
       * generated in both polarities.
       *
       * @param n_variables Number of variables from which we can
       * choose. Variables are generated from interval
       * [0...variables-1].
       * @param n_literals Number of literals to generated to this
       * element.
       */
      void InitRandomly (unsigned int n_variables,
            unsigned int n_literals);

      /**@brief Returns determining value of this element.
       */
      inline bool determining_value () const
      {
         return DETERMINING_VALUE;
      }
     
      /**@brief Returns character describing operator sign of this
       * element.
       */
      inline char operator_sign () const
      {
         return OPERATOR_SIGN;
      }

      /**@brief Checks, if this element contains given variable.
       *
       * @param variable_index Index of variable to check.
       */
      inline bool ContainsVariable (unsigned int variable_index) const
      {
         return variable_index == (* literals_.lower_bound(2 * variable_index)) / 2;
      }
      /**@brief Checks, if this element contains positive literal with
       * given variable.
       *
       * @param variable_index Index of variable to check.
       */
      inline bool ContainsPositiveLiteral (unsigned int variable_index) const
      {
         return literals_.find(2*variable_index) != literals_.end();
      }
      /**@brief Checks, if this element contains negative literal with
       * given variable.
       *
       * @param variable_index Index of variable to check.
       */
      inline bool ContainsNegativeLiteral (unsigned int variable_index) const
      {
         return literals_.find(2*variable_index+1) != literals_.end();
      }

      inline unsigned int MaxVariable () const
      {
         LiteralSet::const_iterator max_variable;
         max_variable = literals_.end();
         -- max_variable;
         return (* max_variable) / 2;
      }

      /**@brief Determines polarity of given variable in this element.
       *
       * @param variable_index Index of variable to check.
       *
       * @return A value determining polarity of given variable.
       */
      LiteralPolarity GetPolarity (unsigned int variable_index) const;
      
      /**@brief Removes literal with given variable from the element.
       *
       * @param variable_index Index of variable determining the
       * literal to be removed. If it appears both positively and
       * negatively in the element, then both occurrences are removed.
       *
       * @return Polarity of variable before removal as it is returned
       * by GetPolarity.
       */
      LiteralPolarity RemoveVariable (unsigned int variable_index);

      /**@brief Adds literal with given variable to the element.
       *
       * Whether literal will be positive or negative is given by @p
       * positive parameter. 
       *
       * @param variable_index Index of variable determining the
       * literal to be added.
       * @param negative Determines, whether the literal should be
       * positive (false) or negative (true).
       */
      void AddLiteral (unsigned int variable_index, bool negative);

      /**@brief Removes all literals from the element.
       */
      inline void RemoveAllLiterals ()
      {
         literals_.clear ();
         number_of_negative_ = 0;
      }

      /**@brief Changes polarity of given variable in element and
       * returns its former polarity.
       *
       * If the variable is not present in the element at all, then 0
       * is returned ( = its former polarity). If variable is present
       * both positively and negatively in element, then nothing
       * happens.
       *
       * @param variable_index Index of variable whose polarity is to
       * be changed.
       *
       * @return Former polarity of the variable in the element, 0, if
       * it is not present in the element.
       */
      LiteralPolarity ChangePolarity (unsigned int variable_index);

      /**@brief Changes polarity of all literals in the element.
       */
      void ChangePolarityOfAllLiterals ();

      /**@brief Returns number of conflicts with given element.
       */
      unsigned int 
         NumberOfConflicts (
               const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element) const;

      /**@brief Checks, whether this element contains a variable with
       * both positive and negative occurrences.
       */
      bool ContainsConflictOccurrence () const;

      /**@brief Merges this element with given one.
       *
       * Adds all literals from given element to this one. Note, that
       * this procedure does not check, whether given element is in
       * conflict with this one, you can check the number of conflicts
       * independently using NumberOfConflicts function.
       *
       * @param element Element to merge with.
       */
      void MergeWithElement (
            const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element);

      /**@brief Returns unique partial input determined by this
       * element.
       *
       * The partial input returned in output parameter @p
       * partial_input assigns all variables in the
       * element, so that their value is complement of
       * determining_value, variables which are not present in the
       * element are not assigned a value. If a variable appears both
       * positively and negatively in the element, then it is assigned
       * bf::kUnknownValue.
       *
       * At the beginning all values of @p partial_input are set to
       * unknown value. If index of some variable should exceed size
       * of the underlying array of @p partial_input, then its size is
       * increased to accomodate this variable and all unknown values
       * are set to uknown.
       * 
       * @param partial_input Output variable in which the result
       * partial input is stored.
       */
      void PartialInputFromElement (bf::PartialInput * partial_input) const;

      /**@brief Returns number of literals present in the element.
       */
      inline size_t NumberOfLiterals () const
      {
         return literals_.size();
      }
      /**@brief Returns number of positive literals present in the
       * element.
       */
      inline size_t NumberOfPositiveLiterals () const
      {
         return literals_.size() - number_of_negative_;
      }
      /**@brief Returns number of negative literals present in the
       * element.
       */
      inline size_t NumberOfNegativeLiterals () const
      {
         return number_of_negative_;
      }

      /**@brief Returns set of literals.
       *
       * Set of literals is organized as follows, literal with
       * variable var is stored as 2*var in case of positive literal
       * and it is stored as 2*var+1 as negative literal.
       */
      inline const LiteralSet& literals () const
      {
         return literals_;
      }
      
      /**@brief Evaluates this element on given partial input.
       *
       * @param partial_input Partial input on which this element
       * should be evaluated.
       * 
       * @return Value on given input, which can be unknown.
       */
      PIValues ValueForPartialInput (const bf::PartialInput& partial_input) const;

      /**@brief Checks the state of this element after applying given
       * partial input.
       *
       * Possible states of an element after applying given partial
       * input are given by #ElementState enum type.
       *
       * @param partial_input Partial input to be applied to the
       * element.
       * @param undetermined_literal If this parameter is not NULL,
       * and an undetermined literal remains in the element after
       * applying @p partial_input, then one of the undetermined
       * literals is returned in this variable. This is useful for
       * unit propagation and similar functions especially in case
       * the element is unit.
       *
       * @return The state of this element after applying @p
       * partial_input.
       */
      ElementState
         StateForPartialInput (const bf::PartialInput& partial_input,
               unsigned int * undetermined_literal) const;

      /**@brief Applies given partial assignment to this element.
       *
       * Note, by applying a partial assignment it can happen, that an
       * empty element is produced, however in this case it could have
       * a value opposite to the normal value of an empty element.
       * Thus return value returns the value of element on given
       * assignment which helps to determine, whether this happened.
       *
       * @param partial_assignment partial assignment to be applied to
       * this element.
       *
       * @return Value on given input, if element remains nonempty,
       * then unknown value is returned.
       */
      PIValues ApplyPartialAssignment (const bf::PartialInput&  partial_assignment);
      /**@brief Returns Boolean value on given Boolean input.
       *
       * @param input Input on which this element should be evaluated.
       *
       * @return Value on given input.
       */
      bool ValueForInput (const bf::Input& input) const;

      /**@brief Compares this element to given one and checks, if they
       * are equal.
       *
       * @param element Element to which we shall compare.
       *
       * @return True, if given element contains the same literals as
       * this one, false otherwise.
       */
      inline bool 
         IsEqual (
               const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element) const
      {
         return (literals_ == element.literals());
      }
      /**@brief Checks, whether this element is a subelement of given
       * one.
       *
       * @param element Element to which we shall compare.
       *
       * @return True, if set of literals of this element form a
       * subset of literals of @p element.
       */
      bool 
         IsSubelementOf (
               const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element) const;

      /**@brief Creates a copy of this element.
       */
      inline NFElement<DETERMINING_VALUE, OPERATOR_SIGN> * Copy () const
      {
         bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN> * element_copy =
            new bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>();
         element_copy->InitWithElement(*this);
         return element_copy;
      }
      /**@brief Creates a resolvent of this element with given one.
       *
       * Resolvent is created only if there is exactly one conflict
       * between this element and @p element. If more than one
       * conflict occurs, then after this method finishes, resolvent
       * contains all non-conflict literals. This means, that this can
       * be used to create merge of two clauses as well. Number of
       * conflicts is returned and thus everything went correctly if
       * the returned value is 1. Note, that if there is some variable
       * which occurs in one of the elements both positively and
       * negatively, and in the other it appears in at least with one
       * of these polarities, then the result is, that in the result
       * it also appears with one polarity (the other). If a variable
       * is contained with both polarities in both elements, then this
       * situation is undetected (due to the moving in both lists
       * simultaneously), and conflict is not accounted for.
       *
       * This is not of a particular concern however, because
       * importance of resolution lies in correct usage, i.e. it is
       * assumed that the two elements have exactly one conflict
       * variable and that none of elements contains a variable with
       * both polarities.
       *
       * @param element Element to resolve with.
       * @param resolvent Output variable, into which new resolvent
       * should be stored.
       *
       * @return Number of conflicts (i.e. 1 means correct output);
       */
      unsigned int Resolve (
            const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element,
            NFElement<DETERMINING_VALUE, OPERATOR_SIGN>* resolvent) const;

      /**@brief Creates a string representation of this element.
       *
       * Representation is not enclosed in brackets (caller must
       * decide for itself, whether it is neccessary), and
       * operator_sign is used to separate the literals.
       *
       * @param variable_names Names of variables to be used in the
       * string representation. Although it is an input variable, it
       * is given as a pointer not by a constant reference. This value
       * can be NULL, in which case index numbers are used instead of
       * names.
       * @param output_string String, to which the string
       * representation of this subformula will be appended.
       */
      void ToString (const std::vector<std::string> * variable_names,
            std::string * output_string) const;

      /**@brief Writes element in DIMACS format into given string.
       *
       * Note, that in NormalForm, variables are indexed from 0,
       * i.e. variables have indices from interval 0,..., n-1, where n
       * denotes the number of variables. On the other hand, in a
       * DIMACS file, variables have indices from interval 1,...,
       * n. Thus when reading a DIMACS file, we store variable i
       * (DIMACS) as variable (i-1) (NormalForm). When writing
       * DIMACS file, we output variable i (NormalForm) as (i+1)
       * (DIMACS).
       *
       * @param output_string String to which output should be
       * appended.
       */
      void DIMACSString (std::string * output_string) const;
   };

   /**@brief Class describing a clause.
    *
    * A clause is an NFElement with determining value equal to true
    * and with operator sign equal to +.
    */
   typedef NFElement<true, '+'> Clause;
   /**@brief Class describing a clause.
    *
    * A clause is an NFElement with determining value equal to false
    * and with operator sign equal to &.
    */
   typedef NFElement<false, '&'> Term;

   /**@brief Comparison of two elements.
    *
    * Compares two elements and decides which should go first in a
    * lexicographical ordering.
    *
    * @param first_element First element to compare.
    * @param second_element Second element to compare.
    *
    * @return True, if first_element is lexicographically smaller than
    * the second one, i.e. in an ordering like in a list the first
    * element should go before the second one. False otherwise.
    */
   template<class ElementType>
   bool CompareNFElements (
         const ElementType* first_element,
         const ElementType* second_element)
   {
      return first_element != NULL
         && second_element != NULL
         && first_element->literals() < second_element->literals();
   }
}

// Implementation
template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::InitWithPartialInput (
      const bf::PartialInput& partial_input)
{
   literals_.clear();
   number_of_negative_ = 0;
   for (unsigned int variable_index = 0;
         variable_index < partial_input.size();
         ++ variable_index)
   {
      switch (partial_input [variable_index])
      {
         case bf::kFalseValue:
            AddLiteral(variable_index, ! determining_value());
            break;
         case bf::kTrueValue:
            AddLiteral(variable_index, determining_value());
            break;
         case bf::kUnknownValue:
         default:
            break;
      }
   }
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::InitRandomly (
    unsigned int n_variables,
            unsigned int n_literals)
{
   literals_.clear();
   number_of_negative_ = 0;
   for (unsigned int number_of_generated = 0;
         number_of_generated < n_literals;
         ++ number_of_generated)
   {
      unsigned int lit = rand() % (2 * n_variables);
      number_of_negative_ += lit % 2;
      literals_.insert (lit);
   }
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
typename bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::LiteralPolarity
bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::GetPolarity (
      unsigned int variable_index) const
{
   std::set<unsigned int>::const_iterator lower_bound = 
      literals_.lower_bound(variable_index * 2);
   int polarity = kNoOccurrence;
   if (lower_bound != literals_.end()
         && (* lower_bound) == 2 * variable_index)
   {
      polarity |= kPositiveOccurrence;
      ++ lower_bound;
   }
   if (lower_bound != literals_.end()
         && (* lower_bound) == 2 * variable_index + 1)
   {
      polarity |= kNegativeOccurrence;
   }
   return static_cast<LiteralPolarity>(polarity);
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
typename bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::LiteralPolarity
bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::RemoveVariable (unsigned int variable_index)
{
   LiteralPolarity polarity = GetPolarity (variable_index);

   if (polarity)
   {
      std::set<unsigned int>::iterator first = literals_.lower_bound (2 * variable_index);
      std::set<unsigned int>::iterator second = first;
      if (polarity == kBothOccurences)
      {
         ++ second;
      }
      literals_.erase (first, second);
   }

   return polarity;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::AddLiteral (unsigned int variable_index, bool negative)
{
   literals_.insert(2 * variable_index + negative);
   number_of_negative_ += negative;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
typename bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::LiteralPolarity
bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ChangePolarity (unsigned int variable_index)
{
   LiteralPolarity polarity = GetPolarity (variable_index);
   if (polarity == kPositiveOccurrence)
   {
      literals_.erase (2 * variable_index);
      literals_.insert (2 * variable_index + 1);
   }
   else if (polarity == kNegativeOccurrence)
   {
      literals_.erase (2 * variable_index + 1);
      literals_.insert (2 * variable_index);
   }
   return polarity;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ChangePolarityOfAllLiterals ()
{
   std::set<unsigned int> negated_literals;
   for (std::set<unsigned int>::iterator literal = literals_.begin ();
         literal != literals_.end ();
         ++ literal)
   {
      negated_literals.insert((* literal) % 2 == 0
            ? (* literal) + 1
            : (* literal) - 1);
   }
   number_of_negative_ = literals_.size() - number_of_negative_;
   literals_.swap (negated_literals);
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
unsigned int bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::NumberOfConflicts (const bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element) const
{
   std::set<unsigned int>::const_iterator my_literal = literals_.begin();
   std::set<unsigned int>::const_iterator other_literal = element.literals().begin();
   unsigned int number_of_conflicts = 0;
   while (my_literal != literals_.end()
         && other_literal != element.literals().end())
   {
      if ((* my_literal) / 2 == (* other_literal) / 2)
      {
         if ((* my_literal) != (* other_literal))
         {
            ++ number_of_conflicts;
         }
         ++ my_literal;
         ++ other_literal;
      }
      else if (((* my_literal) / 2) < ((* other_literal) / 2))
      {
         ++ my_literal;
      }
      else // ((* other_literal) / 2) < ((* my_literal) / 2))
      {
         ++ other_literal;
      }
   }
   while (my_literal != literals_.end())
   {
      ++ my_literal;
   }
   while (other_literal != element.literals().end())
   {
      ++ other_literal;
   }
   return number_of_conflicts;

}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
bool bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ContainsConflictOccurrence () const
{
   std::set<unsigned int>::const_iterator literal = literals_.begin ();
   while (literal != literals_.end())
   {
      unsigned int lit_number = (* literal);
      ++ literal;
      if (literal != literals_.end()
            && (* literal) / 2 == lit_number / 2)
      {
         return true;
      }
   }
   return false;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::MergeWithElement (
      const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element)
{
   // Count negative literals, which will be added.
   unsigned int number_of_negative_diff = 0;
   std::set<unsigned int>::const_iterator my_literal = literals_.begin();
   std::set<unsigned int>::const_iterator other_literal = element.literals().begin();
   while (my_literal != literals_.end()
         && other_literal != element.literals().end())
   {
      if ((* my_literal) == (* other_literal))
      {
         ++ my_literal;
         ++ other_literal;
      }
      else if (((* my_literal) / 2) < ((* other_literal) / 2))
      {
         ++ my_literal;
      }
      else // ((* other_literal) / 2) < ((* my_literal) / 2))
      {
         number_of_negative_diff += (* other_literal) % 2;
         ++ other_literal;
      }
   }
   number_of_negative_ += number_of_negative_diff;
   literals_.insert (element.literals().begin(), element.literals().end());
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::PartialInputFromElement (
      bf::PartialInput * partial_input) const
{
   if (partial_input == NULL)
   {
      std::cerr << "[bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::PartialInputFromElement] partial_input == NULL" << std::endl;
      throw bf::UnexpectedNULLException();
   }
   size_t size = partial_input->size ();
   unsigned int max_variable = MaxVariable ();
   if (max_variable >= size)
   {
      size = max_variable + 1;
   }
   partial_input->assign(size, bf::kUnknownValue);
   
   for (std::set<unsigned int>::iterator literals_iterator = literals_.begin();
         literals_iterator != literals_.end();
         ++ literals_iterator)
   {
      if ((* partial_input) [(* literals_iterator) / 2] == bf::kUnknownValue)
      {
         (* partial_input) [(* literals_iterator) / 2] = 
            ((* literals_iterator) % 2 
             ? static_cast<bf::PIValues>(determining_value())
             : static_cast<bf::PIValues>(! determining_value()));
      }
      else
      {
         (* partial_input) [(* literals_iterator) / 2] = bf::kUnknownValue;
      }
   }
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
bf::PIValues
   bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ValueForPartialInput (
         const bf::PartialInput& partial_input) const
{
   bf::PIValues output_value = static_cast<bf::PIValues>(! determining_value());
   for (std::set<unsigned int>::const_iterator literals_iterator = literals_.begin();
         literals_iterator != literals_.end();
         ++ literals_iterator)
   {
      bf::PIValues literal_value = partial_input [(* literals_iterator) / 2];
      if (literal_value == bf::kUnknownValue)
      {
         output_value = bf::kUnknownValue;
      }
      else
      {
         if ((* literals_iterator) % 2)
         {
            literal_value = static_cast<bf::PIValues>(1 - literal_value);
         }
         if (literal_value == determining_value())
         {
            output_value = literal_value;
            break;
         }
      }
   }
   return output_value;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
typename bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ElementState
bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::StateForPartialInput (
    const bf::PartialInput& partial_input,
      unsigned int * undetermined_literal) const
{
   int num_undetermined = 0;
   unsigned int undetermined_lit = 0;
   bf::PIValues output_value = static_cast<bf::PIValues>(! determining_value());
   for (std::set<unsigned int>::const_iterator literals_iterator = literals_.begin();
         literals_iterator != literals_.end();
         ++ literals_iterator)
   {
      bf::PIValues literal_value = partial_input [(* literals_iterator) / 2];
      if (literal_value == bf::kUnknownValue)
      {
         output_value = bf::kUnknownValue;
         if (! num_undetermined)
         {
            undetermined_lit = *literals_iterator;
         }
         ++ num_undetermined;
      }
      else
      {
         if ((* literals_iterator) % 2)
         {
            literal_value = static_cast<bf::PIValues>(1 - literal_value);
         }
         if (literal_value == determining_value())
         {
            output_value = literal_value;
            break;
         }
      }
   }
   if (undetermined_literal != NULL)
   {
      *undetermined_literal = undetermined_lit;
   }
   ElementState state = kUndetermined;
   if (output_value == determining_value()
         || !num_undetermined)
   {
      state = (output_value == kTrueValue
            ? kSatisfied
            : kUnsatisfied);
   }
   else if (num_undetermined == 1)
   {
      state = kUnit;
   }
   else if (num_undetermined == 2)
   {
      state = kQuadratic;
   }
   else if (num_undetermined == 3)
   {
      state = kCubic;
   }
   return state;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
bf::PIValues
   bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ApplyPartialAssignment (
         const bf::PartialInput& partial_assignment)
{
   bf::PIValues output_value = static_cast<bf::PIValues>(! determining_value());
   std::set<unsigned int>::iterator literals_iterator = literals_.begin();
   while (literals_iterator != literals_.end())
   {
      std::set<unsigned int>::iterator this_literal = literals_iterator ++;
      bf::PIValues literal_value = partial_assignment [(* this_literal) / 2];
      if (literal_value == bf::kUnknownValue)
      {
         output_value = bf::kUnknownValue;
      }
      else
      {
         if ((* this_literal) % 2)
         {
            literal_value = static_cast<bf::PIValues>(1 - literal_value);
            -- number_of_negative_;
         }
         literals_.erase(this_literal);
         if (literal_value == determining_value())
         {
            output_value = literal_value;
            literals_.clear();
            number_of_negative_ = 0;
            break;
         }
      }
   }
   return output_value;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
bool bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ValueForInput (
         const bf::Input& input) const
{
   bool output_value = ! determining_value();
   for (std::set<unsigned int>::const_iterator literals_iterator = literals_.begin();
         literals_iterator != literals_.end();
         ++ literals_iterator)
   {
      if ((*literals_iterator) / 2 >= input.size ())
      {
         fprintf(stderr, "[bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ValueForInput] Variable %u exceeds the size of input %lu\n", (*literals_iterator)/2, input.size());
         throw IndexOutOfBoundsException();
      }
      bool literal_value = ((* literals_iterator) % 2
            ? ! input [(* literals_iterator) / 2]
            : input [(* literals_iterator) / 2]);
      if (literal_value == determining_value())
      {
         output_value = determining_value();
         break;
      }
   }
   return output_value;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
bool bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::IsSubelementOf (
      const bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element) const
{
   std::set<unsigned int>::const_iterator my_literal = literals_.begin ();
   std::set<unsigned int>::const_iterator other_literal = element.literals().begin();

   while (my_literal != literals_.end()
         && other_literal != element.literals().end())
   {
      if ((* my_literal) == (* other_literal))
      {
         ++ my_literal;
         ++ other_literal;
      }
      else if ((* my_literal) < (* other_literal))
      {
         return false;
      }
      else
      {
         ++ other_literal;
      }
   }
   if (my_literal != literals_.end())
   {
      return false;
   }
   return true;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
unsigned int
bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::Resolve (
      const NFElement<DETERMINING_VALUE, OPERATOR_SIGN>& element,
      NFElement<DETERMINING_VALUE, OPERATOR_SIGN> * resolvent) const
{
   if (resolvent == NULL)
   {
      std::cerr << "[bf::NFresolvent::Resolvent] resolvent == NULL" << std::endl;
      throw bf::UnexpectedNULLException();
   }
   std::set<unsigned int>::const_iterator my_literal = literals_.begin();
   std::set<unsigned int>::const_iterator other_literal = element.literals().begin();
   unsigned int number_of_conflicts = 0;
   resolvent->RemoveAllLiterals();
   while (my_literal != literals_.end()
         && other_literal != element.literals().end())
   {
      if ((* my_literal) / 2 == (* other_literal) / 2)
      {
         if ((* my_literal) != (* other_literal))
         {
            ++ number_of_conflicts;
         }
         else
         {
            resolvent->AddLiteral((* my_literal) / 2, (* my_literal) % 2);
         }
         ++ my_literal;
         ++ other_literal;
      }
      else if (((* my_literal) / 2) < ((* other_literal) / 2))
      {
         resolvent->AddLiteral((* my_literal) / 2, (* my_literal) % 2);
         ++ my_literal;
      }
      else // ((* other_literal) / 2) < ((* my_literal) / 2))
      {
         resolvent->AddLiteral((* other_literal) / 2, (* other_literal) % 2);
         ++ other_literal;
      }
   }
   while (my_literal != literals_.end())
   {
      resolvent->AddLiteral((* my_literal) / 2, (* my_literal) % 2);
      ++ my_literal;
   }
   while (other_literal != element.literals().end())
   {
      resolvent->AddLiteral((* other_literal) / 2, (* other_literal) % 2);
      ++ other_literal;
   }
   return number_of_conflicts;
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ToString (
      const std::vector<std::string> * variable_names,
      std::string * output_string) const
{
   if (output_string == NULL)
   {
      std::cerr << "[bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::ToString] output_string == NULL" << std::endl;
      throw bf::UnexpectedNULLException();
   }
   std::set<unsigned int>::const_iterator literal = literals_.begin();
   char name [64] = "";
   while (literal != literals_.end())
   {
      if ((* literal) % 2)
      {
         (* output_string) += '-';
      }
      if (variable_names != NULL
            && (* literal) / 2 < variable_names->size())
      {
         (* output_string) += (* variable_names)[(* literal) / 2];
      }
      else
      {
         snprintf (name, 64, "%d", (* literal) / 2);
         (* output_string) += name;
      }
      ++ literal;
      if (literal != literals_.end())
      {
         (* output_string) += operator_sign();
      }
   }
}

   template<bool DETERMINING_VALUE, char OPERATOR_SIGN>
void bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::DIMACSString (std::string * output_string) const
{
   if (output_string == NULL)
   {
      std::cerr << "[bf::NFElement<DETERMINING_VALUE, OPERATOR_SIGN>::DIMACSString] output_string == NULL" << std::endl;
      throw bf::UnexpectedNULLException();
   }

   char name [64] = "";
   for (std::set<unsigned int>::const_iterator literal = literals_.begin();
         literal != literals_.end();
         ++ literal)
   {
      if ((* literal) % 2)
      {
         (* output_string) += '-';
      }
      snprintf (name, 64, "%d ", (* literal) / 2 + 1);
      (* output_string) += name;
   }
   (* output_string) += '0';
}
#endif

