//
//  normalform.cpp
//  bfmanip++
//
//  Created by Petr Kučera on 6.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//

#include <stdio.h>
#include <ctype.h>

bool DimacsSkipPreamble (FILE * dimacs_file)
{
   int c = '\0';
   bool found_parameters = false;
   while (!feof (dimacs_file)
         && ! found_parameters)
   {
      c = fgetc (dimacs_file);
      while (! feof (dimacs_file)
            && isspace (c))
      {
         c = fgetc (dimacs_file);
      }
      if (c == 'p')
      {
         ungetc (c, dimacs_file);
         found_parameters = true;
      }
      else
      {
         if (c != 'c')
         {
            return false;
         }
         while (! feof (dimacs_file)
               && c != '\n')
         {
            c = fgetc (dimacs_file);
         }
      }
   }
   if (feof (dimacs_file)
         || c != 'p')
   {
      return false;
   }
   return true;
}
