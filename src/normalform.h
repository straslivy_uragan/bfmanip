//
//  normalform.h
//  bfmanip++
//
//  Created by Petr Kučera on 6.5.11.
//  Copyright 2011 Strašlivý Uragán. All rights reserved.
//
/**@file normalform.h
 * @brief File containing declaration of class NormalForm
 * describing commong properties of sets of elements.
 */
#ifndef NORMALFORM_H
#define NORMALFORM_H

#include <cstdio>
#include <list>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#include "nfelement.h"
#include "bfexception.h"

namespace bf {
   /**@brief Class NormalForm with common properties of normal forms,
    * i.e. sets of elements.
    *
    * @param ElementType Type of elements in this normal form, should
    * be of kind ElementType, otherwise it will not work.
    * @param DETERMINING_VALUE Determining value for the normal form,
    * for disjunction the determining value is true, for conjunction
    * it is false.
    * @param OPERATOR_SIGN Character with operator.
    * It can have value '\0', in which case no operator is used
    * between elements in the output string, you can combine this
    * with brackets around elements.
    * @param USE_BRACKETS Determines whether brackets around elements should be
    * used in the output string.
    * Is brackets are used, then no operator is used in output.
    */
   template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
   class NormalForm
   {
      public:
         /**@brief List of (pointers to) elements. */
         typedef std::list<ElementType*> ElementList;
      private:
      /**@brief List with elements.
       *
       * Elements are not sorted in the list as we do not want to
       * impose any ordering on elements, which might not be suitable
       * for the application. E.g. under normal circumstances we want
       * to write elements into the output string in the order in
       * which they were added. In case of elements, most work is done
       * by traversing set of elements, for which the vector is
       * suitable.
       *
       * Pointers to elements are stored, the memory allocated for
       * elements is supposed to be owned by the normal form and thus
       * it is freed, if an element is deleted from the list.
       */
      ElementList elements_;

      /**@brief Number of variables on which this normal form is
       * defined.
       *
       * It is not usually used internally, main purpose of this
       * variable is to allow to store this value in the normal form.
       */
      int number_of_variables_;

      private:
         NormalForm<ElementType, DETERMINING_VALUE,
            OPERATOR_SIGN, USE_BRACKETS>(
                  const NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>&);               
         void operator=(
               const NormalForm<ElementType, DETERMINING_VALUE,
               OPERATOR_SIGN, USE_BRACKETS>&);
         
         /**@brief Returns newly allocated empty element of the
          * appropriate type.
          *
          * In case of CNF this method is overriden so that it returns
          * clauses, in case of DNF it is overriden so that it returns
          * terms.
          */
         ElementType * NewElement ();

      public:
         /**@brief Constructor initializing an empty element.
         */
         inline NormalForm<ElementType, DETERMINING_VALUE,
                OPERATOR_SIGN, USE_BRACKETS> () :
            elements_ (),
            number_of_variables_ (0)
         {}

         /**@brief A destructor.
          */
         virtual inline ~NormalForm<ElementType, DETERMINING_VALUE,
                 OPERATOR_SIGN, USE_BRACKETS> () {}
        
         /**@brief Returns the number of elements in the normal form.
          */
         inline size_t NumberOfElements() const
         {
            return elements_.size();
         }

         inline const ElementList& elements () const
         {
            return elements_;
         }

         /**@brief Initialize object as a copy of given normal form.
          *
          * @param normal_form A normal form to copy.
          */
         void InitWithNF (const NormalForm<ElementType, DETERMINING_VALUE,
               OPERATOR_SIGN, USE_BRACKETS>& normal_form);

         /**@brief Reads DIMACS file with given name and initializes
          * normal form with its content.
          *
          * Note, that in NormalForm<ElementType, DETERMINING_VALUE,
          * OPERATOR_SIGN, USE_BRACKETS>, variables are indexed from 0,
          * i.e. variables have indices from interval 0,..., n-1, where n
          * denotes the number of variables. On the other hand, in a
          * DIMACS file, variables have indices from interval 1,...,
          * n. Thus when reading a DIMACS file, we store variable i
          * (DIMACS) as variable (i-1) (NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>). When writing
          * DIMACS file, we output variable i (NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>) as (i+1)
          * (DIMACS).
          *
          * @return True, if file was successfully read, false
          * otherwise.
          */
         bool InitWithDIMACSFile (FILE * dimacs_file);

         /**@brief Initializes random normal form with given
          * parameters.
          * 
          * Elements are not checked for nontriviality, thus the
          * number of nontrivial elements generated can be smaller.
          * Number of literals per element is the maximum, it can be
          * smaller for any particular element, if a variable with
          * same polarity is generated more than once.
          *
          * @param variables Number of variables in new normal form.
          * Variables are generated from interval [0...variables-1].
          * @param n_elements Number of elements to be created.
          * @param literals_per_element Number of literals in one
          * element.
          */
         void InitRandomly (unsigned int variables, 
               unsigned int n_elements,
               unsigned int literals_per_element);

         /**@brief Creates a newly allocated copy of this object.
          */
         NormalForm<ElementType, DETERMINING_VALUE,
                OPERATOR_SIGN, USE_BRACKETS> * Copy () const
         {
            NormalForm<ElementType, DETERMINING_VALUE,
            OPERATOR_SIGN, USE_BRACKETS> * nf_copy =
               new NormalForm<ElementType, DETERMINING_VALUE,
            OPERATOR_SIGN, USE_BRACKETS> ();
            nf_copy -> InitWithNF (*this);
            return nf_copy;
         }

         /**@brief Returns number of variables in this normal form.
          */
         int NumberOfVariables () const
         {
            return number_of_variables_;
         }
         
         /**@brief Sets number of variables in this normal form to
          * given value.
          *
          * @param new_number_of_variables New number of variables.
          */
         void SetNumberOfVariables (
               int new_number_of_variables)
         {
            number_of_variables_ = new_number_of_variables;
         }
         
         /**@brief Returns determining value of this NF.
          */
         bool DeterminingValue () const
         {
            return DETERMINING_VALUE;
         }
         
         /**@brief Returns determining value of this NF as PIValues
          * constant.
          */
         PIValues DeterminingValuePart () const
         {
            return static_cast<bf::PIValues>(DETERMINING_VALUE);
         }

         /**@brief Returns value of empty NF.
          */
         bool ValueOfEmptyNF () const
         {
            return ! DETERMINING_VALUE;
         }
         
         /**@brief Returns value of empty NF as PIValues constant.
          */
         PIValues ValueOfEmptyNFPart () const
         {
            return static_cast<bf::PIValues>(! DETERMINING_VALUE);
         }

         /**@brief Evaluates this normal form on given input.
          *
          * @param input Input on which this normal form should be
          * evaluated.
          *
          * @return The value of this normal form on given input.
          */
         bool ValueForInput(const Input& input) const;
         /**@brief Evaluates this normal form on given partial input.
          *
          * @param partial_input Partial input on which this normal
          * form should be evaluated.
          *
          * @return The value of this normal form on given partial
          * input. It can be unknown, if it cannot be determined using
          * the values in partial_input.
          */
         PIValues ValueForPartialInput(const PartialInput& partial_input) const;
         /**@brief Applies given partial assignment on this normal
          * form.
          *
          * The partial input is applied to every element in this
          * normal form. Element whose values are determined by given
          * partial input are removed. If the result should be a
          * normal form with a constant value (true/false), then at
          * the end it is either empty, or it consists of only empty
          * element. If an element evaluates to a value of empty NF
          * (i.e. to negation of determining value of this NF), then
          * it is removed from the list. If it evaluates to a
          * determining value, then it is left in the list, it is
          * empty and thus it still has this value (if determining
          * values of NF and its elements are negations of each other,
          * which is assumed correct setting). This means, that in
          * this case duplicate empty elements may occur in the NF.
          *
          * @param partial_input Partial assignment to be applied to
          * the normal form.
          *
          * @return The value of this normal form on given partial
          * input. I.e. the same value ValueForPartialInput would
          * return.
          */
         PIValues ApplyPartialAssignment(const PartialInput& partial_input);

         /**@brief Performs unit propagation starting with given
          * partial input.
          *
          * Unit propagation is performed as if before it given
          * partial input would be applied to this normal form. The
          * result is then stored in the partial input by filling it
          * with forced values of variables set during the unit
          * propagation process. The return value determines, whether
          * an empty element was not created during the unit propagation
          * (true), or was created (false). If an empty element is
          * encountered, the unit propagation process immediately
          * stops. The underlying normal form is not modified.
          *
          * The efficiency of this method is not great, because it
          * must build all the data structures, if one wants to make a
          * really efficient unit propagation, one must make a new
          * implementation. This implementation is here mainly to have
          * a simple procedure for dealing with only reasonably big
          * formulas. On the other hand, we do not want to store a lot
          * of information in the NF data structure to make really
          * efficient unit propagation. Moreover we want to keep this
          * normal form data structure dynamic and allow its
          * modifications, and thus it would be complicated to keep
          * appropriate lists. Thus this is a simple quadratic
          * implementation, not a linear one.
          *
          * @return True, if empty clause was not encountered during
          * the unit propagation, false if it was.
          */
         bool UnitPropagation(PartialInput* partial_input) const;

         /**@brief Removes clauses which are subsumed by some other
          * clause in the CNF. */
         void RemoveSubsumptions();

         /**@brief Returns the first element in the list. */
         inline ElementType& FirstElement ()
         {
            ElementType * element = elements_.front ();
            return *element;
         }

         /**@brief Returns the last element in the list. */
         inline ElementType& LastElement ()
         {
            ElementType * element = elements_.back ();
            return *element;
         }

         /**@brief Checks whether given element is contained in this
          * normal form.
          *
          * For the equality test ElementType::IsEqual member function
          * is used.
          *
          * @param element Element we want to find in the normal form.
          *
          * @return True, if element appears in the normal form, false
          * otherwise.
          */
         inline bool ContainsElement(const ElementType& element) const
         {
            return (FindElement (element) != NULL);
         }

         /**@brief Looks for given element and if it is found in the
          * list of elements, pointer to the first occurrence is
          * returned.
          *
          * Note, that memory allocated by the element returned by
          * this function is owned by this normal form and thus it is
          * valid as long as returned element remains part of it.
          *
          * @return Pointer to given element within the lists of
          * elements, or NULL, if it was not found in the list of
          * elements.
          */
         const ElementType * FindElement (const ElementType& element) const;
         
         /**@brief Looks for given element and if it is found in the
          * list of elements, pointer to the first occurrence is
          * returned.
          *
          * Note, that memory allocated by the element returned by
          * this function is owned by this normal form and thus it is
          * valid as long as returned element remains part of it.
          *
          * @return Pointer to given element within the lists of
          * elements, or NULL, if it was not found in the list of
          * elements.
          */
         ElementType * FindElement (const ElementType& element);

         /**@brief Checks whether a subelement of given element is
          * present in this normal form.
          *
          * This function uses ElementType::IsSubelement(const
          * ElementType&) function for this check.
          *
          * @param element Element which we want to check.
          *
          * @return True, if a subelement of given element is present
          * in the normal form, false otherwise.
          */
         inline bool ContainsSubelement(const ElementType& element) const
         {
            return FindSubelement (element) != NULL;
         }
         /**@brief Looks for subelement of given element and if it is
          * found in the list of elements, pointer to the first
          * occurrence is returned.
          *
          * Note, that memory allocated by the element returned by
          * this function is owned by this normal form and thus it is
          * valid as long as returned element remains part of it.
          *
          * @return Subelement of given element, or NULL, if it was
          * not found in the list of elements.
          */
         const ElementType * FindSubelement (const ElementType& element) const;
         
         /**@brief Looks for subelement of given element and if it is
          * found in the list of elements, pointer to the first
          * occurrence is returned.
          *
          * Note, that memory allocated by the element returned by
          * this function is owned by this normal form and thus it is
          * valid as long as returned element remains part of it.
          *
          * @return Subelement of given element, or NULL, if it was
          * not found in the list of elements.
          */
         ElementType * FindSubelement (const ElementType& element);

         /**@brief Adds given element into the normal form.
          *
          * Adds given element at the end of the list with elements.
          * This function does not check, whether this element is
          * already present in the normal form. 
          *
          * @param element Element to be added.
          */
         inline void AddElement(const ElementType& element)
         {
            elements_.push_back(element.Copy());
         }

         /**@brief Finds first occurrence of given element in the
          * normal form and removes it.
          *
          * @param element Element to be removed.
          *
          * @return True, if given element was found and removed,
          * false otherwise.
          */
         bool RemoveElement(const ElementType& element);
         /**@brief Finds and removes all occurrences of given element
          * and returns their number.
          *
          * @param element Element to be removed.
          *
          * @return Number of elements actually removed from the
          * normal form.
          */
         unsigned int RemoveAllOccurrencesOfElement(const ElementType& element);
         /**@brief Removes all elements from the structure.
          */
         void RemoveAllElements();
        
         /**@brief Removes trivial elements, i.e. elements with no
          * literal and elements with conflicting literal.
          *
          * @return Number of removed elements.
          */
         unsigned int RemoveTrivialElements ();

         /**@brief Sorts elements in the list lexicographically and
          * possibly removes duplicates.
          *
          * This function first calls list::sort to sort elements and
          * then list::unique to remove duplicates. The reason why we
          * do not provide removing duplicates independently on the
          * sorting procedure is, that normally list is not sorted and
          * it might not work as expected.
          *
          * @param remove_duplicates Determines if duplicates should
          * be removed after sorting.
          */
         void SortElements (bool remove_duplicates);

         /**@brief Creates a string representation of this normal form.
          *
          * Note, that if OPERATOR_SIGN=='\0', empty string is used
          * instead.
          *
          * @param variable_names Names of variables to be used in the
          * string representation. Although it is an input variable, it
          * is given as a pointer not by a constant reference. This value
          * can be NULL, in which case index numbers are used instead of
          * names.
          * @param output_string String, to which the string
          * representation of this normal form will be appended.
          */
         void ToString (const std::vector<std::string> * variable_names,
               std::string * output_string) const;

         /**@brief Writes this normal form in DIMACS format to given
          * file.
          *
          * Note, that in NormalForm<ElementType, DETERMINING_VALUE,
          * OPERATOR_SIGN, USE_BRACKETS>, variables are indexed from
          * 0, i.e. variables have indices from interval 0,..., n-1,
          * where n denotes the number of variables. On the other
          * hand, in a DIMACS file, variables have indices from
          * interval 1,..., n. Thus when reading a DIMACS file, we
          * store variable i (DIMACS) as variable (i-1)
          * (NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN,
          * USE_BRACKETS>). When writing DIMACS file, we output
          * variable i (NormalForm<ElementType, DETERMINING_VALUE,
          * OPERATOR_SIGN, USE_BRACKETS>) as (i+1) (DIMACS).
          *
          * @param dimacs_file File to which output should be written.
          */
         void WriteDIMACSFile (FILE * dimacs_file) const;
         
         /**@brief Creates canonical formula for this normal form.
          *
          * In case of DNF the canonical formula consists of all prime
          * implicants, in case of CNF it consists of all prime
          * implicates. The canonical formula is generated by
          * resolution.
          *
          * @param[out] canonical Pointer to a normal form to which the result
          * canonical formula should be put.
          */
         void CreateCanonical(NormalForm<ElementType, DETERMINING_VALUE,
            OPERATOR_SIGN, USE_BRACKETS> *canonical,
            bool (*hook_func) (const ElementType &)
            );
         
   };

   /**@brief Class describing a CNF.
    *
    * It is a CNF with value of determining value being false, operator
    * sign being '&', which is using brackets around element. It is
    * designed to be used with clauses, use with other kinds of
    * elements may lead to unexpected behaviour.
    */
   typedef NormalForm<bf::Clause, false, '\0', true> CNF;
 
   /**@brief Class describing a DNF.
    *
    * It is a DNF with value of determining value being true, operator
    * sign being '+', which is not using brackets around element. It is
    * designed to be used with clauses, use with other kinds of
    * elements may lead to unexpected behaviour.
    */  
   typedef NormalForm<bf::Term, true, '|', true> DNF;
}

// Implementation
template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE,
OPERATOR_SIGN, USE_BRACKETS>::InitWithNF (
    const bf::NormalForm<ElementType, DETERMINING_VALUE,
    OPERATOR_SIGN, USE_BRACKETS>& normal_form)
{
   RemoveAllElements ();
   number_of_variables_ = normal_form.NumberOfVariables ();
   for (typename ElementList::const_iterator element
         = normal_form.elements_.begin();
         element != normal_form.elements_.end();
         ++ element)
   {
      elements_.push_back ((*element)->Copy());
   }
}

/* Skips preamble, i.e. lines starting with 'c'. When line starting
 * with 'p' is encountered, ungetc is used to push it back to stream,
 * thus it is then the first character read. If line starting with 'p'
 * is not encountered, then false is returned, otherwise true is
 * returned. I.e. return value determines success.
 */
bool DimacsSkipPreamble (FILE * dimacs_file);

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bool bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::InitWithDIMACSFile (FILE * dimacs_file)
{
   RemoveAllElements ();
   if (dimacs_file == NULL)
   {
      return false;
   }

   //Skip preamble and find line with cnf parameters.
   if (! DimacsSkipPreamble (dimacs_file))
   {
      return false;
   }
   
   // Read parameters
   int number_of_clauses = 0;
   if (fscanf (dimacs_file, "p cnf %d %d",
            &number_of_variables_, &number_of_clauses) != 2)
   {
      return false;
   }
   // Read elements (clauses)
   while (!feof (dimacs_file))
   {
      ElementType * element = NewElement ();
      int variable = 0;
      while (fscanf (dimacs_file, "%d", &variable) == 1
            && variable != 0)
      {
         element->AddLiteral(static_cast<unsigned int>(abs (variable) - 1),
               variable < 0);
      }
      if (element->NumberOfLiterals ())
      {
         elements_.push_back(element);
      }
      else
      {
         delete element;
         while (!feof (dimacs_file) && getc(dimacs_file) != '\n')
            ;
                
      }
   }
   return true;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::InitRandomly (unsigned int variables, 
      unsigned int n_elements,
      unsigned int literals_per_element)
{
   RemoveAllElements();
   number_of_variables_ = variables;
   unsigned int number_of_generated = 0;
   while (number_of_generated < n_elements)
   {
      ElementType * element = NewElement();
      element->InitRandomly(variables, literals_per_element);
      if (!element->ContainsConflictOccurrence())
      {
         elements_.push_back(element);
         ++ number_of_generated;
      }
   }
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bool bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::ValueForInput(const bf::Input& input) const
{
   for (typename ElementList::const_iterator element = elements_.begin();
         element != elements_.end();
         ++ element)
   {
      if ((*element)->ValueForInput(input) == DETERMINING_VALUE)
      {
         return DETERMINING_VALUE;
      }
   }
   return ! DETERMINING_VALUE;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bf::PIValues bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::ValueForPartialInput(const bf::PartialInput& partial_input) const
{
   bf::PIValues value = ValueOfEmptyNFPart ();
   for (typename ElementList::const_iterator element = elements_.begin();
         element != elements_.end();
         ++ element)
   {
      bf::PIValues el_value = (*element)->ValueForPartialInput(partial_input);
      if (el_value == bf::kUnknownValue)
      {
         value = bf::kUnknownValue;
      }
      else if (el_value == DeterminingValuePart())
      {
         return el_value;
      }
   }
   return value;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bf::PIValues 
bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::ApplyPartialAssignment(const bf::PartialInput& partial_input)
{
   bf::PIValues value = ValueOfEmptyNFPart ();
   typename ElementList::iterator element = elements_.begin();
   while (element != elements_.end())
   {
      bf::PIValues el_value = (*element)->ApplyPartialAssignment(partial_input);
      if (el_value == bf::kUnknownValue)
      {
         if (value != DeterminingValuePart ())
         {
            value = bf::kUnknownValue;
         }
      }
      else if (el_value == DeterminingValuePart ())
      {
         value = el_value;
      }
      else
      {
         // Remove element from the list.
         // It has the same value as empty NF.
         delete (* element);
         element = elements_.erase (element);
         continue;
      }
      ++ element;
   }
   return value;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bool
bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::UnitPropagation(PartialInput* partial_input) const
{
   bool input_changed = true;
   bool no_empty_element = true;
   while (input_changed && no_empty_element)
   {
       input_changed = false;
      for (typename ElementList::const_iterator element = elements_.begin();
            element != elements_.end();
            ++ element)
      {
         unsigned int undetermined_literal = 0;
         ElementType* an_element = *element;
         typename ElementType::ElementState state =
            an_element->StateForPartialInput(*partial_input,
                  &undetermined_literal);
         if (state == ElementType::kUnsatisfied)
         {
            no_empty_element = false;
            break;
         }
         else if (state == ElementType::kUnit)
         {
            (*partial_input)[undetermined_literal/2] =
               (undetermined_literal % 2 == 1
                ? kFalseValue
                : kTrueValue);
            input_changed = true;
         }
      }
   }
   return no_empty_element;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
const ElementType *
bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::FindElement(
    const ElementType& element) const
{
   for (typename ElementList::const_iterator my_element = elements_.begin();
         my_element != elements_.end();
         ++ my_element)
   {
      if ((* my_element)->IsEqual (element))
      {
         return (* my_element);
      }
   }
   return NULL;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
ElementType * bf::NormalForm<ElementType, DETERMINING_VALUE,
OPERATOR_SIGN, USE_BRACKETS>::FindElement(const ElementType& element)
{
   for (typename ElementList::const_iterator my_element = elements_.begin();
         my_element != elements_.end();
         ++ my_element)
   {
      if ((* my_element)->IsEqual (element))
      {
         return (* my_element);
      }
   }
   return NULL;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
const ElementType * bf::NormalForm<ElementType, DETERMINING_VALUE,
OPERATOR_SIGN, USE_BRACKETS>::FindSubelement(
      const ElementType& element) const
{
   for (typename ElementList::const_iterator my_element = elements_.begin();
         my_element != elements_.end();
         ++ my_element)
   {
      if ((* my_element)->IsSubelementOf (element))
      {
         return (* my_element);
      }
   }
   return NULL;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
ElementType * bf::NormalForm<ElementType, DETERMINING_VALUE,
OPERATOR_SIGN, USE_BRACKETS>::FindSubelement(const ElementType& element)
{
   for (typename ElementList::const_iterator my_element = elements_.begin();
         my_element != elements_.end();
         ++ my_element)
   {
      if ((* my_element)->IsSubelementOf (element))
      {
         return (* my_element);
      }
   }
   return NULL;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bool bf::NormalForm<ElementType, DETERMINING_VALUE,
OPERATOR_SIGN, USE_BRACKETS>::RemoveElement(const ElementType& element)
{
   for (typename ElementList::iterator my_element = elements_.begin();
         my_element != elements_.end();
         ++ my_element)
   {
      if ((*my_element)->IsEqual (element))
      {
         delete (*my_element);
         elements_.erase(my_element);
         return true;
      }
   }
   return false;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
unsigned int bf::NormalForm<ElementType, DETERMINING_VALUE,
OPERATOR_SIGN, USE_BRACKETS>::RemoveAllOccurrencesOfElement(const ElementType& element)
{
   unsigned int number_of_removed = 0;
   for (typename ElementType::iterator my_element = elements_.begin();
         my_element != elements_.end();
         ++ my_element)
   {
      if ((*my_element)->IsEqual (element))
      {
         delete (*my_element);
         my_element=elements_.erase(my_element);
         ++ number_of_removed;
      }
   }
   return number_of_removed;
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::RemoveAllElements()
{
   for (typename ElementList::iterator element = elements_.begin ();
         element != elements_.end();
         ++ element)
   {
      delete (* element);
   }
   elements_.clear();
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
unsigned int bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::RemoveTrivialElements ()
{
   unsigned int number_of_removed = 0;
   typename ElementList::iterator element = elements_.begin ();
   while (element != elements_.end())
   {
      if ((*element)->NumberOfLiterals() == 0
            || (*element)->ContainsConflictOccurrence ())
      {
         delete (*element);
         element = elements_.erase(element);
         ++ number_of_removed;
      }
      else
      {
         ++ element;
      }
   }
   return number_of_removed;
}

template<class ElementType,
   bool DETERMINING_VALUE,
   char OPERATOR_SIGN,
   bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::RemoveSubsumptions()
{
   for(
   typename ElementList::iterator element=elements_.begin();
   element!=elements_.end();
   ++element)
   {
      for(typename ElementList::iterator el=elements_.begin();
            el!=elements_.end();
            ++el)
      {
         if(el!=element
               && (*element)->IsSubelementOf(*(*el)))
         {
            ElementType *elp = *el;
            elements_.erase(el++);
            delete (elp);
         }
      }
   }
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
bool RemoveTheFirstElementType (ElementType* first_element,
      ElementType* second_element)
{
   if (first_element != NULL
      && second_element != NULL
      && first_element->IsEqual(*second_element))
   {
      delete first_element;
      return true;
   }
   return false;
}


template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::SortElements (bool remove_duplicates)
{
   elements_.sort(bf::CompareNFElements<ElementType>);
   if (remove_duplicates)
   {
      elements_.unique(RemoveTheFirstElementType);
   }
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::ToString (const std::vector<std::string> * variable_names,
      std::string * output_string) const
{
   typename ElementList::const_iterator element = elements_.begin();
   while (element != elements_.end())
   {
      if (USE_BRACKETS)
      {
         (* output_string) += '(';
      }
      (* element)->ToString(variable_names, output_string);
      if (USE_BRACKETS)
      {
         (* output_string) += ')';
      }
      ++ element;
      if (OPERATOR_SIGN != '\0'
            && element != elements_.end ())
      {
         (* output_string) += OPERATOR_SIGN;
      }
   }
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::WriteDIMACSFile (FILE * dimacs_file) const
{
   if (dimacs_file == NULL)
   {
      std::cerr << "[bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::WriteDIMACSFile] dimacs_file == NULL" << std::endl;
      throw bf::UnexpectedNULLException();
   }

   time_t current_time;
   time (&current_time);
   fprintf (dimacs_file, "c File created by bfmanip++ on %s", ctime (&current_time));
   fprintf (dimacs_file, "p cnf %u %u\n", number_of_variables_, static_cast<unsigned int>(elements_.size()));
   for (typename ElementList::const_iterator element = elements_.begin();
         element != elements_.end();
         ++ element)
   {
      std::string output_string;
      (*element)->DIMACSString(&output_string);
      fprintf (dimacs_file, "%s\n", output_string.c_str());
   }
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
ElementType * bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN,
            USE_BRACKETS>::NewElement ()
{
   return new ElementType();
}

template<class ElementType,
      bool DETERMINING_VALUE,
      char OPERATOR_SIGN,
      bool USE_BRACKETS>
void 
bf::NormalForm<ElementType, DETERMINING_VALUE, OPERATOR_SIGN, USE_BRACKETS>::
   CreateCanonical(NormalForm<ElementType, DETERMINING_VALUE,
         OPERATOR_SIGN, USE_BRACKETS> *canonical,
         bool (*hook_func) (const ElementType &)
         )
{
   if (canonical == NULL)
   {
      std::cerr << "[bf::NormalForm::CreateCanonical canonical == NULL" << std::endl;
      throw bf::UnexpectedNULLException();
   }
   canonical->RemoveAllElements();
   std::list<ElementType*> unmarked;
   std::list<ElementType*> marked;

   /* Reduce (simplify) list */
   for (typename ElementList::const_iterator element
         = elements_.begin();
         element != elements_.end();
         ++ element)
   {
      typename ElementList::iterator unmarked_element = unmarked.begin();
      while(unmarked_element != unmarked.end())
      {
         if ((*unmarked_element)->IsSubelementOf(*(*element)))
         {
            break;
         }
         else if ((*element)->IsSubelementOf(*(*unmarked_element)))
         {
            unmarked_element = unmarked.erase(unmarked_element);
         }
         else
         {
            ++ unmarked_element;
         }
      }
      if (unmarked_element == unmarked.end())
      {
         unmarked.push_back((*element)->Copy());
      }
   }

   /* Main cycle */
   while(unmarked.size() > 0)
   {
      ElementType *unmarked_element = unmarked.front();
      unmarked.pop_front();
      /* Check for subsumption with marked elements. */
      typename ElementList::iterator marked_element = marked.begin();
      while(marked_element != marked.end())
      {
         if ((*marked_element)->IsSubelementOf(*unmarked_element))
         {
            break;
         }
         else if (unmarked_element->IsSubelementOf(*(*marked_element)))
         {
            ElementType *to_delete = *marked_element;
            marked_element = marked.erase(marked_element);
            delete to_delete;
         }
         else
         {
            ++ marked_element;
         }
      }
      if(marked_element == marked.end())
      {
         /* Generate resolvents */
         marked_element = marked.begin();
         while(marked_element != marked.end())
         {
            ElementType resolvent;
            if ((*marked_element)->Resolve(*unmarked_element, &resolvent) == 1)
            {
               /* Add resolvent to unmarked */
               unmarked.push_back(resolvent.Copy());
            }
            ++ marked_element;
         }
         marked.push_back(unmarked_element);
         if (hook_func != NULL && !hook_func(*unmarked_element))
         {
            break;
         }
      }
   }
   /* We have list of prime implica*s in marked. */
   canonical->SetNumberOfVariables(number_of_variables_);
   canonical->elements_ = marked;
}

#endif

